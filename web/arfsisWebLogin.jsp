<%@page import="be.mxs.common.util.system.UpdateSystem"%>
<%@include file="/includes/helper.jsp"%>
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<%@ page contentType="text/html; charset=UTF-8" %>
<%
	MedwanQuery.getInstance();
	String sWebLanguage="en";
	if(request.getParameter("language")!=null){
		sWebLanguage=request.getParameter("language");
	}
	UpdateSystem.reloadSingletonNoSession();
	String sUserName = checkString(request.getParameter("username"));
	String sPassword = checkString(request.getParameter("password"));
	String sMessage="";
	
	if(sUserName.length()>0 && sPassword.length()>0){
		if(User.validate(sUserName, sPassword)){
			User user = null;
			int nUserId = User.getUseridByAlias(sUserName);
			if(nUserId>-1){
				user = User.getByAlias(sUserName);
			}
			else{
				try{
					user=User.get(Integer.parseInt(sUserName));
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			if(user!=null && user.userid.length()>0 && User.hasPermission(user.userid,ScreenHelper.getSQLDate(new java.util.Date()))){
				session.setAttribute("activeUser",user);
                MedwanQuery.setSession(session,user);
				out.println("<script>window.location.href='"+sCONTEXTPATH+"/arfsisweb/index.jsp';</script>");
				out.flush();
			}
			else{
				sMessage=getTran(request,"web","invalidlogin",sWebLanguage);
			}
		}
		else{
			//Maybe this is patient login
			int personid = SH.convertFromUUID(sUserName);
			if(personid>-1){
				AdminPerson person = AdminPerson.get(personid+"");
				if(person.getID("candidate").equalsIgnoreCase(sPassword)){
					//This is patient login. We load a dummy user and set the parameter "patientid"
					User user = User.get(ci("mpiPatientUserId",4));
					user.setParameter("patientid", personid+"");
					user.setParameter("userlanguage", SH.c(person.language).length()>0 && SH.cs("supportedLanguages", "en").toLowerCase().contains(person.language.toLowerCase())?person.language.toLowerCase():"en");
					user.person=person;
					session.setAttribute("activeUser",user);
					session.setAttribute("activePatient",person);
	                MedwanQuery.setSession(session,user);
					out.println("<script>window.location.href='"+sCONTEXTPATH+"/arfsisweb/index.jsp';</script>");
					out.flush();
				}
				else if(person.comment.contains("*"+sPassword+"*")){
					//This is a one time login. We load a dummy user and set the parameter "patientid"
					User user = User.get(ci("mpiPatientUserId",4));
					user.setParameter("patientid", personid+"");
					user.setParameter("onetime", "1");
					user.setParameter("userlanguage", SH.c(person.language).length()>0 && SH.cs("supportedLanguages", "en").toLowerCase().contains(person.language.toLowerCase())?person.language.toLowerCase():"en");
					user.person=person;
					session.setAttribute("activeUser",user);
					session.setAttribute("activePatient",person);
	                MedwanQuery.setSession(session,user);
	                user.person.comment=user.person.comment.replaceAll("\\*"+sPassword+"\\*", "");
	                user.person.store();
					out.println("<script>window.location.href='"+sCONTEXTPATH+"/arfsisweb/index.jsp';</script>");
					out.flush();
				}
				else{
					sMessage=getTran(request,"web","invalidlogin",sWebLanguage);
				}
			}
			else{
				sMessage=getTran(request,"web","invalidlogin",sWebLanguage);
			}
		}
	}
	
	
%>
<title><%=getTranNoLink("web","ARFSIS-WEB",sWebLanguage) %></title>
<html>
	<head>
		<%=sKHINFAVICON %>
	</head>
	<style>
	* {
		box-sizing: border-box;
	}

	body {
		background: #f6f5f7;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		font-family: 'Montserrat', sans-serif;
		height: 100vh;
		margin: -20px 0 50px;
	}

	h1 {
		font-weight: bold;
		margin: 0;
	}

	h2 {
		text-align: center;
	}

	p {
		font-size: 14px;
		line-height: 20px;
		letter-spacing: 0.5px;
		margin: 20px 0 30px;
	}

	span {
		font-size: 14px;
	}

	a {
		color: #333;
		font-size: 14px;
		text-decoration: none;
		margin: 15px 0;
	}

	button {
		border-radius: 20px;
		border: 1px solid #005f9b;
		background-color: #005f9b;
		color: #FFFFFF;
		font-size: 12px;
		font-weight: bold;
		padding: 12px 45px;
		letter-spacing: 1px;
		text-transform: uppercase;
		transition: transform 80ms ease-in;
	}

	button:active {
		transform: scale(0.95);
	}

	button:focus {
		outline: none;
	}

	button.ghost {
		background-color: transparent;
		border-color: #FFFFFF;
	}

	form {
		background-color: #FFFFFF;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		padding: 0 50px;
		height: 100%;
		text-align: center;
	}

	input {
		background-color: #eee;
		border: none;
		padding: 12px 15px;
		margin: 8px 0;
		width: 100%;
	}

	.container {
		background-color: #fff;
		border-radius: 10px;
		box-shadow: 0 14px 28px rgba(0,0,0,0.25), 
				0 10px 10px rgba(0,0,0,0.22);
		position: relative;
		overflow: hidden;
		width: 768px;
		max-width: 100%;
		min-height: 480px;
	}

	.form-container {
		position: absolute;
		top: 0;
		height: 100%;
		transition: all 0.6s ease-in-out;
	}

	.sign-in-container {
		left: 0;
		width: 50%;
		z-index: 2;
	}

	.container.right-panel-active .sign-in-container {
		transform: translateX(100%);
	}

	.sign-up-container {
		left: 0;
		width: 50%;
		opacity: 0;
		z-index: 1;
	}

	.container.right-panel-active .sign-up-container {
		transform: translateX(100%);
		opacity: 1;
		z-index: 5;
		animation: show 0.6s;
	}

	@keyframes show {
		0%, 49.99% {
			opacity: 0;
			z-index: 1;
		}
		
		50%, 100% {
			opacity: 1;
			z-index: 5;
		}
	}

	.overlay-container {
		position: absolute;
		top: 0;
		left: 50%;
		width: 50%;
		height: 100%;
		overflow: hidden;
		transition: transform 0.6s ease-in-out;
		z-index: 100;
	}

	.container.right-panel-active .overlay-container{
		transform: translateX(-100%);
	}

	.overlay {
		background: #FF416C;
		background: -webkit-linear-gradient(to right, #005f9b, #0a8fe3);
		background: linear-gradient(to right, #0a8fe3, #005f9b);
		background-repeat: no-repeat;
		background-size: cover;
		background-position: 0 0;
		color: #FFFFFF;
		position: relative;
		left: -100%;
		height: 100%;
		width: 200%;
		transform: translateX(0);
		transition: transform 0.6s ease-in-out;
	}

	.container.right-panel-active .overlay {
		transform: translateX(50%);
	}

	.overlay-panel {
		position: absolute;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		padding: 0 40px;
		text-align: center;
		top: 0;
		height: 100%;
		width: 50%;
		transform: translateX(0);
		transition: transform 0.6s ease-in-out;
	}

	.overlay-left {
		transform: translateX(-20%);
	}

	.container.right-panel-active .overlay-left {
		transform: translateX(0);
	}

	.overlay-right {
		right: 0;
		transform: translateX(0);
	}

	.container.right-panel-active .overlay-right {
		transform: translateX(20%);
	}

	.social-container {
		margin: 20px 0;
	}

	footer {
		background-color: #222;
		color: #fff;
		font-size: 14px;
		bottom: 0;
		position: fixed;
		left: 0;
		right: 0;
		text-align: center;
		z-index: 999;
	}

	footer p {
		margin: 10px 0;
	}

	footer i {
		color: red;
	}

	footer a {
		color: #3c97bf;
		text-decoration: none;
		
	}
	/* ---- reset ---- */
	body{
		margin:0;
		font:normal 75% Arial, Helvetica, sans-serif;
	}
	canvas{
		display: block;
		vertical-align: bottom;
	}
	/* ---- particles.js container ---- */
	#particles-js{
		position:absolute;
		width: 100%;
		height: 100%;
		background-color: #f0f2f8;
		background-repeat: no-repeat;
		background-size: 20%;
		background-position: 50% 50%;
	}
	/* ---- stats.js ---- */
	.count-particles{
		background: #000022;
		position: absolute;
		top: 48px;
		left: 0;
		width: 80px;
		color: #13E8E9;
		font-size: .8em;
		text-align: left;
		text-indent: 4px;
		line-height: 14px;
		padding-bottom: 2px;
		font-family: Helvetica, Arial, sans-serif;
		font-weight: bold;
	}
	.js-count-particles{
		font-size: 1.1em;
	}
	#stats, .count-particles{
		-webkit-user-select: none;
		margin-top: 5px;
		margin-left: 5px;
	}
	#stats{
		
		overflow: hidden;
	}

	</style>
	<body>
	<div id="particles-js"></div>
	<div class="container" id="container">
		<div class="form-container sign-up-container">
			<form action="#">
			<h3 style="color:#747474; margin:10px 0">Iniciar Sesión con Huella</h3>
						
			<span style="color:#747474">Toca el sensor de huella dactilar</span>
			<img height='150px' style="margin:40px 0" src="_img/arfsis/huella.png" border="0">
		</form>
		</div>
		<div class="form-container sign-in-container">
			<form name='transactionForm' method='post'>
				<input type='hidden' name='formaction' id='formaction'/>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
					<tr>
						<td align="center" width="1%">
							<img height='50px' style="margin-bottom:30px" src="_img/arfsis/logo.png" border="0">
						</td>
					</tr>
					<tr>
						<td>
							<h3 style="color:#747474; margin:10px 0">Iniciar Sesión</h3>
						
							<span style="color:#747474">Ingrese su usuario y contraseña</span>
							</br></br>
							<input type="text" name='username' placeholder="<%=getTranNoLink("web","Usuario",sWebLanguage) %>" />
							
							<input type="password" name='password' placeholder="<%=getTranNoLink("web","Contraseña",sWebLanguage) %>" />
							<%-- <a href="#" style="color: #005f9b;">¿Olvide mi contraseña?</a> --%>
							</br></br>
							<button type="submit" ><%=getTranNoLink("web","Iniciar Sesion",sWebLanguage) %></button>
						</td>
					</tr>
				<% if(sMessage.length()>0){ %>
					<tr>
						<td colspan='2' style="color:red">
							<p><small><%=sMessage %></small></p>
						</td>
					</tr>
				<% } %>
				</table>
			</form>
		</div>
		<div class="overlay-container">
			<div class="overlay">
				<div class="overlay-panel overlay-left">
					
					<h2 style="margin:5px">ARFSIS Web</h2>
					<p>Ingrese mediante su usuario y contrasña</p>
					<button class="ghost" id="signIn">Ingresar con Datos</button>
				</div>
				<div class="overlay-panel overlay-right">
					<h2 style="margin:5px">ARFSIS Web</h2>
					<p>Ingrese utilizando su huella dactilar</p>
					<%-- <button class="ghost" id="signUp">Ingresar con Huella</button> --%>
					<img height='150px' style="margin-bottom:30px" src="_img/arfsis/huella-white.png" border="0">
					<em style="font-size:12px;">Toca el sensor de huella dactilar</em>
				</div>
			</div>
		</div>
	</div>
	<script src="./arfsisweb/particles.min.js">
	<script>
		const signUpButton = document.getElementById('signUp');
		const signInButton = document.getElementById('signIn');
		const container = document.getElementById('container');

		signUpButton.addEventListener('click', () => {
			container.classList.add("right-panel-active");
		});

		signInButton.addEventListener('click', () => {
			container.classList.remove("right-panel-active");
		});
	</script>
	<script>
	particlesJS("particles-js", {
		fpsLimit: 60,
		particles: {
			number: {
			value: 80,
			density: {
				enable: true,
				value_area: 800
			}
			},
			color: {
			/* value: ["#2e7db6", "#4c7ec8", "#003296", "#36C5F0"]*/
			value: ["#abb3b9", "#b8c1d0", "#999a9c", "#8f9394"]
			},
			shape: {
			type: ["circle"],
			stroke: {
				width: 0,
				color: "#fff"
			},
			polygon: {
				nb_sides: 5
			}
			},
			opacity: {
			value: 1,
			random: false,
			anim: {
				enable: false,
				speed: 12,
				opacity_min: 0.1,
				sync: false
			}
			},
			size: {
			value: 8,
			random: true,
			anim: {
				enable: false,
				speed: 7,
				size_min: 10,
				sync: false
			}
			},
			line_linked: {
			enable: true,
			distance: 150,
			color: "#808080",
			opacity: 0.4,
			width: 1
			},
			move: {
			enable: true,
			speed: 3,
			direction: "none",
			random: false,
			straight: false,
			out_mode: "out",
			bounce: false,
			attract: {
				enable: false,
				rotateX: 600,
				rotateY: 1200
			}
			}
		},
		interactivity: {
			detect_on: "canvas",
			events: {
			onhover: {
				enable: true,
				mode: "grab"
			},
			onclick: {
				enable: true,
				mode: "push"
			},
			resize: true
			},
			modes: {
			grab: {
				distance: 140,
				line_linked: {
				opacity: 1
				}
			},
			bubble: {
				distance: 400,
				size: 40,
				duration: 2,
				opacity: 8,
				speed: 3
			},
			repulse: {
				distance: 200,
				duration: 0.4
			},
			push: {
				particles_nb: 4
			},
			remove: {
				particles_nb: 2
			}
			}
		},
  
		"retina_detect": true
	});
	</script>
	
	</body>
</html>
