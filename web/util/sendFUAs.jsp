<%@page import="pe.gob.sis.*"%>
<%@page import="org.jnp.interfaces.java.javaURLContextFactory"%>
<%@include file="/includes/validateUser.jsp"%>
<%
	String year = checkString(request.getParameter("year"));
	String month = checkString(request.getParameter("month"));
	
	if(year.length()==0){
		year = new SimpleDateFormat("yyyy").format(new java.util.Date());
	}
	if(month.length()==0){
		month = new SimpleDateFormat("M").format(new java.util.Date());
	}
	
	int nyear= Integer.parseInt(year);
	int nmonth= Integer.parseInt(month);
%>
<%=sJSPROTOTYPE %>

<form name="transactionForm" method="post" action="util/testFUA.jsp">
	<input type='hidden' name='sendfile' id='sendfile'/>
	<table width='100%'>
		<tr class='admin'>
			<td colspan='2'><%=getTran(request,"web.manage","sendFUAs",sWebLanguage) %></td>
		</tr>
		<tr>
			<td class='admin' width='1%' nowrap><%=getTran(request,"web","year",sWebLanguage) %>&nbsp;&nbsp;</td>
			<td class='admin2'>
				<select name='year' id='year' class='text' onchange='listFUAs();'>
					<%
						for(int n=nyear; n>nyear-10;n--){
							out.println("<option value='"+n+"'>"+n+"</option>");
						}
					%>
				</select>
			</td>
		</tr>
		<tr>
			<td class='admin' width='1%' nowrap><%=getTran(request,"web","month",sWebLanguage) %>&nbsp;&nbsp;</td>
			<td class='admin2'>
				<select name='month' id='month' class='text' onchange='listFUAs();'>
					<%
						for(int n=1;n<13;n++){
							out.println("<option value='"+n+"' "+(n==nmonth?"selected":"")+">"+n+"</option>");
						}
					%>
				</select>
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td>
				<input class='button' name='submitButton1' type='hidden' onclick='doSendPaqueteV1();' value='<%=getTranNoLink("web.manage","sendfuas",sWebLanguage) %> v1'/>
				<input class='button' name='submitButton2' type='button' onclick='doSendPaqueteV2();' value='<%=getTranNoLink("web.manage","sendfuas",sWebLanguage) %>'/>
				<input class='button' name='submitButton3' type='hidden' onclick='doSendPaqueteTest("v1");' value='Test 1'/>
				<input class='button' name='submitButton3' type='hidden' onclick='doSendPaqueteTest("v2");' value='Test 2'/>
				<input class='button' name='downloadButton' type='button' onclick='document.getElementById("sendfile").value="";transactionForm.submit();' value='<%=getTranNoLink("web","download.file",sWebLanguage) %>'/>
			</td>
			<td width="200">
				<span id="loading" style="display:none"><img height="15px" src="<%=sCONTEXTPATH%>/_img/themes/default/ajax-loader.gif"></span>
			</td>
			<td>
				<input type="text" id="paquete" value="" />
				<input class='button' name='submitButton' type='button' onclick='javascript:doCheckPaquete();' value='Verificar paquete enviado al SIS'/>
			</td>
		</tr>
	</table>
</form>
<div id='respuesta'></div>
<div id='fualist'/>
<script>
  function listFUAs(){
      var params = "year="+document.getElementById("year").value+
                   "&month="+document.getElementById("month").value+
                   "&error=0";
      var url = '<c:url value="/util/listFUAs.jsp"/>?ts='+new Date().getTime();
      new Ajax.Request(url,{
        method: "POST",
        parameters: params,
        onSuccess: function(resp){
          document.getElementById("fualist").innerHTML = resp.responseText;
        }
      });
  }
  listFUAs();

  function doSendPaqueteTest(version){
	  document.getElementById("loading").style.display="";
	  var params = "year="+document.getElementById("year").value+
			  "&month="+document.getElementById("month").value+
			  "&v="+version+
			  "&sendfile=1";
	  var url = '<c:url value="/util/testFUA_TestV2.jsp"/>?ts='+new Date();
	  new Ajax.Request(url,{
		  method: "POST",
		  parameters: params,
		  onSuccess: function(resp){
			  console.log(resp.responseText);
		  }
	  });
  }
  function doSendPaqueteV1(){
	  document.getElementById("loading").style.display="";
	  var params = "year="+document.getElementById("year").value+
			  "&month="+document.getElementById("month").value+
			  "&sendfile=1";
	  var url = '<c:url value="/util/testFUA_SendV1.jsp"/>?ts='+new Date();
	  new Ajax.Request(url,{
		  method: "POST",
		  parameters: params,
		  onSuccess: function(resp){
			  console.log(resp.responseText);
			  let respuesta = resp.responseText.split("@");
			  document.getElementById("loading").style.display="none";
			  if(respuesta[0]==0) {
				  document.getElementById("paquete").value=respuesta[1].trim();
				  alert(respuesta[3].split("$")[2]);
			  }
			  if(respuesta[0]==1) {
			  	  alert(respuesta[3]);
			  }
		  }
	  });
  }
  function doSendPaqueteV2(){
	  document.getElementById("loading").style.display="";
	  var params = "year="+document.getElementById("year").value+
				  "&month="+document.getElementById("month").value+
				  "&sendfile=1";
	  var url = '<c:url value="/util/testFUA_SendV2.jsp"/>?ts='+new Date();
	  new Ajax.Request(url,{
		  method: "POST",
		  parameters: params,
		  onSuccess: function(resp){
			  console.log(resp.responseText);
			  let respuesta = resp.responseText.split("@");
			  document.getElementById("loading").style.display="none";
			  if(respuesta[0]==0) {
				  alert("El paquete ha subido con �xito.");
				  document.getElementById("paquete").value=respuesta[1].trim();
			  }
			  if(respuesta[0]==-1) {
				  alert("Procesando...");
			  }
			  if(respuesta[0]==1) {
				  alert("Error funcional: " + respuesta[2].trim());
			  }
			  if(respuesta[0]==15) {
				  alert("no tiene permiso");
			  }
			  if(respuesta[0]==100) {
				  alert("header invalido");
			  }
		  }
	  });
  }

  function doCheckPaquete(){
	  if(document.getElementById("paquete").value.length!=19) {
		  alert("Necesita ingresar el numero de paquete.");
		  return;
	  }
	  document.getElementById("loading").style.display="";
	  var params = "paquete="+document.getElementById("paquete").value.trim()+
			  "&sendfile=1";
	  var url = '<c:url value="/util/testFUA_Check.jsp"/>?ts='+new Date();
	  new Ajax.Request(url,{
		  method: "POST",
		  parameters: params,
		  onSuccess: function(resp){
			  // console.log(resp); //responseText
			  // document.getElementById("respuesta").innerHTML = resp.responseText;
			  document.getElementById("loading").style.display="none";
			  alert(resp.responseText.trim());
		  }
	  });
  }
</script>


