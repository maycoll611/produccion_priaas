 <%@page import="pe.gob.sis.*,java.util.*,javax.xml.soap.*,org.dom4j.*,org.dom4j.io.*,java.io.*,org.json.*"%>
<%@page import="pe.gob.sis.FUAGenerator"%>
<%
	FUAGenerator generator = new FUAGenerator();
	String zipfile = generator.generateFUAs(Integer.parseInt(request.getParameter("year")),Integer.parseInt(request.getParameter("month")));
    SOAPMessage mresponse = generator.sendSOAPFUAZip_v2(zipfile.split("/")[zipfile.split("/").length-1], zipfile);
    String Resultado = generator.getPrettyPrint(mresponse);
    String jsonString = "";
    try {
        JSONObject json = XML.toJSONObject(Resultado);
        jsonString = json.toString(4);
    }catch (JSONException e) {
        // TODO: handle exception
        jsonString = e.toString(); // System.out.println(e.toString());
    }
    SOAPBody body = mresponse.getSOAPBody();
    String Retorno = "";
    if(jsonString.contains("codigo")){
        Retorno = body.getElementsByTagName("codigo").item(0).getTextContent();
        if(Retorno.contains("0")) {
            if(jsonString.contains("paqueteNombre"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("paqueteNombre").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
            if(jsonString.contains("paqueteId"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("paqueteId").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
        }
        if(Retorno.contains("1")) {
            if(jsonString.contains("paqueteNombre"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("paqueteNombre").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
            if(jsonString.contains("respuesta"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("respuesta").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
        }
    } else {
        Retorno = "-1@0@0";
    }
    response.setContentType("application/html; charset=windows-1252");
%>
 <%=Retorno%>
