 <%@page import="pe.gob.sis.*,java.util.*,javax.xml.soap.*,org.dom4j.*,org.dom4j.io.*,java.io.*,org.json.*"%>
<%@page import="pe.gob.sis.FUAGenerator"%>
<%
	FUAGenerator generator = new FUAGenerator();
	String zipfile = generator.generateFUAs(Integer.parseInt(request.getParameter("year")),Integer.parseInt(request.getParameter("month")));
    SOAPMessage mresponse;
    String version = request.getParameter("v");
    if(version.contains("v1"))
        mresponse = generator.sendSOAPFUAZip(zipfile.split("/")[zipfile.split("/").length-1], zipfile);
    else
        mresponse = generator.sendSOAPFUAZip_v2(zipfile.split("/")[zipfile.split("/").length-1], zipfile);
    //System.out.println(generator.getPrettyPrint(mresponse));
    String Resultado = generator.getPrettyPrint(mresponse);
    String jsonString = "";
    try {
        JSONObject json = XML.toJSONObject(Resultado);
        jsonString = json.toString(4);
    }catch (JSONException e) {
        // TODO: handle exception
        // System.out.println(e.toString());
    }

%>
 <%=jsonString%>