 <%@page import="pe.gob.sis.*,java.util.*,javax.xml.soap.*,org.dom4j.*,org.dom4j.io.*,java.io.*,org.json.*"%>
<%@page import="pe.gob.sis.FUAGenerator"%>
<%
	FUAGenerator generator = new FUAGenerator();
	String zipfile = generator.generateFUAs(Integer.parseInt(request.getParameter("year")),Integer.parseInt(request.getParameter("month")));
    SOAPMessage mresponse = generator.sendSOAPFUAZip(zipfile.split("/")[zipfile.split("/").length-1], zipfile);
    String Resultado = generator.getPrettyPrint(mresponse);
    String jsonString = "";
    try {
        JSONObject json = XML.toJSONObject(Resultado);
        jsonString = json.toString(4);
    }catch (JSONException e) {
        // TODO: handle exception
        // System.out.println(e.toString());
    }
    SOAPBody body = mresponse.getSOAPBody();
    String Retorno = "";
    if(jsonString.contains("codigoRespuesta")){
        Retorno = body.getElementsByTagName("codigoRespuesta").item(0).getTextContent();
        if(Retorno.contains("0")) {
            if(jsonString.contains("ns2:identificadorPaquete"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("ns2:identificadorPaquete").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
            if(jsonString.contains("ns2:paqueteIdGenerado"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("ns2:paqueteIdGenerado").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
            if(jsonString.contains("ns2:respuestaString"))
                Retorno = Retorno +"@"+ body.getElementsByTagName("ns2:respuestaString").item(0).getTextContent();
            else
                Retorno = Retorno +"@0";
        }
        if(Retorno.contains("1")) {
            if(jsonString.contains("descripcionRespuesta"))
                Retorno = Retorno +"@0@0@"+ body.getElementsByTagName("descripcionRespuesta").item(0).getTextContent();
            else
                Retorno = Retorno +"@0@0@0";
        }
    } else {
        Retorno = "-1@0@0@0";
    }
    response.setContentType("application/html; charset=UFT-8");
%>
 <%=Retorno%>
