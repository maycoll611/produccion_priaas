<%@page import="pe.gob.sis.arfsisweb.createResponsable,org.json.*,pe.gob.sis.*"%>
<%@page import="pe.gob.sis.arfsisweb.*"%>
<%@ page import="java.sql.*" %>
<%
	String dni = request.getParameter("dni");
	String tipo = request.getParameter("tipo");
	String columns = request.getParameter("columns");
	
	SoapWebService sws = new SoapWebService();
	sws.setsTipoDocumento(tipo);
	sws.setsDocumento(dni);
	
	JSONObject objResponseR = sws.personalSalud();
	String result =  objResponseR.toString();
	System.out.println(objResponseR); 	
	String responsable = objResponseR.getString("message");
	
	if(!responsable.equals(""))
	{
		String[] splitResponsable = responsable.split("\\|"); 
		
		MysqlConnect conn = new MysqlConnect("bdsis_maestros");
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		//consultar si existe
		ps = conn.connect().prepareStatement("SELECT * FROM bdsis_maestros.a_resatencion WHERE pers_IdResAtencion='"+splitResponsable[0]+"'");
		rs = ps.executeQuery();
		
		boolean exist = false;
	    if(rs.next()) {
	    	System.out.println("update");
	    	try {
				PreparedStatement updateRow = conn.connect().prepareStatement("UPDATE bdsis_maestros.a_resatencion SET pers_IdTipoDocumento = ?,pers_ApePaterno = ?,pers_ApeMaterno = ?,pers_PriNombre = ?,pers_IdTipoPersonalSalud = ?,pers_Colegiatura = ?,pers_IdEspecialidad = ?,pers_NroEspecialidad = ?,pers_IdEstado = ?,PERS_ESEGRESADO = ? WHERE pers_IdResAtencion= ?");
				updateRow.setString(1, splitResponsable[1]);
				updateRow.setString(2, splitResponsable[2]);
				updateRow.setString(3, splitResponsable[3]);
				updateRow.setString(4, splitResponsable[4]);
				updateRow.setString(5, splitResponsable[5]);
				updateRow.setString(6, splitResponsable[6]);
				updateRow.setString(7, splitResponsable[7].equals("") ? null:splitResponsable[7]);
				updateRow.setString(8, splitResponsable[8]);
				updateRow.setString(9, splitResponsable[9]);
				if(splitResponsable.length == 11){
					updateRow.setString(10, splitResponsable[10]);	
				}else{
					updateRow.setString(10, "");
				}
				updateRow.setString(11, splitResponsable[0]);
				
				updateRow.execute();
				updateRow.close();
				
			}catch(Exception e){
				System.err.println("Exception");
				e.printStackTrace();
				System.out.println(e); 	
			}
	    }else{
			try {		
				System.out.println("insert");
				PreparedStatement newInsert = conn.connect().prepareStatement("INSERT INTO bdsis_maestros.a_resatencion (pers_IdResAtencion,pers_IdTipoDocumento,pers_ApePaterno,pers_ApeMaterno,pers_PriNombre,pers_IdTipoPersonalSalud,pers_Colegiatura,pers_IdEspecialidad,pers_NroEspecialidad,pers_IdEstado,PERS_ESEGRESADO) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
				newInsert.setString(1, splitResponsable[0]);
				newInsert.setString(2, splitResponsable[1]);
				newInsert.setString(3, splitResponsable[2]);
				newInsert.setString(4, splitResponsable[3]);
				newInsert.setString(5, splitResponsable[4]);
				newInsert.setString(6, splitResponsable[5]);
				newInsert.setString(7, splitResponsable[6]);
				newInsert.setString(8, splitResponsable[7]);
				newInsert.setString(9, splitResponsable[8]);
				newInsert.setString(10, splitResponsable[9]);
				if(splitResponsable.length == 11){
					newInsert.setString(11, splitResponsable[10]);	
				}else{
					newInsert.setString(11, "");
				}
				newInsert.setString(12, splitResponsable[11]);
					
			}catch(Exception e){
				System.err.println("Exception");
				e.printStackTrace();
				System.out.println(e); 	
			}
	    }
	    
	    ps.close();
	 	rs.close();
	}
	
	String resultado ;
	createResponsable datos = new createResponsable("a_resatencion");
	datos.column(columns);
	
	resultado = datos.datosJSON(dni,tipo);
%>
<%=resultado %>
