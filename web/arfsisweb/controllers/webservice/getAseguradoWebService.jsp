<%@page import="pe.gob.sis.*,org.json.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Date"%>

<%@page import="java.nio.charset.StandardCharsets,be.openclinic.mobilemoney.*"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.entity.*,org.apache.commons.codec.binary.*,javax.crypto.*,javax.crypto.spec.*,org.apache.http.client.*,org.apache.http.client.methods.*,
				org.apache.http.client.utils.*,java.net.*,org.apache.http.impl.client.*,org.apache.http.*"%>

<%@ page import="net.admin.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@page import="pe.gob.sis.arfsisweb.*"%>
<%

	String disa = request.getParameter("disa");
	String lote = request.getParameter("lote");
	String numero = request.getParameter("numero");
	String correlativo = request.getParameter("correlativo");
	String columns = request.getParameter("columns");
	String webservice = request.getParameter("webservice");
	String centroDigitacionId = request.getParameter("centro_digitacion_id");
	String fechaAte = request.getParameter("fecha_ate");

	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_webservices WHERE sis_centro_digitacion = '"+centroDigitacionId+"' AND sis_denominacion = '"+webservice+"'");
	ResultSet rs = sql.executeQuery();
	
	String url = "";
	String user = "";
	String password = "";
	String consultor= "";
	while(rs.next())
	{
		switch (rs.getString("sis_parametro")){
	        case "url":{
	            url = rs.getString("sis_valor");
	            break;
	        }
	        case "user":{
	        	user = rs.getString("sis_valor");
	            break;
	        }
	        case "password":{
	        	password = new WebService().decrypt(rs.getString("sis_valor"));
	            break;
	        }
	        case "consultor":{
	        	consultor = rs.getString("sis_valor");
	            break;
	        }
	        default: {
	            System.out.println("Opcion incorrecta");
	        }
	  }
	}
	try {
		
		HttpClient httpclient = HttpClients.createDefault();
		String baseurl = url+"?docTipo=1&docNum="+numero+"&cons="+consultor;
		URIBuilder builder = new URIBuilder(baseurl);
		URI uri = builder.build();
		HttpGet req = new HttpGet(uri);
		
		String valueToEncode = user+":"+password;
		
		String auth = "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());	
		req.setHeader("Authorization",auth);
		req.setHeader("Content-Type", "application/json");
		
		HttpResponse resp = httpclient.execute(req);
		HttpEntity entity = resp.getEntity();
		String responseBody = EntityUtils.toString(resp.getEntity(), StandardCharsets.UTF_8);
		
		JSONObject objResponse = new JSONObject(responseBody);
		System.out.println(objResponse);
		
		Afiliado afiliado = new Afiliado();
		String idSiasis = (String) objResponse.get("registro");
		if(!idSiasis.equals(""))
		{
			afiliado.initialize(idSiasis);
		}
		/* afiliado.setAfi_IdSiasis((String) objResponse.get("registro")); */
		afiliado.setAfi_Dni(numero);
		afiliado.setAfi_IdTipoDocumento("1");
		afiliado.setAfi_ApePaterno((String) objResponse.get("apellidoPaterno"));
		afiliado.setAfi_ApeMaterno((String) objResponse.get("apellidoMaterno"));
		String nombres = (String) objResponse.get("nombres");
		if(nombres.split(" ").length > 1)
		{
			afiliado.setAfi_Nombres(nombres.split(" ")[0]);
			afiliado.setAfi_SegNombre(nombres.split(" ")[1]);	
		}
		else
		{
			afiliado.setAfi_Nombres(nombres);
		}
		
		SimpleDateFormat sdFecFormato = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecFormato = sdFecFormato.parse((String) objResponse.get("afiliacionFecha"));
		sdFecFormato.applyPattern("yyyy-MM-dd");
		System.out.println(sdFecFormato.format(dFecFormato));
		
		afiliado.setAfi_FecFormato(sdFecFormato.format(dFecFormato).toString());
		
		
		afiliado.setAfi_IdEESSAte((String) objResponse.get("ipressRenaes"));
		afiliado.setAfi_TipoTabla((String) objResponse.get("tabla"));
		afiliado.setAfi_IdSexo((String) objResponse.get("genero"));
		
		SimpleDateFormat sdFecNac = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecNac = sdFecNac.parse((String) objResponse.get("nacimientoFecha"));
	  	sdFecNac.applyPattern("yyyy-MM-dd");
		
		
		afiliado.setAfi_FecNac(sdFecNac.format(dFecFormato).toString());
		
		afiliado.setAfi_IdDistrito((String) objResponse.get("ubigeo"));
		afiliado.setAfi_IdDisa(objResponse.get("contratoDisa").toString());
		afiliado.setAfi_TipoFormato((String) objResponse.get("contratoFormato"));
		afiliado.setAfi_NroFormato((String) objResponse.get("contratoNumero"));
		if(!objResponse.get("contratoSecuencia").toString().equals("null"))
		{
			afiliado.setAfi_CorrelativoIns(objResponse.get("contratoSecuencia").toString());	
		}
		/* AFILIADOS */
		
		
		afiliado.setAfi_Plan((String) objResponse.get("seguroPlan"));
		afiliado.setAfi_idGrupoPoblacional((String) objResponse.get("grupoPoblacional"));
		
		
		afiliado.store();
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println (e);
	}
	
	String resultado ;
	GetDataAsegurado datos = new GetDataAsegurado("m_afiliados");
	datos.column(columns);
	
	int serverId = MedwanQuery.getInstance().getServerId();
	String userId = activeUser.userid;
	resultado = datos.getDatosJSONU(disa, lote, numero, correlativo,userId,fechaAte);
	out.println(resultado);

%>