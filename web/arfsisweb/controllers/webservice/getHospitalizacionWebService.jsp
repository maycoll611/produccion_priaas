<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,java.text.SimpleDateFormat"%>
<%@page import="pe.gob.sis.*,org.json.*,java.util.*"%>

<%
	JSONObject result = new JSONObject();
	boolean resultStatus = false;
	
	String fechaIngreso = request.getParameter("fechaIngreso");
	String fechaAtencion = request.getParameter("fechaAtencion");
	String servicio = request.getParameter("servicio");
	String disa = request.getParameter("disa");
	String lote = request.getParameter("lote");
	String numeroFormato = request.getParameter("numeroFormato");
	String secuencia = request.getParameter("secuencia");

	SoapWebService sws = new SoapWebService();
	try{
		String[] splitFechaIngreso = fechaIngreso.split("/"); 
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf1.parse(splitFechaIngreso[2]+"-"+splitFechaIngreso[1]+"-"+splitFechaIngreso[0]);
		Calendar cFechaIngreso = Calendar.getInstance();
		cFechaIngreso.setTime(date);
		
		/* cFechaIngreso.setTime(sdf1.parse(splitFechaIngreso[2]+"-"+splitFechaIngreso[1]+"-"+splitFechaIngreso[0])); */
		
		String[] splitFechaAtencion= fechaIngreso.split("/");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		Date date2 = sdf2.parse(splitFechaAtencion[2]+"-"+splitFechaAtencion[1]+"-"+splitFechaAtencion[0]);
		Calendar cFechaAtencion = Calendar.getInstance();
		cFechaAtencion.setTime(date2);
		
		
		sws.setcFechaAtencion(cFechaAtencion);
		sws.setcFechaIngreso(cFechaIngreso);
		sws.setsServicio(servicio);
		sws.setsDisa(disa);
		sws.setsLote(lote);
		sws.setsNumeroFormato(numeroFormato);
		sws.setsSecuencia(secuencia);
		
		JSONObject objResponseH = sws.hospitalizado();
		String message = objResponseH.get("message").toString();
		String status = objResponseH.get("status").toString();
		
		if(status.equals("true"))
		{
			String[] messageSplit = message.split("\\|");
			
			if(messageSplit[messageSplit.length -1].equals("Existe cobertura."))
			{
				System.out.println(messageSplit[messageSplit.length -1]);
				System.out.println(objResponseH.get("message"));
				System.out.println(objResponseH.get("status"));
				resultStatus = true;
			}else {
				resultStatus = false;
			}

		}
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println(e);
	    resultStatus = false;	    
	}
	result.put("status", resultStatus);
	
%>
<%=result%>

