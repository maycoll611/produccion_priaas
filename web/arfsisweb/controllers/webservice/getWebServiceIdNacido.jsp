<%@page import="pe.gob.sis.*,org.json.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Date"%>
<%@page import="org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@page import="java.nio.charset.StandardCharsets,be.openclinic.mobilemoney.*"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.entity.*,org.apache.commons.codec.binary.*,javax.crypto.*,javax.crypto.spec.*,org.apache.http.client.*,org.apache.http.client.methods.*,
				org.apache.http.client.utils.*,java.net.*,org.apache.http.impl.client.*,org.apache.http.*"%>

<%@ page import="net.admin.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>

<%
	int tipo = Integer.parseInt(request.getParameter("tipo"));
	String documento = request.getParameter("documento");
	String disa = request.getParameter("disa");
	String formato = request.getParameter("formato");
	String correlativo = request.getParameter("correlativo");
	String secuencia = request.getParameter("secuencia");
	
	String columns = request.getParameter("columns");
	String webservice = request.getParameter("webservice");
	String centroDigitacionId = request.getParameter("centro_digitacion_id");
	
	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_webservices WHERE sis_centro_digitacion = '"+centroDigitacionId+"' AND sis_denominacion = '"+webservice+"'");
	ResultSet rs = sql.executeQuery();
	
	String url = "";
	String user = "";
	String password = "";
	String consultor= "";
	while(rs.next())
	{
		switch (rs.getString("sis_parametro")){
	        case "url":{
	            url = rs.getString("sis_valor");
	            break;
	        }
	        case "user":{
	        	user = rs.getString("sis_valor");
	            break;
	        }
	        case "password":{
	        	password = new WebService().decrypt(rs.getString("sis_valor"));
	            break;
	        }
	        case "consultor":{
	        	consultor = rs.getString("sis_valor");
	            break;
	        }
	        default: {
	            System.out.println("Opcion incorrecta");
	        }
	  }
	}	
	try {
		HttpClient httpclient = HttpClients.createDefault();
		
		String baseurl = "";
		
		if(tipo == 0)
		{
			baseurl = url+"?docTipo="+tipo+"&docNum="+disa+"-"+formato+"-"+correlativo+"-"+secuencia+"&cons="+consultor;
		}else if(tipo == 1)
		{
			baseurl = url+"?docTipo="+tipo+"&docNum="+documento+"&cons="+consultor;
		}
		
		URIBuilder builder = new URIBuilder(baseurl);
		URI uri = builder.build();
		HttpGet req = new HttpGet(uri);
		
		String valueToEncode = user+":"+password;
		
		String auth = "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());	
		  	System.out.println(auth);
		req.setHeader("Authorization",auth);
		req.setHeader("Content-Type", "application/json");
		
		HttpResponse resp = httpclient.execute(req);
		HttpEntity entity = resp.getEntity();
		String responseBody = EntityUtils.toString(resp.getEntity(), StandardCharsets.UTF_8);
		
		JSONObject objResponse = new JSONObject(responseBody); 
		System.out.println(objResponse);
		Afiliado afiliado = new Afiliado();
		afiliado.setAfi_IdSiasis((String) objResponse.get("registro"));
		afiliado.setAfi_Dni(documento);
		afiliado.setAfi_IdTipoDocumento("1");
		afiliado.setAfi_ApePaterno((String) objResponse.get("apellidoPaterno"));
		afiliado.setAfi_ApeMaterno((String) objResponse.get("apellidoMaterno"));
		String nombres = (String) objResponse.get("nombres");
		if(nombres.split(" ").length > 1)
		{
			afiliado.setAfi_Nombres(nombres.split(" ")[0]);
			afiliado.setAfi_SegNombre(nombres.split(" ")[1]);	
		}
		else
		{
			afiliado.setAfi_Nombres(nombres);
		}
		
		SimpleDateFormat sdFecFormato = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecFormato = sdFecFormato.parse((String) objResponse.get("afiliacionFecha"));
		sdFecFormato.applyPattern("yyyy-MM-dd");
		System.out.println(sdFecFormato.format(dFecFormato));
		
		afiliado.setAfi_FecFormato(sdFecFormato.format(dFecFormato).toString());
		
		
		afiliado.setAfi_IdEESSAte((String) objResponse.get("ipressRenaes"));
		afiliado.setAfi_TipoTabla((String) objResponse.get("tabla"));
		afiliado.setAfi_IdSexo((String) objResponse.get("genero"));
		
		SimpleDateFormat sdFecNac = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecNac = sdFecNac.parse((String) objResponse.get("nacimientoFecha"));
	  	sdFecNac.applyPattern("yyyy-MM-dd");
		
		
		afiliado.setAfi_FecNac(sdFecNac.format(dFecFormato).toString());
		
		afiliado.setAfi_IdDistrito((String) objResponse.get("ubigeo"));
		afiliado.setAfi_IdDisa(objResponse.get("contratoDisa").toString());
		afiliado.setAfi_TipoFormato((String) objResponse.get("contratoFormato"));
		afiliado.setAfi_NroFormato((String) objResponse.get("contratoNumero"));
		if(!objResponse.get("contratoSecuencia").toString().equals("null"))
		{
			afiliado.setAfi_CorrelativoIns(objResponse.get("contratoSecuencia").toString());	
		}
		/* AFILIADOS */
		
		
		afiliado.setAfi_Plan((String) objResponse.get("seguroPlan"));
		afiliado.setAfi_idGrupoPoblacional((String) objResponse.get("grupoPoblacional"));
		
		
		afiliado.store();
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println (e);
	}
	
	GetDataBD data = new GetDataBD();
	data.setModel("bdsis_asegurados.m_afiliados");
	
	JSONArray whereD = new JSONArray();
	if(tipo==1||tipo==7){
		whereD.put("afi_Dni = "+documento);
		whereD.put("afi_IdTipoDocumento = "+tipo);
	}
	if(tipo==0){
		if(disa!=""){
			whereD.put("afi_IdDisa = "+disa);
		}
		whereD.put("afi_TipoFormato = "+formato);
		whereD.put("afi_NroFormato = "+correlativo);
		if(secuencia!=""){
			whereD.put("afi_CorrelativoIns = "+secuencia);
		}
	}
	
	data.setColumn(columns);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	JSONObject resultado = new JSONObject();
	
	resultado.put("data",getData);
	if(getData.length()==0){
		resultado.put("status",false);
	}else{
		resultado.put("status",true);
	}
	out.println(resultado);

%>