<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%@page import="java.util.Base64"%>
<%@page import="java.util.Date"%>
<%@page import="java.nio.charset.StandardCharsets,be.openclinic.mobilemoney.*"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.entity.*,org.apache.commons.codec.binary.*,javax.crypto.*,javax.crypto.spec.*,org.apache.http.client.*,org.apache.http.client.methods.*,
				org.apache.http.client.utils.*,java.net.*,org.apache.http.impl.client.*,org.apache.http.*,java.util.Calendar,java.text.SimpleDateFormat"%>
<%@ page import="java.sql.*" %>
<%
	String column = request.getParameter("column");
	String buscar = request.getParameter("search");
	String columns = request.getParameter("columns");
	String unidad = request.getParameter("unidad");
	String webservice = request.getParameter("webservice");
	String centroDigitacionId = request.getParameter("centro_digitacion_id");
	String fechaAtencion = request.getParameter("fecha");
	
	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_webservices WHERE sis_centro_digitacion = '"+centroDigitacionId+"' AND sis_denominacion = '"+webservice+"'");
	ResultSet rs = sql.executeQuery();
	
	String url = "";
	String user = "";
	String password = "";
	String consultor= "";
	while(rs.next())
	{		
		switch (rs.getString("sis_parametro")){
	        case "url":{
	            url = rs.getString("sis_valor");
	            break;
	        }
	        case "user":{
	        	user = rs.getString("sis_valor");
	            break;
	        }
	        case "password":{
	        	password = new WebService().decrypt(rs.getString("sis_valor"));
	            break;
	        }
	        case "consultor":{
	        	consultor = rs.getString("sis_valor");
	            break;
	        }
	        default: {
	            System.out.println("Opcion incorrecta");
	        }
	  }
	}
	
	JSONObject objResponse;
	JSONObject objData = new JSONObject();
	try {
		HttpClient httpclient = HttpClients.createDefault();
		String baseurl = url+"/insumo/"+buscar;
		URIBuilder builder = new URIBuilder(baseurl);
		URI uri = builder.build();
		HttpGet req = new HttpGet(uri);
		
		String valueToEncode = user+":"+password;
		
		String auth = "Basic " + Base64.getEncoder().encodeToString(valueToEncode.getBytes());	
		req.setHeader("Authorization",auth);
		req.setHeader("Content-Type", "application/json");
		
		HttpResponse resp = httpclient.execute(req);
		HttpEntity entity = resp.getEntity();
		String responseBody = EntityUtils.toString(resp.getEntity(), StandardCharsets.UTF_8);
		
		objResponse = new JSONObject(responseBody);
		objData.put("ins_CodIns", objResponse.get("cod"));
		objData.put("ins_Nombre", objResponse.get("nom"));
		
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println(e);
	}
	
	/* GetDataBD data = new GetDataBD();
	data.setModel("m_insumos");
	
	JSONArray joinD = new JSONArray();
	
	JSONArray whereD = new JSONArray();
	whereD.put(column+" = "+buscar);
	whereD.put("ins_IdEstado = '0'");
	
	data.setColumn(columns);
	data.setJoin(joinD);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData(); */
	
	JSONObject resultado = new JSONObject();
	resultado.put("data",objData);
	
	if(objData.length()==0){
		resultado.put("status",false);
		resultado.put("precio",new JSONObject());
	}else{
resultado.put("status",true);
		
		JSONObject json = new JSONObject(resultado); 
		
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -1);
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMM");
		String mesAnterior = formato.format(c.getTime());
	
		GetDataBD precio = new GetDataBD();
		precio.setModel("bdsis_tarifas.sis_tarifa_medicamento_insumo");
		precio.setColumn("precioADJ,precioOPE");
		JSONObject res = precio.getPrecio(unidad,buscar,mesAnterior,fechaAtencion);
		
		resultado.put("precio",res);
	}

%>
<%=resultado.toString() %>
