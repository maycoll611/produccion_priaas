<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="javax.json.Json"%>
<%@page import="pe.gob.sis.arfsisweb.model.*,pe.gob.sis.arfsisweb.*,org.json.*,java.io.BufferedReader,java.util.Date,java.text.SimpleDateFormat"%>
<%@page contentType="application/json; charset=UTF-8"  pageEncoding="UTF-8"%>

<%		
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable();
	datatable.setModel("bdsis_maestros.m_reglasconsistencia");
	datatable.setColumn("rct_id_rc,rct_v_denominacion,rct_v_descripcion,rcc_estado_interfaz,rcc_estado_activo,rcc_IdConvenio");
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();
    
    relation1.put("relation", "bdsis_maestros.a_detallereglasconvenio");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "(a_detallereglasconvenio.rcc_idrc");
    relation1.put("foreign_key", "m_reglasconsistencia.rct_id_rc AND rcc_IdConvenio = '0' ) ");
    //relation1.put("where", "a_detallereglasconvenio.rcc_IdConvenio = '0' ");
    
    relations.add(0,relation1);
    
  	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");
	/* datatable.getDataJson(); */
	
%>
<%=datatable.getDataJson() %>

