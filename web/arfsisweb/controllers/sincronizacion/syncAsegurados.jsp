<%@page import="be.mxs.common.util.io.MessageReader,
                be.mxs.common.util.io.MessageReaderMedidoc,
                java.util.*,be.openclinic.finance.*,be.openclinic.pharmacy.*,
                javazoom.upload.MultipartFormDataRequest,
                javazoom.upload.UploadFile,org.dom4j.*,org.dom4j.io.*,be.openclinic.medical.*,
                java.io.*,be.mxs.common.util.system.*,be.mxs.common.util.db.*,
                pe.gob.sis.MysqlConnect,
                org.json.*"%>

<%@page import="pe.gob.sis.UnzipUtility" %>                
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >
    <jsp:setProperty name="upBean" property="folderstore" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>' />
    <jsp:setProperty name="upBean" property="parser" value="<%= MultipartFormDataRequest.DEFAULTPARSER   %>"/>
    <jsp:setProperty name="upBean" property="filesizelimit" value="8589934592"/>
    <jsp:setProperty name="upBean" property="overwrite" value="true"/>
    <jsp:setProperty name="upBean" property="dump" value="true"/>
    <jsp:setProperty name="upBean" property="parsertmpdir" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>'/>
</jsp:useBean>
<%
/*************************************
Nombre         : syncAsegurados
Proposito      : Sincronizacion de asegurados
Creado por     : ETIPULA
Fec Creacion   : 20230124
Observaciones  : Ninguna 
*************************************/

int lines=0;
String message="";

JSONObject result = new JSONObject();
JSONArray array = new JSONArray();

if (MultipartFormDataRequest.isMultipartFormData(request)) {
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    if (mrequest !=null) {
        Hashtable files = mrequest.getFiles();
        if ( (files != null) && (!files.isEmpty()) ) {
            UploadFile file = (UploadFile)files.get("filename");

            if(file!=null && file.getFileName()!=null){
                String fullFileName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")+"/"+file.getFileName();
                String tmpDirName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp");
                new File(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")).mkdirs();
                upBean.setFolderstore(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"));
                upBean.setParsertmpdir(application.getRealPath("/")+"/"+MedwanQuery.getInstance().getConfigString("tempdir","/tmp/"));
                upBean.store(mrequest, "filename");
                
                String zipFilePath = fullFileName.substring(0, fullFileName.length()-4);
				String destDirectory = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"); 
				UnzipUtility unzipper = new UnzipUtility();
				try {
				    unzipper.unpack(zipFilePath, destDirectory);
				} catch (Exception ex) {
				    ex.printStackTrace();
				}
                
                File txtFile = new File(zipFilePath+".txt");
                File zipFile = new File(fullFileName);
                
                PreparedStatement ps = null;
            	ResultSet rs = null;
            	
            	MysqlConnect con = new MysqlConnect("bdsis_maestros");
            	
            	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_sincronizacion WHERE tipo='1' AND nombre = '"+file.getFileName()+"' LIMIT 1");
            	ResultSet rsi = sql.executeQuery();
            	
            	int size = 0;
            	String fecha = "";
                while (rsi.next()) {
                    size++;
                    fecha += rsi.getString("fecha");
                }
                if(size == 0)
                {
                	PreparedStatement updateRow = con.connect().prepareStatement("UPDATE openclinic_dbo.sis_sincronizacion SET estado='1'");
            		updateRow.execute();
            		updateRow.close();
            		
                    PreparedStatement insertSync = con.connect().prepareStatement("INSERT INTO openclinic_dbo.sis_sincronizacion "
            				+ "(nombre,tipo,resultado,log,estado) "
            				+ "VALUES (?,?,?,?,?)");
                    insertSync.setString(1, file.getFileName());
                    insertSync.setString(2, "1");
                    
                    int totalExito=0, totalError=0;
                    String log ="";
                    
                    try
                    {
                       
                        String query = "LOAD DATA INFILE '"+tmpDirName+"/"+txtFile.getName()+"' REPLACE INTO TABLE `bdsis_asegurados`.`m_afiliados` CHARACTER SET latin1 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\r\\n' (`afi_IdSiasis`,`afi_TipoTabla`,`afi_IdDisa`,`afi_TipoFormato`,@afi_NroFormato,@afi_CorrelativoIns,@afi_IdTipoDocumento,@afi_IdEESSAte,@afi_FecFormato,@afi_ApePaterno,@afi_ApeMaterno,@afi_Nombres,@afi_SegNombre,@afi_IdSexo,@afi_FecNac,@afi_IdDistrito,@afi_IdEstado,@afi_FecBaja,@afi_Dni,@afi_Plan,@afi_idGrupoPoblacional,@AFI_DIRECCION,@afi_FecFinCobertura,@afi_FecFallecimiento) SET afi_NroFormato=NULLIF(@afi_NroFormato,''),afi_CorrelativoIns=NULLIF(@afi_CorrelativoIns,''),afi_IdTipoDocumento=NULLIF(@afi_IdTipoDocumento,''),afi_IdEESSAte=NULLIF(@afi_IdEESSAte,''),afi_FecFormato=NULLIF(@afi_FecFormato,''),afi_ApePaterno=NULLIF(@afi_ApePaterno,''),afi_ApeMaterno=NULLIF(@afi_ApeMaterno,''),afi_Nombres=NULLIF(@afi_Nombres,''),afi_SegNombre=NULLIF(@afi_SegNombre,''),afi_IdSexo=NULLIF(@afi_IdSexo,''),afi_FecNac=NULLIF(@afi_FecNac,''),afi_IdDistrito=NULLIF(@afi_IdDistrito,''),afi_IdEstado=NULLIF(@afi_IdEstado,''),afi_FecBaja=NULLIF(@afi_FecBaja,''),afi_Dni=NULLIF(@afi_Dni,''),afi_Plan=NULLIF(@afi_Plan,''),afi_idGrupoPoblacional=NULLIF(@afi_idGrupoPoblacional,''),AFI_DIRECCION=NULLIF(@AFI_DIRECCION,''),afi_FecFinCobertura=NULLIF(@afi_FecFinCobertura,''),afi_FecFallecimiento=NULLIF(@afi_FecFallecimiento,'')";                        
                        ps = con.connect().prepareStatement(query);
                        rs = ps.executeQuery();
                        
                        message = rs.toString();
                        totalExito++;
                    	result.put("status",true);
                    }
                    catch(Exception e)
                    {
                    	 message = e.getMessage().toString();
                     	totalError++;   
                     	result.put("status",false);
                    }
                    result.put("message","Total exito: "+totalExito+" Total error: "+totalError);
        			log+=file.getFileName()+": "+message+"\n";
        			result.put("log",log);
        			
        			insertSync.setString(3, "Total exito: "+totalExito+" Total error: "+totalError);
                	insertSync.setString(4, log);
                	insertSync.setString(5, "0");
                	insertSync.execute();
                	insertSync.close();
                	sql.close();
                }
                else{
                	/* System.out.println("este archivo ya se ejecuto el: "+fecha); */
                	result.put("status",false);
                	result.put("message","este archivo ya se ejecuto el: "+fecha);
                }
                
            txtFile.delete();
            zipFile.delete();
            }
           
        }
       
    }
   
}
%>
<%=result.toString()%>