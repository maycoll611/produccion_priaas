<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
/*************************************
Nombre         : ListTableMaestros
Proposito      : Listado de detalle de tablas maestras
Creado por     : ETIPULA
Fec Creacion   : 20230213
Observaciones  : Ninguna 
*************************************/

	String model = request.getParameter("table");
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable();
	datatable.setModel("bdsis_tarifas."+model);
	
	MysqlConnect con = new MysqlConnect("bdsis_tarifas");
	
	PreparedStatement sql = con.connect().prepareStatement("SHOW COLUMNS FROM bdsis_tarifas."+model);
	ResultSet rsi = sql.executeQuery();
	
	String columns = "";
	while(rsi.next())
	{
		columns+=rsi.getString("Field")+",";
	}
	columns = columns.substring(0,columns.length()-1);
	System.out.println(columns);
	
	datatable.setColumn(columns);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
%>
<%=datatable.getDataJson() %>

