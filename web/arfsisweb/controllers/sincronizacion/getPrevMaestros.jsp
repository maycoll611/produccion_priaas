<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,be.openclinic.pharmacy.Product,pe.gob.sis.arfsisweb.TransactionFUA"%>
<%@include file="/includes/validateUser.jsp" %>

<%		
/*************************************
Nombre         : getPrevMaestros
Proposito      : Lista de paquetes anteriores
Creado por     : ETIPULA
Fec Creacion   : 20230503
Observaciones  : Ninguna 
*************************************/
GetDataBD data = new GetDataBD();
data.setModel("openclinic_dbo.sis_sincronizacion");

JSONArray whereD = new JSONArray();
whereD.put("tipo = '2'");

data.setColumn("nombre,fecha");
data.setWhere(whereD);
JSONArray getData = data.getArrayJsonData();
System.out.println(getData.toString());
%>
<%=getData.toString()%>

