<%@page import="be.mxs.common.util.io.MessageReader,
                be.mxs.common.util.io.MessageReaderMedidoc,
                java.util.*,be.openclinic.finance.*,be.openclinic.pharmacy.*,
                javazoom.upload.MultipartFormDataRequest,
                javazoom.upload.UploadFile,org.dom4j.*,org.dom4j.io.*,be.openclinic.medical.*,
                java.io.*,be.mxs.common.util.system.*,be.mxs.common.util.db.*,
                pe.gob.sis.MysqlConnect,
                org.json.*"%>
<%@page import="pe.gob.sis.UnzipUtility" %>                
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>

<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >
    <jsp:setProperty name="upBean" property="folderstore" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>' />
    <jsp:setProperty name="upBean" property="parser" value="<%= MultipartFormDataRequest.DEFAULTPARSER   %>"/>
    <jsp:setProperty name="upBean" property="filesizelimit" value="8589934592"/>
    <jsp:setProperty name="upBean" property="overwrite" value="true"/>
    <jsp:setProperty name="upBean" property="dump" value="true"/>
    <jsp:setProperty name="upBean" property="parsertmpdir" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>'/>
</jsp:useBean>
<%
/*************************************
Nombre         : syncMaestros
Proposito      : Sincronizacion de maestros
Creado por     : ETIPULA
Fec Creacion   : 20230127
Observaciones  : Ninguna 
*************************************/

String message="";
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();

if (MultipartFormDataRequest.isMultipartFormData(request)) {
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    if (mrequest !=null) {
        Hashtable files = mrequest.getFiles();
        if ( (files != null) && (!files.isEmpty()) ) {
            UploadFile file = (UploadFile)files.get("filename");

            if(file!=null && file.getFileName()!=null){
                String fullFileName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")+"/"+file.getFileName();
                String tmpDirName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp");
                new File(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")).mkdirs();
                upBean.setFolderstore(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"));
                upBean.setParsertmpdir(application.getRealPath("/")+"/"+MedwanQuery.getInstance().getConfigString("tempdir","/tmp/"));
                upBean.store(mrequest, "filename");
                
                String zipFilePath = fullFileName.substring(0, fullFileName.length()-4);
				String destDirectory = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"); 
				UnzipUtility unzipper = new UnzipUtility();
				try {
				    unzipper.unpack(zipFilePath, destDirectory);
				} catch (Exception ex) {
				    ex.printStackTrace();
				}
                
                /* File txtFile = new File(zipFilePath); */
                /* System.out.println(tmpDirName); */
                File tmpDir = new File(tmpDirName);
                /* System.out.println(tmpDir.isFile()); */
                
                /* String[] fileList = tmpDir.list();
                
                if(fileList == null || fileList.length == 0)
                {
                	System.out.println("No hay elementos dentro de la carpeta actual");
                }
                else{
                	for(int i=0; i < fileList.length; i++)
                	{
                		System.out.println(fileList[i]);
                	}
                } */
                PreparedStatement ps = null;
            	ResultSet rs = null;
            	
                File[] tmpFiles = tmpDir.listFiles();
                if(tmpFiles == null || tmpFiles.length == 0)
                {
                	System.out.println("No hay elementos dentro de la carpeta actual");
                }
                else{
                	
                	/* File fileQuery = new File(tmpDirName+"\\A_QUERY_ARFSIS.txt");
                	InputStream inputStream = null;
                	inputStream = new FileInputStream(fileQuery);
                	
                	
                	File fileQuerySql = new File(tmpDirName+"\\fileName.sql"); // destination dir of your file
                	boolean success = fileQuery.renameTo(fileQuerySql);
                	if (success) {
                	    // File has been renamed
                	}
                	 */
                	/* System.out.println(fileQuery); */
                	MysqlConnect con = new MysqlConnect("bdsis_maestros");
                	
                	/* PreparedStatement sqlTxt = con.connect().prepareStatement("source "+tmpDirName+"\\A_QUERY_ARFSIS.txt");
                	sqlTxt.executeQuery(); */
                	PreparedStatement fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=0");
                	fkChecks.executeQuery();
                	
                	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_sincronizacion WHERE tipo='2' AND nombre = '"+file.getFileName()+"' LIMIT 1");
                	ResultSet rsi = sql.executeQuery();
                	
                	int size = 0;
                	String fecha = "";
                    while (rsi.next()) {
                        size++;
                        fecha += rsi.getString("fecha");
                    }
                    if(size == 0)
                    {
	                	PreparedStatement updateRow = con.connect().prepareStatement("UPDATE openclinic_dbo.sis_sincronizacion SET estado='1'");
	            		updateRow.execute();
	            		updateRow.close();
	            		
	                    PreparedStatement insertSync = con.connect().prepareStatement("INSERT INTO openclinic_dbo.sis_sincronizacion "
	            				+ "(nombre,tipo,resultado,log,estado) "
	            				+ "VALUES (?,?,?,?,?)");
	                    insertSync.setString(1, file.getFileName());
	                    insertSync.setString(2, "2");
	                    
	                    int totalArchivos = tmpFiles.length, totalExito=0, totalError=0;
	                    String log ="";
	                    
	                	for(int i=0; i < tmpFiles.length; i++)
	                	{
	                		File tmpFile = tmpFiles[i];
	                		System.out.println(tmpFile.getName());
	                		/* PreparedStatement sqlc = con.connect().prepareStatement("SELECT tb_Descripcion FROM bdsis_asegurados.m_tablas WHERE tb_archiRetro='"+tmpFile.getName()+"' LIMIT 1"); */
	                		/* PreparedStatement sqlc = con.connect().prepareStatement("SELECT tb_Descripcion,tb_archiRetro FROM openclinic_dbo.sis_tablas");
	                		
	                    	ResultSet rsc = sqlc.executeQuery();
	                    	while(rsc.next())
	                    	{ */
	                    		/* String tmpTableName= rsc.getString("tb_Descripcion");
	                    		String tmpTableFile= rsc.getString("tb_archiRetro"); */
	                    		
	                    		/* File txtFile = new File(tmpDirName+"/"+tmpTableFile); */
	                    		
	                    		/* if (txtFile.exists()) { */
		                    		/* System.out.println(tmpTableName); */
		                		if(tmpFile.getName().contains(".txt"))
		                		{ 
		                			String[] name = tmpFile.getName().split(".txt");
		                			String nameTable = name[0];
		                			try
		                            {
		                                /* Class.forName("com.mysql.jdbc.Driver");
		                                Connection con=DriverManager.getConnection(
		                                        "jdbc:mysql://localhost:13306/bdsis_maestros","openclinic","0pen");
		                                Statement con_bdsis=con.createStatement(); */
		                                
		                                
		                                String query = "LOAD DATA INFILE '"+tmpDirName+"/"+tmpFile.getName()+"' REPLACE INTO TABLE `bdsis_tarifas`.`"+nameTable+"` CHARACTER SET latin1 FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\r\\n';";
		                                ps = con.connect().prepareStatement(query);
		                    	    	rs = ps.executeQuery();
		                                /* ResultSet rs = con_bdsis.executeQuery(query); */
		                                //esultSet rs = con_bdsis.executeQuery()
		                            	message = rs.toString();
		                            	/* System.out.println(nameTable+":"+message); */
		                            	totalExito++;
		                            	result.put("status",true);
		                            }
		                            catch(Exception e)
		                            {
		                                message = e.getMessage().toString();
		                                /* System.out.println(nameTable+":"+message); */
		                            	totalError++;   
		                            	result.put("status",true);
		                            } 
		                			result.put("message","total archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
		                			log+=tmpFile.getName()+": "+message+"\n";
		                			result.put("log",log);
		                		}
		                		tmpFile.delete();
	                		/* } */
	                	}
	                    	
	                	insertSync.setString(3, "total archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
	                	insertSync.setString(4, log);
	                	insertSync.setString(5, "0");
	                	insertSync.execute();
	                	insertSync.close();
	                	sql.close();
                    }
                    else{
                    	/* System.out.println("este archivo ya se ejecuto el: "+fecha); */
                    	result.put("status",false);
                    	result.put("message","este archivo ya se ejecuto el: "+fecha);
                    }
                    
                    fkChecks.close();
                    fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=1");
                	fkChecks.executeQuery();
                }
                	
                File zipFile = new File(fullFileName);
                if(rs!=null)rs.close();
	            if(ps!=null)ps.close();             
            zipFile.delete();
            }
           
        }
       
    }
}
%>
<%=result.toString()%>