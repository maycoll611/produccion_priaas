<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,be.openclinic.pharmacy.Product,pe.gob.sis.arfsisweb.TransactionFUA"%>
<%@include file="/includes/validateUser.jsp" %>

<%		
/*************************************
Nombre         : getStateSync
Proposito      : Data de paquetes y estado de paquetes
Creado por     : ETIPULA
Fec Creacion   : 20230503
Observaciones  : Ninguna 
*************************************/
JSONObject result = new JSONObject();
JSONObject resultA = new JSONObject();
JSONObject resultM = new JSONObject();

MysqlConnect con = new MysqlConnect("openclinic_dbo");

/* AFILIADOS */
PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM sis_sincronizacion AS s WHERE s.tipo = '1' ORDER by s.fecha DESC LIMIT 1");
ResultSet rs = sql.executeQuery();	

while(rs.next())
{
	resultA.put("fecha", rs.getString("fecha"));
	resultA.put("nombre", rs.getString("nombre"));
}
rs.close();
/* MAESTROS */
sql = con.connect().prepareStatement("SELECT * FROM sis_sincronizacion AS s WHERE s.tipo = '2' ORDER by s.fecha DESC LIMIT 1");
rs = sql.executeQuery();	

while(rs.next())
{
	resultM.put("fecha", rs.getString("fecha"));
	resultM.put("nombre", rs.getString("nombre"));
}
rs.close();

result.put("afiliados", resultA);
result.put("maestros", resultM);

%>
<%=result %>

