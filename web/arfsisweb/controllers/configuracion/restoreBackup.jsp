<%@page import="be.mxs.common.util.io.MessageReader,
                be.mxs.common.util.io.MessageReaderMedidoc,
                java.util.*,be.openclinic.finance.*,be.openclinic.pharmacy.*,
                javazoom.upload.MultipartFormDataRequest,
                javazoom.upload.UploadFile,org.dom4j.*,org.dom4j.io.*,be.openclinic.medical.*,
                java.io.*,be.mxs.common.util.system.*,be.mxs.common.util.db.*,
                pe.gob.sis.MysqlConnect,
                org.json.*"%>

<%@page import="pe.gob.sis.UnzipUtility" %>                
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >
    <jsp:setProperty name="upBean" property="folderstore" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>' />
    <jsp:setProperty name="upBean" property="parser" value="<%= MultipartFormDataRequest.DEFAULTPARSER   %>"/>
    <jsp:setProperty name="upBean" property="filesizelimit" value="8589934592"/>
    <jsp:setProperty name="upBean" property="overwrite" value="true"/>
    <jsp:setProperty name="upBean" property="dump" value="true"/>
    <jsp:setProperty name="upBean" property="parsertmpdir" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>'/>
</jsp:useBean>
<%	
	String db = request.getParameter("db");
	String cpu = request.getParameter("cpu");

    int lines=0;
String message="";

JSONObject result = new JSONObject();
JSONArray array = new JSONArray();

if (MultipartFormDataRequest.isMultipartFormData(request)) {
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    if (mrequest !=null) {
        Hashtable files = mrequest.getFiles();
        if ( (files != null) && (!files.isEmpty()) ) {
            UploadFile file = (UploadFile)files.get("filename");

            if(file!=null && file.getFileName()!=null){
                String fullFileName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")+"/"+file.getFileName();
                String tmpDirName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp");
                new File(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")).mkdirs();
                upBean.setFolderstore(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"));
                upBean.setParsertmpdir(application.getRealPath("/")+"/"+MedwanQuery.getInstance().getConfigString("tempdir","/tmp/"));
                upBean.store(mrequest, "filename");
                
                String zipFilePath = fullFileName.substring(19, fullFileName.length()-27);
                
				String destDirectory = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"); 

                File zipFile = new File(fullFileName);
                

				System.out.println(db);
				System.out.println(zipFilePath);
				System.out.println(cpu);
				if(zipFilePath.equals(db)){
					String executeCmd = "";	
					
					if(cpu.equals("V64")){
                        executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mariadb\\bin && 7z e "+fullFileName+" -so -pARFSISsecretWEB | mariadb -u openclinic -p0pen --port=13306";
                    } else if(cpu.equals("V32")){
                        executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mariadb\\bin && 7z e "+fullFileName+" -so -pARFSISsecretWEB | mysql -u openclinic -p0pen --port=13306";
                    }

					Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
						
					int processComplete = runtimeProcess.waitFor();
					if(processComplete == 0){
						
						result.put("status",true);
						
					} 
					else {
						result.put("status",false);
						
					}
				}
				else {
					result.put("status",false);
					result.put("message","El archivo cargado no coincide con el formato y/o base de datos");
				}
			
                
            	zipFile.delete();
            }
           
        }
       
    }
   
}
%>
<%=result.toString()%>