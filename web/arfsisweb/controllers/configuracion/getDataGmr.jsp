<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
/*************************************
Nombre         : getDataGmr
Proposito      : Obtener datos de maestros de macroregion
Creado por     : ETIPULA
Fec Creacion   : 20230227
Observaciones  : Ninguna 
*********************************/

	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");
	
	DataTable datatable = new DataTable();
	datatable.setModel("m_macroregion");
	datatable.setColumn(columns);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
%>
<%=datatable.getDataJson() %>