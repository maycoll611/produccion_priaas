<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
	
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");

	DataTable datatable = new DataTable();
	datatable.setModel("m_eess");
	datatable.setColumn(columns);

	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
	Map<String,String> relation3 = new HashMap<String, String>();
 
    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();

    relation1.put("relation", "bdsis_maestros.m_odsis");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "m_odsis.ods_IdOdsis");
    relation1.put("foreign_key", "m_eess.pre_IdOdsis");

    relation2.put("relation", "bdsis_maestros.m_macroregion");
    relation2.put("type_relation", "JOIN");
    relation2.put("key", "m_macroregion.MACR_I_IDMACROREGION");
    relation2.put("foreign_key", "m_odsis.ods_IdMacroRegion");
    
    relation3.put("relation", "bdsis_maestros.m_disas");
    relation3.put("type_relation", "JOIN");
    relation3.put("key", "m_disas.dis_IdDisa");
    relation3.put("foreign_key", "m_eess.pre_IdDisa");
    
    relations.add(0,relation1);
    relations.add(1,relation2);
    relations.add(2,relation3);
    
	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("pre_IdEstado = '0'");

%>
<%=datatable.getDataJson() %>