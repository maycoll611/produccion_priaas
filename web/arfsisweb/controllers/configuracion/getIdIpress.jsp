<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	String column = request.getParameter("column");
	String buscar = request.getParameter("search");
	String columns = request.getParameter("columns");
	
	GetDataBD data = new GetDataBD();
	data.setModel("m_eess");
	
	JSONArray joinD = new JSONArray();
	joinD.put("JOIN a_categoriaeess on cat_IdCategoriaEESS = pre_IdCategoriaEESS");
	
	JSONArray whereD = new JSONArray();
	whereD.put("pre_CodigoRENAES = "+"00"+buscar);
	whereD.put("pre_IdEstado = '0'");
	whereD.put("pre_DigitaAtenciones = 'S'");
	
	data.setColumn(columns);
	data.setJoin(joinD);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	JSONObject resultado = new JSONObject();
	resultado.put("data",getData);
	if(getData.length()==0){
		resultado.put("status",false);
	}else{
		resultado.put("status",true);
	}
%>
<%=resultado.toString() %>


