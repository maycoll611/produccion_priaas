<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<%
/*************************************
Nombre         : getDataGmr
Proposito      : Obtener datos de maestros de macroregion
Creado por     : ETIPULA
Fec Creacion   : 20230227
Observaciones  : Ninguna 
*********************************/


	GetDataBD version = new GetDataBD();
	version.setModel("openclinic_dbo.sis_version");
	version.setColumn("descripcion,mayor,menor,parche,estabilidad");
	JSONArray whereV = new JSONArray();
	whereV.put("estado = '1'");
	version.setWhere(whereV);
	
	/* permisos */	
	GetDataBD usuario= new GetDataBD();
	usuario.setModel("openclinic_dbo.sis_usuarios");
	usuario.setColumn("sis_usuario_permisos.sis_identificador,sis_usuario_roles.sis_identificador,sis_oc_userid,sis_centro_digitacion,sis_gmr,sis_udr,sis_disa,sis_ue,sis_eess,sis_email_institucion,sis_email_particular,ods_Descripcion,dis_Descripcion,pin_Nombre");
		
	JSONArray join = new JSONArray();
	join.put("JOIN openclinic_dbo.sis_usuario_perfiles ON sis_usuario_perfiles.sis_rol_id = sis_usuarios.sis_rol");
	join.put("JOIN openclinic_dbo.sis_usuario_permisos ON sis_usuario_permisos.sis_permiso_id = sis_usuario_perfiles.sis_permiso_id");
	join.put("JOIN openclinic_dbo.sis_usuario_roles ON sis_usuario_roles.sis_rol_id = sis_usuario_perfiles.sis_rol_id");
	join.put("LEFT JOIN m_odsis ON m_odsis.ods_IdOdsis = sis_usuarios.sis_udr");
	join.put("LEFT JOIN m_disas ON m_disas.dis_IdDisa = sis_usuarios.sis_disa");
	join.put("LEFT JOIN m_pinstalacion ON m_pinstalacion.pin_IdPPDD = sis_usuarios.sis_centro_digitacion");
	usuario.setJoin(join);
	
	JSONArray where = new JSONArray();
	where.put("sis_usuarios.sis_oc_userid = '"+activeUser.userid+"'");
	usuario.setWhere(where);
	
	JSONObject getPermisos = usuario.getJsonData();
	JSONObject resultado = new JSONObject();
	
	resultado.put("usuario",usuario.getArrayJsonData());
	resultado.put("nombres",activeUser.person.firstname+" "+activeUser.person.lastname);
	resultado.put("version",version.getJsonData());
	
	if(getPermisos.length()==0){
		resultado.put("status",false);
	}else{
		resultado.put("status",true);
	}
%>
<%=resultado.toString() %>