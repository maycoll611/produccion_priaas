<%@page errorPage="/includes/error.jsp"%>
<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,java.io.BufferedReader"%>

<%@include file="/includes/validateUser.jsp"%>

<%
		JSONObject allErrors = new JSONObject();
		JSONArray ruleErrors = new JSONArray();
		
		BufferedReader br = request.getReader();
		String params = br.readLine();
		
		JSONObject json = new JSONObject(params);
		
	    String sOldPassword = json.getString("oldPassword");
	    String sNewPassword1 = json.getString("newPassword1");
	    String sNewPassword2 = json.getString("newPassword2");
		
	    
	    /* resultado.put("usuario",usuario.getArrayJsonData());
		resultado.put("nombres",activeUser.person.firstname+" "+activeUser.person.lastname); */
		
		/* if(getPermisos.length()==0){
			resultado.put("status",false);
		}else{
			resultado.put("status",true);
		} */
    //--- SAVE PASSWORD ---------------------------------------------------------------------------

		int minimumChars         = MedwanQuery.getInstance().getConfigInt("PasswordMinimumCharacters"),
		        notReusablePasswords = MedwanQuery.getInstance().getConfigInt("PasswordNotReusablePasswords");
		
		boolean lettersObliged      = MedwanQuery.getInstance().getConfigString("PasswordObligedLetters").equals("on"),
	            uppercaseObliged    = MedwanQuery.getInstance().getConfigString("PasswordObligedUppercase").equals("on"),
	            lowercaseObliged    = MedwanQuery.getInstance().getConfigString("PasswordObligedLowerCase").equals("on"),
	            numbersObliged      = MedwanQuery.getInstance().getConfigString("PasswordObligedNumbers").equals("on"),
	            alfanumericsObliged = MedwanQuery.getInstance().getConfigString("PasswordObligedAlfanumerics").equals("on");
		
		boolean status = true;
		boolean noReuseOfOldPwd = (notReusablePasswords > 0);
        // reuse of old pwd allowed ?
        if(noReuseOfOldPwd){
            // how many of the used passwords must be considered ?
            int oldPwdCount = 10000; // all
            if(notReusablePasswords > 0){
                oldPwdCount = notReusablePasswords;
            }
            boolean passwordIsUsedBefore = User.isPasswordUsedBefore(sNewPassword1,activeUser,oldPwdCount);
            if(passwordIsUsedBefore || sNewPassword1.equals(sOldPassword)){
                String msg = getTran(request,"web.userProfile","PasswordNoReuseOfOldPwdAllowed",sWebLanguage).replaceAll("#numberOfPasswords#",Integer.toString(notReusablePasswords));
                status = false;
                ruleErrors.put(msg);
            }
        }
        
        //--- apply rules ---
        // minimum characters
        if(minimumChars > -1){
            if(sNewPassword1.length() < minimumChars){
                String msg = getTran(request,"Web.UserProfile","PasswordMinimumCharactersObliged",sWebLanguage);
                msg = msg.replaceFirst("#minChars#",minimumChars+"");
                status = false;
                ruleErrors.put(msg);
            }
        }

        // numbers obliged && letters obliged
        if(numbersObliged && lettersObliged){
            if(!ScreenHelper.containsNumber(sNewPassword1)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","PasswordNumbersObliged",sWebLanguage);
            	ruleErrors.put(msg);
            }

            if(!ScreenHelper.containsLetter(sNewPassword1)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","PasswordLettersObliged",sWebLanguage);
            	ruleErrors.put(msg);
            }
        }
        else{
            // numbers obliged && letters allowed
            if(numbersObliged){
                if(!ScreenHelper.containsNumber(sNewPassword1)){
                	status = false;
                    String msg = getTran(request,"Web.UserProfile","PasswordNumbersObliged",sWebLanguage);
                    ruleErrors.put(msg);
                }
            }

            // letters obliged && numbers allowed
            if(lettersObliged){
                if(!ScreenHelper.containsLetter(sNewPassword1)){
                	status = false;
                	String msg = getTran(request,"Web.UserProfile","PasswordLettersObliged",sWebLanguage);
                    ruleErrors.put(msg);
                }
            }
        }

        // uppercase obliged and lowercase obliged

        if(uppercaseObliged && lowercaseObliged){
            if(!ScreenHelper.containsUppercase(sNewPassword1)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","PasswordUppercaseObliged",sWebLanguage);
            	ruleErrors.put(msg);
            }

            if(!ScreenHelper.containsLowercase(sNewPassword1)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","PasswordLowercaseObliged",sWebLanguage);
            	ruleErrors.put(msg);
            }
        }
        else{
            // uppercase obliged
            if(uppercaseObliged){
                if(!ScreenHelper.containsUppercase(sNewPassword1)){
                	status = false;
                	String msg = getTran(request,"Web.UserProfile","PasswordUppercaseObliged",sWebLanguage);
                	ruleErrors.put(msg);
                }
            }

            // lowercase obliged
            if(lowercaseObliged){
                if(!ScreenHelper.containsLowercase(sNewPassword1)){
                	status = false;
                	String msg = getTran(request,"Web.UserProfile","PasswordLowercaseObliged",sWebLanguage);
                	ruleErrors.put(msg);
                }
            }
        }

        // alfanumerics obliged
        if(alfanumericsObliged){
            if(!ScreenHelper.containsAlfanumerics(sNewPassword1)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","PasswordAlfanumericsObliged",sWebLanguage);
            	ruleErrors.put(msg);
            }
        }
        
        //--- compare passwords ---
        if(!sNewPassword1.equals(sNewPassword2)){
            /* errorNewPassword = true; */
            status = false;
            String msg = getTran(request,"Web.UserProfile","ErrorNewPassword",sWebLanguage);
            ruleErrors.put(msg);
        }
        else{
            byte[] aOldPassword = activeUser.encryptOld(sOldPassword);
			
			if(activeUser.checkPassword(aOldPassword))
			{
				aOldPassword = activeUser.encryptOld(sOldPassword);	
			}else{
				aOldPassword = activeUser.encrypt(sOldPassword);
			}
			
            if(!activeUser.checkPassword(aOldPassword)){
            	status = false;
            	String msg = getTran(request,"Web.UserProfile","ErrorOldPassword",sWebLanguage);
            	ruleErrors.put(msg);
                /* errorOldPassword = true;
                allErrors.append("<img src='"+sCONTEXTPATH+"/_img/icons/icon_warning.gif' style='vertical-align:-3px'/>&nbsp;");
                allErrors.append("<b>").append(getTran(request,"Web.UserProfile","ErrorOldPassword",sWebLanguage)).append("</b><br>"); */
            }
            else{
                if(status){
                    // all OK :
                    byte[] aNewPassword = activeUser.encrypt(sNewPassword1);

                    // store the new password in session
                    activeUser.password = aNewPassword;
                    session.setAttribute("activeUser",activeUser);

                    //store new password in DB
                    activeUser.savePasswordToDB();
                    
                    //*** 2 : set the updatetime to now when the password is used before, otherwise add the new password ***
                    String sSql = "UPDATE UsedPasswords SET updatetime = ?"+
                   	              " WHERE userId = ?"+
                                  "  AND CAST(encryptedPassword AS BINARY) = ?";
                    Connection conn = MedwanQuery.getInstance().getAdminConnection();
                    PreparedStatement ps = conn.prepareStatement(sSql);
                    ps.setTimestamp(1,getSQLTime()); // now
                    ps.setInt(2,Integer.parseInt(activeUser.userid));
                    ps.setBytes(3,activeUser.encrypt(sNewPassword1));
                    int updatedRecords = ps.executeUpdate();
                    ps.close();
                    conn.close();
                    
                    if(updatedRecords==0){                
                    	sSql = "INSERT INTO UsedPasswords(usedPasswordId,encryptedPassword,userId,updatetime,serverid)"+
                               " VALUES (?,?,?,?,?)";
                        conn = MedwanQuery.getInstance().getAdminConnection();
                        ps = conn.prepareStatement(sSql);
                        ps.setInt(1,MedwanQuery.getInstance().getOpenclinicCounter("UsedPasswords"));
                        ps.setBytes(2,activeUser.encrypt(sNewPassword1));
                        ps.setInt(3,Integer.parseInt(activeUser.userid));
                        ps.setTimestamp(4,getSQLTime()); // now
                        ps.setInt(5,MedwanQuery.getInstance().getConfigInt("serverId"));
                        ps.executeUpdate();
                        ps.close();
                        conn.close();
                    }

                    // let user remember when he changed his password
                    Parameter pwdChangeParam = new Parameter("pwdChangeDate",System.currentTimeMillis()+"");
                    activeUser.updateParameter(pwdChangeParam);

                    // display how long the password remains valid
                    int availability = MedwanQuery.getInstance().getConfigInt("PasswordAvailability");

                    // return to userprofile index
                    /* allErrors.append("<script>window.location.href='main.do?Page=userprofile/index.jsp"); */

                    /* allErrors.put("status", status); */
                    /* allErrors.put("errors", ruleErrors); */
                }
            }
        }
        allErrors.put("errors", ruleErrors);
        allErrors.put("status", status);
        out.print(allErrors);
%>