<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,pe.gob.sis.arfsisweb.model.*"%>
<%
/*************************************
Nombre         : getListPermisos
Proposito      : Listado de Permisos
Creado por     : FLIZARRAGA
Fec Creacion   : 20230303
Observaciones  : Ninguna 
*************************************/

	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable(); 
	datatable.setModel("openclinic_dbo.sis_usuario_permisos");
	datatable.setColumn("sis_permiso_id,sis_identificador,sis_descripcion");

	// Map<String,String> relations = new ArrayList<Map<String,String>>();

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);

%>
<%=datatable.getDataJson() %>