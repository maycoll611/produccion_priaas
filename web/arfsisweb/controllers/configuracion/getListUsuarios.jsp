<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
/*************************************
Nombre         : ListAsegurados
Proposito      : Listado de Asegurados
Creado por     : ETIPULA
Fec Creacion   : 20230224
Observaciones  : Ninguna 
*************************************/


	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String rol = request.getParameter("rol");
	
	DataTable datatable = new DataTable();
	datatable.setModel("ocadmin_dbo.users");
	datatable.setColumn("sis_usuario_id,users.userid,users.personid,admin.lastname,admin.middlename,admin.firstname,admin.natreg,admin.gender,admin.person_type,sis_usuario_roles.sis_identificador,sis_email_institucion,sis_email_particular,sis_centro_digitacion,sis_gmr,sis_udr,sis_disa,sis_ue,sis_eess,sis_rol");
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
	Map<String,String> relation3 = new HashMap<String, String>();
 
    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();

    relation1.put("relation", "ocadmin_dbo.admin");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "admin.personid");
    relation1.put("foreign_key", "users.personid");

    relation2.put("relation", "openclinic_dbo.sis_usuarios");
    relation2.put("type_relation", "JOIN");
    relation2.put("key", "sis_usuarios.sis_oc_userid");
    relation2.put("foreign_key", "users.userid");
    
    relation3.put("relation", "openclinic_dbo.sis_usuario_roles");
    relation3.put("type_relation", "LEFT JOIN");
    relation3.put("key", "sis_usuario_roles.sis_rol_id");
    relation3.put("foreign_key", "sis_usuarios.sis_rol");
    
    relations.add(0,relation1);
    relations.add(1,relation2);
    relations.add(2,relation3);
    
	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere(" natreg  not in ('00000000','11111111')");	
 	
	
	/* datatable.getDataJson(); */
%>
<%=datatable.getDataJson() %>

