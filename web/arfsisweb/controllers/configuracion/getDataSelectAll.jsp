<%@page import="org.json.*,pe.gob.sis.arfsisweb.*"%>
<%
/*************************************
Nombre         : getDataSelectAll
Proposito      : obtencion de datos para configuracion de usuarios
Creado por     : ETIPULA
Fec Creacion   : 20230226
Observaciones  : Ninguna 
*************************************/

	/* obtener lista de establecimientos */
	/* GetDataBD dataEess = new GetDataBD();
	dataEess.setModel("bdsis_maestros.m_eess");
	dataEess.setColumn("pre_IdEESS,pre_Nombre,pre_IdDisa,pre_IdOdsis,pre_CodEjeAdm");
	JSONArray whereEess = new JSONArray();
	whereEess.put("pre_IdEstado = '0'");
	dataEess.setWhere(whereEess); */
	String rol = request.getParameter("rol");
	
	/* obtener lista de gmr */
 	GetDataBD roles = new GetDataBD();
 	roles.setModel("openclinic_dbo.sis_usuario_roles");
 	roles.setColumn("sis_rol_id,sis_identificador");
 	JSONArray whereR = new JSONArray();
 	/* if(!rol.equals("Administrador"))
 	{ */
 		whereR.put("sis_identificador != 'Administrador'");	
 	/* } */
	roles.setWhere(whereR);
 	
	JSONObject data = new JSONObject();
	data.put("roles", roles.getArrayJsonData());
	
	GetDataBD tipoDocumento = new GetDataBD();
	tipoDocumento.setModel("bdsis_maestros.a_tipodocumento");
	tipoDocumento.setColumn("ide_IdTipoDocumento,ide_Descripcion");
	JSONArray whereTd = new JSONArray();
	whereTd.put("ide_IdEstado = '0'");
	whereTd.put("ide_IdTipoDocumento NOT IN (10,11)");
	tipoDocumento.setWhere(whereTd);
	
	data.put("tipo_documentos", tipoDocumento.getArrayJsonData());
%>
<%=data.toString()%>
