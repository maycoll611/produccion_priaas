<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
/*************************************
Nombre         : ListWebservices
Proposito      : Listado de WebServices
Creado por     : ETIPULA
Fec Creacion   : 20230310
Observaciones  : Ninguna 
*************************************/


	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable();
	datatable.setModel("openclinic_dbo.sis_webservices");
	datatable.setColumn("sis_webservice_id,sis_denominacion,sis_parametro,sis_valor,sis_centro_digitacion,pin_Nombre");
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
	Map<String,String> relation3 = new HashMap<String, String>();
 
    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();

    relation1.put("relation", "bdsis_maestros.m_pinstalacion");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "m_pinstalacion.pin_IdPPDD");
    relation1.put("foreign_key", "sis_webservices.sis_centro_digitacion");
    
    relations.add(0,relation1);
    
	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	/* datatable.setWhere("sis_parametro != 'password'"); */
%>
<%=datatable.getDataJson() %>

