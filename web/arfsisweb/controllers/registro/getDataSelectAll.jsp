<%@page import="org.json.*,pe.gob.sis.arfsisweb.*"%>
<%
	/*

	*/
	GetDataBD dataOrigenPersonal = new GetDataBD();
	dataOrigenPersonal.setModel("a_origenpersonal");
	dataOrigenPersonal.setColumn("ope_IdOrigenPersonal,ope_Descripcion");
	JSONArray whereOrigenPersonal = new JSONArray();
	whereOrigenPersonal.put("ope_IdEstado = '0'");
	dataOrigenPersonal.setWhere(whereOrigenPersonal);
	/*

	*/
	GetDataBD dataLugarAtencion = new GetDataBD();
	dataLugarAtencion.setModel("a_lugaratencion");
	dataLugarAtencion.setColumn("lug_IdLugar,lug_Descripcion");
	JSONArray whereLugarAtencion = new JSONArray();
	whereLugarAtencion.put("lug_IdEstado = '0'");
	dataLugarAtencion.setWhere(whereLugarAtencion);
	/*

	*/
	GetDataBD dataTipoAtencion = new GetDataBD();
	dataTipoAtencion.setModel("a_tipoatencion");
	dataTipoAtencion.setColumn("tat_IdTipoAtencion,tat_Descripcion");
	JSONArray whereTipoAtencion = new JSONArray();
	whereTipoAtencion.put("tat_IdEstado = '0'");
	dataTipoAtencion.setWhere(whereTipoAtencion);
	/*

	*/
	GetDataBD datatipoFormato = new GetDataBD();
	datatipoFormato.setModel("a_tipoformato");
	datatipoFormato.setColumn("tfrm_IdTipoFormato,tfrm_Descripcion");
	JSONArray wheretipoFormato = new JSONArray();
	wheretipoFormato.put("tfrm_IdEstado = '0'");
	datatipoFormato.setWhere(wheretipoFormato);
	/*

	*/
	GetDataBD dataTipoDocumento = new GetDataBD();
	dataTipoDocumento.setModel("a_tipodocumento");
	dataTipoDocumento.setColumn("ide_IdTipoDocumento,ide_Descripcion");
	JSONArray whereTipoDocumento = new JSONArray();
	whereTipoDocumento.put("ide_IdEstado = '0'");
	dataTipoDocumento.setWhere(whereTipoDocumento);
	
	/*

	*/
	GetDataBD datacomponente = new GetDataBD();
	datacomponente.setModel("a_componentes");
	datacomponente.setColumn("com_IdComponente,com_Descripcion");
	JSONArray wherecomponente = new JSONArray();
	wherecomponente.put("com_IdEstado = '0'");
	datacomponente.setWhere(wherecomponente);
	/*

	*/
	GetDataBD datasexo = new GetDataBD();
	datasexo.setModel("a_sexo");
	datasexo.setColumn("sex_IdSexo,sex_Descripcion");
	JSONArray wheresexo = new JSONArray();
	wheresexo.put("sex_IdEstado = '0'");
	datasexo.setWhere(wheresexo);
	/*

	*/
	GetDataBD datacondicionMaterna = new GetDataBD();
	datacondicionMaterna.setModel("a_condicionmaterna");
	datacondicionMaterna.setColumn("cdm_IdCondicion,cdm_Descripcion");
	JSONArray wherecondicionMaterna = new JSONArray();
	wherecondicionMaterna.put("cdm_IdEstado = '0'");
	datacondicionMaterna.setWhere(wherecondicionMaterna);
	
	/*

	*/
	GetDataBD dataconceptoPrestacional = new GetDataBD();
	dataconceptoPrestacional.setModel("a_modalidadatencion");
	dataconceptoPrestacional.setColumn("mod_IdModalidad,mod_Descripcion");
	JSONArray whereconceptoPrestacional = new JSONArray();
	whereconceptoPrestacional.put("mod_IdEstado = '0'");
	dataconceptoPrestacional.setWhere(whereconceptoPrestacional);
	/*

	*/
	GetDataBD datadestinoAsegurado = new GetDataBD();
	datadestinoAsegurado.setModel("a_destinoasegurado");
	datadestinoAsegurado.setColumn("des_IdDestinoAsegurado,des_Descripcion");
	JSONArray wheredestinoAsegurado = new JSONArray();
	wheredestinoAsegurado.put("des_IdEstado = '0'");
	datadestinoAsegurado.setWhere(wheredestinoAsegurado);
	/*

	*/
	GetDataBD datatipoDiagnostico = new GetDataBD();
	datatipoDiagnostico.setModel("a_tipodiagnostico");
	datatipoDiagnostico.setColumn("tda_IdTipoDiagnostico,tda_Descripcion");
	JSONArray wheretipoDiagnostico = new JSONArray();
	wheretipoDiagnostico.put("tda_IdEstado = '0'");
	datatipoDiagnostico.setWhere(wheretipoDiagnostico);
	
	/*

	*/
	GetDataBD datagrupoRiesgo = new GetDataBD();
	datagrupoRiesgo.setModel("m_gruporiesgo");
	datagrupoRiesgo.setColumn("rgo_IdGrupoRiesgo,rgo_Descripcion");
	JSONArray wheregrupoRiesgo = new JSONArray();
	wheregrupoRiesgo.put("rgo_IdEstado = '0'");
	datagrupoRiesgo.setWhere(wheregrupoRiesgo);
	/*

	*/
	GetDataBD datatipoResponsable = new GetDataBD();
	datatipoResponsable.setModel("a_tipopersonalsalud");
	datatipoResponsable.setColumn("tps_IdTipoPersonalSalud,tps_Descripcion");
	JSONArray wheretipoResponsable = new JSONArray();
	wheretipoResponsable.put("tps_IdEstado = '0'");
	datatipoResponsable.setWhere(wheretipoResponsable);
	/*

	*/
	GetDataBD dataespecilidad = new GetDataBD();
	dataespecilidad.setModel("a_especialidad");
	dataespecilidad.setColumn("esp_IdEspecialidad,esp_Descripcion");
		/*JSONArray whereespecilidad = new JSONArray();
	whereespecilidad.put("esp_IdEstado = '0'");
	dataespecilidad.setWhere(whereespecilidad);


	
	GetDataBD dataComponente = new GetDataBD();
	dataComponente.setModel("a_componentes");
	dataComponente.setColumn("com_IdComponente,com_Descripcion");
	JSONArray whereComponente = new JSONArray();
	whereComponente.put("com_IdEstado = '0'");
	dataComponente.setWhere(whereComponente);
	*/
	 
	JSONObject data = new JSONObject();
	data.put("origenPersonal", dataOrigenPersonal.getArrayJsonData());
	data.put("lugarAtencion", dataLugarAtencion.getArrayJsonData());
	data.put("tipoAtencion", dataTipoAtencion.getArrayJsonData());
	data.put("tipoFormato", datatipoFormato.getArrayJsonData());
	data.put("tipoDocumento", dataTipoDocumento.getArrayJsonData());
	data.put("componentes", datacomponente.getArrayJsonData());
	data.put("sexo", datasexo.getArrayJsonData());
	data.put("condicionMaterna", datacondicionMaterna.getArrayJsonData());
	data.put("conceptoPrestacional", dataconceptoPrestacional.getArrayJsonData());
	data.put("destinoAsegurado", datadestinoAsegurado.getArrayJsonData());
	data.put("tipoDiagnostico", datatipoDiagnostico.getArrayJsonData());
	data.put("grupoRiesgo", datagrupoRiesgo.getArrayJsonData());
	data.put("tipoResponsable", datatipoResponsable.getArrayJsonData());
	data.put("especialidades", dataespecilidad.getArrayJsonData());
		
%>
<%=data.toString()%>
