<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
	
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");

	DataTable datatable = new DataTable();
	datatable.setModel("m_eess");
	datatable.setColumn(columns);
	
	Map<String,String> relation1 = new HashMap<String, String>();
	List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();
    
    relation1.put("relation", "bdsis_maestros.a_categoriaeess");
    relation1.put("type_relation", "LEFT JOIN");
    relation1.put("key", "a_categoriaeess.cat_IdCategoriaEESS");
    relation1.put("foreign_key", "m_eess.pre_IdCategoriaEESS");
    
    relations.add(0,relation1);
    
  	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");

%>
<%=datatable.getDataJson() %>