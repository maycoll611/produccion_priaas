<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,java.util.Date,java.text.SimpleDateFormat,java.util.Calendar"%>

<%
	String column = request.getParameter("column");
	String buscar = request.getParameter("search");
	String columns = request.getParameter("columns");
	String unidad = request.getParameter("unidad");
	String fechaAtencion = request.getParameter("fecha");
	
	GetDataBD data = new GetDataBD();
	data.setModel("m_medicamentos");
	
	JSONArray joinD = new JSONArray();
	
	JSONArray whereD = new JSONArray();
	whereD.put(column+" = '"+buscar+"'");
	whereD.put("med_IdEstado = '0'");
	
	data.setColumn(columns);
	data.setJoin(joinD);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	JSONObject resultado = new JSONObject();
	resultado.put("data",getData);
	if(getData.length()==0){
		resultado.put("status",false);
		resultado.put("precio",new JSONObject());
	}else{
		resultado.put("status",true);
		
		JSONObject json = new JSONObject(resultado); 
		
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -1);
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMM");
		String mesAnterior = formato.format(c.getTime());
		
		GetDataBD precio = new GetDataBD();
		precio.setModel("bdsis_tarifas.sis_tarifa_medicamento_insumo");
		precio.setColumn("precioADJ,precioOPE");
		JSONObject res = precio.getPrecio(unidad,buscar,mesAnterior,fechaAtencion);
		
		resultado.put("precio",res);
	}
	
		


%>
<%=resultado.toString() %>
