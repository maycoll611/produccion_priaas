<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");

	DataTable datatable = new DataTable();
	datatable.setModel("a_resatencion");
	datatable.setColumn(columns);
	
	//JSONArray joinD = new JSONArray();
	//joinD.put("JOIN a_tipopersonalsalud on tps_IdTipoPersonalSalud = pers_IdTipoPersonalSalud");
	//joinD.put("LEFT JOIN a_especialidad on esp_IdEspecialidad = pers_IdEspecialidad");
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();

    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();
    
    relation1.put("relation", "a_tipopersonalsalud");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "tps_IdTipoPersonalSalud");
    relation1.put("foreign_key", "pers_IdTipoPersonalSalud");
    
    relation2.put("relation", "a_especialidad");
    relation2.put("type_relation", "LEFT JOIN");
    relation2.put("key", "esp_IdEspecialidad");
    relation2.put("foreign_key", "pers_IdEspecialidad");
    
    relations.add(0,relation1);
    relations.add(1,relation2);
    
  	datatable.setRelations(relations);


	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");

%>
<%=datatable.getDataJson() %>