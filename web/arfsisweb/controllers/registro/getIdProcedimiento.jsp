<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
	String codigo_cpms = request.getParameter("codigo_cpms");
	String codigo_cpt = request.getParameter("codigo_cpt");
	String columns = request.getParameter("columns");
	String nivel = request.getParameter("nivel");

	///
	GetDataBD data = new GetDataBD();
	data.setModel("m_apodiag");
	
	JSONArray joinD = new JSONArray();
	joinD.put("JOIN M_PROCEDIMIENTOS on apo_CodProcedimiento=PRCD_V_CODPROCEDIMIENTO");
	joinD.put("JOIN M_CPMS_EQV ON PRCD_V_CODPROCEDIMIENTO=CPMS_V_CODSIS");
	joinD.put("JOIN M_CPMS ON CPMS_V_CODIGO=CPMS_V_CODCPMS");
	joinD.put("JOIN M_SUBSECCIONCPMS ON SSCP_N_IDSUBSECCIONCPMS=CPMS_N_IDSUBSECCIONCPMS");
	joinD.put("JOIN M_SECCIONCPMS ON SCPM_N_IDSECCIONCPMS=SSCP_N_IDSECCIONCPMS");
	joinD.put("JOIN I_TARIFARIOCPMS ON TCPM_N_IDCPMS =CPMS_N_IDCPMS AND TCPM_N_CODAPO= CPMS_V_CODSIS");
	joinD.put("JOIN M_GRUPOCPMS ON GCPM_N_IDGRUPOCPMS=SCPM_N_IDGRUPOCPMS");
	
	JSONArray whereD = new JSONArray();
	if(codigo_cpt!=""){
		whereD.put("CPMS_V_CODSIS = '"+codigo_cpt+"'");
	}
	
	whereD.put("CPMS_V_CODCPMS = '"+codigo_cpms+"'");
	//whereD.put("APO_IDESTADO = '0'");
	whereD.put("TCPM_N_IDNIVELEESS = "+Integer.parseInt(nivel));
	whereD.put("(PRCD_V_CODPROCEDIMEQUIVALEN IS NULL OR PRCD_V_CODPROCEDIMEQUIVALEN = '')");
	
	data.setColumn(columns);
	data.setJoin(joinD);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	JSONObject resultado = new JSONObject();
	resultado.put("data",getData);
	if(getData.length()==0){
		resultado.put("status",false);
		resultado.put("precio",new JSONObject());
	}else{
		resultado.put("status",true);
		
		GetDataBD precio = new GetDataBD();
		precio.setModel("bdsis_tarifas.sis_tarifa_procedimiento");
		precio.setColumn("nivel_i,nivel_ii");
		
		JSONArray where = new JSONArray();
		where.put("codigoPro = '"+codigo_cpt+"'");
		
		precio.setWhere(where);
		JSONObject res = precio.getJsonData();
		
		resultado.put("precio",res); 
	}

%>
<%=resultado.toString() %>

