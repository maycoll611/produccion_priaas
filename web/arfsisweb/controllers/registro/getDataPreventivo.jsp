<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
	
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");

	DataTable datatable = new DataTable();
	datatable.setModel("m_smi");
	datatable.setColumn(columns);

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");

%>
<%=datatable.getDataJson() %>
