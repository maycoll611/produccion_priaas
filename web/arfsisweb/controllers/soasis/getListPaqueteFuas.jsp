<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@page import="org.jnp.interfaces.java.javaURLContextFactory"%>
<%@include file="/includes/validateUser.jsp" %>
<%
    FUAGenerator generator = new FUAGenerator();
    String data = "";
    int cols =  Integer.parseInt(request.getParameter("rows"));
    int pagina = Integer.parseInt(request.getParameter("page"));
    int inicia = 0;
    int limite = 0;
    int total = 0;
    
    generator.loadPaqueteSOASIS(request.getParameter("periodo"), request.getParameter("mes"), request.getParameter("ppdd"));

    total = generator.getPaquetes().size();
	if(total>0){
        inicia = cols * pagina;
        limite = (pagina + 1) * cols;
        if(limite > total){
            limite=total;
        }
	}
	for(int n=inicia;n<limite;n++){
        Paquete paq = (Paquete)generator.getPaquetes().elementAt(n);
        data = data + "{";
        
        // data = data + "\"NUMERO\": \""+paq.getSIS_PAQ_ID()+"\",";
        // data = data + "\"PAQUETE\": \""+paq.getSIS_PAQ_NOMBRE()+"\",";
        // data = data + "\"PERIODO\": \""+paq.getSIS_PAQ_PERIODO()+"\",";
        // data = data + "\"PPDD\": \""+paq.getSIS_PAQ_PPDD()+"\",";
        // data = data + "\"NUMERO\": \""+paq.getSIS_PAQ_ENVIO()+"\",";
        // data = data + "\"FECHACREA\": \""+paq.getSIS_PAQ_FECHAGEN()+"\",";
        // data = data + "\"FECHAENVIA\": \""+paq.getSIS_PAQ_FECHAENV()+"\",";
        // data = data + "\"FECHARTPA\": \""+paq.getSIS_PAQ_FECHARPT()+"\",";
        // data = data + "\"FECHAANU\": \""+paq.getSIS_PAQ_FECHAANU()+"\",";
        // data = data + "\"FECHAANU\": \""+paq.getSIS_PAQ_FECHAANU()+"\"";
        data = data + "}";
        if(n<limite-1) {
            data = data + ",";
        }
	}
    
    response.setContentType("application/json;charset=utf8"); 
%>
<%@page contentType="application/json; charset=UTF-8"%>
{
    "resultado":[
        <%=data%>
        ],
    "totalRecords":<%=generator.getFuas().size() %>,
    "page":<%=request.getParameter("page") %>,
    "rows":<%=request.getParameter("rows") %>
}


