<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%		
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String ppdd = request.getParameter("ppdd");

	DataTable datatable = new DataTable();
	datatable.setModel("openclinic_dbo.sis_atencion");
	datatable.setColumn("SIS_ATENCION_1,SIS_ATENCION_39,SIS_ATENCION_6,SIS_ATENCION_3,SIS_ATENCION_4,SIS_ATENCION_42,SIS_ATENCION_79,SIS_ATENCION_80,SIS_ATENCION_26,SIS_ATENCION_27,SIS_ATENCION_28,SIS_FUA_STATUS,SIS_FUA_SERVERID,SIS_FUA_OBJECTID,SIS_FUA_OBSERVACION");
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();

    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();
    
    relation1.put("relation", "openclinic_dbo.sis_fua");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3)");
    relation1.put("foreign_key", "sis_atencion.SIS_ATENCION_1");
    
    relations.add(0,relation1);
    
  	datatable.setRelations(relations);

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("sis_fua.SIS_FUA_STATUS  = 'sent' AND SIS_ATENCION_9='"+ppdd+"'");
	/* datatable.getDataJson(); */
	
%>
<%=datatable.getDataJson() %>


