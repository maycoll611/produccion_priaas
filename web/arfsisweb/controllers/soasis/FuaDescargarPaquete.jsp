 <%@page import="pe.gob.sis.*,net.admin.*,java.util.*,javax.xml.soap.*,org.dom4j.*,org.dom4j.io.*,java.io.*,org.json.*"%>
<%@page import="pe.gob.sis.FUAGenerator"%>
<%--@page errorPage = "testFUA_return.jsp"--%>
<%
    User activeUser = null;
    activeUser = (User)session.getAttribute("activeUser");
	FUAGenerator generator = new FUAGenerator();
	String zipfile = generator.generateFUAsforDESCARGA(request.getParameter("year1"),request.getParameter("month1"),activeUser.userid, request.getParameter("ppdd1"));
	
    if(zipfile == "0")
    {%>
        <script>
            alert("Debe configurar el WS SOASIS.");
        </script>
    <%
    }
    else {
        response.setContentType("application/octet-stream; charset=windows-1252");
        response.setHeader("Content-Disposition", "Attachment;Filename=\""+zipfile.split("/")[zipfile.split("/").length-1]+"\"");
    
        ServletOutputStream os = response.getOutputStream();
        File file = new File(zipfile);
        byte[] b = new byte[(int) file.length()];
        InputStream is = new FileInputStream(file);
        is.read(b);
        is.close();    
        for(int n=0; n<b.length; n++){
            os.write(b[n]);
        }
        os.flush();
        os.close();
    }
%>