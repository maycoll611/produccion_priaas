<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%		
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

    String periodo = request.getParameter("periodo");
    String mes = request.getParameter("mes");
    String ppdd = request.getParameter("ppdd");


	DataTable datatable = new DataTable();
	datatable.setModel("openclinic_dbo.sis_fua_paquete");
	datatable.setColumn("SIS_PAQ_ID,SIS_PAQ_PERIODO,SIS_PAQ_NOMBRE,SIS_PAQ_ENVIO,SIS_PAQ_CANTIDAD,SIS_PAQ_ESTADO,SIS_PAQ_PPDD,SIS_PAQ_FECHAGEN,SIS_PAQ_FECHAENV,SIS_PAQ_FECHARPT,SIS_PAQ_FECHAANU");

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("sis_fua_paquete.SIS_PAQ_PERIODO='"+periodo+""+mes+"' and sis_fua_paquete.SIS_PAQ_PPDD='"+ppdd+"'");
	
%>
<%=datatable.getDataJson() %>



