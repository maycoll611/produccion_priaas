<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	JSONObject data = new JSONObject();
	JSONObject resultado1 = new JSONObject();
	JSONObject resultado2 = new JSONObject();
	JSONObject resultado3 = new JSONObject();
	
	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	
	PreparedStatement sqlCountAtenciones = con.connect().prepareStatement("SELECT COUNT(*) as count FROM openclinic_dbo.sis_atencion");
	ResultSet rs1 = sqlCountAtenciones.executeQuery();
	if(rs1.next())
	{
		resultado1.put("total", rs1.getString("count"));
	}
	PreparedStatement sqlCountAtencionesDay = con.connect().prepareStatement("SELECT COUNT(*) as count FROM openclinic_dbo.sis_atencion WHERE STR_TO_DATE(SIS_ATENCION_83,'%d/%m/%Y %H:%i') >= DATE_SUB(NOW(),INTERVAL 1 DAY)");
	ResultSet rs2 = sqlCountAtencionesDay.executeQuery();
	if(rs2.next())
	{
		resultado1.put("totalDay", rs2.getString("count"));
	}
	data.put("atenciones", resultado1);
	
	PreparedStatement sqlCountAtendidos = con.connect().prepareStatement("SELECT COUNT(SIS_ATENCION_25) as count FROM openclinic_dbo.sis_atencion GROUP BY SIS_ATENCION_25");
	ResultSet rs3 = sqlCountAtendidos.executeQuery();
	int count1 = 0;
	while(rs3.next())
	{
		count1++;
	}
	resultado2.put("total", count1);
	
	PreparedStatement sqlCountAtendidosDay = con.connect().prepareStatement("SELECT COUNT(*) as count FROM openclinic_dbo.sis_atencion WHERE STR_TO_DATE(SIS_ATENCION_83,'%d/%m/%Y %H:%i') >= DATE_SUB(NOW(),INTERVAL 1 DAY) GROUP BY SIS_ATENCION_25");
	ResultSet rs4 = sqlCountAtendidosDay.executeQuery();
	int count2 = 0;
	while(rs4.next())
	{
		count2++;
	}
	resultado2.put("totalDay", count2);
	data.put("atendidos", resultado2);
	//---------------
	PreparedStatement sqlCountDiagnosticos = con.connect().prepareStatement("SELECT COUNT(*) as count FROM openclinic_dbo.sis_diagnosticos");
	ResultSet rs5 = sqlCountDiagnosticos.executeQuery();
	if(rs5.next())
	{
		resultado3.put("total", rs5.getString("count"));
	}
	PreparedStatement sqlCountDiagnosticosDay = con.connect().prepareStatement("SELECT COUNT(*) as count FROM openclinic_dbo.sis_diagnosticos AS sd JOIN openclinic_dbo.sis_atencion AS sa ON sa.SIS_ATENCION_1 = sd.SIS_DIAGNOSTICOS_1 WHERE STR_TO_DATE(SIS_ATENCION_83,'%d/%m/%Y %H:%i') >= DATE_SUB(NOW(),INTERVAL 1 DAY)");
	ResultSet rs6 = sqlCountDiagnosticosDay.executeQuery();
	if(rs6.next())
	{
		resultado3.put("totalDay", rs6.getString("count"));
	}
	data.put("diagnosticos", resultado3);
	
	// datatable
	GetDataBD datatable = new GetDataBD();
	datatable.setModel("openclinic_dbo.sis_atencion");
	datatable.setColumn("SIS_ATENCION_1,SIS_ATENCION_6,SIS_ATENCION_3,SIS_ATENCION_4,SIS_ATENCION_26,SIS_ATENCION_27,SIS_ATENCION_28,SIS_ATENCION_29,SIS_FUA_STATUS");
	
	JSONArray joinMe = new JSONArray();
	joinMe.put("join openclinic_dbo.sis_fua on SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3)=SIS_ATENCION_1 ");

	datatable.setJoin(joinMe);
	datatable.setOrder("order by SIS_ATENCION_1 desc");
	datatable.setLimit(10);
	JSONArray resData = datatable.getArrayJsonData();
	
	data.put("datatable", resData);
	
	out.print(data);
%>


