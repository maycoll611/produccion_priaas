<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>
<%
/*************************************
Nombre         : repConsumo
Proposito      : Generar reporte consumo
Creado por     : FLIZARRAGA
Fec Creacion   : 20230317
Observaciones  : Ninguna 
*************************************/
	
	int pagina = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));

	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");

	String resultado ;
	RepDiagnosticos datos = new RepDiagnosticos("sis_diagnosticos");
	datos.limit(rows);
	datos.page(pagina);

	resultado = datos.datosJSONCons(procesoOFormato,opcion,componente,periodo,year,desde,hasta,ppdd,eess,usuario,respAten);

%>
<%=resultado %>