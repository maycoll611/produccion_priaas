<%
/*************************************
Nombre        : resRegistroFiltros
Proposito     : Filtros para generación de reporte de Resumen de registro
Creado por    : FLIZARRAGA
Fec Creacion  : 20230228
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230228  FLIZARRAGA     Se creo la función de filtros para mejorar filtros de reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepResRegistro datos = new RepResRegistro();

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>