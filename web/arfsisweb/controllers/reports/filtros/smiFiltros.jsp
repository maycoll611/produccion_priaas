<%
/*************************************
Nombre        : smiFiltros
Proposito     : Filtros para generación de reporte de Servicio Materno Infantil
Creado por    : FLIZARRAGA
Fec Creacion  : 20230206
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230206  FLIZARRAGA     Se creo la función de filtros para el reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepSMI datos = new RepSMI("sis_smi");	

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>