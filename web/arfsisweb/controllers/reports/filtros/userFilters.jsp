<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*,java.sql.*,pe.gob.sis.MysqlConnect"%>

<%	
    String filtro = request.getParameter("filtro");
    String value = request.getParameter("value");    

    MysqlConnect con = new MysqlConnect("openclinic_dbo");    
    
    JSONObject data = new JSONObject();
    JSONArray arrayFiltro = new JSONArray();

    String sql = "";

    if(filtro.equals("ppdd")){
        sql = "SELECT pin_IdPPDD AS id, pin_Nombre AS ppdd"+ 
        	" FROM bdsis_maestros.m_pinstalacion"+
            " WHERE pin_IdPPDD =" + value;
    }
    else if(filtro.equals("usuario")){
        sql = "SELECT natreg AS id, CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " JOIN ocadmin_dbo.users ON ocadmin_dbo.users.personid = ocadmin_dbo.admin.personid"+
            " WHERE userid =" + value;
    }
    
    System.out.println(sql);

    PreparedStatement ps2 = con.connect().prepareStatement(sql);
    ResultSet rs2 = ps2.executeQuery();

    while(rs2.next()){
        JSONObject result = new JSONObject();
        result.put("id", rs2.getString(1));
        result.put(filtro, rs2.getString(2));

        arrayFiltro.put(result);
    }
    rs2.close();
    ps2.close();

    con.disconnect();
    
    data.put("dataFiltro", arrayFiltro);

    String resultado;
    
    resultado = data.toString();

    System.out.println(resultado);

%>
<%=resultado %>