<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*,java.sql.*,pe.gob.sis.MysqlConnect"%>

<%	
    String filtro = request.getParameter("filtro");
    String ppdd = request.getParameter("ppdd");
    String user = request.getParameter("user");    
    String eess = request.getParameter("eess");    

    MysqlConnect con = new MysqlConnect("openclinic_dbo");    
    
    JSONObject data = new JSONObject();
    JSONArray arrayPpdd = new JSONArray();
    JSONArray arrayUsuario = new JSONArray();
    JSONArray arrayEess = new JSONArray();

    String sqlPpdd = "";
    String sqlUsuario = "";
    String sqlEess = "";

    //if(filtro.equals("ppdd")){
        sqlPpdd = "SELECT pin_IdPPDD AS id, pin_Nombre AS ppdd"+ 
        	" FROM bdsis_maestros.m_pinstalacion"+
            " WHERE pin_IdPPDD =" + ppdd;
    //}
    //else if(filtro.equals("usuario")){
        sqlUsuario = "SELECT natreg AS id, CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " JOIN ocadmin_dbo.users ON ocadmin_dbo.users.personid = ocadmin_dbo.admin.personid"+
            " WHERE userid =" + user;
    //}
    sqlEess = "SELECT SUBSTRING(bdsis_maestros.m_eess.pre_CodigoRENAES,3,8) AS id, pre_Nombre AS eess"+
            " FROM bdsis_maestros.m_eess"+
            " WHERE pre_CodigoRENAES =" + eess;

    PreparedStatement ps1 = con.connect().prepareStatement(sqlPpdd);
    ResultSet rs1 = ps1.executeQuery();

    while(rs1.next()){
        JSONObject result = new JSONObject();
        result.put("id", rs1.getString(1));
        result.put("ppdd", rs1.getString(2));

        arrayPpdd.put(result);
    }
    rs1.close();
    ps1.close();

    PreparedStatement ps2 = con.connect().prepareStatement(sqlUsuario);
    ResultSet rs2 = ps2.executeQuery();

    while(rs2.next()){
        JSONObject result = new JSONObject();
        result.put("id", rs2.getString(1));
        result.put("usuario", rs2.getString(2));

        arrayUsuario.put(result);
    }
    rs2.close();
    ps2.close();


    PreparedStatement ps3 = con.connect().prepareStatement(sqlEess);
    ResultSet rs3 = ps3.executeQuery();

    while(rs3.next()){
        JSONObject result = new JSONObject();
        result.put("id", rs3.getString(1));
        result.put("eess", rs3.getString(2));

        arrayEess.put(result);
    }
    rs3.close();
    ps3.close();

    con.disconnect();
    
    data.put("ppdds", arrayPpdd);
    data.put("usuarios", arrayUsuario);
    data.put("eesss", arrayEess);

    String resultado;
    
    resultado = data.toString();

    System.out.println(resultado);

%>
<%=resultado %>