<%
/*************************************
Nombre        : medicamentosFiltros
Proposito     : Filtros para generación de reporte de medicamentos
Creado por    : FLIZARRAGA
Fec Creacion  : 20230206
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230206  FLIZARRAGA     Se creo la función de filtros para mejorar filtros de reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepMedicamentos datos = new RepMedicamentos("sis_medicamentos");	

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>