<%
/*************************************
Nombre        : jrRAServPrincAdic
Proposito     : Generación de reporte de Regitro de Atenciones - Servicio Principal y Adicional en Excel
Creado por    : FLIZARRAGA
Fec Creacion  : 20230302
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230302  FLIZARRAGA     Se creo la función para generar el reporte de Registro de Atenciones - Servicio Principal y Adicional en Excel
20230302  FLIZARRAGA	 Se modifico la funcion para generar en memoria el reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*,net.sf.jasperreports.engine.JRExporterParameter,java.io.ByteArrayOutputStream,net.sf.jasperreports.engine.export.JRXlsExporter,net.sf.jasperreports.engine.JasperReport,net.sf.jasperreports.engine.JasperRunManager,be.mxs.common.util.db.MedwanQuery,java.io.OutputStream,net.sf.jasperreports.engine.JasperExportManager,net.sf.jasperreports.engine.JREmptyDataSource,net.sf.jasperreports.engine.JasperPrint,net.sf.jasperreports.engine.JasperFillManager,net.sf.jasperreports.engine.JasperCompileManager,java.time.Period,java.time.format.DateTimeFormatter,java.time.LocalDate,pe.gob.sis.MysqlConnect,net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,java.io.InputStream,java.io.FileInputStream,java.util.HashMap,java.util.List,java.util.Map,java.util.ArrayList,java.sql.ResultSet,java.sql.PreparedStatement,pe.gob.sis.arfsisweb.reports.jasperreports.RAServPrincAdicModel"%>

<%	
	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");
	String disa = request.getParameter("disa");
	String odsis = request.getParameter("odsis");
	 
	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	
	JSONArray array = new JSONArray();

	String filtrosSql = "";
		
        if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
            if(procesoOFormato.equals("1")) {
                if(opcion.equals("1")){
                    filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";                     
                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            } else if(procesoOFormato.equals("2")) {
                if(opcion.equals("1")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  

                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            }
            if(!componente.equals("3") && !componente.equals("")) {
            	filtrosSql = filtrosSql +" AND SIS_ATENCION_15 = '"+componente+"'";
            }
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
        	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
        	if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("") && ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        	if(!eess.equals("") && !ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }

		String sql = "SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS nro_formato, SIS_ATENCION_73 AS resp_dni, SUBSTRING(SIS_ATENCION_39,1,10) AS fecha, CONCAT(SIS_ATENCION_26,' ',SIS_ATENCION_27,' ',SIS_ATENCION_28,' ',SIS_ATENCION_29) AS beneficiario,"+
                    " SIS_ATENCION_30 AS fec_nac, SIS_ATENCION_5 AS edad_benef, SIS_ATENCION_31 AS sexo_benef, pre_Nombre AS eess,"+
                    " ser_IdServicio AS id_servicio, 'Princ' AS tipo_servicio, tps_Descripcion AS tipo_prof, CONCAT(pers_PriNombre,' ',pers_OtrNombre,' ',pers_ApePaterno,' ',pers_ApeMaterno) AS personal_salud,"+
                    " iseg_Tarifario AS tarifa, SIS_SMI_3 AS nro_cred, SIS_ATENCION_87 AS valorizacion"+
                    " FROM sis_atencion"+
                    " LEFT JOIN bdsis_maestros.m_servicios ON openclinic_dbo.sis_atencion.SIS_ATENCION_42 = bdsis_maestros.m_servicios.ser_IdServicio"+
                    " LEFT JOIN bdsis_maestros.m_eess ON openclinic_dbo.sis_atencion.SIS_ATENCION_6 = SUBSTRING(bdsis_maestros.m_eess.pre_CodigoRENAES,3,8)"+
                    " LEFT JOIN bdsis_maestros.a_tipopersonalsalud ON openclinic_dbo.sis_atencion.SIS_ATENCION_74 = bdsis_maestros.a_tipopersonalsalud.tps_IdTipoPersonalSalud"+
                    " LEFT JOIN bdsis_maestros.a_resatencion ON openclinic_dbo.sis_atencion.SIS_ATENCION_73 = bdsis_maestros.a_resatencion.pers_IdResAtencion"+
                    " LEFT JOIN bdsis_maestros.a_institucion ON openclinic_dbo.sis_atencion.SIS_ATENCION_5 = bdsis_maestros.a_institucion.iseg_idOdsis"+
                    " LEFT JOIN openclinic_dbo.sis_smi ON (openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_smi.SIS_SMI_1) AND SIS_SMI_2 = '120' "+
                    filtrosSql+
                    " UNION"+
                    " SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS nro_formato, SIS_ATENCION_73 AS resp_dni, SUBSTRING(SIS_ATENCION_39,1,10) AS fecha, CONCAT(SIS_ATENCION_26,' ',SIS_ATENCION_27,' ',SIS_ATENCION_28,' ',SIS_ATENCION_29) AS beneficiario,"+
                    " SIS_ATENCION_30 AS fec_nac, SIS_ATENCION_5 AS edad_benef, SIS_ATENCION_31 AS sexo_benef, pre_Nombre AS eess,"+
                    " SIS_SERVICIOSADICIONALES_2 AS id_servicio, 'Adic' AS tipo_servicio, tps_Descripcion AS tipo_prof, CONCAT(pers_PriNombre,' ',pers_OtrNombre,' ',pers_ApePaterno,' ',pers_ApeMaterno) AS personal_salud,"+
                    " iseg_Tarifario AS tarifa, SIS_SMI_3 AS nro_cred, SIS_ATENCION_87 AS valorizacion"+
                    " FROM sis_serviciosadicionales"+
                    " LEFT JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_serviciosadicionales.SIS_SERVICIOSADICIONALES_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1"+
                    " LEFT JOIN bdsis_maestros.m_servicios ON openclinic_dbo.sis_atencion.SIS_ATENCION_42 = bdsis_maestros.m_servicios.ser_IdServicio"+
                    " LEFT JOIN bdsis_maestros.m_eess ON openclinic_dbo.sis_atencion.SIS_ATENCION_6 = SUBSTRING(bdsis_maestros.m_eess.pre_CodigoRENAES,3,8)"+
                    " LEFT JOIN bdsis_maestros.a_tipopersonalsalud ON openclinic_dbo.sis_atencion.SIS_ATENCION_74 = bdsis_maestros.a_tipopersonalsalud.tps_IdTipoPersonalSalud"+
                    " LEFT JOIN bdsis_maestros.a_resatencion ON openclinic_dbo.sis_atencion.SIS_ATENCION_73 = bdsis_maestros.a_resatencion.pers_IdResAtencion"+
                    " LEFT JOIN bdsis_maestros.a_institucion ON openclinic_dbo.sis_atencion.SIS_ATENCION_5 = bdsis_maestros.a_institucion.iseg_idOdsis"+
                    " LEFT JOIN openclinic_dbo.sis_smi ON (openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_smi.SIS_SMI_1) AND SIS_SMI_2 = '120' "+
                    filtrosSql+
                    " ORDER BY nro_formato, tipo_servicio DESC";

        PreparedStatement ps = con.connect().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        List<RAServPrincAdicModel> listItems = new ArrayList<RAServPrincAdicModel>();
        
        Integer totalReg = 0;

        while(rs.next()){
                            
            RAServPrincAdicModel ra = new RAServPrincAdicModel();
            ra.setNroFormato(rs.getString("nro_formato"));
            ra.setRespDni(rs.getString("resp_dni"));
            ra.setFecha(rs.getString("fecha"));
            ra.setBeneficiario(rs.getString("beneficiario"));
            ra.setFecNac(rs.getString("fec_nac"));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate fecha_nacimiento = LocalDate.parse(rs.getString("fec_nac"),formatter);
			Period edad = Period.between(fecha_nacimiento,LocalDate.now());
            ra.setEdadBenef(Integer.toString(edad.getYears()));
            ra.setSexoBenef(rs.getString("sexo_benef"));
            ra.setEess(rs.getString("eess"));
            ra.setIdServicio(rs.getString("id_servicio"));
            ra.setTipoServicio(rs.getString("tipo_servicio"));
            ra.setTipoProf(rs.getString("tipo_prof"));
            ra.setPersonalSalud(rs.getString("personal_salud"));
            ra.setTarifa(rs.getString("tarifa"));
            if(rs.getString("nro_cred") == null){
            	ra.setNroCred(" ");
            } else {
            	ra.setNroCred(rs.getString("nro_cred"));
            }
            ra.setValorizacion(rs.getString("valorizacion"));
            
            totalReg +=1;

            listItems.add(ra);
        }
        
		rs.close();
	    ps.close();

		String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " WHERE natreg ="+usuario;
		JSONArray arrayUser = new JSONArray();        
        if(!usuario.equals("")) {
        	PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
            ResultSet rsUser = psUser.executeQuery();
            while(rsUser.next()){
                JSONObject result = new JSONObject();
                result.put("usuario",rsUser.getString(1));
                arrayUser.put(result);
            }
            rsUser.close();
            psUser.close();
        }

	    con.disconnect();

        JRBeanCollectionDataSource dsraspa = new JRBeanCollectionDataSource(listItems);
        
        String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");
                
        Map<String, Object> parameters = new HashMap<String, Object>();
        
        if(!disa.equals("")) {
        	parameters.put("disa", disa);
        } else {
        	parameters.put("disa", " ");
        }
        if(!odsis.equals("")) {
        	parameters.put("odsis", odsis);
        } else {
        	parameters.put("odsis", " ");
        }
        if(!usuario.equals("")) {
        	parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
        } else {
        	parameters.put("usuario", " ");
        }
        parameters.put("subtitle", " ");
        parameters.put("totalReg", totalReg.toString());
        parameters.put("dsraspa", dsraspa);

        JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/RAServPrincAdic.jrxml");

        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

        byte[] bytes = new byte[10000];
        
        JRXlsExporter exporter = new JRXlsExporter();
        
        ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
        
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
        exporter.exportReport();
        
        bytes = xlsReport.toByteArray();
        
        response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",  "inline; filename=RAServPrincAdic.xls");
        response.setContentLength(bytes.length);
        xlsReport.close();
        
        OutputStream output = response.getOutputStream();
        output.write(bytes,0,bytes.length);
        output.flush();
        output.close();
%>
