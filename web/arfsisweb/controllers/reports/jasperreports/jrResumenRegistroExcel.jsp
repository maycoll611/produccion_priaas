<%
/*************************************
Nombre        : jrResumenRegistro
Proposito     : Generación de reporte de Resumen de Registro en Excel
Creado por    : FLIZARRAGA
Fec Creacion  : 20230228
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230228  FLIZARRAGA     Se creo la función para generar el reporte de Resumen de Registro en Excel
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*,net.sf.jasperreports.engine.JRExporterParameter,java.io.ByteArrayOutputStream,net.sf.jasperreports.engine.export.JRXlsExporter,net.sf.jasperreports.engine.JasperReport,net.sf.jasperreports.engine.JasperRunManager,be.mxs.common.util.db.MedwanQuery,java.io.OutputStream,net.sf.jasperreports.engine.JasperExportManager,net.sf.jasperreports.engine.JREmptyDataSource,net.sf.jasperreports.engine.JasperPrint,net.sf.jasperreports.engine.JasperFillManager,net.sf.jasperreports.engine.JasperCompileManager,java.time.Period,java.time.format.DateTimeFormatter,java.time.LocalDate,pe.gob.sis.MysqlConnect,net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,java.io.InputStream,java.io.FileInputStream,java.util.HashMap,java.util.List,java.util.Map,java.util.ArrayList,java.sql.ResultSet,java.sql.PreparedStatement,pe.gob.sis.arfsisweb.reports.jasperreports.ResumenRegistroModel"%>

<%	
	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");
	String disa = request.getParameter("disa");
	String odsis = request.getParameter("odsis");

    MysqlConnect con = new MysqlConnect("openclinic_dbo");

    JSONArray array = new JSONArray();
    JSONArray arrayPrestaciones = new JSONArray();
    JSONArray arraySMIs = new JSONArray();
    JSONArray arrayDiagnosticos = new JSONArray();
    JSONArray arrayMedEntr = new JSONArray();
    JSONArray arrayInsEntr = new JSONArray();
    JSONArray arrayProcEntr = new JSONArray();

    String sqlPrestaciones = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac"+
                     " FROM sis_atencion";

        String sqlSMIs = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac"+
                     " FROM sis_smi"+
                     " JOIN bdsis_maestros.m_smi ON openclinic_dbo.sis_smi.SIS_SMI_2 = bdsis_maestros.m_smi.smi_IdSmi"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_smi.SIS_SMI_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

        String sqlDiagnosticos = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac"+
                     " FROM sis_diagnosticos"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_diagnosticos.SIS_DIAGNOSTICOS_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1 AND SIS_ATENCION_VERSION = SIS_DIAGNOSTICOS_VERSION";

        String sqlMedEntr = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac,"+
                     " SIS_MEDICAMENTOS_5 AS cant_entr"+
                     " FROM sis_medicamentos"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_medicamentos.SIS_MEDICAMENTOS_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

        String sqlInsEntr = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac,"+
                     " SIS_INSUMOS_5 AS cant_entr"+
                     " FROM sis_insumos"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_insumos.SIS_INSUMOS_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

        String sqlProcEntr = "SELECT"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac,"+
                     " SIS_PROCEDIMIENTOS_6 AS cant_entr"+
                     " FROM sis_procedimientos"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_procedimientos.SIS_PROCEDIMIENTOS_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

        String filtrosSql = "";

        if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
            if(procesoOFormato.equals("1")) {
                if(opcion.equals("1")){
                    filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";                     
                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            } else if(procesoOFormato.equals("2")) {
                if(opcion.equals("1")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  

                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            }
            if(!componente.equals("3") && !componente.equals("")) {
            	filtrosSql = filtrosSql +" AND SIS_ATENCION_15 = '"+componente+"'";
            }
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
        	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
        	if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("") && ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        	if(!eess.equals("") && !ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }     

        sqlPrestaciones = sqlPrestaciones + filtrosSql;
        sqlSMIs = sqlSMIs + filtrosSql;
        sqlDiagnosticos = sqlDiagnosticos + filtrosSql;
        sqlMedEntr = sqlMedEntr + filtrosSql;
        sqlInsEntr = sqlInsEntr + filtrosSql;
        sqlProcEntr = sqlProcEntr + filtrosSql;

        // PRESTACIONES

        PreparedStatement psPrestaciones = con.connect().prepareStatement(sqlPrestaciones);
        ResultSet rsPrestaciones = psPrestaciones.executeQuery();
        while(rsPrestaciones.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsPrestaciones.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsPrestaciones.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
            }
            arrayPrestaciones.put(result);
        }
        rsPrestaciones.close();
        psPrestaciones.close();

        // SMI

        PreparedStatement psSMI = con.connect().prepareStatement(sqlSMIs);
        ResultSet rsSMI = psSMI.executeQuery();
        while(rsSMI.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsSMI.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsSMI.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
            }
            arraySMIs.put(result);
        }
        rsSMI.close();
        psSMI.close();

        // Diagnosticos

        PreparedStatement psDiagnosticos = con.connect().prepareStatement(sqlDiagnosticos);
        ResultSet rsDiagnosticos = psDiagnosticos.executeQuery();
        while(rsDiagnosticos.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsDiagnosticos.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsDiagnosticos.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
            }
            arrayDiagnosticos.put(result);
        }
        rsDiagnosticos.close();
        psDiagnosticos.close();

        // MEDICAMENTOS ENTREGADOS

        PreparedStatement psME = con.connect().prepareStatement(sqlMedEntr);
        ResultSet rsME = psME.executeQuery();
        while(rsME.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsME.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsME.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
                else if(i==3){
                    result.put("cant_entr",rsME.getString(i));
                }
            }
            arrayMedEntr.put(result);
        }
        rsME.close();
        psME.close();

        // INSUMOS ENTREGADOS

        PreparedStatement psIE = con.connect().prepareStatement(sqlInsEntr);
        ResultSet rsIE = psIE.executeQuery();
        while(rsIE.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsIE.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsIE.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
                else if(i==3){
                    result.put("cant_entr",rsIE.getString(i));
                }
            }
            arrayInsEntr.put(result);
        }
        rsIE.close();
        psIE.close();

        // PROCEDIMIENTOS ENTREGADOS

        PreparedStatement psPE = con.connect().prepareStatement(sqlProcEntr);
        ResultSet rsPE = psPE.executeQuery();
        while(rsPE.next()){
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1){
                if(i==1){                    
                    result.put("gestante",rsPE.getString(i));
                }
                else if(i==2){
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rsPE.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
                    result.put("edad",edad.getYears());
                }
                else if(i==3){
                    result.put("cant_entr",rsPE.getString(i));
                }
            }
            arrayProcEntr.put(result);
        }
        rsPE.close();
        psPE.close();

		String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " WHERE natreg ="+usuario;
		JSONArray arrayUser = new JSONArray();        
        if(!usuario.equals("")) {
        	PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
            ResultSet rsUser = psUser.executeQuery();
            while(rsUser.next()){
                JSONObject result = new JSONObject();
                result.put("usuario",rsUser.getString(1));
                arrayUser.put(result);
            }
            rsUser.close();
            psUser.close();
        }

        con.disconnect();

        List<ResumenRegistroModel> listItems = new ArrayList<ResumenRegistroModel>();

        int ge1Total = 0;
        int ge2Total = 0;
        int ge3Total = 0;
        int ge4Total = 0;
        int ge5Total = 0;
        int gestanteTotal = 0;
        int totalG = 0;

        // PRESTACIONES

        if(arrayPrestaciones.length()>0){
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arrayPrestaciones.length();i++){
                total+=1;
                if(arrayPrestaciones.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arrayPrestaciones.getJSONObject(i).getInt("edad")>=0 && arrayPrestaciones.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arrayPrestaciones.getJSONObject(i).getInt("edad")>=12 && arrayPrestaciones.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arrayPrestaciones.getJSONObject(i).getInt("edad")>=18 && arrayPrestaciones.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arrayPrestaciones.getJSONObject(i).getInt("edad")>=30 && arrayPrestaciones.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arrayPrestaciones.getJSONObject(i).getInt("edad")>=60 && arrayPrestaciones.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Prestaciones");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        // SMI

        if(arraySMIs.length()>0){
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arraySMIs.length();i++){
                total+=1;
                if(arraySMIs.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arraySMIs.getJSONObject(i).getInt("edad")>=0 && arraySMIs.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arraySMIs.getJSONObject(i).getInt("edad")>=12 && arraySMIs.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arraySMIs.getJSONObject(i).getInt("edad")>=18 && arraySMIs.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arraySMIs.getJSONObject(i).getInt("edad")>=30 && arraySMIs.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arraySMIs.getJSONObject(i).getInt("edad")>=60 && arraySMIs.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Servicios Materno Infantiles");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        // DIAGNOSTICOS

        if(arrayDiagnosticos.length()>0){
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arrayDiagnosticos.length();i++){
                total+=1;
                if(arrayDiagnosticos.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arrayDiagnosticos.getJSONObject(i).getInt("edad")>=0 && arrayDiagnosticos.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arrayDiagnosticos.getJSONObject(i).getInt("edad")>=12 && arrayDiagnosticos.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arrayDiagnosticos.getJSONObject(i).getInt("edad")>=18 && arrayDiagnosticos.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arrayDiagnosticos.getJSONObject(i).getInt("edad")>=30 && arrayDiagnosticos.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arrayDiagnosticos.getJSONObject(i).getInt("edad")>=60 && arrayDiagnosticos.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Diagnósticos");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        // MEDICAMENTOS ENTREGADOS

        if(arrayMedEntr.length()>0){
            JSONObject result = new JSONObject();
            result.put("registro","Medicamentos Entregados");
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arrayMedEntr.length();i++){
                total+=1;
                if(arrayMedEntr.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arrayMedEntr.getJSONObject(i).getInt("edad")>=0 && arrayMedEntr.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arrayMedEntr.getJSONObject(i).getInt("edad")>=12 && arrayMedEntr.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arrayMedEntr.getJSONObject(i).getInt("edad")>=18 && arrayMedEntr.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arrayMedEntr.getJSONObject(i).getInt("edad")>=30 && arrayMedEntr.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arrayMedEntr.getJSONObject(i).getInt("edad")>=60 && arrayMedEntr.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Medicamentos Entregados");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        // INSUMOS ENTREGADOS

        if(arrayInsEntr.length()>0){
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arrayInsEntr.length();i++){
                total+=1;
                if(arrayInsEntr.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arrayInsEntr.getJSONObject(i).getInt("edad")>=0 && arrayInsEntr.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arrayInsEntr.getJSONObject(i).getInt("edad")>=12 && arrayInsEntr.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arrayInsEntr.getJSONObject(i).getInt("edad")>=18 && arrayInsEntr.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arrayInsEntr.getJSONObject(i).getInt("edad")>=30 && arrayInsEntr.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arrayInsEntr.getJSONObject(i).getInt("edad")>=60 && arrayInsEntr.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Insumos Entregados");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        // PROCEDIMIENTOS REALIZADOS

        if(arrayProcEntr.length()>0){
            JSONObject result = new JSONObject();
            result.put("registro","Procedimientos Realizados");
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<arrayProcEntr.length();i++){
                total+=1;
                if(arrayProcEntr.getJSONObject(i).getString("gestante").equals("1")){
                    gestante+=1;
                }
                if(arrayProcEntr.getJSONObject(i).getInt("edad")>=0 && arrayProcEntr.getJSONObject(i).getInt("edad")<=11){                       
                    ge1+=1;
                } else if(arrayProcEntr.getJSONObject(i).getInt("edad")>=12 && arrayProcEntr.getJSONObject(i).getInt("edad")<=17){
                    ge2 += 1;
                } else if(arrayProcEntr.getJSONObject(i).getInt("edad")>=18 && arrayProcEntr.getJSONObject(i).getInt("edad")<=29){
                    ge3 += 1;
                } else if(arrayProcEntr.getJSONObject(i).getInt("edad")>=30 && arrayProcEntr.getJSONObject(i).getInt("edad")<=59){
                    ge4 += 1;
                } else if(arrayProcEntr.getJSONObject(i).getInt("edad")>=60 && arrayProcEntr.getJSONObject(i).getInt("edad")<=120){
                    ge5 += 1;
                }
            }
            totalG+=total;
            ge1Total+=ge1;
            ge2Total+=ge2;
            ge3Total+=ge3;
            ge4Total+=ge4;
            ge5Total+=ge5;
            gestanteTotal+=gestante;

            ResumenRegistroModel resRegistro = new ResumenRegistroModel();
            resRegistro.setRegistro("Procedimientos Realizados");
            resRegistro.setGe1(ge1);
            resRegistro.setGe2(ge2);
            resRegistro.setGe3(ge3);
            resRegistro.setGe4(ge4);
            resRegistro.setGe5(ge5);
            resRegistro.setGestante(gestante);
            resRegistro.setTotal(total);

            listItems.add(resRegistro);
        }

        ResumenRegistroModel resRegistro = new ResumenRegistroModel();
        resRegistro.setRegistro("Total");
        resRegistro.setGe1(ge1Total);
        resRegistro.setGe2(ge2Total);
        resRegistro.setGe3(ge3Total);
        resRegistro.setGe4(ge4Total);
        resRegistro.setGe5(ge5Total);
        resRegistro.setGestante(gestanteTotal);
        resRegistro.setTotal(totalG);

        listItems.add(resRegistro);

    JRBeanCollectionDataSource dsrr = new JRBeanCollectionDataSource(listItems);
        
    String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");
        
    Map<String, Object> parameters = new HashMap<String, Object>();

    if(!disa.equals("")) {
        parameters.put("disa", disa);
    } else {
        parameters.put("disa", " ");
    }
    if(!odsis.equals("")) {
        parameters.put("odsis", odsis);
    } else {
        parameters.put("odsis", " ");
    }
    if(!usuario.equals("")) {
        parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
    } else {
        parameters.put("usuario", " ");
    }
    if(!ppdd.equals("")) {
    	parameters.put("ppdd", ppdd);
    } else {
    	parameters.put("ppdd", " ");
    }
    if(!eess.equals("")) {
    	parameters.put("eess", eess);
    } else {
    	parameters.put("eess", " ");
    }
    parameters.put("subtitle", " ");
    parameters.put("dsrr", dsrr);

    JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/ResumenRegistro.jrxml");
		
    JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

    byte[] bytes = new byte[10000];
    
    JRXlsExporter exporter = new JRXlsExporter();
    
    ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
    
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
    exporter.exportReport();
    
    bytes = xlsReport.toByteArray();
    
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition",  "inline; filename=ResumenRegistro.xls");
    response.setContentLength(bytes.length);
    xlsReport.close();
    
    OutputStream output = response.getOutputStream();
    output.write(bytes,0,bytes.length);
    output.flush();
    output.close();
    
%>