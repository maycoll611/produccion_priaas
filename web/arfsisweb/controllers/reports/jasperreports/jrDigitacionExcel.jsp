<%@page import="pe.gob.sis.arfsisweb.reports.*,
                pe.gob.sis.MysqlConnect,
                pe.gob.sis.arfsisweb.reports.jasperreports.*,
                org.json.*,
                net.sf.jasperreports.engine.*,
                net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,
                net.sf.jasperreports.engine.export.JRXlsExporter,
                be.mxs.common.util.db.MedwanQuery,
                java.io.*,
                java.time.*,
                java.time.format.DateTimeFormatter,
                java.util.*,
                java.sql.*"%>
<%	
	String mesOanio = request.getParameter("mesOanio");
	String mes = request.getParameter("mes");
	String anio = request.getParameter("anio");
	// String ppdd = request.getParameter("ppdd");

    MysqlConnect con = new MysqlConnect("openclinic_dbo");

    JSONArray array = new JSONArray();
    JSONArray arrayDigitador = new JSONArray();
	
	String sql = "SELECT SIS_ATENCION_82 AS dni, CONCAT(firstname,' ',lastname) AS digitador, SIS_FUA_STATUS AS estado, CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) AS fecha"
                    +" FROM sis_atencion"
                    +" JOIN ocadmin_dbo.admin ON openclinic_dbo.sis_atencion.SIS_ATENCION_82 = ocadmin_dbo.admin.natreg"
                    +" LEFT JOIN openclinic_dbo.sis_fua ON SIS_ATENCION_1 = SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3)";

    String sqlDigitador = "SELECT DISTINCT(SIS_ATENCION_82) AS dni, CONCAT(firstname,' ',lastname) AS digitador"
                    +" FROM sis_atencion"
                    +" JOIN ocadmin_dbo.admin ON openclinic_dbo.sis_atencion.SIS_ATENCION_82 = ocadmin_dbo.admin.natreg"
                    +" LEFT JOIN openclinic_dbo.sis_fua ON SIS_ATENCION_1 = SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3)";

    String filtrosSql = "";

    if(mesOanio.equals("1")){
        filtrosSql = "WHERE SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4) = '"+anio+"'";
        //if(!ppdd.equals("")) {
        //    filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        //}
    }
    else if(mesOanio.equals("2")){
        filtrosSql = "WHERE SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4) = '"+anio+"'";
        filtrosSql = filtrosSql + " AND SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2) = '"+mes+"'";
        //if(!ppdd.equals("")) {
        //    filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        //}
    }
            
    sql = sql + filtrosSql;
    sqlDigitador = sqlDigitador + filtrosSql;

	PreparedStatement ps = con.connect().prepareStatement(sql);
    ResultSet rs = ps.executeQuery();

    PreparedStatement ps2 = con.connect().prepareStatement(sqlDigitador);
    ResultSet rs2 = ps2.executeQuery();

    while(rs.next()){        	
        JSONObject result = new JSONObject();
        result.put("dni", rs.getString(1));
        result.put("digitador", rs.getString(2));
        result.put("estado",rs.getString(3));
        result.put("fecha",rs.getString(4));
        
        array.put(result);
    }

    while(rs2.next()){
        JSONObject result = new JSONObject();
        result.put("dni",rs2.getString(1));
        result.put("digitador",rs2.getString(2));
        arrayDigitador.put(result);
    }
    rs2.close();
    ps2.close();

    rs.close();
    ps.close();

		// String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
        //    " FROM ocadmin_dbo.admin"+
        //    " WHERE natreg ="+usuario;
		// JSONArray arrayUser = new JSONArray();        
        // if(!usuario.equals("")) {
        //	PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
        //    ResultSet rsUser = psUser.executeQuery();
        //    while(rsUser.next()){
        //        JSONObject result = new JSONObject();
        //        result.put("usuario",rsUser.getString(1));
        //        arrayUser.put(result);
        //    }
        //    rsUser.close();
        //    psUser.close();
        // }
	    con.disconnect();
        
        List<DigitacionModel> listItems = new ArrayList<DigitacionModel>();

        Integer totalReg = 0;
        
        System.out.println(arrayDigitador.length());

        if(array.length()>0){
            for(int j=0;j<arrayDigitador.length();j++){
                int d01 = 0;
                int d02 = 0;
                int d03 = 0;
                int d04 = 0;
                int d05 = 0;
                int d06 = 0;
                int d07 = 0;
                int d08 = 0;
                int d09 = 0;
                int d10 = 0;
                int d11 = 0;
                int d12 = 0;
                int d13 = 0;
                int d14 = 0;
                int d15 = 0;
                int d16 = 0;
                int d17 = 0;
                int d18 = 0;
                int d19 = 0;
                int d20 = 0;
                int d21 = 0;
                int d22 = 0;
                int d23 = 0;
                int d24 = 0;
                int d25 = 0;
                int d26 = 0;
                int d27 = 0;
                int d28 = 0;
                int d29 = 0;
                int d30 = 0;
                int d31 = 0;
                int fAbiertos = 0;
                int fCerrados = 0;
                int tFUAs = 0;
                for(int i=0;i<array.length();i++){
                    if(array.getJSONObject(i).getString("dni").equals(arrayDigitador.getJSONObject(j).getString("dni"))){
                        tFUAs += 1;
                        if(array.getJSONObject(i).getString("estado").equals("open")){
                            fAbiertos += 1;
                        } else if(array.getJSONObject(i).getString("estado").equals("closed")){
                            fCerrados += 1;
                        }
                        if(mesOanio.equals("2")){
                            switch(array.getJSONObject(i).getString("fecha").substring(8,10)){
                                case "01":{
                                    d01 += 1;
                                    break;
                                }
                                case "02":{
                                    d02 += 1;
                                    break;
                                }
                                case "03":{
                                    d03 += 1;
                                    break;
                                }
                                case "04":{
                                    d04 += 1;
                                    break;
                                }
                                case "05":{
                                    d05 += 1;
                                    break;
                                }
                                case "06":{
                                    d06 += 1;
                                    break;
                                }
                                case "07":{
                                    d07 += 1;
                                    break;
                                }
                                case "08":{
                                    d08 += 1;
                                    break;
                                }
                                case "09":{
                                    d09 += 1;
                                    break;
                                }
                                case "10":{
                                    d10 += 1;
                                    break;
                                }
                                case "11":{
                                    d11 += 1;
                                    break;
                                }
                                case "12":{
                                    d12 += 1;
                                    break;
                                }
                                case "13":{
                                    d13 += 1;
                                    break;
                                }
                                case "14":{
                                    d14 += 1;
                                    break;
                                }
                                case "15":{
                                    d15 += 1;
                                    break;
                                }
                                case "16":{
                                    d16 += 1;
                                    break;
                                }
                                case "17":{
                                    d17 += 1;
                                    break;
                                }
                                case "18":{
                                    d18 += 1;
                                    break;
                                }
                                case "19":{
                                    d19 += 1;
                                    break;
                                }
                                case "20":{
                                    d20 += 1;
                                    break;
                                }
                                case "21":{
                                    d21 += 1;
                                    break;
                                }
                                case "22":{
                                    d22 += 1;
                                    break;
                                }
                                case "23":{
                                    d23 += 1;
                                    break;
                                }
                                case "24":{
                                    d24 += 1;
                                    break;
                                }
                                case "25":{
                                    d25 += 1;
                                    break;
                                }
                                case "26":{
                                    d26 += 1;
                                    break;
                                }
                                case "27":{
                                    d27 += 1;
                                    break;
                                }
                                case "28":{
                                    d28 += 1;
                                    break;
                                }
                                case "29":{
                                    d29 += 1;
                                    break;
                                }
                                case "30":{
                                    d30 += 1;
                                    break;
                                }
                                case "31":{
                                    d31 += 1;
                                    break;
                                } 
                            }
                        } else if(mesOanio.equals("1")){
                            switch(array.getJSONObject(i).getString("fecha").substring(5,7)){
                                case "01":{
                                    d01 += 1;
                                    break;
                                }
                                case "02":{
                                    d02 += 1;
                                    break;
                                }
                                case "03":{
                                    d03 += 1;
                                    break;
                                }
                                case "04":{
                                    d04 += 1;
                                    break;
                                }
                                case "05":{
                                    d05 += 1;
                                    break;
                                }
                                case "06":{
                                    d06 += 1;
                                    break;
                                }
                                case "07":{
                                    d07 += 1;
                                    break;
                                }
                                case "08":{
                                    d08 += 1;
                                    break;
                                }
                                case "09":{
                                    d09 += 1;
                                    break;
                                }
                                case "10":{
                                    d10 += 1;
                                    break;
                                }
                                case "11":{
                                    d11 += 1;
                                    break;
                                }
                                case "12":{
                                    d12 += 1;
                                    break;
                                }
                            }
                        }
                        
                    }
                }

                System.out.println(fAbiertos);

                DigitacionModel digitacion = new DigitacionModel();
                digitacion.setDni(arrayDigitador.getJSONObject(j).getString("dni"));
                digitacion.setDigitador(arrayDigitador.getJSONObject(j).getString("digitador"));
                digitacion.setFabiertos(fAbiertos);
                digitacion.setFcerrados(fCerrados);
                digitacion.setTfuas(tFUAs);
                digitacion.setD01(d01);
                digitacion.setD02(d02);
                digitacion.setD03(d03);
                digitacion.setD04(d04);
                digitacion.setD05(d05);
                digitacion.setD06(d06);
                digitacion.setD07(d07);
                digitacion.setD08(d08);
                digitacion.setD09(d09);
                digitacion.setD10(d10);
                digitacion.setD11(d11);
                digitacion.setD12(d12);
                digitacion.setD13(d13);
                digitacion.setD14(d14);
                digitacion.setD15(d15);
                digitacion.setD16(d16);
                digitacion.setD17(d17);
                digitacion.setD18(d18);
                digitacion.setD19(d19);
                digitacion.setD20(d20);
                digitacion.setD21(d21);
                digitacion.setD22(d22);
                digitacion.setD23(d23);
                digitacion.setD24(d24);
                digitacion.setD25(d25);
                digitacion.setD26(d26);
                digitacion.setD27(d27);
                digitacion.setD28(d28);
                digitacion.setD29(d29);
                digitacion.setD30(d30);
                digitacion.setD31(d31);

                totalReg +=1;

                listItems.add(digitacion);
            }
        }
        System.out.println(listItems);

        JRBeanCollectionDataSource dsrdig = new JRBeanCollectionDataSource(listItems);
        
        String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");
        
        Map<String, Object> parameters = new HashMap<String, Object>();

        // if(!usuario.equals("")) {
        //	parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
        //} else {
        //	parameters.put("usuario", " ");
        //}
        parameters.put("subtitle", " ");
        parameters.put("totalReg", totalReg.toString());
        parameters.put("dsrdig", dsrdig);

        if(mesOanio.equals("2")){
            parameters.put("subtitle", anio + "-"+ mes);
            JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/RepDigitacionMensual.jrxml");

            JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

            byte[] bytes = new byte[10000];
            
            JRXlsExporter exporter = new JRXlsExporter();
            
            ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
            
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
            exporter.exportReport();
            
            bytes = xlsReport.toByteArray();
            
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition",  "inline; filename=DigitacionMensual.xls");
            response.setContentLength(bytes.length);
            xlsReport.close();
            
            OutputStream output = response.getOutputStream();
            output.write(bytes,0,bytes.length);
            output.flush();
            output.close();
        }
        else if(mesOanio.equals("1")){
            parameters.put("subtitle", anio);
            JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/RepDigitacionAnual.jrxml");

            JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

            byte[] bytes = new byte[10000];
            
            JRXlsExporter exporter = new JRXlsExporter();
            
            ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
            
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
            exporter.exportReport();
            
            bytes = xlsReport.toByteArray();
            
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition",  "inline; filename=DigitacionAnual.xls");
            response.setContentLength(bytes.length);
            xlsReport.close();
            
            OutputStream output = response.getOutputStream();
            output.write(bytes,0,bytes.length);
            output.flush();
            output.close();
        }

%>