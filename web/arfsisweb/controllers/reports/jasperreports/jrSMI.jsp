<%
/*************************************
Nombre        : jrSMI
Proposito     : Generación de reporte de SMI en PDF
Creado por    : FLIZARRAGA
Fec Creacion  : 20230206
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230206  FLIZARRAGA     Se creo la función para generar el reporte de SMI en PDF
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,
                pe.gob.sis.MysqlConnect,
                pe.gob.sis.arfsisweb.reports.jasperreports.*,
                org.json.*,
                net.sf.jasperreports.engine.*,
                net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,
                net.sf.jasperreports.engine.export.JRXlsExporter,
                be.mxs.common.util.db.MedwanQuery,
                java.io.*,
                java.time.*,
                java.time.format.DateTimeFormatter,
                java.util.*,
                java.sql.*"%>

<%	
	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");
	String disa = request.getParameter("disa");
	String odsis = request.getParameter("odsis");

    String tipoArchivo = request.getParameter("tipoArchivo");
	
    MysqlConnect con = new MysqlConnect("openclinic_dbo");

	JSONArray array = new JSONArray();
	JSONArray arraySMI = new JSONArray();

	String sql = "SELECT CONCAT(smi_IdSmi,' ',smi_Descripcion) AS smi,"+
                     " SIS_ATENCION_35 AS gestante,"+
                     " SIS_ATENCION_30 AS fec_nac"+
                     " FROM sis_smi"+
                     " JOIN bdsis_maestros.m_smi ON openclinic_dbo.sis_smi.SIS_SMI_2 = bdsis_maestros.m_smi.smi_IdSmi"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_smi.SIS_SMI_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

    String sql2 = "SELECT CONCAT(smi_IdSmi,' ',smi_Descripcion) AS smi"+
                     " FROM sis_smi"+
                     " JOIN bdsis_maestros.m_smi ON openclinic_dbo.sis_smi.SIS_SMI_2 = bdsis_maestros.m_smi.smi_IdSmi"+
                     " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_smi.SIS_SMI_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";

	String filtrosSql = "";

	if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
		if(procesoOFormato.equals("1")) {
			if(opcion.equals("1")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";                     
			} else if(opcion.equals("2")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
			} else if(opcion.equals("3")){
				filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
			}
		} else if(procesoOFormato.equals("2")) {
			if(opcion.equals("1")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  
			 
			} else if(opcion.equals("2")){
				filtrosSql = filtrosSql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
			} else if(opcion.equals("3")){
				filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
			}
		}
		if(!componente.equals("3") && !componente.equals("")) {
			filtrosSql = filtrosSql +" AND SIS_ATENCION_15 = '"+componente+"'";
		}
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}
	if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
		filtrosSql = filtrosSql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}
	if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("") && ppdd.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
		if(!eess.equals("") && !ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}
			 
	sql = sql + filtrosSql;
	sql2 = sql2 + filtrosSql;
			 
	sql2 = sql2 + " GROUP BY smi";

	PreparedStatement ps = con.connect().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        PreparedStatement ps2 = con.connect().prepareStatement(sql2);
        ResultSet rs2 = ps2.executeQuery();


        while(rs.next()){        	
            JSONObject result = new JSONObject();
            for(int i=1;i<=3;i+=1)
			{
				if(i==1) {
					result.put("smi", rs.getString(i));
				} else if (i==2) {
					result.put("gestante", rs.getString(i));
				} else if(i==3) {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate fecha_nacimiento = LocalDate.parse(rs.getString(i),formatter);
					Period edad = Period.between(fecha_nacimiento,LocalDate.now());
					result.put("edad",edad.getYears());
				}
			}
			array.put(result);
        }

        while(rs2.next()){
            JSONObject result = new JSONObject();
            result.put("smi",rs2.getString(1));
            arraySMI.put(result);
        }

        rs2.close();
        ps2.close();

		rs.close();
	    ps.close();

		String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " WHERE natreg ="+usuario;
		JSONArray arrayUser = new JSONArray();        
        if(!usuario.equals("")) {
        	PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
            ResultSet rsUser = psUser.executeQuery();
            while(rsUser.next()){
                JSONObject result = new JSONObject();
                result.put("usuario",rsUser.getString(1));
                arrayUser.put(result);
            }
            rsUser.close();
            psUser.close();
        }
	    con.disconnect();
        
        List<SMIModel> listItems = new ArrayList<SMIModel>();

        Integer totalReg = 0;

        if(array.length()>0){
            for(int j=0;j<arraySMI.length();j++){
            int ge1 = 0;
            int ge2 = 0;
            int ge3 = 0;
            int ge4 = 0;
            int ge5 = 0;
            int gestante = 0;
            int total = 0;
            for(int i=0;i<array.length();i++){                
                if(array.getJSONObject(i).getString("smi").equals(arraySMI.getJSONObject(j).getString("smi"))){ 	
                	// System.out.println("iguales");
                    total+=1;
                    if(array.getJSONObject(i).getString("gestante").equals("1")){
                        gestante += 1;
                    }
                    if(array.getJSONObject(i).getInt("edad")>=0 && array.getJSONObject(i).getInt("edad")<=11){                        
                        ge1+=1;
                    } else if(array.getJSONObject(i).getInt("edad")>=12 && array.getJSONObject(i).getInt("edad")<=17){
                        ge2 += 1;
                    } else if(array.getJSONObject(i).getInt("edad")>=18 && array.getJSONObject(i).getInt("edad")<=29){
                        ge3 += 1;
                    } else if(array.getJSONObject(i).getInt("edad")>=30 && array.getJSONObject(i).getInt("edad")<=59){
                        ge4 += 1;
                    } else if(array.getJSONObject(i).getInt("edad")>=60 && array.getJSONObject(i).getInt("edad")<=120){
                        ge5 += 1;
                    }
                }
            }

            SMIModel smi = new SMIModel();
            smi.setSmi(arraySMI.getJSONObject(j).getString("smi"));
            smi.setGe1(ge1);
            smi.setGe2(ge2);
            smi.setGe3(ge3);
            smi.setGe4(ge4);
            smi.setGe5(ge5);
            smi.setGestante(gestante);
            smi.setTotal(total);

            totalReg +=1;

            listItems.add(smi);
            }
        }

        JRBeanCollectionDataSource dssmi = new JRBeanCollectionDataSource(listItems);
        
        String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");

        InputStream logoReporte = new FileInputStream(rutaproy + "_img/arfsis/logoReporte.jpg");
        
		JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/SMI.jrxml");

        Map<String, Object> parameters = new HashMap<String, Object>();

        if(!disa.equals("")) {
        	parameters.put("disa", disa);
        } else {
        	parameters.put("disa", " ");
        }
        if(!odsis.equals("")) {
        	parameters.put("odsis", odsis);
        } else {
        	parameters.put("odsis", " ");
        }
        if(!usuario.equals("")) {
        	parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
        } else {
        	parameters.put("usuario", " ");
        }
        parameters.put("subtitle", " ");
        parameters.put("totalReg", totalReg.toString());
        parameters.put("dssmi", dssmi);
        
	if(tipoArchivo.equals("pdf")){
		parameters.put("logoReporte", logoReporte);
    	        
		byte[] bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource());
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",  "inline; filename=SMI.pdf");
		ServletOutputStream output = response .getOutputStream();
		response.getOutputStream();
		output.write(bytes,0,bytes.length);
		output.flush();
		output.close();
	}
    else if(tipoArchivo.equals("excel")){
		JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

        byte[] bytes = new byte[10000];
		
		JRXlsExporter exporter = new JRXlsExporter();
		
		ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
		exporter.exportReport();
		
		bytes = xlsReport.toByteArray();
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition",  "inline; filename=SMI.xls");
		response.setContentLength(bytes.length);
		xlsReport.close();
		
		OutputStream output = response.getOutputStream();
		output.write(bytes,0,bytes.length);
		output.flush();
		output.close();
	}
%>