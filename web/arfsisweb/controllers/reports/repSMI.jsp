<%
/*************************************
Nombre        : repSMI
Proposito     : Generación de reporte de Servicio Materno Infantil
Creado por    : FLIZARRAGA
Fec Creacion  : 20230206
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230206  FLIZARRAGA     Se creo la función para generar el reporte de Servicio Materno Infantil
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	int pagina = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));

	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");

	String tipoReporte = request.getParameter("tipoReporte");

	String resultado = "";
	RepSMI datos = new RepSMI("sis_smi");
	datos.limit(rows);
	datos.page(pagina);
	
	if(tipoReporte.equals("atenciones")){
		resultado = datos.datosJSON(procesoOFormato,opcion,componente,periodo,year,desde,hasta,ppdd,eess,usuario,respAten);
	}
	else if(tipoReporte.equals("detallados")){
		resultado = datos.datosJSONDetalle(procesoOFormato,opcion,componente,periodo,year,desde,hasta,ppdd,eess,usuario,respAten);
	}
%>
<%=resultado %>
