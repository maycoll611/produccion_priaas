import { fileURLToPath, URL } from 'node:url';

import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';

import svgLoader from 'vite-svg-loader';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
    process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
    return {
        plugins: [
            vue(),
            svgLoader({
                defaultImport: 'url'
            })
        ],
        base: command === 'serve' ? '' : process.env.VITE_APP_BASE_URL + '/arfsisweb/sakai/dist/',
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url))
            }
        },
        build: {
            minify: false,
            minifyIdentifiers: false,
            minifySyntax: false,
            // entry: 'src/main.js',
            // outDir: resolve(__dirname, 'dist'),
            rollupOptions: {
                output: {
                    assetFileNames: (assetInfo) => {
                        let extType = assetInfo.name.split('.').at(1);
                        if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(extType)) {
                            extType = 'img';
                        }
                        return `assets/${extType}/[name][extname]`;
                    },
                    chunkFileNames: 'assets/js/[name].js',
                    entryFileNames: 'assets/js/[name].js'
                }
            }
        },
        configureServer: ({ app }) => {
            app.use(cors({ origin: '*' }));
        }
    };
});
