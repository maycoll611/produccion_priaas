import { defineStore } from 'pinia';
import axios from 'axios';
import { useLayout } from '@/layout/composables/layout';
const { isDarkTheme, contextPath } = useLayout();
export const useUserStore = defineStore('userStore', {
    state: () => ({
        user: {},
        permissions: [],
        rol: '',
        version: {
            descripcion: '',
            mayor: '',
            menor: '',
            parche: '',
            estabilidad: ''
        }
    }),
    actions: {
        async initData() {
            await axios.get(contextPath + '/arfsisweb/controllers/configuracion/getUserPermissions.jsp').then((response) => {
                let permisos = [];
                response.data.usuario.map((u) => {
                    permisos.push(u['sis_usuario_permisos.sis_identificador']);
                });
                let rol = '';
                rol = response.data.usuario[0]['sis_usuario_roles.sis_identificador'];

                let usuario = {};
                usuario.centroDigitacion = response.data.usuario[0]['sis_centro_digitacion'];
                usuario.disa = response.data.usuario[0]['sis_disa'];
                usuario.eess = response.data.usuario[0]['sis_eess'];
                usuario.emailInstitucion = response.data.usuario[0]['sis_email_institucion'];
                usuario.emailParticular = response.data.usuario[0]['sis_email_particular'];
                usuario.ocUserid = response.data.usuario[0]['sis_oc_userid'];
                usuario.udr = response.data.usuario[0]['sis_udr'];
                usuario.ue = response.data.usuario[0]['sis_ue'];
                usuario.nom_udr = response.data.usuario[0]['ods_Descripcion'];
                usuario.nom_disa = response.data.usuario[0]['dis_Descripcion'];
                usuario.nom_ppdd = response.data.usuario[0]['pin_Nombre'];

                usuario.nombres = response.data.nombres;

                let version = {};

                version.descripcion = 'v ' + response.data.version.descripcion;
                version.mayor = response.data.version.mayor;
                version.menor = response.data.version.menor;
                version.parche = response.data.version.parche;
                version.estabilidad = response.data.version.estabilidad;
                this.permissions = permisos;
                this.user = usuario;
                this.rol = rol;
                this.version = version;
                // userStore.setPermissions(permisos);
                // userStore.setUser(usuario);
                // userStore.setRol(rol);
                // userStore.setVersion(version);
            });
        },
        setUser(val) {
            this.user = val;
        },
        setPermissions(val) {
            this.permissions = val;
        },
        setRol(val) {
            this.rol = val;
        },
        setVersion(val) {
            this.version = val;
        }
    }
});
