<%@page import="java.util.Date" %>
<%@include file="/includes/validateUser.jsp"%>
<%
    Date date = new Date();
    Long time = date.getTime();
    String timestamp = time.toString();
    
%>
<!DOCTYPE html>
<html lang="es">

<head>
      <meta charset="UTF-8">
      <link rel="icon" href="/sakai-vue/favicon.ico">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>ARFSIS-WEB</title>
      <link id="theme-css" rel="stylesheet" type="text/css" href="<%=sCONTEXTPATH%>/arfsisweb/sakai/dist/themes/saga-blue/theme.css">
  	  <script type="module" crossorigin src="<%=sCONTEXTPATH%>/arfsisweb/sakai/dist/assets/js/index.js?t=<%=timestamp%>"></script>
  	  <link rel="stylesheet" href="<%=sCONTEXTPATH%>/arfsisweb/sakai/dist/assets/css/index.css?t=<%=timestamp%>">
</head>

<body>
      <div id="app"></div>
      
</body>

</html>