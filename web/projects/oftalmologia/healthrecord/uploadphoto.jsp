<%@page import="java.io.*,
				javazoom.upload.UploadFile,
				javazoom.upload.MultipartFormDataRequest" %>
<%@include file="/includes/helper.jsp"%>
<%@page errorPage="/includes/error.jsp"%>

<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean">
    <jsp:setProperty name="upBean" property="folderstore" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")%>'/>
    <jsp:setProperty name="upBean" property="parsertmpdir" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")%>'/>
    <jsp:setProperty name="upBean" property="parser" value="<%=MultipartFormDataRequest.CFUPARSER%>"/>
</jsp:useBean>

<%
	if(request.getParameter("me")!=null){
		session.setAttribute("me",request.getParameter("me"));
	}
	response.setHeader("Content-Type","text/html; charset=ISO-8859-1");
	//response.setHeader("Content-Type","application/json"); 
	
	String sProject = null;
	String sWebLanguage = null;
	User activeUser = null;
	AdminPerson activePatient = null;
	
	sProject = "/"+MedwanQuery.getInstance().getConfigString("projectname","openclinic");
	sWebLanguage = checkString((String)session.getAttribute(sAPPTITLE+"WebLanguage"));
	activeUser = (User)session.getAttribute("activeUser");
	activePatient = (AdminPerson)session.getAttribute("activePatient");
		
	int i=0;

    String filenameImg = ""; // request.getParameter("filenameImg"); // getContentType();	
    String SCANDIR_BASE = MedwanQuery.getInstance().getConfigString("scanDirectoryMonitor_basePath","C:/projects/openclinic/tomcat8/webapps/openclinic/scan");
   
    String sFolderStore = SCANDIR_BASE+"/photo/";
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    
    String sFileName = "";
    boolean isMultipart2 = MultipartFormDataRequest.isMultipartFormData(request);
    if (!isMultipart2) {
  	  //  Debug.println("NOT MULTIPART");
  	   out.print("NOT MULTIPART " + mrequest.getParameter("name") );
    } else {
    	
    	try{
            Hashtable files = mrequest.getFiles();
            String sHashName = new java.util.Date().getTime()+"";
            String sFilename = sHashName + "-" + mrequest.getParameter("filenameImg") + ".png";
            if(files!=null && !files.isEmpty()){
                UploadFile file = (UploadFile) files.get("image");
                file.setFileName( sFilename ); 
                
                upBean.setFolderstore(sFolderStore);
                // upBean.setParsertmpdir("C:/projects/openclinic/tomcat8/webapps/openclinic/tmp/");
                upBean.store(mrequest, "image");
                MedwanQuery.getInstance().getObjectCache().removeObject("transaction",mrequest.getParameter("uploadtransactionid"));
                out.print(sFilename);
            }
        }
        catch(Exception e){
        	Debug.printStackTrace(e);
        }
    }
%>