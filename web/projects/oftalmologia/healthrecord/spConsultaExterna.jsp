<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>

<%=checkPermission(out,"occup.clinicalexamination", "select",activeUser)%>

<script>
  <%-- ACTIVATE TAB --%>
  function activateTab(iTab){
    document.getElementById('tr1-view').style.display = 'none';
    document.getElementById('tr3-view').style.display = 'none';
    document.getElementById('tr4-view').style.display = 'none';

    td1.className = "tabunselected";
    td3.className = "tabunselected";
    td4.className = "tabunselected";

    if (iTab==1){
      document.getElementById('tr1-view').style.display = '';
      td1.className="tabselected";
    }
    else if (iTab==3){
      document.getElementById('tr3-view').style.display = '';
      td3.className="tabselected";
    }
    else if (iTab==4){
      document.getElementById('tr4-view').style.display = '';
      td4.className="tabselected";
    }
  }

  function deleteRowFromArrayString(sArray,rowid){
    var array = sArray.split("$");
    for (var i=0;i<array.length;i++){
      if (array[i].indexOf(rowid)>-1){
        array.splice(i,1);
      }
    }
    return array.join("$");
  }

  function getRowFromArrayString(sArray,rowid){
    var array = sArray.split("$");
    var row = "";
    for (var i=0;i<array.length;i++){
      if (array[i].indexOf(rowid)>-1){
        row = array[i].substring(array[i].indexOf("=")+1);
        break;
      }
    }
    return row;
  }

  function getCelFromRowString(sRow,celid){
    var row = sRow.split("�");
    return row[celid];
  }

  function replaceRowInArrayString(sArray,newRow,rowid){
    var array = sArray.split("$");
    for (var i=0;i<array.length;i++){
      if (array[i].indexOf(rowid)>-1){
        array.splice(i,1,newRow);
        break;
      }
    }
    return array.join("$");
  }
</script>


<form name="transactionForm" id="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>' >
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<COMMENT/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
	<ENDCOMMENT/>
    <input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
    <input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
    <input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>

    <input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_RECRUITMENT_CONVOCATION_ID" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_RECRUITMENT_CONVOCATION_ID" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="subClass" value="GENERAL"/>

    <%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>

    <%-- TITLE --%>
    <table class="list" width='100%' cellspacing="0" cellpadding="0">
        <tr class="admin">
            <td width="1%" nowrap>
                <a href="javascript:openHistoryPopup();" title="<%=getTranNoLink("Web.Occup","History",sWebLanguage)%>">...</a>&nbsp;
                <%=getTran(request,"Web.Occup","medwan.common.date",sWebLanguage)%>
            </td>
            <td nowrap>
                <input type="text" class="text" size="12" maxLength="10" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" id="trandate" OnBlur='checkDate(this)'>
                <script>writeTranDate();</script>
            </td>
            <td width="90%"><%=contextHeader(request,sWebLanguage)%></td>
        </tr>
    </table>

    <br/>

    <%-- TABS --%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class='tabs' width='5'>&nbsp;</td>
            <td class='tabunselected' width="1%" onclick="activateTab(1)" id="td1" nowrap>&nbsp;<b><%=getTran(request,"Web.Occup","medwan.healthrecord.tab.summary",sWebLanguage)%></b>&nbsp;</td>
            <td class='tabs' width='5'>&nbsp;</td>
            <td class='tabunselected' width="1%" onclick="activateTab(3)" id="td3" nowrap>&nbsp;<b><%=getTran(request,"Web.Occup","Familiaal_Anamnese",sWebLanguage)%></b>&nbsp;</td>
            <td class='tabs' width='5'>&nbsp;</td>
            <td class='tabunselected' width="1%" onclick="activateTab(4)" id="td4" nowrap>&nbsp;<b><%=getTran(request,"Web.Occup","Persoonlijke_Antecedenten",sWebLanguage)%></b>&nbsp;</td>
            <td width="*" class='tabs'>&nbsp;</td>
        </tr>
    </table>

    <%-- HIDEABLE --%>
    <table style="vertical-align:top;" width="100%" border="0" cellspacing="0">
        <tr id="tr1-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("healthrecord/spConsultaExternaSummary.jsp"),pageContext);%></td>
        </tr>
        <tr id="tr3-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("healthrecord/spConsultaExternaFamiliaal.jsp"),pageContext);%></td>
        </tr>
        <tr id="tr4-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("healthrecord/spConsultaExternaPersoonlijk.jsp"),pageContext);%></td>
        </tr>
    </table>

    <%-- BUTTONS --%>
    <%=ScreenHelper.alignButtonsStart()%>
        <%=getButtonsHtml(request,activeUser,activePatient,"occup.clinicalexamination",sWebLanguage)%>     
    <%=ScreenHelper.alignButtonsStop()%>

    <%=ScreenHelper.contextFooter(request)%>
</form>

<script>
  activateTab(1);

  function convertTable(sText){
    var aRows = sText.split("</TR>");
    sText = "";
    for (var i=0;i<aRows.length;i++){
      var sReturn = "";
      var sRow = aRows[i];
      var aTds = sRow.split("</TD>");
      for (var y=0;y<aTds.length;y++){
        var sTD = aTds[y];
        if ((sTD.indexOf("delete")<0)&&(sTD.indexOf("<TD>")>-1)){
          sReturn += sTD+"</TD>";
        }
      }

      if (sReturn.length>0){
        sText+=("<TR>"+sReturn+"</TR>");
      }
    }
    return sText;
  }

  <%-- SUBMIT FORM --%>
  function submitForm(){
	var maySubmit = true;
    if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
		alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
		searchEncounter();
	}	
    else {
	    // check familiaal-fields for content
	    if (isAtLeastOneKinderenFieldFilled()){
	      if(maySubmit){
	        if(!addKinderen()){ maySubmit = false; }
	      }
	    }
	
	    if (isAtLeastOneFamiFieldFilled()){
	      if(maySubmit){
	        addFami();
	      }
	    }

	    // check persoonlijk-fields for content
	    if (isAtLeastOneChirurgieFieldFilled()){
	      if(maySubmit){
	        if(!addChirurgie()){ maySubmit = false; }
	      }
	    }
	
	    if (isAtLeastOneHeelkundeFieldFilled()){
	      if(maySubmit){
	        if(!addHeelkunde()){ maySubmit = false; }
	      }
	    }
	
	    if (isAtLeastOneLetselsFieldFilled()){
	      if(maySubmit){
	        addLetsels();
	      }
	    }

	//Summary

	//familiaal
	    while (sKinderen.indexOf("rowKinderen")>-1){
	      sTmpBegin = sKinderen.substring(sKinderen.indexOf("rowKinderen"));
	      sTmpEnd = sTmpBegin.substring(sTmpBegin.indexOf("=")+1);
	      sKinderen = sKinderen.substring(0,sKinderen.indexOf("rowKinderen"))+sTmpEnd;
	    }
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_KINDEREN" property="itemId"/>]>.value")[0].value = sKinderen.substring(0,250);
	
	    while (sFami.indexOf("rowFami")>-1){
	      sTmpBegin = sFami.substring(sFami.indexOf("rowFami"));
	      sTmpEnd = sTmpBegin.substring(sTmpBegin.indexOf("=")+1);
	      sFami = sFami.substring(0,sFami.indexOf("rowFami"))+sTmpEnd;
	    }
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIALE_ANTECEDENTEN" property="itemId"/>]>.value")[0].value = sFami.substring(0,250);
	
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT1" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(250,500);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT2" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(500,750);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT3" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(750,1000);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT4" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(1000,1250);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT5" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(1250,1500);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT6" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(1500,1750);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT7" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(1750,2000);
	    document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_FAMILIAAL_COMMENT" property="itemId"/>]>.value')[0].value.substring(0,250);
	
	//persoonlijk
	    while (sChirurgie.indexOf("rowChirurgie")>-1){
	      sTmpBegin = sChirurgie.substring(sChirurgie.indexOf("rowChirurgie"));
	      sTmpEnd = sTmpBegin.substring(sTmpBegin.indexOf("=")+1);
	      sChirurgie = sChirurgie.substring(0,sChirurgie.indexOf("rowChirurgie"))+sTmpEnd;
	    }
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_MEDISCHE_ANTECEDENTEN1" property="itemId"/>]>.value")[0].value = sChirurgie.substring(0,250);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_MEDISCHE_ANTECEDENTEN2" property="itemId"/>]>.value")[0].value = sChirurgie.substring(250,500);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_MEDISCHE_ANTECEDENTEN3" property="itemId"/>]>.value")[0].value = sChirurgie.substring(500,750);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_MEDISCHE_ANTECEDENTEN4" property="itemId"/>]>.value")[0].value = sChirurgie.substring(750,1000);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_MEDISCHE_ANTECEDENTEN5" property="itemId"/>]>.value")[0].value = sChirurgie.substring(1000,1250);
	    <%if(MedwanQuery.getInstance().getConfigInt("enableCCBRT",0)==0){ %>
	
	    while (sLetsels.indexOf("rowLetsels")>-1){
	      sTmpBegin = sLetsels.substring(sLetsels.indexOf("rowLetsels"));
	      sTmpEnd = sTmpBegin.substring(sTmpBegin.indexOf("=")+1);
	      sLetsels = sLetsels.substring(0,sLetsels.indexOf("rowLetsels"))+sTmpEnd;
	    }
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_LETSELS1" property="itemId"/>]>.value")[0].value = sLetsels.substring(0,250);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_LETSELS2" property="itemId"/>]>.value")[0].value = sLetsels.substring(250,500);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_LETSELS3" property="itemId"/>]>.value")[0].value = sLetsels.substring(500,750);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_LETSELS4" property="itemId"/>]>.value")[0].value = sLetsels.substring(750,1000);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_LETSELS5" property="itemId"/>]>.value")[0].value = sLetsels.substring(1000,1250);
		<%}%>

	    while (sHeelkunde.indexOf("rowHeelkunde")>-1){
	      sTmpBegin = sHeelkunde.substring(sHeelkunde.indexOf("rowHeelkunde"));
	      sTmpEnd = sTmpBegin.substring(sTmpBegin.indexOf("=")+1);
	      sHeelkunde = sHeelkunde.substring(0,sHeelkunde.indexOf("rowHeelkunde"))+sTmpEnd;
	    }
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_HEELKUNDE1" property="itemId"/>]>.value")[0].value = sHeelkunde.substring(0,250);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_HEELKUNDE2" property="itemId"/>]>.value")[0].value = sHeelkunde.substring(250,500);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_HEELKUNDE3" property="itemId"/>]>.value")[0].value = sHeelkunde.substring(500,750);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_HEELKUNDE4" property="itemId"/>]>.value")[0].value = sHeelkunde.substring(750,1000);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_HEELKUNDE5" property="itemId"/>]>.value")[0].value = sHeelkunde.substring(1000,1250);
	
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(0,250);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT1" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(250,500);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT2" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(500,750);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT3" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(750,1000);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT4" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(1000,1250);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT5" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(1250,1500);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT6" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(1500,1750);
	    document.getElementsByName("currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CE_PERSONAL_COMMENT7" property="itemId"/>]>.value")[0].value = document.getElementsByName('PersoonlijkComment')[0].value.substring(1750,2000);
	
	    if(maySubmit){
	      var temp = Form.findFirstElement(transactionForm);//for ff compatibility
	      transactionForm.saveButton.style.visibility = "hidden";
	      <%
	          SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
				
	    		  out.print(takeOverTransaction(sessionContainerWO, activeUser,"document.transactionForm.submit();"));
	      %>
	    }
    }
  }

  function subScreen(screenName){
    document.getElementsByName('be.mxs.healthrecord.updateTransaction.actionForwardKey')[0].value = screenName;
    submitForm();
  }
  function searchEncounter(){
      pu = openPopup("/_common/search/searchEncounter.jsp&ts=<%=getTs()%>&VarCode=encounteruid&VarText=&FindEncounterPatient=<%=activePatient.personid%>");
  }
  if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
	  alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
	  searchEncounter();
  }


  <%-- SELECT KEYWORDS --%>
  function selectKeywords(destinationidfield,destinationtextfield,labeltype,divid,element){
      if(element){
          //document.getElementById("keywordstd").style="position: absolute;top: "+element.getBoundingClientRect().top;
      }
      var bShowKeywords=true;
      document.getElementById("activeDestinationIdField").value=destinationidfield;
      document.getElementById("activeDestinationTextField").value=destinationtextfield;
      document.getElementById("activeLabeltype").value=labeltype;
      document.getElementById("activeDivld").value=divid;

      document.getElementById("key1").width = "16";
      document.getElementById("key2").width = "16";
      // document.getElementById("key3").width = "16";
      // document.getElementById("key4").width = "16";
      // document.getElementById("key6").width = "16";

      document.getElementById("title1").style.textDecoration = "none";
      document.getElementById("title2").style.textDecoration = "none";
      // document.getElementById("title3").style.textDecoration = "none";
      // document.getElementById("title4").style.textDecoration = "none";
      // document.getElementById("title6").style.textDecoration = "none";

      if(labeltype=='ikirezi2.functional.signs'){
          document.getElementById("title1").style.textDecoration = "underline";
          document.getElementById('key1').width = '32';
          //document.getElementById('keywordstd').style = "vertical-align:top";
      }
      else if(labeltype=='ikirezi2.inspection'){
          document.getElementById("title2").style.textDecoration = "underline";
          document.getElementById('key2').width = '32';
          //document.getElementById('keywordstd').style = "vertical-align:top";
      }
      /*
      else if(labeltype=='ikirezi2.palpation'){
          document.getElementById("title3").style.textDecoration = "underline";
          document.getElementById('key3').width = '32';
          //document.getElementById('keywordstd').style = "vertical-align:top";
      }
      else if(labeltype=='ikirezi2.auscultation'){
          document.getElementById("title4").style.textDecoration = "underline";
          document.getElementById('key4').width = '32';
          //document.getElementById('keywordstd').style = "vertical-align:top";
      }
      else if(labeltype=='reference'){
          document.getElementById("title6").style.textDecoration = "underline";
          document.getElementById('key6').width = '32';
          //document.getElementById('keywordstd').style = "vertical-align:bottom";
      } */
      else{
          bShowKeywords=false;
      }

      if(bShowKeywords){
          var params = "";
          var today = new Date();
          var url = '<c:url value="/healthrecord/ajax/getKeywords.jsp"/>'+
              '?destinationidfield='+destinationidfield+
              '&destinationtextfield='+destinationtextfield+
              '&labeltype='+labeltype+
              '&filetype=new'+
              '&ts='+today;
          new Ajax.Request(url,{
              method: "POST",
              parameters: params,
              onSuccess: function(resp){
                  $(divid).innerHTML = resp.responseText;
                  if(resp.responseText.indexOf("<recheck/>")>-1){
                      window.setTimeout('storekeywordsubtype(document.getElementById("keywordsubtype").value);',200);
                  }
                  <%
                      if(checkString((String)request.getSession().getAttribute("editmode")).equalsIgnoreCase("1")){%>
                  myselect=document.getElementById('keywordsubtype');
                  myselect.style='border:2px solid black; border-style: dotted';
                  myselect.onclick=function(){window.open('<%=request.getRequestURI().replaceAll(request.getServletPath(),"")%>/popup.jsp?Page=system/manageTranslations.jsp&FindLabelType=keywordsubtypes.'+labeltype+'&find=1','popup','toolbar=no,status=yes,scrollbars=yes,resizable=yes,width=800,height=500,menubar=no');return false;};
                  <%
                      }
                  %>
              },
              onFailure: function(){
                  $(divid).innerHTML = "";
              }
          });
      }
      else{
          $(divid).innerHTML = "";
      }
  }

  function newkeyword(){
      openPopup("/healthrecord/ajax/newKeyword.jsp&ts=<%=getTs()%>&labeltype="+document.getElementById("activeLabeltype").value+"."+document.getElementById("keywordsubtype").value);
  }

  function deletekeyword(labeltype,labelid){
      if(confirm("<%=getTranNoLink("web","areyousure",sWebLanguage)%>")){
          var params = "";
          var today = new Date();
          var url = '<c:url value="/healthrecord/ajax/deleteKeyword.jsp"/>'+
              '?labeltype='+labeltype+'&labelid='+labelid;
          new Ajax.Request(url,{
              method: "POST",
              parameters: params,
              onSuccess: function(resp){
                  refreshKeywords();
              },
              onFailure: function(){
              }
          });
      }
  }

  function refreshKeywords(){
      selectKeywords(document.getElementById("activeDestinationIdField").value,
          document.getElementById("activeDestinationTextField").value,
          document.getElementById("activeLabeltype").value,
          document.getElementById("activeDivld").value);
  }

  <%-- ADD KEYWORD --%>
  function addKeyword(id,label,destinationidfield,destinationtextfield){
      while(document.getElementById(destinationtextfield).innerHTML.indexOf('&nbsp;')>-1){
          document.getElementById(destinationtextfield).innerHTML=document.getElementById(destinationtextfield).innerHTML.replace('&nbsp;','');
      }
      var ids = document.getElementById(destinationidfield).value;
      if((ids+";").indexOf(id+";")<=-1){
          document.getElementById(destinationidfield).value = ids+";"+id;

          if(document.getElementById(destinationtextfield).innerHTML.length > 0){
              if(!document.getElementById(destinationtextfield).innerHTML.endsWith("| ")){
                  document.getElementById(destinationtextfield).innerHTML+= " | ";
              }
          }

          document.getElementById(destinationtextfield).innerHTML+= "<span style='white-space: nowrap;'><a href='javascript:deleteKeyword(\""+destinationidfield+"\",\""+destinationtextfield+"\",\""+id+"\");'><img width='8' src='<c:url value="/_img/themes/default/erase.png"/>' class='link' style='vertical-align:-1px'/></a> <b>"+label+"</b></span> | ";
      }
  }

  function storekeywordsubtype(s){
      var params = "";
      var today = new Date();
      var url = '<c:url value="/healthrecord/ajax/storeKeywordSubtype.jsp"/>'+
          '?subtype='+s;
      new Ajax.Request(url,{
          method: "POST",
          parameters: params,
          onSuccess: function(resp){
              selectKeywords(document.getElementById("activeDestinationIdField").value,
                  document.getElementById("activeDestinationTextField").value,
                  document.getElementById("activeLabeltype").value,
                  document.getElementById("activeDivld").value);
          },
          onFailure: function(){
          }
      });
  }

  <%-- DELETE KEYWORD --%>
  function deleteKeyword(destinationidfield,destinationtextfield,id){
      var newids = "";

      var ids = document.getElementById(destinationidfield).value.split(";");
      for(n=0; n<ids.length; n++){
          if(ids[n].indexOf("$")>-1){
              if(id!=ids[n]){
                  newids+= ids[n]+";";
              }
          }
      }

      document.getElementById(destinationidfield).value = newids;
      var newlabels = "";
      var labels = document.getElementById(destinationtextfield).innerHTML.split(" | ");
      for(n=0;n<labels.length;n++){
          if(labels[n].trim().length>0 && labels[n].indexOf(id)<=-1){
              newlabels+=labels[n]+" | ";
          }
      }

      document.getElementById(destinationtextfield).innerHTML = newlabels;
  }
</script>

<%=writeJSButtons("transactionForm","saveButton")%>