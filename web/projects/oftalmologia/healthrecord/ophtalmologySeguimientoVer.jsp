<%@page import="be.mxs.common.model.vo.healthrecord.TransactionVO,
                be.mxs.common.model.vo.healthrecord.ItemVO,
                be.openclinic.pharmacy.Product,
                java.text.DecimalFormat,
                be.openclinic.medical.Problem,
                be.openclinic.medical.Diagnosis,
                be.openclinic.system.Transaction,
                be.openclinic.system.Item,
                be.openclinic.medical.Prescription,
                java.util.*" %>
<%@ page import="java.sql.Date" %>
<%@ page import="be.openclinic.medical.PaperPrescription" %>
<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>
<%=checkPermission(out,"occup.ophtalmology.consultation","select",activeUser)%>
<style>
	TD.admin {
    border: 0.5px solid white;
    }
    tr.admin {
    border-bottom: 1px solid white;
    }
</style>
<form id="transactionForm" name="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>'>
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
  
    <input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
    <input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
    <input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>
    <input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
    <%--=ScreenHelper.writeDefaultHiddenInput((TransactionVO)transaction, "ITEM_TYPE_CONTEXT_DEPARTMENT") --%>
    <%--=ScreenHelper.writeDefaultHiddenInput((TransactionVO)transaction, "ITEM_TYPE_CONTEXT_CONTEXT") --%>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>

    <%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>
    <%=contextHeader(request,sWebLanguage)%>
	<%
		Vector EstudiosImagen = activePatient.getImagesPacsPatientByPersonId(activePatient.personid);

		String sMOTIVOCONSULTA = "";
		String sMOTIVOCONSULTA_CD_1 = "";
		String sMOTIVOCONSULTA_ANAMNESIS = "";
		String sAV_ACTUAL = "";
		String sAV_ACTUAL_LA_ESF_OD = "";
		String sAV_ACTUAL_LA_ESF_OI = "";
		String sAV_ACTUAL_LA_CIL_OD = "";
		String sAV_ACTUAL_LA_CIL_OI = "";
		String sAV_ACTUAL_LA_EJE_OD = "";
		String sAV_ACTUAL_LA_EJE_OI = "";
		String sAV_ACTUAL_LA_DIP = "";
		String sAV_ACTUAL_AR_ESF_OD = "";
		String sAV_ACTUAL_AR_ESF_OI = "";
		String sAV_ACTUAL_AR_CIL_OD = "";
		String sAV_ACTUAL_AR_CIL_OI = "";
		String sAV_ACTUAL_AR_EJE_OD = "";
		String sAV_ACTUAL_AR_EJE_OI = "";
		String sAV_ACTUAL_AR_DIP = "";
		String sANTECED = "";
		String sANTECED_DIABETES = "";
		String sANTECED_HTA = "";
		String sANTECED_DISLIPIDEMIA = "";
		String sANTECED_GLAUCOMA = "";
		String sANTECED_ALERGIA = "";
		String sANTECED_HIST_QUIRURGICA = "";
		String sANTECED_HIST_FAMILIAR = "";
		String sANTECED_MEDICAMENTOS = "";
		String sANTECED_OTROS = "";
		String sPIO_OD = "";
		String sPIO_OI = "";
		String sAGUDEZAVISUAL_SE_OD = "";
		String sAGUDEZAVISUAL_SE_OI = "";
		String sAGUDEZAVISUAL_AE_OD = "";
		String sAGUDEZAVISUAL_AE_OI = "";
		String sAGUDEZAVISUAL_CC_OD = "";
		String sAGUDEZAVISUAL_CC_OI = "";
		String sRETRACCIONFINAL_ESF_OD = "";
		String sRETRACCIONFINAL_ESF_OI = "";
		String sRETRACCIONFINAL_CIL_OD = "";
		String sRETRACCIONFINAL_CIL_OI = "";
		String sRETRACCIONFINAL_EJE_OD = "";
		String sRETRACCIONFINAL_EJE_OI = "";
		String sRETRACCIONFINAL_DIP_OD = "";
		String sRETRACCIONFINAL_DIP_OI = "";
		String sPARPADO_OD = "";
		String sPARPADO_OI = "";
		String sBIOMICROSCOPIA_CONJUNTIVA_OD = "";
		String sBIOMICROSCOPIA_CONJUNTIVA_OI = "";
		String sBIOMICROSCOPIA_CORNEA_OD = "";
		String sBIOMICROSCOPIA_CORNEA_OI = "";
		String sBIOMICROSCOPIA_CAMARAANTERIOR_OD = "";
		String sBIOMICROSCOPIA_CAMARAANTERIOR_OI = "";
		String sBIOMICROSCOPIA_IRIS_OD = "";
		String sBIOMICROSCOPIA_IRIS_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_COR_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_COR_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_NUC_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_NUC_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_CAP_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_CAP_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_AFA_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_AFA_OI = "";
		String sFONDOOJO_NERVIOOPTICO_OD = "";
		String sFONDOOJO_NERVIOOPTICO_OI = "";
		String sFONDOOJO_MACULA_OD = "";
		String sFONDOOJO_MACULA_OI = "";
		String sFONDOOJO_PERIFERIA_OD = "";
		String sFONDOOJO_PERIFERIA_OI = "";
		String sDRAWING = "";
		String sCAMARA = "";
		String sCAMARA_PHOTO = "";
		String sDIAGNOSTICO = "";
		String sOBSERVACION = "";
		String sPROXIMA_CITA = "";

        if(transaction != null){
            TransactionVO tran = (TransactionVO)transaction;
            if(tran!=null){

				sMOTIVOCONSULTA = 	getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_MOTIVOCONSULTA");
				sMOTIVOCONSULTA_CD_1 = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_MOTIVOCONSULTA_CD_1");
				sMOTIVOCONSULTA_ANAMNESIS = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_MOTIVOCONSULTA_ANAMNESIS");
				sAV_ACTUAL 			 = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL");
				sAV_ACTUAL_LA_ESF_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD");
				sAV_ACTUAL_LA_ESF_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI");
				sAV_ACTUAL_LA_CIL_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD");
				sAV_ACTUAL_LA_CIL_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI");
				sAV_ACTUAL_LA_EJE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD");
				sAV_ACTUAL_LA_EJE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI");
				sAV_ACTUAL_LA_DIP 	 = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP");
				sAV_ACTUAL_AR_ESF_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD");
				sAV_ACTUAL_AR_ESF_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI");
				sAV_ACTUAL_AR_CIL_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD");
				sAV_ACTUAL_AR_CIL_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI");
				sAV_ACTUAL_AR_EJE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD");
				sAV_ACTUAL_AR_EJE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI");
				sAV_ACTUAL_AR_DIP = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP");
				sANTECED = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED");
				sANTECED_DIABETES = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_DIABETES");
				sANTECED_HTA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HTA");
				sANTECED_DISLIPIDEMIA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_DISLIPIDEMIA");
				sANTECED_GLAUCOMA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_GLAUCOMA");
				sANTECED_ALERGIA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_ALERGIA");
				sANTECED_HIST_QUIRURGICA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HIST_QUIRURGICA");
				sANTECED_HIST_FAMILIAR = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HIST_FAMILIAR");
				sANTECED_MEDICAMENTOS = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_MEDICAMENTOS");
				sANTECED_OTROS = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_OTROS");
				sPIO_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PIO_OD");
				sPIO_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PIO_OI");
				sAGUDEZAVISUAL_SE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OD");
				sAGUDEZAVISUAL_SE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OI");
				sAGUDEZAVISUAL_AE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OD");
				sAGUDEZAVISUAL_AE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OI");
				sAGUDEZAVISUAL_CC_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OD");
				sAGUDEZAVISUAL_CC_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OI");
				sRETRACCIONFINAL_ESF_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_ESF_OD");
				sRETRACCIONFINAL_ESF_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_ESF_OI");
				sRETRACCIONFINAL_CIL_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_CIL_OD");
				sRETRACCIONFINAL_CIL_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_CIL_OI");
				sRETRACCIONFINAL_EJE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_EJE_OD");
				sRETRACCIONFINAL_EJE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_EJE_OI");
				sRETRACCIONFINAL_DIP_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_DIP_OD");
				sRETRACCIONFINAL_DIP_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_RETRACCIONFINAL_DIP_OI");
				sPARPADO_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PARPADO_OD");
				sPARPADO_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PARPADO_OI");
				sBIOMICROSCOPIA_CONJUNTIVA_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CONJUNTIVA_OD");
				sBIOMICROSCOPIA_CONJUNTIVA_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CONJUNTIVA_OI");
				sBIOMICROSCOPIA_CORNEA_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CORNEA_OD");
				sBIOMICROSCOPIA_CORNEA_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CORNEA_OI");
				sBIOMICROSCOPIA_CAMARAANTERIOR_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CAMARAANTERIOR_OD");
				sBIOMICROSCOPIA_CAMARAANTERIOR_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CAMARAANTERIOR_OI");
				sBIOMICROSCOPIA_IRIS_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_IRIS_OD");
				sBIOMICROSCOPIA_IRIS_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_IRIS_OI");
				sBIOMICROSCOPIA_CRISTALINO_COR_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OD");
				sBIOMICROSCOPIA_CRISTALINO_COR_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OI");
				sBIOMICROSCOPIA_CRISTALINO_NUC_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OD");
				sBIOMICROSCOPIA_CRISTALINO_NUC_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OI");
				sBIOMICROSCOPIA_CRISTALINO_CAP_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OD");
				sBIOMICROSCOPIA_CRISTALINO_CAP_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OI");
				sBIOMICROSCOPIA_CRISTALINO_AFA_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OD");
				sBIOMICROSCOPIA_CRISTALINO_AFA_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OI");
				sFONDOOJO_NERVIOOPTICO_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_NERVIOOPTICO_OD");
				sFONDOOJO_NERVIOOPTICO_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_NERVIOOPTICO_OI");
				sFONDOOJO_MACULA_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_MACULA_OD");
				sFONDOOJO_MACULA_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_MACULA_OI");
				sFONDOOJO_PERIFERIA_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_PERIFERIA_OD");
				sFONDOOJO_PERIFERIA_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_FONDOOJO_PERIFERIA_OI");
				sDRAWING = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_DRAWING");
				sCAMARA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_CAMARA");
				sCAMARA_PHOTO = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_CAMARA_PHOTO");
				sDIAGNOSTICO = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_DIAGNOSTICO");
				sOBSERVACION = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_OBSERVACION");
				sPROXIMA_CITA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PROXIMA_CITA");
				/*
				sTranCamera_Photo = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OPHTALMOLOGY_CONSULTATION_VER_CAMERA_PHOTO");
				If(sTranCamera_Photo.length() > 0)
				{
					sTranCamera_Photo_link = sCONTEXTPATH + '/scan/photo/' + sTranCamera_Photo;
				}
				*/
            }
        }
    %>  
    
    <table class="list" cellspacing="1" cellpadding="0" width="100%">
		<tr>
            <td style="vertical-align:top;padding:0">
				<table class="list" width="100%" cellspacing="1" cellpadding='0'>
					<%-- DATE --%>
					<tr>
						<td class='admin' colspan='8'>
							<table width='100%' cellspacing='0' cellpadding='0'>
								<tr>
									<td width='1%' nowrap rowspan='2'>
										<img style='vertical-align: middle' height='50px' src='<%=sCONTEXTPATH %>/_img/ver.png'/>&nbsp;&nbsp; 
									</td>
									<td class="admin" width="<%=sTDAdminWidth%>">
										<a href="javascript:openHistoryPopup();" title="<%=getTranNoLink("Web.Occup","History",sWebLanguage)%>">...</a>&nbsp;
										<%=getTran(request,"Web.Occup","medwan.common.date",sWebLanguage)%>
										<input type="text" class="text" size="12" maxLength="10" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" id="trandate" OnBlur='checkDate(this)'>
			                			<script>writeTranDate();</script>
									</td>
									<td class='admin' width='40%'>
										<div id='covidtest' style='color: red;font-size: 14px'></div>
									</td>
								</tr>
								<tr class='admin2'>
									<td colspan='2'>
										<!-- <strong>Alergia a:</strong> --> <%-- =getTran(request,"examination","1216",sWebLanguage) --%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%-- VITAL SIGNS --%>
					<tr class="admin">
						<td colspan="8"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_MOTIVOCONSULTA", sWebLanguage, "onchange='showtests();'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","motivo.consulta",sWebLanguage)%> &nbsp;</td>
					</tr>
					<tr id='admissionreasons_items' style='display: none'>
						<td class="admin2" colspan="8">
							<input type="hidden" id="complaints" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_MOTIVOCONSULTA_CD_1" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_MOTIVOCONSULTA_CD_1" property="value"/>"/>
							<table width='100%'>
								<%
									String sComplaints = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_MOTIVOCONSULTA_CD_1");
								
									Label label;
									int counter = 0;
									Hashtable labelTypes = (Hashtable)MedwanQuery.getInstance().getLabels().get(sWebLanguage.toLowerCase());
									if(labelTypes !=null){
										Hashtable labelIds = (Hashtable)labelTypes.get("cdo.1");
										if(labelIds!=null) {
											Enumeration idsEnum = labelIds.elements();
											Hashtable hSelected = new Hashtable();
											while (idsEnum.hasMoreElements()) {
												label = (Label)idsEnum.nextElement();
												hSelected.put(label.value.toUpperCase(),label.id);
											}
											
											Vector keys = new Vector(hSelected.keySet());
											Collections.sort(keys);
											Iterator it = keys.iterator();
											String sLabelValue, sLabelID;
											while(it.hasNext()){
												sLabelValue = (String)it.next();
												sLabelID = (String)hSelected.get(sLabelValue);
												if(counter % 6 ==0){
													if(counter>0){
														out.println("</tr>");
													}
													out.println("<tr>");
												}
												counter++;
												
												%><td nowrap width='25%'><input class='hand' type="checkbox" name="complaints.<%=sLabelID%>" id="complaints.<%=sLabelID%>" value="<%=sLabelID%>" <%=sComplaints.indexOf("*"+sLabelID+"*")>-1?"checked":"" %>/>&nbsp;<label for="complaints.<%=sLabelID%>" class="hand"><%=sLabelValue%></label></td><%
											}
										}
									}
								%>
								</tr>
								<tr>
									<td nowrap class="admin" width="110">
										<%=getTran(request,"ophtalmology.consultation.ver","anamnesis",sWebLanguage)%> &nbsp; 
									</td>
									<td class="admin2" colspan="5">
										<textarea <%=setRightClickMini("ITEM_TYPE_OC_VER_MOTIVOCONSULTA_ANAMNESIS")%> onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="complaints_comment" class="text" cols="80" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_MOTIVOCONSULTA_ANAMNESIS" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_MOTIVOCONSULTA_ANAMNESIS" translate="false" property="value"/></textarea>									
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<%-- AGUDEZA VISUAL ACTUAL --%>
					<tr class="admin">
						<td colspan="8">
							<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_AV_ACTUAL", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_AV_ACTUAL\",\"div_avactual\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","agudeza.vusual.actual",sWebLanguage)%> &nbsp;
						</td>
					</tr>
					<tr class="div_avactual">
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							<%-- vision.acuity ----------------------------------------------------------------------%>
							<%-- right : current glasses / dx --%>
							<tr>
								<td class="admin" width="150" rowspan="6"><%=getTran(request,"ophtalmology.consultation.ver","agudeza.vusual.actual",sWebLanguage)%></td>
								<td class="admin2" width="150" rowspan="2"><%=getTran(request,"ophtalmology.consultation.ver","lentes.actuales",sWebLanguage)%></td>
								<td class="admin2" width="50"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_ESF_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_CIL_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_EJE_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
								<td rowspan="2" class="admin2">
									<%=getTran(request,"openclinic.chuk","add",sWebLanguage)%>+&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_DIP%>" size="4" onblur="isMyNumber(this);"/>&nbsp;D
								</td>
							</tr>
							<%-- left : current glasses / dx --%>
							<tr>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_ESF_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_CIL_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_EJE_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
							</tr>
							<%-- right : autorefactor --%>
							<tr>
								<td class="admin2" rowspan="2"><%=getTran(request,"ophtalmology.consultation.ver","autorefractor",sWebLanguage)%></td>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_ESF_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_CIL_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_EJE_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
								<td rowspan="2" class="admin2">
									<%=getTran(request,"openclinic.chuk","add",sWebLanguage)%>+&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_DIP%>" size="4" onblur="isMyNumber(this);"/>&nbsp;D
								</td>
							</tr>
							<%-- left : autorefactor --%>
							<tr>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_ESF_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_CIL_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_EJE_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
							</tr>				
							
						</table>
						</td>
					</tr>
					<%-- Historial --%>
					<tr class="admin">
						<td colspan="8">
							<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_ANTECED", sWebLanguage, "onchange='showtests();'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","antecedentes",sWebLanguage)%> &nbsp;
						</td>
					</tr>
					<tr id='history_items' style='display: none'>
						<td colspan='8'>
							<table class="list" width="100%" cellspacing="1" cellpadding='0'>
								<tr>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","diabetes",sWebLanguage) %> 
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(null, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_DIABETES", "yesnounknown", sWebLanguage, "") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","hta",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(null, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HTA", "yesnounknown", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","dislipidemia",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_DISLIPIDEMIA", "yesnounknown", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","glaucoma",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_GLAUCOMA", "yesnounknown", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","alergias",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_ALERGIA", "yesnounknown", sWebLanguage, "onclick='triage()'") %>
									</td>
								</tr>
								<tr>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","historia_familiar",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HIST_QUIRURGICA", "cdo.9", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","historia_quirurgica",sWebLanguage) %>
									</td>
									<td class='admin2top' colspan="1">
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HIST_FAMILIAR", "cdo.10", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","Medicamentos",sWebLanguage) %>
									</td>
									<td class='admin2top' colspan="3">
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_MEDICAMENTOS", "cdo.7", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","otros",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<textarea onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="history_comment" class="text" cols="25" rows="1" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_ANTECED_OTROS" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_ANTECED_OTROS" translate="false" property="value"/></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%-- SALUD OCULAR --%>
					<tr class="admin" style="display: none;">
						<td width="245" colspan="1">
							<%=getTran(request,"ophtalmology.consultation.ver","salud.ocular",sWebLanguage)%> &nbsp;
							
						</td>
						<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
						<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
						<td width="230">&nbsp;D</td>
						<td colspan="4">&nbsp;I</td>
					</tr>
					<tr>
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							<%-- PIO --%>
							<tr>
								<td colspan="4" width="340">
									<table width="100%">
									<tr class="admin">
										<td colspan="2">
											<strong> <%=getTran(request,"ophtalmology.consultation.ver","salud.ocular",sWebLanguage)%>  </strong>&nbsp;
										</td>
										<td></td>
										<td></td>
									</tr>
									</table>
								</td>
								<td colspan="2" rowspan="23" class="admin2" width="500">
									<table width="100%">
									<tr class="admin">
										<td colspan="2" width="150">
											<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_DRAWING", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_DRAWING\",\"div_drawing\");'")%> &nbsp; <strong> <%=getTran(request,"ophtalmology.consultation.ver","drawing",sWebLanguage)%>  </strong>&nbsp;
											<img src='<%=sCONTEXTPATH%>/_img/icons/icon_minus.png' onclick='document.getElementById("drawing").style.display="none";'/> <img src='<%=sCONTEXTPATH%>/_img/icons/icon_plus.png' onclick='document.getElementById("drawing").style.display="";'/>
										</td>
										<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
										<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
									</tr>
									<tr class="div_drawing">
										<td colspan="4">
											<div id='drawing' style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OCDRAWING").length()>0?"":""%>'>  <%-- ITEM_TYPE_OC_VER_DRAWING --%>
												<%=ScreenHelper.createDrawingDiv(request, "canvasDiv", "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OCDRAWING", transaction, MedwanQuery.getInstance().getConfigString("defaultEyeDiagram","/_img/ophtalmo_ver.png"),"eyes.image") %>
											</div>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<%-- PIO --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_PIO", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_PIO\",\"div_pio\");'")%> &nbsp; <strong> <%=getTran(request,"ophtalmology.consultation.ver","pio",sWebLanguage)%>  </strong>&nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<tr class="div_pio">
								<td class="admin" width="150" colspan="2"><%=getTran(request,"ophtalmology.consultation.ver","pio",sWebLanguage)%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_PIO_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PIO_OD" property="itemId"/>]>.value" value="<%=sPIO_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;mmHg
								</td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_PIO_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PIO_OI" property="itemId"/>]>.value" value="<%=sPIO_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;mmHg
								</td>
							</tr>
							<%-- AGUDEZA VISUAL --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_AGUDEZAVISUAL", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_AGUDEZAVISUAL\",\"div_agudezavisual\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","agudeza_visual",sWebLanguage)%> &nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","SE",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OI", 10)%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","AE",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OI", 10)%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","CC",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OI", 10)%></td>
							</tr>
							<%-- RETRACCION FINAL --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_RETRACCIONFINAL", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_RETRACCIONFINAL\",\"div_retraccion\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","retraccion_final",sWebLanguage)%> &nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<tr class="div_retraccion">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","esfera",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_ESF_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_ESF_OI", 10)%></td>
							</tr>
							<tr class="div_retraccion">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","cilindro",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_CIL_OD", 10)%> D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> </td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_CIL_OI", 10)%> D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> </td>
							</tr>
							<tr class="div_retraccion">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","eje",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_EJE_OD", 10)%> &nbsp;Dx  &#176;</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_EJE_OI", 10)%> &nbsp;Dx  &#176;</td>
							</tr>
							<tr class="div_retraccion">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","dip",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_DIP_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_RETRACCIONFINAL_DIP_OI", 10)%></td>
							</tr>
							<%-- BIOMICROSCOPIA --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_BIOMICROSCOPIA", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_BIOMICROSCOPIA\",\"div_biomicroscopia\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","biomicroscopia",sWebLanguage)%> &nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<%-- PARPADOS --%>
							<tr class="div_biomicroscopia">
								<td class="admin"><%=getTran(request,"ophtalmology.consultation.ver","parpados",sWebLanguage)%></td>
								<td class="admin"></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_PARPADO_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_PARPADO_OI", 30, 1)%>
								</td>
							</tr>
							<%-- BIOMICROSCOPIA -1. COnjuntiva --%>
							<tr class="div_biomicroscopia">
								<td colspan="2" class="admin"><%=getTran(request,"ophtalmology.consultation.ver","conjuntiva",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CONJUNTIVA_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CONJUNTIVA_OI", 30, 1)%>
								</td>
							</tr>							
							<%-- BIOMICROSCOPIA -2. Cornea --%>
							<tr class="div_biomicroscopia">
								<td colspan="2" class="admin"><%=getTran(request,"ophtalmology.consultation.ver","cornea",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CORNEA_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CORNEA_OI", 30, 1)%>
								</td>
							</tr>
							<%-- BIOMICROSCOPIA -3. CA --%>
							<tr class="div_biomicroscopia">
								<td colspan="2" class="admin"><%=getTran(request,"ophtalmology.consultation.ver","CA",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CAMARAANTERIOR_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CAMARAANTERIOR_OI", 30, 1)%>
								</td>
							</tr>
							<%-- BIOMICROSCOPIA -4. Iris --%>
							<tr class="div_biomicroscopia">
								<td colspan="2" class="admin"><%=getTran(request,"ophtalmology.consultation.ver","iris",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_IRIS_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_BIOMICROSCOPIA_IRIS_OI", 30, 1)%>
								</td>
							</tr>
							<%-- BIOMICROSCOPIA -5. Cristalino --%>
							<tr class="div_biomicroscopia">
								<td colspan="2" class="admin"><%=getTran(request,"ophtalmology.consultation.ver","cristalino",sWebLanguage)%></td>
								<td class="admin2">
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OD" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OD;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","cortical",sWebLanguage)%><br/>
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OD" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OD;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","nuclear",sWebLanguage)%><br/>
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OD" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OD;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","C/P",sWebLanguage)%><br/>
									<select id='cdo9' class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OD" property="itemId"/>]>.value" onchange="">
										<%=ScreenHelper.writeSelectUpperCase("cdo.ver.1",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OD"),sWebLanguage,false,false)%>
									</select>
								</td>
								<td class="admin2">
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OI" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_COR_OI;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","cortical",sWebLanguage)%><br/>
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OI" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_NUC_OI;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","nuclear",sWebLanguage)%><br/>
									<input type="checkbox" class="hand" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OI" property="itemId"/>]>.value" <mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_CAP_OI;value=medwan.common.true" property="value" outputString="checked"/> value="medwan.common.true"/>&nbsp;<%=getTran(request,"ophtalmology.consultation.ver","C/P",sWebLanguage)%><br/>
									<select id='cdo9' class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OI" property="itemId"/>]>.value" onchange="">
										<%=ScreenHelper.writeSelectUpperCase("cdo.ver.1",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_BIOMICROSCOPIA_CRISTALINO_AFA_OI"),sWebLanguage,false,false)%>
									</select>
								</td>
							</tr>
							<%-- FONDO DE OJO  --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_FONDOOJO", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_FONDOOJO\",\"div_fondodeojo\");'")%> &nbsp; <strong> <%=getTran(request,"ophtalmology.consultation.ver","fondo.de.ojo",sWebLanguage)%>  </strong>&nbsp;
								</td>
								<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td width="150"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>	
							<%-- FONDO DE OJO - 1 --%>
							<tr class="div_fondodeojo">
								<td class="admin" colspan="2"><%=getTran(request,"ophtalmology.consultation.ver","nervio.optico",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_NERVIOOPTICO_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_NERVIOOPTICO_OI", 30, 1)%>
								</td>
							</tr>
							<%-- FONDO DE OJO - 2--%>
							<tr class="div_fondodeojo">
								<td class="admin" colspan="2"><%=getTran(request,"ophtalmology.consultation.ver","macula",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_MACULA_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_MACULA_OI", 30, 1)%>
								</td>
							</tr>
							<%-- FONDO DE OJO - 3--%>
							<tr class="div_fondodeojo">
								<td class="admin" colspan="2"><%=getTran(request,"ophtalmology.consultation.ver","periferia",sWebLanguage)%></td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_PERIFERIA_OD", 30, 1)%>
								</td>
								<td class="admin2">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_FONDOOJO_PERIFERIA_OI", 30, 1)%>
								</td>
							</tr>							
						</table>
						</td>
					</tr>
					<!-- CAMERA ITEM -->
					<tr class="admin">
						<td colspan="8"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_CAMARA", sWebLanguage, "onchange='showtests();'")%>&nbsp; <%=getTran(request,"ophtalmology.consultation.ver","camara",sWebLanguage)%> &nbsp;</td>
					</tr>
					<link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/bootstrap.min.css" rel="stylesheet">
				    <link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/icon.css" rel="stylesheet">
				    <link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/webcam.css" rel="stylesheet" type="text/css" media="screen">
				    <script src="<%=sCONTEXTPATH%>/projects/optica/_common/_script/jquery-3.3.1.min.js"></script>
			        <tr id='camera_items' style='display: none'>
			            <td class="admin" width="110" style='vertical-align: top'>
			            	<%=getTran(request,"web","camara",sWebLanguage)%>
			            	<%-- <img src='<%=sCONTEXTPATH%>/_img/icons/icon_minus.png' onclick='document.getElementById("photo").style.display="none";'/> --%>
							<img src='<%=sCONTEXTPATH%>/_img/icons/icon_plus.png' onclick='document.getElementById("photo").style.display="";'/>
			            	<main id="webcam-app" width="110" >
								<div class="form-control webcam-start" id="webcam-control">
										<label class="form-switch">
										<input type="checkbox" id="webcam-switch">
										<i></i> <br>
										<span id="webcam-caption">Abrir Camara</span>
										</label>      
										<!-- <button id="cameraFlip" class="btn d-none"></button>  -->             
								</div>
								<div id="errorMsg" class="col-12 col-md-6 alert-danger d-none">
									No se pudo iniciar la camara. Permita permiso para acceder a la camara. <br/>
									Se recomienda abrir a traves de navegadores como Chrome.
									<!--  <button id="closeError" class="btn btn-primary ml-3">OK</button> -->
								</div>
								<div class="md-modal md-effect-12">
									<div id="app-panel" class="app-panel md-content row p-0 m-0">     
										<div id="webcam-container" class="webcam-container col-12 d-none p-0 m-0">
											<video id="webcam" autoplay="autoplay" playsinline="playsinline"></video>
											<canvas id="canvasPhoto" class="canvasPhoto d-none" width="600" height="400"></canvas>
											<div class="flash"></div>
											<!-- <audio id="snapSound" src="audio/snap.wav" preload = "auto"></audio>  -->
										</div>
										<div id="cameraControls" class="cameraControls">
											<a href="#" id="exit-app" title="Cerrar camara" class="d-none"><i class="material-icons">exit_to_app</i></a>
											<a href="#" id="take-photo" title="Tomar Foto"><i class="material-icons">camera_alt</i></a>
											<!--  <a href="#" id="download-photo" download="Foto-id-<%=activePatient.personid%>.png" target="_blank" title="Guardar Foto" class="d-none"><i class="material-icons">file_download</i></a>  -->
											<a href="#" id="download-photo" title="Guardar Foto" class="d-none"><i class="material-icons">file_download</i></a>  
											<a href="#" id="resume-camera"  title="Retornar a Camara" class="d-none"><i class="material-icons">camera_front</i></a>
											<a href="#" id="grabar-photo" title="Grabar" class="d-none"><i class="material-icons">save</i></a>
										</div>
									</div>        
								</div>
								<div class="md-overlay"></div>
							</main>
			            </td>
			        	<td class='admin2' colspan='7'>
							<div id='photo' style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO").length()>0?"":""%>'>
								<input type="hidden" id="photoId" class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO" property="value"/>">
								<a href="<%=sCONTEXTPATH%>/scan/photo/<%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO")%>" id="photoLink" target="_blank" style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO").length()>0?"":"none"%>'>
									<% if(((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO").length()>0) { %>
										<img src="<%=sCONTEXTPATH%>/scan/photo/<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_CAMARA_PHOTO" property="value"/>" title="photoImg" id="photoImg" name="photoImg" width="479px" />
									<% } else { %>
										<img src="" title="photoImg" id="photoImg" name="photoImg" width="479px" />
								<% } %>
								</a>
						    </div>
			        	</td>
			        </tr>
					<%-- ADICIONALES --%>
					<tr class="admin" style="height: 1px;">
						<td colspan="8">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							<%-- OBSERVACION --%>
							<tr>
								<td class="admin" colspan="3"><%=getTran(request,"ophtalmology.consultation.ver","seguimiento",sWebLanguage)%></td>
								<td class="admin2" colspan="4">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_SEGUIMIENTO", 90, 8)%>
								</td>
							</tr>
							<%-- PROXIMA CITA --%>
							<tr>
								<td class="admin"><%=getTran(request,"ophtalmology.consultation.ver","proxima_cita",sWebLanguage)%>&nbsp;</td>
								<td class="admin2" colspan="4">
									<select class='text' id='frequency' onchange='calculateNextDate()'>
										<option/>
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='3'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
										<option value='6'>6</option>
										<option value='7'>7</option>
										<option value='8'>8</option>
										<option value='9'>9</option>
										<option value='10'>10</option>
										<option value='11'>11</option>
										<option value='12'>12</option>
									</select>
									<select class='text' id='frequencytype' onchange='calculateNextDate()'>
										<option/>
										<option value='week'><%=getTran(request,"web","weeks",sWebLanguage) %></option>
										<option value='month'><%=getTran(request,"web","months",sWebLanguage) %></option>
										<option value='year'><%=getTran(request,"web","year",sWebLanguage) %></option>
									</select>
									<input type="hidden" id="ccbrtdate" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PROXIMA_CITA" property="itemId"/>]>.value"/>
									<%=(ScreenHelper.writeDateField("vafollowupdate", "transactionForm", ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PROXIMA_CITA"), false, true, sWebLanguage, sCONTEXTPATH))%>
								</td>
								<td class="admin2" colspan="2">
									<a href="javascript:openPopup('planning/findPlanning.jsp&isPopup=1&FindDate='+document.getElementById('vafollowupdate').value+'&FindUserUID=',1024,600,'Agenda','toolbar=no,status=yes,scrollbars=no,resizable=yes,width=1024,height=600,menubar=no');void(0);"><%=getTran(request,"web","findappointment",sWebLanguage) %></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>	
			</td>
            <%-- COLUMNA DERECHA --%>
            <td class="admin2" width="28%" style="vertical-align:top;padding-top:0">
				<%-- DIAGNOSTICOS --%>
				<%ScreenHelper.setIncludePage(customerInclude("healthrecord/diagnosesEncoding.jsp"),pageContext);%>
				<br/>
				<%-- MEDICAMENTOS --%>
				<table width="100%" class="list" cellspacing="1">
					<tr class="admin">
						<td align="center">
							<%-- <a href="javascript:openPopup('medical/managePrescriptionsPopup.jsp&amp;skipEmpty=1',900,400,'medication');void(0);"> --%>
								<%=getTran(request,"Web.Occup","medwan.healthrecord.medication",sWebLanguage)%>
							<%-- </a> --%>
						</td>
					</tr>
					<%
						Vector paperprescriptions = PaperPrescription.find(activePatient.personid,"",ScreenHelper.stdDateFormat.format(((TransactionVO)transaction).getUpdateTime()),ScreenHelper.stdDateFormat.format(((TransactionVO)transaction).getUpdateTime()),"","DESC");
						if(paperprescriptions.size()>0){
							out.print("<tr><td><table width='100%'>");
							String l="";
							for(int n=0;n<paperprescriptions.size();n++){
								if(l.length()==0){
									l="1";
								}
								else{
									l="";
								}
								PaperPrescription paperPrescription = (PaperPrescription)paperprescriptions.elementAt(n);
								Vector products =paperPrescription.getProducts();
								
								String vProducts = "";
								for(int i=0;i<products.size();i++){
									vProducts = vProducts + products.elementAt(i) + "WWWWWWWWWW";
								}
								//vProducts = vProducts.replaceAll("\n", "");
								vProducts = vProducts.replaceAll("\r", "");
								//out.println(vProducts);
								out.println("<tr class='list"+l+"' id='pp"+paperPrescription.getUid()+"'>");
								out.println("<td valign='top' width='90px'>");
								out.println("<img src='_img/icons/icon_edit1.gif' style='cursor:hand;' onclick='openPopup(\"medical/managePrescriptionForm.jsp&amp;op=edit&amp;date="+ScreenHelper.stdDateFormat.format(paperPrescription.getBegin())+"&amp;products="+vProducts+"\",850,430,\"medication\");void(0);'/>");
								out.println("<img src='_img/icons/icon_print.png' style='cursor:hand;' onclick='openPopup(\"medical/managePrescriptionForm.jsp&amp;op=print&amp;date="+ScreenHelper.stdDateFormat.format(paperPrescription.getBegin())+"&amp;products="+vProducts+"\",850,430,\"medication\");void(0);'/>");
								out.println("<img src='_img/icons/icon_delete.png' style='cursor:hand;' onclick='deletepaperprescription(\""+paperPrescription.getUid()+"\");'/>");
								out.println("<b>"+ScreenHelper.stdDateFormat.format(paperPrescription.getBegin())+"</b></td><td><i>");
								for(int i=0;i<products.size();i++){
									out.print(products.elementAt(i)+"<br/>");
								}
								out.println("</i></td>");
								out.println("</tr>");
								
							}
							out.print("</table></td></tr>");
						}
					%>
					<tr>
						<td>
						<a href="javascript:openPopup('medical/managePrescriptionForm.jsp&amp;skipEmpty=1',850,430,'medication');void(0);"><%=getTran(request,"web","medicationpaperprescription",sWebLanguage)%></a></td>
					</tr>
				</table>
				<br/>
				<%-- IMAGENES PACS --%>
				<table class="list" width="100%" cellspacing="0" cellpadding="2">
				  <tbody><tr class="admin">
				    <td align="center">IMAGENES PACS</td>
				  </tr>
				  <tr>
				    <td>
					  <table width="100%">
						<tbody>
						<tr class="admin">
							<!--  <td>Date</td> -->
							<td>Identificadores de estudio</td>
							<td>Parte del cuerpo</td>
							<!-- <td>Modalidad</td>
							<td>Fabricante</td>  -->
							<td colspan="2">Visualizaci&oacute;n</td>
							<!-- <td>Ver en STONE</td> -->
						</tr>
						<%
		    			   if(EstudiosImagen.size()<1) { 
		    			%>
		    				<tr>
		    					<td class="admin2" colspan="5">No hay imagen disponible para este paciente cargadas en PACS</td>
		    				</tr>
		    			<%	 
		    			 } else {
		    				for(int i=0; i<EstudiosImagen.size(); i++){
		    					Object a = EstudiosImagen.get(i);
		    					String[] b = a.toString().split(",");
		    					String dni = b[0].substring(b[0].indexOf("=")+1, b[0].length());
		    					String url_stones = "http://server:8042/stone-webviewer/index.html?study="+b[1].substring(b[1].indexOf("=")+1, b[1].length()-0); 
								String url_osimis = "http://server:8042/osimis-viewer/app/index.html?study="+b[2].substring(b[2].indexOf("=")+1, b[2].length()-1); 
		    				%>
		    					<tr>
		    						<td class="admin2">PACS-<%=i+1%>-<%=dni%></td>
		    						<td class="admin2">EYE</td>
		    						<!-- <td class="admin2">OT</td>
		    						<td class="admin2">Optomed Plc.</td> -->
		    						<td class="admin2"><a href="<%=url_osimis%>" target="_blank">Ver en OSIMIS </a></td>
									<td class="admin2"><a href="<%=url_stones%>" target="_blank">Ver en STONE </a></td>
		    					</tr>
		 	    	    <%	    
		 	    	        }
		    			 }
		    			%>
						</tbody>
					   </table>
					</td>
				  </tr>
				  </tbody>
				</table>
				<br/>
				<%-- IMAGENES JPG / PDF --%>
				<table class="list" width="100%" cellspacing="0" cellpadding="2">
				  <tbody><tr class="admin">
				    <td align="center">OTROS EXAMENES</td>
				  </tr>
				  <tr>
				    <td>
					  <table width="100%">
						<tbody>
						<tr class="admin">
							<!--  <td>Date</td> -->
							<td>Identificadores de estudio</td>
							<td>Tipo</td>
							<td>Visualizaci &#176;n</td>
						</tr>
						</tbody>
					   </table>
					</td>
				  </tr>
				  </tbody>
				</table>
			</td>			
		</tr>
        <tr style="display:none;">
            <td style="padding:0">           
			    <table class="list" cellspacing="1" cellpadding="0" width="100%">
			        <%-- anamnese --%>
			        <tr>
			            <td class="admin" colspan="3"><%=getTran(request,"openclinic.chuk","anamnese",sWebLanguage)%></td>
			            <td class="admin2" colspan="4">
			                <textarea onKeyup="resizeTextarea(this,10);limitChars(this,255);" <%=setRightClick(session,"ITEM_TYPE_OPHTALMOLOGY_CONSULTATION_ANAMNESE")%> class="text" cols="80" rows="2" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPHTALMOLOGY_CONSULTATION_ANAMNESE" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPHTALMOLOGY_CONSULTATION_ANAMNESE" property="value"/></textarea>
			            </td>
			        </tr>
                </table>
            </td>
		</tr>
    </table>
        
	<%-- BUTTONS --%>
	<%=ScreenHelper.alignButtonsStart()%>
	    <%=getButtonsHtml(request,activeUser,activePatient,"occup.ophtalmology.consultation",sWebLanguage)%>
	<%=ScreenHelper.alignButtonsStop()%>

    <%=ScreenHelper.contextFooter(request)%>
</form>
<input type="hidden" id="dataImg" name="dataImg" />
<form id="FormPhoto" method="post" action="uploadphoto.jsp" style="visibility:hidden;">						
 	<input type="hidden" id="filenameImg" name="filenameImg" />
 	<input type="submit" id="submitFormPhoto" name="submitFormPhoto" />
</form>
<%--
<form name="printPrescriptionForm" method="post" action="<c:url value='/'/>medical/createPrescriptionPdf.jsp">
	<textarea class="text" name="prescription" id="prescription" rows="1" cols="65">Medicamentos</textarea><br/>
	<input type="text" class="text" size="10" maxLength="10" name="prescriptiondate" value="<%=ScreenHelper.stdDateFormat.format(new java.util.Date())%>" id="prescriptiondate">
	<input type="text" name="personid" id="personid" value="<%=activePatient.personid%>"/> 
	<input type="hidden" name="rxnormcodes" id="rxnormcodes" value=""/>
	<input type="hidden" name="rxnormnames" id="rxnormnames" value=""/>
	<input type="button" class="button" name="print" value="<%=getTranNoLink("web","print",sWebLanguage)%>" onclick="printprescription('asdasda asdasd ');"/>
</form>
--%>
<script>
showtests2("ITEM_TYPE_OC_VER_PIO","div_pio");
showtests2("ITEM_TYPE_OC_VER_AGUDEZAVISUAL","div_agudezavisual");
showtests2("ITEM_TYPE_OC_VER_RETRACCIONFINAL","div_retraccion");
showtests2("ITEM_TYPE_OC_VER_BIOMICROSCOPIA","div_biomicroscopia");
showtests2("ITEM_TYPE_OC_VER_FONDOOJO","div_fondodeojo");
showtests2("ITEM_TYPE_OC_VER_AV_ACTUAL","div_avactual");
showtests2("ITEM_TYPE_OC_VER_DRAWING","div_drawing");


function printprescription(vPrescription){
	prescription.value = vPrescription;
	// prescriptiondate = vPrescriptiondate;
	printPrescriptionForm.submit();
}
   function showtests(){
	  if(document.getElementById('ITEM_TYPE_OC_VER_MOTIVOCONSULTA').checked){
		  document.getElementById('admissionreasons_items').style.display='';
	  }
	  else{
		  document.getElementById('admissionreasons_items').style.display='none';
	  }
	  if(document.getElementById('ITEM_TYPE_OC_VER_ANTECED').checked){
		  document.getElementById('history_items').style.display='';
	  }
	  else{
		  document.getElementById('history_items').style.display='none';
	  }
	  if(document.getElementById('ITEM_TYPE_OC_VER_CAMARA').checked){
		  document.getElementById('camera_items').style.display='';
	  }
	  else{
		  document.getElementById('camera_items').style.display='none';
	  }
  }
   
   function showtests2(item,clase){
     const boxes = document.getElementsByClassName(clase);
     if(document.getElementById(item).checked){
    	 for (const box of boxes) {
    		box.style.display = '';
    	 }
	 }
	 else{
		 for (const box of boxes) {
			box.style.display = 'none';
		 }
	 }
   }

  function calculateNextDate(){
	  var nextdate = new Date();
	  if(document.getElementById('frequencytype').value.length>0 && document.getElementById('frequency').value.length>0){
		  if(document.getElementById('frequencytype').value=='month'){
			  nextdate.setMonth(nextdate.getMonth()+document.getElementById('frequency').value*1);
		  }
		  else if(document.getElementById('frequencytype').value=='year'){
			  nextdate.setYear(nextdate.getFullYear()+document.getElementById('frequency').value*1);
		  }
		  else if(document.getElementById('frequencytype').value=='week'){
			  nextdate.setDate(nextdate.getDate()+document.getElementById('frequency').value*7);
		  }
		  document.getElementById('vafollowupdate').value=("0"+nextdate.getDate()).substring(("0"+nextdate.getDate()).length-2,("0"+nextdate.getDate()).length)+"/"+("0"+(nextdate.getMonth()+1)).substring(("0"+(nextdate.getMonth()+1)).length-2,("0"+(nextdate.getMonth()+1)).length)+"/"+nextdate.getFullYear();
  	 }
  }

  <%-- SUBMIT FORM --%>
  function submitForm(){
    if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
      alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
	  searchEncounter();
	}	
    else{
		document.getElementById("ccbrtdate").value=document.getElementById("vafollowupdate").value;
		if(document.getElementById("drawing").style.display=='none'){
	    	document.getElementById("canvasDivDrawing").value='';
	    }
		document.getElementById("complaints").value = "";
		var inputs = document.getElementsByTagName("input");
		for(var i = 0; i < inputs.length; i++) {
			if(inputs[i].name.indexOf("complaints.")==0 && inputs[i].checked) {
				document.getElementById("complaints").value+="*"+inputs[i].value+"*";
			}
		}
		var temp = Form.findFirstElement(transactionForm);//for ff compatibility
		//document.getElementById("buttonsDiv").style.visibility = "hidden";
		//document.transactionForm.submit();
		<%
			SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
			out.print(takeOverTransaction(sessionContainerWO, activeUser,"document.transactionForm.submit();"));
		%>
    }
  }
  
  function searchEncounter(){
    openPopup("/_common/search/searchEncounter.jsp&ts=<%=getTs()%>&Varcode=encounteruid&VarText=&FindEncounterPatient=<%=activePatient.personid%>");
  }
  
  if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
    alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
    searchEncounter();
  }	
  
  <%-- IS MY NUMBER --%>
  function isMyNumber(sObject){
    if(sObject.value==0) return false;
    sObject.value = sObject.value.replace(",",".");
    var string = sObject.value;
    var vchar = "01234567890.+-";
    var dotCount = 0;
    for(var i=0; i < string.length; i++){
      if(vchar.indexOf(string.charAt(i)) == -1){
        sObject.value = "";
        return false;
      }
      else{
        if(string.charAt(i)=="."){
          dotCount++;
          if(dotCount > 1){
            sObject.value = "";
            return false;
          }
        }
        if((string.charAt(i)=="-")||(string.charAt(i)=="+")){
          if(i > 0){
            sObject.value = "";
            return false;
          }
        }
      }
    }
    if(sObject.value.length > 250){
      sObject.value = sObject.value.substring(0,249);
    }
    return true;
  }

  showtests();
</script>


<script src="<%=sCONTEXTPATH%>/projects/optica/_common/_script/webcam.js"></script>
<script type="text/javascript">
function appendFileAndSubmit(){
    var form = document.getElementById("FormPhoto");
    $j("#filenameImg").val(""+$j("#serverId").val()+"-"+$j("#transactionId").val()+"-"+<%=activePatient.personid%>);
    var ImageURL = $j("#dataImg").val();// "data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    				// $j("#dataImg").val();//
    var block = ImageURL.split(";");
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    var blob = b64toBlob(realData, contentType);
    var fd = new FormData(form);
    fd.append("image", blob); // image file
    // fd.append("name", "Ronnie");
    // Submit Form and upload file
    $j.ajax({
        url: "projects/optica/healthrecord/uploadphoto.jsp", // uploadphoto.jsp
        data: fd,// the formData function is available in almost all new browsers.
        type:"POST",
        contentType: false, // "multipart/form-data", // "multipart/form-data", //false,
        processData: false,
        cache: false,
        dataType: "html", //"json", // Change this according to your response from the server.  html
        error:function(err){
        	console.log("Request error:");
            console.log(err);
        },
        success:function(data){
        	console.log("Request success: ");
            // console.log(data);
            let photoname = data.replace(/\s+/g, '');
			console.log(photoname);
            $j("#photoId").val(photoname); // "openclinic/scan/photo/"+  replace(/\s+/g, '')
            //document.getElementById("webcam-app").style.backgroundImage = "url('scan/photo/img-12345678.jpg')";
            document.getElementById("photoImg").src='scan/photo/' + photoname;
            document.getElementById("photoLink").href='scan/photo/' + photoname;
            document.getElementById("photoLink").style.display='block';
            document.getElementById("photo").style.display='block';
            $j("#exit-app").click();
        },
        complete:function(){
            console.log("Request finished:");
        }
    });
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}
</script>

<%=writeJSButtons("transactionForm","saveButton")%>