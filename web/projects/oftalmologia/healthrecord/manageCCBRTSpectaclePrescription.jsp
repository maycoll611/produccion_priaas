<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>

<%=checkPermission(out,"occup.spectacleprescription","select",activeUser)%>

<form name="transactionForm" id="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>'>
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
    
    <input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
    <input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
    <input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>
    <input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>
    
    <%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>
    <%=contextHeader(request,sWebLanguage)%>
	
    <table class="list" width="100%" cellspacing="1">
		<tr>
			<td style="vertical-align:top;padding:0;" class="admin2">    
			    <table class="list" width="100%" cellspacing="1">
			        <%-- DATE --%>
			        <tr>
			            <td class="admin" width="<%=sTDAdminWidth%>">
			                <a href="javascript:openHistoryPopup();" title="<%=getTranNoLink("web.occup","History",sWebLanguage)%>">...</a>&nbsp;
			                <%=getTran(request,"web.occup","medwan.common.date",sWebLanguage)%>
			            </td>
			            <td class="admin2">
			                <input type="text" class="text" size="12" maxLength="10" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" id="trandate" OnBlur='checkDate(this)'>
			                <script>writeTranDate();</script>
			            </td>
			        </tr>
			        <%-- SPECTACLE PRESCRIPTION --%>
			        <tr class='admin'>
			        	<td colspan='2'><%=getTran(request,"web","spectacleprescription",sWebLanguage)%></td>
			        </tr>
			        <tr>
			            <td class="admin"><%=getTran(request,"web","prescription",sWebLanguage)%>&nbsp;</td>
			        	<td>
			        		<table width='100%' cellspacing='0' cellpadding='0'>
			        			<tr>
			        				<td class="admin">
										<select id='cdo.type.lentes' class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T1" property="itemId"/>]>.value">
											<%=ScreenHelper.writeSelectUpperCase("cdo.type.lentes",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T1"),sWebLanguage,false,false)%>
										</select>
									</td>
			            			<td class="admin" width='20%'><%=getTran(request,"web.optical","sphere",sWebLanguage)%>&nbsp;</td>
			            			<td class="admin" width='20%'><%=getTran(request,"web.optical","cylinder",sWebLanguage)%>&nbsp;</td>
			            			<td class="admin" width='20%'><%=getTran(request,"web.optical","axis",sWebLanguage)%>&nbsp;</td>
									<td class="admin" width='20%'><%=getTran(request,"web.optical","dip",sWebLanguage)%>&nbsp;</td>
									<td class="admin" width='10%'>&nbsp; </td>
			        			</tr>
			        			<tr>
			        				<%
										String sphere_right = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_SPHERE");
										String sphere_left = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_SPHERE");
										String cylinder_right = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_CYLINDER");
										String cylinder_left = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_CYLINDER");
										String axis_right = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_AXIS");
										String axis_left = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_AXIS");
										String add_right = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_ADD_OD_SPHERE");
										String add_left = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_ADD_OS_SPHERE");
										String pddistance = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_PDDISTANCE");
										String remark = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTICAL_COMMENT");
										String item_t1_dip = ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T1_DIP");
			        					if(((TransactionVO)transaction).getTransactionId()<0){
			        						sphere_right=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_RIGHT_1");
			        						sphere_left=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_LEFT_1");
			        						cylinder_right=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_RIGHT_2");
			        						cylinder_left=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_LEFT_2");
			        						axis_right=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_RIGHT_3");
			        						axis_left=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_LEFT_3");
			        						add_right=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_ADD_L");
			        						add_left=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REFRACTION_ADD_R");
			        						pddistance=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_RECOMMANDATION");
			        						remark=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_REMARKS");
											item_t1_dip=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T1_DIP");
			        					}
									%>
			            			<td class="admin"><%=getTran(request,"web.optical","od",sWebLanguage)%>&nbsp;</td>
			            			<td class="admin2"><input style='text-align: center' size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_SPHERE" property="itemId"/>]>.value" value="<%=sphere_right %>"></td>
			            			<td class="admin2"><input style='text-align: center' size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_CYLINDER" property="itemId"/>]>.value" value="<%=cylinder_right%>"></td>
			            			<td class="admin2"><input style='text-align: center' size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OD_AXIS" property="itemId"/>]>.value" value="<%=axis_right %>"></td>
									<td class="admin2"><input style='text-align: center' size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T1_DIP" property="itemId"/>]>.value" value="<%=item_t1_dip %>"></td>
									<td class="admin2">&nbsp;</td>
			        			</tr>
			        			<tr>
			            			<td class="admin"><%=getTran(request,"web.optical","os",sWebLanguage)%>&nbsp;</td>
			            			<td class="admin2"><input style='text-align: center'  size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_SPHERE" property="itemId"/>]>.value" value="<%=sphere_left %>"></td>
			            			<td class="admin2"><input style='text-align: center'  size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_CYLINDER" property="itemId"/>]>.value" value="<%=cylinder_left%>"></td>
			            			<td class="admin2"><input style='text-align: center'  size="5" class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_DISTANCE_OS_AXIS" property="itemId"/>]>.value" value="<%=axis_left %>"></td>
									<td class="admin2">&nbsp;</td>
									<td class="admin2">&nbsp;</td>
			        			</tr>
								<tr>
									<td colspan='6'>&nbsp;<td/>
								</tr>
								<tr>
			            			<td class="admin">
										<select id='cdo.type.lentes' class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2" property="itemId"/>]>.value">
											<%=ScreenHelper.writeSelectUpperCase("cdo.type.lentes",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2"),sWebLanguage,false,false)%>
										</select>
									</td>
									<td class="admin2" colspan='3'>
										<input onKeyup="this.value=this.value.toUpperCase();" type="text" id="ITEM_TYPE_T2_ADDITIONAL" class="text ITEM_TYPE_T2" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2_ADDITIONAL" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2_ADDITIONAL" translate="false" property="value"/>" style="width: 80%;"/>
									</td>
									<td class="admin2">
										<input style='text-align: center' size="5" id="ITEM_TYPE_T2_DIP" class="text ITEM_TYPE_T2" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2_DIP" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T2_DIP" property="value"/>">
									</td>
									<td class="admin2">&nbsp;</td>
			        			</tr>
								<tr>
									<td colspan='6'>&nbsp;<td/>
								</tr>
								<tr>
			            			<td class="admin">
										<select id='cdo.type.lentes' class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3" property="itemId"/>]>.value">
											<%=ScreenHelper.writeSelectUpperCase("cdo.type.lentes",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3"),sWebLanguage,false,false)%>
										</select>
									</td>
									<td class="admin2" colspan='3'>
										<input onKeyup="this.value=this.value.toUpperCase();" type="text" id="ITEM_TYPE_T3_ADDITIONAL" class="text ITEM_TYPE_T3" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3_ADDITIONAL" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3_ADDITIONAL" translate="false" property="value"/>" style="width: 80%;"/>
									</td>
									<td class="admin2">
										<input style='text-align: center' size="5" id="ITEM_TYPE_T3_DIP" class="text ITEM_TYPE_T3" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3_DIP" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_T3_DIP" property="value"/>">
									</td>
									<td class="admin2">&nbsp;</td>
			        			</tr>
			        		</table>
			        	</td>
			        </tr>
			        <tr>
			        	<td class="admin"><%=getTran(request,"web.optical","comment",sWebLanguage)%>&nbsp;</td>
			        	<td class='admin2'>
			        		<table width='100%'>
			        			<tr>
			        				<td>
			        					<textarea onKeyup="resizeTextarea(this,10);limitChars(this,255);" <%=setRightClick(session,"ITEM_TYPE_OPTICAL_COMMENT")%> class="text" cols="120" rows="2" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTICAL_COMMENT" property="itemId"/>]>.value"><%=remark %></textarea>
			        				</td>
			        			</tr>
			        		</table>
			        	</td>
			        </tr>
                </table>
			</td>
		</tr>
    </table>
    
	<%-- BUTTONS --%>
	<%=ScreenHelper.alignButtonsStart()%>
	    <%=getButtonsHtmlA5(request,activeUser,activePatient,"occup.spectacleprescription",sWebLanguage)%>
	<%=ScreenHelper.alignButtonsStop()%>
	
	<%//=ScreenHelper.writePrintButton(sWebLanguage)%>

    <%=ScreenHelper.contextFooter(request)%>
</form>

<script>
  <%-- SUBMIT FORM --%>
  function submitForm(){
    transactionForm.saveButton.disabled = true;
    <%
        SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
        out.print(takeOverTransaction(sessionContainerWO,activeUser,"document.transactionForm.submit();"));
    %>
  }
  
  <%-- SEARCH MANAGER --%>
  function searchManager(managerUidField,managerNameField){
    openPopup("/_common/search/searchUser.jsp&ts=<%=getTs()%>&ReturnUserID="+managerUidField+"&ReturnName="+managerNameField+"&displayImmatNew=no&FindServiceID=<%=MedwanQuery.getInstance().getConfigString("socialServiceID","CNAR.SOC")%>");
    EditEncounterForm.EditEncounterManagerName.focus();
  }      
</script>
    
<%=writeJSButtons("transactionForm","saveButton")%>