<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>
<%=checkPermission(out,"occup.ccbrt.ophtalmologyConsultation","select",activeUser)%>

<form name="transactionForm" id="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>'>
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
   
    <input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
    <input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
    <input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>
    <input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>
    
    <%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>
    <%=contextHeader(request,sWebLanguage)%>
    <%
	   Vector EstudiosImagen = activePatient.getImagesPacsPatientByPersonId(activePatient.personid);
	%>
    <table class="list" width="100%" cellspacing="1">
        <%-- DATE --%>
        <tr>
            <td colspan='2' class="admin" width="<%=sTDAdminWidth%>">
                <a href="javascript:openHistoryPopup();" title="<%=getTranNoLink("Web.Occup","History",sWebLanguage)%>">...</a>&nbsp;
                <%=getTran(request,"Web.Occup","medwan.common.date",sWebLanguage)%>
                <input type="text" class="text" size="12" maxLength="10" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" id="trandate" OnBlur='checkDate(this)'>
                <script>writeTranDate();</script>
            </td>
        </tr>

        <%-- DESCRIPTION --%>
        <tr>
        	<td width="60%">
	        	<table width='100%'>
	        		 <tr style="display:none;">
	           			<%
	           				//Find last optometric examination
	           				TransactionVO optometry = MedwanQuery.getInstance().getLastTransactionByType(Integer.parseInt(activePatient.personid), "be.mxs.common.model.vo.healthrecord.IConstants.TRANSACTION_TYPE_CCBRT_OPTOMETRIST_EXAMINATION");
	           			%>
			            <td class="admin"><%=getTran(request,"ccbrt.va","va",sWebLanguage)+(optometry!=null?"<br/><b>("+ScreenHelper.formatDate(optometry.getUpdateTime())+")</b>":"")%>&nbsp;</td>
			            <td class="admin2" colspan="6"> 
			            	<table width='100%'>
		            			<%
		            				if(optometry!=null){
		            					String sUrl="javascript:window.open(\""+sCONTEXTPATH+"/healthrecord/viewTransaction.jsp?Page=healthrecord/manageCCBRTOptometristExamination.jsp&be.mxs.healthrecord.createTransaction.transactionType="+optometry.getTransactionType()+"&be.mxs.healthrecord.transaction_id="+optometry.getTransactionId()+"&be.mxs.healthrecord.server_id="+optometry.getServerId()+"&useTemplate=no&ts="+getTs()+"&NoBackButton=true\",\"View\",\"toolbar=no,status=yes,scrollbars=yes,resizable=yes,width=800,height=600,menubar=no\")";
		            			%>
			            		<tr>
			            			<td width='1%' nowrap><%=getTran(request,"ccbrt.eye","right",sWebLanguage)%>&nbsp;</td>
			            			<td><b><%=getTran(request,"ccbrt.va.lr",optometry.getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_VA_RIGHT"),sWebLanguage) %></b></td>
			            			<td width='1%' nowrap><%=getTran(request,"ccbrt.eye","left",sWebLanguage)%>&nbsp;</td>
			            			<td><b><%=getTran(request,"ccbrt.va.lr",optometry.getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_VA_LEFT"),sWebLanguage) %></b></td>
			            			<td width='1%' nowrap><%=getTran(request,"ccbrt.eye","dil",sWebLanguage)%> <%=getTran(request,"ccbrt.eye","right",sWebLanguage)%>&nbsp;</td>
				            		<td><b><%=getTran(request,"web",optometry.getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_DIL_RIGHT").length()>0?"yes":"no",sWebLanguage) %></b></td>
			            			<td width='1%' nowrap><%=getTran(request,"ccbrt.eye","dil",sWebLanguage)%> <%=getTran(request,"ccbrt.eye","left",sWebLanguage)%>&nbsp;</td>
				            		<td><b><%=getTran(request,"web",optometry.getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OPTOMETRIST_DIL_LEFT").length()>0?"yes":"no",sWebLanguage) %></b></td>
			            		</tr>
			            		<tr>
			            			<td colspan='8'>
			            				<a href='<%=sUrl%>'><%=getTran(request,"web.occup",optometry.getTransactionType(),sWebLanguage) %></a>
			            			</td>
			            		</tr>
			            		<%	}%>
			            	</table>
			            </td>
			        </tr>
		        	<tr style="display:none;">
			            <td class="admin"><%=getTran(request,"ccbrt.eye","from",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6">
			                <select id="from" class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_FROM" property="itemId"/>]>.value">
			                	<option/>
				            	<%=ScreenHelper.writeSelect(request,"ccbrt.eye.from",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_FROM"),sWebLanguage,false,true) %>
			                </select>
			            </td>
			        </tr>
			        
			        <tr>
			            <td class="admin">
			                <%=getTran(request,"ccbrt.eye","attendencytype",sWebLanguage)%>
			            </td>
			            <td class="admin2" colspan="6">
			                <select id="attendencytype" class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_ATTENDENCYTYPE" property="itemId"/>]>.value">
			                	<option/>
				            	<%=ScreenHelper.writeSelect(request,"ccbrt.attendencytype",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_ATTENDENCYTYPE"),sWebLanguage,false,true) %>
			                </select>
			            </td>
			        </tr>
		        	<tr>
			            <td class="admin" rowspan="1"><%=getTran(request,"web","history",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6"><textarea <%=setRightClickMini("ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_HISTORY")%> onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="complaints_comment" class="text" cols="60" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_HISTORY" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_HISTORY" translate="false" property="value"/></textarea></td>
			        </tr>
			        <!-- 
		        	<tr>
			            <td class="admin"><%=getTran(request,"ccbrt.va","allergies",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6">
			                <select id="vaassessment" class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_ALLERGIES" property="itemId"/>]>.value">
			                	<option/>
				            	<%=ScreenHelper.writeSelect(request,"ccbrt.allergies",((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_ALLERGIES"),sWebLanguage,false,true) %>
			                </select>
			            </td>
			        </tr> 
			        -->
			        <!--  DRAWING ITEM-->
			        <tr>
			            <td class="admin" style='vertical-align: top' width="<%=sTDAdminWidth%>">
			            	<%=getTran(request,"web","drawing",sWebLanguage) %> <img src='<%=sCONTEXTPATH%>/_img/icons/icon_minus.png' onclick='document.getElementById("drawing").style.display="none";'/> <img src='<%=sCONTEXTPATH%>/_img/icons/icon_plus.png' onclick='document.getElementById("drawing").style.display="";'/>
			            </td>
			        	<td class='admin2' colspan='6'>
			        		<div id='drawing' style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OCDRAWING").length()>0?"":"none"%>'>
								<%=ScreenHelper.createDrawingDiv(request, "canvasDiv", "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OCDRAWING", transaction, MedwanQuery.getInstance().getConfigString("defaultEyeDiagram","/_img/eyes.png"),"eyes.image") %>
							</div>
			        	</td>
			        </tr>
			        <!-- PHOTO ITEM -->
			        <% /*
			        INSERT INTO `transactionitems` (`transactionTypeId`,`itemTypeId`,`defaultValue`,`modifier`,`priority`,`updatetime`) VALUES 
			        ('be.mxs.common.model.vo.healthrecord.IConstants.TRANSACTION_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION','be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO','','',0,NULL) 
			        */
			        %>
					<link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/bootstrap.min.css" rel="stylesheet">
				    <link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/icon.css" rel="stylesheet">
				    <link href="<%=sCONTEXTPATH%>/projects/optica/_common/_css/webcam.css" rel="stylesheet" type="text/css" media="screen">
				    <script src="<%=sCONTEXTPATH%>/projects/optica/_common/_script/jquery-3.3.1.min.js"></script>  
				    <script src="<%=sCONTEXTPATH%>/projects/optica/_common/_script/webcam-easy.min.js"></script>
			        <tr>
			            <td class="admin" style='vertical-align: top' width="<%=sTDAdminWidth%>">
			            	<br>
			            	Fotografia : <img src='<%=sCONTEXTPATH%>/_img/icons/icon_minus.png' onclick='document.getElementById("photo").style.display="none";'/> <img src='<%=sCONTEXTPATH%>/_img/icons/icon_plus.png' onclick='document.getElementById("photo").style.display="";'/>
			            	<main id="webcam-app">
							        <div class="form-control webcam-start" id="webcam-control">
							                <label class="form-switch">
							                <input type="checkbox" id="webcam-switch">
							                <i></i> <br>
							                <span id="webcam-caption">Abrir Camara</span>
							                </label>      
							                <button id="cameraFlip" class="btn d-none"></button>                  
							        </div>
							        <div id="errorMsg" class="col-12 col-md-6 alert-danger d-none">
							            No se pudo iniciar la c�mara. Permita permiso para acceder a la c�mara. <br/>
							            Se recomienda abrir a trav�s de navegadores como Chrome.
							            <button id="closeError" class="btn btn-primary ml-3">OK</button>
							        </div>
							        <div class="md-modal md-effect-12">
							            <div id="app-panel" class="app-panel md-content row p-0 m-0">     
							                <div id="webcam-container" class="webcam-container col-12 d-none p-0 m-0">
							                    <video id="webcam" autoplay playsinline width="400" height="200"></video>
							                    <canvas id="canvasPhoto" class="canvasPhoto d-none"></canvas>
							                    <div class="flash"></div>
							                    <!-- <audio id="snapSound" src="audio/snap.wav" preload = "auto"></audio>  -->
							                </div>
							                <div id="cameraControls" class="cameraControls">
							                    <a href="#" id="exit-app" title="Cerrar camara" class="d-none"><i class="material-icons">exit_to_app</i></a>
							                    <a href="#" id="take-photo" title="Tomar Foto"><i class="material-icons">camera_alt</i></a>
							                    <!--  <a href="#" id="download-photo" download="Foto-id-<%=activePatient.personid%>.png" target="_blank" title="Guardar Foto" class="d-none"><i class="material-icons">file_download</i></a>  -->
							                    <a href="#" id="download-photo" title="Guardar Foto" class="d-none"><i class="material-icons">file_download</i></a>  
							                    <a href="#" id="resume-camera"  title="Retornar a Camara" class="d-none"><i class="material-icons">camera_front</i></a>
							                    <a href="#" id="grabar-photo" title="Grabar" class="d-none"><i class="material-icons">save</i></a>
							                </div>
							            </div>        
							        </div>
							        <div class="md-overlay"></div>
							    </main>
			            </td>
			        	<td class='admin2' colspan='6'>
							<div id='photo' style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO").length()>0?"":"none"%>'>
								<%//=ScreenHelper.createPhotoDiv(request, "photoDiv", "be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OCPHOTO", transaction ) %>
								<input type="hidden" id="photoId" class="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO" property="value"/>">
								<a href="<%=sCONTEXTPATH%>/scan/photo/<%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO")%>" id="photoLink" target="_blank" style='display: <%=((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO").length()>0?"":"none"%>'>
								<img src="<%=sCONTEXTPATH%>/scan/photo/<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_PHOTO" property="value"/>" title="photoImg" id="photoImg" name="photoImg" width="479px" />
								</a>
								<br/><br/>
						    </div>
			        	</td>
			        </tr>
			        <tr>
			            <td class="admin" rowspan="1"><%=getTran(request,"web","observation",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6"><textarea <%=setRightClickMini("ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_OBSERVATION")%> onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="complaints_comment" class="text" cols="60" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_OBSERVATION" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_OBSERVATION" translate="false" property="value"/></textarea></td>
			        </tr>
			          <tr>
			            <td class="admin" rowspan="1"><%=getTran(request,"web","conclusions",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6"><textarea <%=setRightClickMini("ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_CONCLUSION")%> onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="complaints_comment" class="text" cols="60" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_CONCLUSION" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_CONCLUSION" translate="false" property="value"/></textarea></td>
			        </tr>
		        	<tr style="display:none;">
			            <td class="admin"><%=getTran(request,"ccbrt.va","examiner",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="6">
			            	<%
			            		ItemVO item = ((TransactionVO)transaction).getItem("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_USER");
			            		String userid="";
			            		if(item!=null){
			            			userid=item.getValue();
			            		}
			            		
			            	%>
			                <input type="hidden" id="ccbrtuser" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_OPHTALMOLOGYCONSULTATION_USER" property="itemId"/>]>.value"/>
			                <input type="hidden" name="diagnosisUser" id="diagnosisUser" value="<%=userid%>">
			                <input class="text" type="text" name="diagnosisUserName" id="diagnosisUserName" readonly size="40" value="<%=User.getFullUserName(userid)%>">
			                <img src="<c:url value="/_img/icons/icon_search.gif"/>" class="link" alt="<%=getTran(null,"web","select",sWebLanguage)%>" onclick="searchUser('diagnosisUser','diagnosisUserName');">
			                <img src="<c:url value="/_img/icons/icon_delete.gif"/>" class="link" alt="<%=getTran(null,"web","clear",sWebLanguage)%>" onclick="document.getElementById('diagnosisUser').value='';document.getElementById('diagnosisUserName').value='';">
			            </td>
			        </tr>
		        	<tr>
			            <td class="admin"><%=getTran(request,"ccbrt.va","followup",sWebLanguage)%>&nbsp;</td>
			            <td class="admin2" colspan="4">
			            	<select class='text' id='frequency' onchange='calculateNextDate()'>
			            		<option/>
			            		<option value='1'>1</option>
			            		<option value='2'>2</option>
			            		<option value='3'>3</option>
			            		<option value='4'>4</option>
			            		<option value='5'>5</option>
			            		<option value='6'>6</option>
			            		<option value='7'>7</option>
			            		<option value='8'>8</option>
			            		<option value='9'>9</option>
			            		<option value='10'>10</option>
			            		<option value='11'>11</option>
			            		<option value='12'>12</option>
			            	</select>
			            	<select class='text' id='frequencytype' onchange='calculateNextDate()'>
			            		<option/>
			            		<option value='week'><%=getTran(request,"web","weeks",sWebLanguage) %></option>
			            		<option value='month'><%=getTran(request,"web","months",sWebLanguage) %></option>
			            		<option value='year'><%=getTran(request,"web","year",sWebLanguage) %></option>
			            	</select>
			                <input type="hidden" id="ccbrtdate" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_EYE_REGISTRY_VAFOLLOWUPDATE" property="itemId"/>]>.value"/>
			            	<%=(ScreenHelper.writeDateField("vafollowupdate", "transactionForm", ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CCBRT_EYE_REGISTRY_VAFOLLOWUPDATE"), false, true, sWebLanguage, sCONTEXTPATH))%>
			            </td>
			            <td class="admin2" colspan="2">
			            	<a href="javascript:openPopup('planning/findPlanning.jsp&isPopup=1&FindDate='+document.getElementById('vafollowupdate').value+'&FindUserUID='+document.getElementById('diagnosisUser').value,1024,600,'Agenda','toolbar=no,status=yes,scrollbars=no,resizable=yes,width=1024,height=600,menubar=no');void(0);"><%=getTran(request,"web","findappointment",sWebLanguage) %></a>
			            </td>
			        </tr>
			        			        
	            </table>
	        </td>
	        <%-- DIAGNOSES --%>
	    	<td class="admin2" style='vertical-align: top'>
		      	<%ScreenHelper.setIncludePage(customerInclude("healthrecord/diagnosesEncoding.jsp"),pageContext);%>
	    		<br><br>
	    		<table class="list" width="98%" cellspacing="0" cellpadding="2">
				  <tbody><tr class="admin">
				    <td align="center">IMAGENES PACS</td>
				  </tr>
				  <tr>
				    <td>
					  <table width="100%">
						<tbody>
						<tr class="admin">
							<!--  <td>Date</td> -->
							<td>Identificadores de estudio</td>
							<td>Parte del cuerpo</td>
							<td>Modalidad</td>
							<td>Fabricante</td>
							<td>Visualizaci�n</td>
							<!-- <td>Ver en STONE</td> -->
						</tr>
						<%
		    			   if(EstudiosImagen.size()<1) { 
		    			%>
		    				<tr>
		    					<td class="admin2" colspan="5">No hay imagen disponible para este paciente cargadas en PACS</td>
		    				</tr>
		    			<%	 
		    			 } else {
		    				for(int i=0; i<EstudiosImagen.size(); i++){
		    					Object a = EstudiosImagen.get(i);
		    					String[] b = a.toString().split(",");
		    					String dni = b[0].substring(b[0].indexOf("=")+1, b[0].length());
		    					String url = "http://localhost:8042/osimis-viewer/app/index.html?study="+b[1].substring(b[1].indexOf("=")+1, b[1].length()-1); 
		    				%>
		    					<tr>
		    						<td class="admin2">PACS-<%=i+1%>-<%=dni%></td>
		    						<td class="admin2">EYE</td>
		    						<td class="admin2">OT</td>
		    						<td class="admin2">Optomed Plc.</td>
		    						<td class="admin2"><a href="<%=url%>" target="_blank">Ver en OSIMIS </a></td>
		    					</tr>
		 	    	    <%	    
		 	    	        }
		    			 }
		    			%>
						</tbody>
					   </table>
					</td>
				  </tr>
				</tbody></table>
	    	</td>
        </tr>
    </table>
            
	<%-- BUTTONS --%>
	<%=ScreenHelper.alignButtonsStart()%>
	    <%=getButtonsHtml(request,activeUser,activePatient,"occup.ccbrt.ophtalmologyConsultation",sWebLanguage)%>
	<%=ScreenHelper.alignButtonsStop()%>
	
    <%=ScreenHelper.contextFooter(request)%>
</form>
<input type="hidden" id="dataImg" name="dataImg" />
<form id="FormPhoto" method="post" action="uploadphoto.jsp" style="visibility:hidden;">						
 	<input type="hidden" id="filenameImg" name="filenameImg" />
 	<input type="submit" id="submitFormPhoto" name="submitFormPhoto" />
 </form>
<script>
function searchEncounter(){
    openPopup("/_common/search/searchEncounter.jsp&ts=<%=getTs()%>&Varcode=encounteruid&VarText=&FindEncounterPatient=<%=activePatient.personid%>");
  }
  
  if( document.getElementById('encounteruid').value=="" <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
  	alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
  	searchEncounter();
  }	

  <%-- SUBMIT FORM --%>
  function calculateNextDate(){
	  var nextdate = new Date();
	  if(document.getElementById('frequencytype').value.length>0 && document.getElementById('frequency').value.length>0){
		  if(document.getElementById('frequencytype').value=='month'){
			  nextdate.setMonth(nextdate.getMonth()+document.getElementById('frequency').value*1);
		  }
		  else if(document.getElementById('frequencytype').value=='year'){
			  nextdate.setYear(nextdate.getFullYear()+document.getElementById('frequency').value*1);
		  }
		  else if(document.getElementById('frequencytype').value=='week'){
			  nextdate.setDate(nextdate.getDate()+document.getElementById('frequency').value*7);
		  }
		  document.getElementById('vafollowupdate').value=("0"+nextdate.getDate()).substring(("0"+nextdate.getDate()).length-2,("0"+nextdate.getDate()).length)+"/"+("0"+(nextdate.getMonth()+1)).substring(("0"+(nextdate.getMonth()+1)).length-2,("0"+(nextdate.getMonth()+1)).length)+"/"+nextdate.getFullYear();
  	 }
  }
  function searchUser(managerUidField,managerNameField){
	  openPopup("/_common/search/searchUser.jsp&ts=<%=getTs()%>&ReturnUserID="+managerUidField+"&ReturnName="+managerNameField+"&displayImmatNew=no&FindServiceID=<%=MedwanQuery.getInstance().getConfigString("CCBRTEyeRegistryService")%>",650,600);
    document.getElementById(diagnosisUserName).focus();
  }

  function submitForm(){
		if(document.getElementById("drawing").style.display=='none'){
	    	document.getElementById("canvasDivDrawing").value='';
	    }
		if(document.getElementById("photo").style.display=='none'){
	    	document.getElementById("photoId").value='';
	    }

	  document.getElementById("ccbrtuser").value=document.getElementById("diagnosisUser").value;
	  document.getElementById("ccbrtdate").value=document.getElementById("vafollowupdate").value;
	    <%
	        SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
	        out.print(takeOverTransaction(sessionContainerWO,activeUser,"document.transactionForm.submit();"));
	    %>
  	
	  
  }

</script>

<script type="text/javascript">

const webcamElement = document.getElementById('webcam');
const canvasElement = document.getElementById('canvasPhoto');
const snapSoundElement = document.getElementById('snapSound');
const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

$("#webcam-switch").change(function () {
    if(this.checked){
        $('.md-modal').addClass('md-show');
        webcam.start()
            .then(result =>{
               cameraStarted();
               console.log("webcam started");
            })
            .catch(err => {
                displayError();
            });
    }
    else {        
        cameraStopped();
        webcam.stop();
        console.log("webcam stopped");
    }        
});

$('#cameraFlip').click(function() {
    webcam.flip();
    webcam.start();  
});


$('#download-photo').click(function() {
	$("#submitFormPhoto").click();
});

$('#closeError').click(function() {
    $("#webcam-switch").prop('checked', false).change();
});

function displayError(err = ''){
    if(err!=''){
        $("#errorMsg").html(err);
    }
    $("#errorMsg").removeClass("d-none");
}

function cameraStarted(){
    $("#errorMsg").addClass("d-none");
    $('.flash').hide();
    $("#webcam-caption").html("on");
    $("#webcam-control").removeClass("webcam-off");
    $("#webcam-control").addClass("webcam-on");
    $(".webcam-container").removeClass("d-none");
    if( webcam.webcamList.length > 1){
        $("#cameraFlip").removeClass('d-none');
    }
    $("#wpfront-scroll-top-container").addClass("d-none");
    window.scrollTo(0, 0); 
    $('body').css('overflow-y','hidden');
}

function cameraStopped(){
    $("#errorMsg").addClass("d-none");
    $("#wpfront-scroll-top-container").removeClass("d-none");
    $("#webcam-control").removeClass("webcam-on");
    $("#webcam-control").addClass("webcam-off");
    $("#cameraFlip").addClass('d-none');
    $(".webcam-container").addClass("d-none");
    $("#webcam-caption").html("Click to Start Camera");
    $('.md-modal').removeClass('md-show');
}

$("#take-photo").click(function () {
    beforeTakePhoto();
    let picture = webcam.snap();
    $("#dataImg").val(picture);  // alert(picture);
    /*
    var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	var image = new Image();
	image.onload = function() {
	  ctx.drawImage(image, 0, 0);
	};
	image.src=picture;
	*/
    afterTakePhoto();
});

function beforeTakePhoto(){
    $('.flash')
        .show() 
        .animate({opacity: 0.3}, 500) 
        .fadeOut(500)
        .css({'opacity': 0.7});
    window.scrollTo(0, 0); 
    $('#webcam-control').addClass('d-none');
    $('#cameraControls').addClass('d-none');
}

function afterTakePhoto(){
    webcam.stop();
    $('#canvasPhoto').removeClass('d-none');
    $('#take-photo').addClass('d-none');
    $('#exit-app').removeClass('d-none');
    $('#download-photo').removeClass('d-none');
    $('#resume-camera').removeClass('d-none');
    $('#cameraControls').removeClass('d-none');
}

function removeCapture(){
    $('#canvasPhoto').addClass('d-none');
    $('#webcam-control').removeClass('d-none');
    $('#cameraControls').removeClass('d-none');
    $('#take-photo').removeClass('d-none');
    $('#exit-app').addClass('d-none');
    $('#download-photo').addClass('d-none');
    $('#resume-camera').addClass('d-none');
}

$("#resume-camera").click(function () {
    webcam.stream()
        .then(facingMode =>{
            removeCapture();
        });
});

$("#exit-app").click(function () {
    removeCapture();
    $("#webcam-switch").prop("checked", false).change();
});

$("#FormPhoto").submit(function(e){
	// alert("para enviar...");
    e.preventDefault();
    appendFileAndSubmit();
});

function appendFileAndSubmit(){
    var form = document.getElementById("FormPhoto");
    
    $("#filenameImg").val(""+$("#serverId").val()+"-"+$("#transactionId").val()+"-"+<%=activePatient.personid%>);
    
    var ImageURL = $("#dataImg").val();// "data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    				// $("#dataImg").val();//
    var block = ImageURL.split(";");
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    var blob = b64toBlob(realData, contentType);
    var fd = new FormData(form);
    fd.append("image", blob); // image file
    // fd.append("name", "Ronnie");
    // Submit Form and upload file
    $.ajax({
        url: "projects/optica/healthrecord/uploadphoto.jsp", // uploadphoto.jsp
        data: fd,// the formData function is available in almost all new browsers.
        type:"POST",
        contentType: false, // "multipart/form-data", // "multipart/form-data", //false,
        processData: false,
        cache: false,
        dataType: "html", //"json", // Change this according to your response from the server.  html
        error:function(err){
        	console.log("Request error:");
            console.log(err);
        },
        success:function(data){
        	console.log("Request success: ");
            console.log(data);
            let photoname = data.replace(/\s+/g, '');
            $("#photoId").val(photoname); // "openclinic/scan/photo/"+  replace(/\s+/g, '')
            //document.getElementById("webcam-app").style.backgroundImage = "url('scan/photo/img-12345678.jpg')";
            document.getElementById("photoImg").src='scan/photo/' + photoname;
            document.getElementById("photoLink").href='scan/photo/' + photoname;
            document.getElementById("photoLink").style.display='block';
            document.getElementById("photo").style.display='block';
            $("#exit-app").click();
        },
        complete:function(){
            console.log("Request finished:");
        }
    });
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

</script>
    
<%=writeJSButtons("transactionForm","saveButton")%>