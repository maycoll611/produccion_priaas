<%@page import="be.mxs.common.model.vo.healthrecord.TransactionVO,
                be.mxs.common.model.vo.healthrecord.ItemVO,
                be.openclinic.pharmacy.Product,
                java.text.DecimalFormat,
                be.openclinic.medical.Problem,
                be.openclinic.medical.Diagnosis,
                be.openclinic.system.Transaction,
                be.openclinic.system.Item,
                be.openclinic.medical.Prescription,
                java.util.*" %>
<%@ page import="java.sql.Date" %>
<%@ page import="be.openclinic.medical.PaperPrescription" %>
<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>
<%=checkPermission(out,"occup.ophtalmology.consultation","select",activeUser)%>
<style>
	TD.admin {
    border: 0.5px solid white;
    }
    tr.admin {
    border-bottom: 1px solid white;
    }
</style>
<form id="transactionForm" name="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>'>
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
  
    <input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
    <input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
    <input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>
    <input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
    <%--=ScreenHelper.writeDefaultHiddenInput((TransactionVO)transaction, "ITEM_TYPE_CONTEXT_DEPARTMENT") --%>
    <%--=ScreenHelper.writeDefaultHiddenInput((TransactionVO)transaction, "ITEM_TYPE_CONTEXT_CONTEXT") --%>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
    <input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>

    <%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>
    <%=contextHeader(request,sWebLanguage)%>
	<%
		Vector EstudiosImagen = activePatient.getImagesPacsPatientByPersonId(activePatient.personid);

		String sMOTIVOCONSULTA = "";
		String sMOTIVOCONSULTA_CD_1 = "";
		String sMOTIVOCONSULTA_ANAMNESIS = "";
		String sAV_ACTUAL = "";
		String sAV_ACTUAL_LA_ESF_OD = "";
		String sAV_ACTUAL_LA_ESF_OI = "";
		String sAV_ACTUAL_LA_CIL_OD = "";
		String sAV_ACTUAL_LA_CIL_OI = "";
		String sAV_ACTUAL_LA_EJE_OD = "";
		String sAV_ACTUAL_LA_EJE_OI = "";
		String sAV_ACTUAL_LA_DIP = "";
		String sAV_ACTUAL_AR_ESF_OD = "";
		String sAV_ACTUAL_AR_ESF_OI = "";
		String sAV_ACTUAL_AR_CIL_OD = "";
		String sAV_ACTUAL_AR_CIL_OI = "";
		String sAV_ACTUAL_AR_EJE_OD = "";
		String sAV_ACTUAL_AR_EJE_OI = "";
		String sAV_ACTUAL_AR_DIP = "";
		String sANTECED = "";
		String sANTECED_DIABETES = "";
		String sANTECED_HTA = "";
		String sANTECED_DISLIPIDEMIA = "";
		String sANTECED_GLAUCOMA = "";
		String sANTECED_ALERGIA = "";
		String sANTECED_HIST_QUIRURGICA = "";
		String sANTECED_HIST_FAMILIAR = "";
		String sANTECED_MEDICAMENTOS = "";
		String sANTECED_OTROS = "";
		String sPIO_OD = "";
		String sPIO_OI = "";
		String sAGUDEZAVISUAL_SE_OD = "";
		String sAGUDEZAVISUAL_SE_OI = "";
		String sAGUDEZAVISUAL_AE_OD = "";
		String sAGUDEZAVISUAL_AE_OI = "";
		String sAGUDEZAVISUAL_CC_OD = "";
		String sAGUDEZAVISUAL_CC_OI = "";
		String sRETRACCIONFINAL_ESF_OD = "";
		String sRETRACCIONFINAL_ESF_OI = "";
		String sRETRACCIONFINAL_CIL_OD = "";
		String sRETRACCIONFINAL_CIL_OI = "";
		String sRETRACCIONFINAL_EJE_OD = "";
		String sRETRACCIONFINAL_EJE_OI = "";
		String sRETRACCIONFINAL_DIP_OD = "";
		String sRETRACCIONFINAL_DIP_OI = "";
		String sPARPADO_OD = "";
		String sPARPADO_OI = "";
		String sBIOMICROSCOPIA_CONJUNTIVA_OD = "";
		String sBIOMICROSCOPIA_CONJUNTIVA_OI = "";
		String sBIOMICROSCOPIA_CORNEA_OD = "";
		String sBIOMICROSCOPIA_CORNEA_OI = "";
		String sBIOMICROSCOPIA_CAMARAANTERIOR_OD = "";
		String sBIOMICROSCOPIA_CAMARAANTERIOR_OI = "";
		String sBIOMICROSCOPIA_IRIS_OD = "";
		String sBIOMICROSCOPIA_IRIS_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_COR_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_COR_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_NUC_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_NUC_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_CAP_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_CAP_OI = "";
		String sBIOMICROSCOPIA_CRISTALINO_AFA_OD = "";
		String sBIOMICROSCOPIA_CRISTALINO_AFA_OI = "";
		String sFONDOOJO_NERVIOOPTICO_OD = "";
		String sFONDOOJO_NERVIOOPTICO_OI = "";
		String sFONDOOJO_MACULA_OD = "";
		String sFONDOOJO_MACULA_OI = "";
		String sFONDOOJO_PERIFERIA_OD = "";
		String sFONDOOJO_PERIFERIA_OI = "";
		String sDRAWING = "";
		String sCAMARA = "";
		String sCAMARA_PHOTO = "";
		String sDIAGNOSTICO = "";
		String sOBSERVACION = "";
		String sPROXIMA_CITA = "";

        if(transaction != null){
            TransactionVO tran = (TransactionVO)transaction;
            if(tran!=null){

				sAV_ACTUAL 			 = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL");
				sAV_ACTUAL_LA_ESF_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD");
				sAV_ACTUAL_LA_ESF_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI");
				sAV_ACTUAL_LA_CIL_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD");
				sAV_ACTUAL_LA_CIL_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI");
				sAV_ACTUAL_LA_EJE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD");
				sAV_ACTUAL_LA_EJE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI");
				sAV_ACTUAL_LA_DIP 	 = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP");
				sAV_ACTUAL_AR_ESF_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD");
				sAV_ACTUAL_AR_ESF_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI");
				sAV_ACTUAL_AR_CIL_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD");
				sAV_ACTUAL_AR_CIL_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI");
				sAV_ACTUAL_AR_EJE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD");
				sAV_ACTUAL_AR_EJE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI");
				sAV_ACTUAL_AR_DIP = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP");
				sANTECED = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED");
				sANTECED_DIABETES = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_DIABETES");
				sANTECED_HTA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HTA");
				sANTECED_DISLIPIDEMIA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_DISLIPIDEMIA");
				sANTECED_GLAUCOMA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_GLAUCOMA");
				sANTECED_ALERGIA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_ALERGIA");
				sANTECED_HIST_QUIRURGICA = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HIST_QUIRURGICA");
				sANTECED_HIST_FAMILIAR = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_HIST_FAMILIAR");
				sANTECED_MEDICAMENTOS = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_MEDICAMENTOS");
				sANTECED_OTROS = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_ANTECED_OTROS");
				sPIO_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PIO_OD");
				sPIO_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_PIO_OI");
				sAGUDEZAVISUAL_SE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OD");
				sAGUDEZAVISUAL_SE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OI");
				sAGUDEZAVISUAL_AE_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OD");
				sAGUDEZAVISUAL_AE_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OI");
				sAGUDEZAVISUAL_CC_OD = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OD");
				sAGUDEZAVISUAL_CC_OI = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OI");
				
				/*
				sTranCamera_Photo = getItemType(tran.getItems(),sPREFIX+"ITEM_TYPE_OPHTALMOLOGY_CONSULTATION_VER_CAMERA_PHOTO");
				If(sTranCamera_Photo.length() > 0)
				{
					sTranCamera_Photo_link = sCONTEXTPATH + '/scan/photo/' + sTranCamera_Photo;
				}
				*/
            }
        }
    %>  
    
    <table class="list" cellspacing="1" cellpadding="0" width="100%">
		<tr>
            <td style="vertical-align:top;padding:0">
				<table class="list" width="100%" cellspacing="1" cellpadding='0'>
					<%-- DATE --%>
					<tr>
						<td class='admin' colspan='8'>
							<table width='100%' cellspacing='0' cellpadding='0'>
								<tr>
									<td width='1%' nowrap rowspan='2'>
										<img style='vertical-align: middle' height='50px' src='<%=sCONTEXTPATH %>/_img/ver.png'/>&nbsp;&nbsp; 
									</td>
									<td class="admin" width="<%=sTDAdminWidth%>">
										<a href="javascript:openHistoryPopup();" title="<%=getTranNoLink("Web.Occup","History",sWebLanguage)%>">...</a>&nbsp;
										<%=getTran(request,"Web.Occup","medwan.common.date",sWebLanguage)%>
										<input type="text" class="text" size="12" maxLength="10" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" id="trandate" OnBlur='checkDate(this)'>
			                			<script>writeTranDate();</script>
									</td>
									<td class='admin' width='40%'>
										<div id='covidtest' style='color: red;font-size: 14px'></div>
									</td>
								</tr>
								<tr class='admin2'>
									<td colspan='2'>
										<!-- <strong>Alergia a:</strong> --> <%-- =getTran(request,"examination","1216",sWebLanguage) --%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%-- AGUDEZA VISUAL ACTUAL --%>
					<tr class="admin">
						<td colspan="8">
							<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_AV_ACTUAL", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_AV_ACTUAL\",\"div_avactual\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","agudeza.vusual.actual",sWebLanguage)%> &nbsp;
						</td>
					</tr>
					<tr class="div_avactual">
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							<%-- vision.acuity ----------------------------------------------------------------------%>
							<%-- right : current glasses / dx --%>
							<tr>
								<td class="admin" width="150" rowspan="6"><%=getTran(request,"ophtalmology.consultation.ver","agudeza.vusual.actual",sWebLanguage)%></td>
								<td class="admin2" width="150" rowspan="2"><%=getTran(request,"ophtalmology.consultation.ver","lentes.actuales",sWebLanguage)%></td>
								<td class="admin2" width="50"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_ESF_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_CIL_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_EJE_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
								<td rowspan="2" class="admin2">
									<%=getTran(request,"openclinic.chuk","add",sWebLanguage)%>+&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_DIP" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_DIP%>" size="4" onblur="isMyNumber(this);"/>&nbsp;D
								</td>
							</tr>
							<%-- left : current glasses / dx --%>
							<tr>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_ESF_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_ESF_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_CIL_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_CIL_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_LA_EJE_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_LA_EJE_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
							</tr>
							<%-- right : autorefactor --%>
							<tr>
								<td class="admin2" rowspan="2"><%=getTran(request,"ophtalmology.consultation.ver","autorefractor",sWebLanguage)%></td>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_ESF_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_CIL_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OD" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_EJE_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
								<td rowspan="2" class="admin2">
									<%=getTran(request,"openclinic.chuk","add",sWebLanguage)%>+&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_DIP" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_DIP%>" size="4" onblur="isMyNumber(this);"/>&nbsp;D
								</td>
							</tr>
							<%-- left : autorefactor --%>
							<tr>
								<td class="admin2"><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_ESF_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_ESF_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									D <img src="<c:url value="/_img/themes/default/up_down_arrow.gif"/>" alt=""/> &nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_CIL_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_CIL_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;
								</td>
								<td class="admin2">
									Dx&nbsp;<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_AV_ACTUAL_AR_EJE_OI" property="itemId"/>]>.value" value="<%=sAV_ACTUAL_AR_EJE_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;&#176;
								</td>
							</tr>				
							
						</table>
						</td>
					</tr>
					<%-- Historial --%>
					<tr class="admin">
						<td colspan="8">
							<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_ANTECED", sWebLanguage, "onchange='showtests();'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","antecedentes",sWebLanguage)%> &nbsp;
						</td>
					</tr>
					<tr id='history_items' style='display: none'>
						<td colspan='8'>
							<table class="list" width="100%" cellspacing="1" cellpadding='0'>
								<tr>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","diabetes",sWebLanguage) %> 
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(null, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_DIABETES", "yesnounknown", sWebLanguage, "") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","hta",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(null, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HTA", "yesnounknown", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","dislipidemia",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_DISLIPIDEMIA", "yesnounknown", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","glaucoma",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_GLAUCOMA", "yesnounknown", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","alergias",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultToggle(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_ALERGIA", "yesnounknown", sWebLanguage, "onclick='triage()'") %>
									</td>
								</tr>
								<tr>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","historia_familiar",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HIST_QUIRURGICA", "cdo.9", sWebLanguage, "onchange='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","historia_quirurgica",sWebLanguage) %>
									</td>
									<td class='admin2top' colspan="1">
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_HIST_FAMILIAR", "cdo.10", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","Medicamentos",sWebLanguage) %>
									</td>
									<td class='admin2top' colspan="3">
										<%=SH.writeDefaultSelect(request, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_ANTECED_MEDICAMENTOS", "cdo.7", sWebLanguage, "onclick='triage()'") %>
									</td>
									<td class='admin2topright'>
										<%=getTran(request,"ophtalmology.consultation.ver","otros",sWebLanguage) %>
									</td>
									<td class='admin2top'>
										<textarea onKeyup="this.value=this.value.toUpperCase();resizeTextarea(this,10);" id="history_comment" class="text" cols="25" rows="1" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_ANTECED_OTROS" property="itemId"/>]>.value"><mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_ANTECED_OTROS" translate="false" property="value"/></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							
							<%-- PIO --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_PIO", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_PIO\",\"div_pio\");'")%> &nbsp; <strong> <%=getTran(request,"ophtalmology.consultation.ver","pio",sWebLanguage)%>  </strong>&nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<tr class="div_pio">
								<td class="admin" width="150" colspan="2"><%=getTran(request,"ophtalmology.consultation.ver","pio",sWebLanguage)%></td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_PIO_OD")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PIO_OD" property="itemId"/>]>.value" value="<%=sPIO_OD%>" size="4" onblur="isMyNumber(this);"/>&nbsp;mmHg
								</td>
								<td class="admin2">
									<input <%=setRightClick(session,"ITEM_TYPE_OC_VER_PIO_OI")%> class="text" type="text" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_OC_VER_PIO_OI" property="itemId"/>]>.value" value="<%=sPIO_OI%>" size="4" onblur="isMyNumber(this);"/>&nbsp;mmHg
								</td>
							</tr>
							<%-- AGUDEZA VISUAL --%>
							<tr class="admin">
								<td colspan="2">
									<%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_OC_VER_AGUDEZAVISUAL", sWebLanguage, "onchange='showtests2(\"ITEM_TYPE_OC_VER_AGUDEZAVISUAL\",\"div_agudezavisual\");'")%> &nbsp; <%=getTran(request,"ophtalmology.consultation.ver","agudeza_visual",sWebLanguage)%> &nbsp;
								</td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","right",sWebLanguage))%></td>
								<td><%=ScreenHelper.boldFirstLetter(getTranNoLink("web","left",sWebLanguage))%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","SE",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_SE_OI", 10)%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","AE",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_AE_OI", 10)%></td>
							</tr>
							<tr class="div_agudezavisual">
								<td colspan="2" class="admin">
									<%=ScreenHelper.boldFirstLetter(getTranNoLink("ophtalmology.consultation.ver","CC",sWebLanguage))%>
								</td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OD", 10)%></td>
								<td class="admin2"><%=SH.writeDefaultTextInputCenter(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_AGUDEZAVISUAL_CC_OI", 10)%></td>
							</tr>
													
						</table>
						</td>
					</tr>
					
					<%-- ADICIONALES --%>
					<tr class="admin" style="height: 1px;">
						<td colspan="8">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="8">
						<table class="list" cellspacing="1" cellpadding="0" width="100%">
							<%-- OBSERVACION --%>
							<tr>
								<td class="admin" colspan="3"><%=getTran(request,"ophtalmology.consultation.ver","observacion",sWebLanguage)%></td>
								<td class="admin2" colspan="4">
									<%=SH.writeDefaultTextArea(session, (TransactionVO)transaction, "ITEM_TYPE_OC_VER_OBSERVACION", 80, 4)%>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>	
			</td>
            <%-- COLUMNA DERECHA --%>
            <td class="admin2" width="18%" style="vertical-align:top;padding-top:0">
				<br/>
			</td>			
		</tr>
        
    </table>
        
	<%-- BUTTONS --%>
	<%=ScreenHelper.alignButtonsStart()%>
	    <%=getButtonsHtml(request,activeUser,activePatient,"occup.ophtalmology.consultation",sWebLanguage)%>
	<%=ScreenHelper.alignButtonsStop()%>

    <%=ScreenHelper.contextFooter(request)%>
</form>
<input type="hidden" id="dataImg" name="dataImg" />

<script>
showtests2("ITEM_TYPE_OC_VER_PIO","div_pio");
showtests2("ITEM_TYPE_OC_VER_AV_ACTUAL","div_avactual");


function printprescription(vPrescription){
	prescription.value = vPrescription;
	// prescriptiondate = vPrescriptiondate;
	printPrescriptionForm.submit();
}
   function showtests(){
	  if(document.getElementById('ITEM_TYPE_OC_VER_ANTECED').checked){
		  document.getElementById('history_items').style.display='';
	  }
	  else{
		  document.getElementById('history_items').style.display='none';
	  }
  }
   
   function showtests2(item,clase){
     const boxes = document.getElementsByClassName(clase);
     if(document.getElementById(item).checked){
    	 for (const box of boxes) {
    		box.style.display = '';
    	 }
	 }
	 else{
		 for (const box of boxes) {
			box.style.display = 'none';
		 }
	 }
   }

  

  <%-- SUBMIT FORM --%>
  function submitForm(){
    if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
      alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
	  searchEncounter();
	}	
    else{
		var inputs = document.getElementsByTagName("input");
		for(var i = 0; i < inputs.length; i++) {
			if(inputs[i].name.indexOf("complaints.")==0 && inputs[i].checked) {
				document.getElementById("complaints").value+="*"+inputs[i].value+"*";
			}
		}
		var temp = Form.findFirstElement(transactionForm);//for ff compatibility
		//document.getElementById("buttonsDiv").style.visibility = "hidden";
		//document.transactionForm.submit();
		<%
			SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
			out.print(takeOverTransaction(sessionContainerWO, activeUser,"document.transactionForm.submit();"));
		%>
    }
  }
  
  function searchEncounter(){
    openPopup("/_common/search/searchEncounter.jsp&ts=<%=getTs()%>&Varcode=encounteruid&VarText=&FindEncounterPatient=<%=activePatient.personid%>");
  }
  
  if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
    alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
    searchEncounter();
  }	
  
  <%-- IS MY NUMBER --%>
  function isMyNumber(sObject){
    if(sObject.value==0) return false;
    sObject.value = sObject.value.replace(",",".");
    var string = sObject.value;
    var vchar = "01234567890.+-";
    var dotCount = 0;
    for(var i=0; i < string.length; i++){
      if(vchar.indexOf(string.charAt(i)) == -1){
        sObject.value = "";
        return false;
      }
      else{
        if(string.charAt(i)=="."){
          dotCount++;
          if(dotCount > 1){
            sObject.value = "";
            return false;
          }
        }
        if((string.charAt(i)=="-")||(string.charAt(i)=="+")){
          if(i > 0){
            sObject.value = "";
            return false;
          }
        }
      }
    }
    if(sObject.value.length > 250){
      sObject.value = sObject.value.substring(0,249);
    }
    return true;
  }

  showtests();
</script>


<%=writeJSButtons("transactionForm","saveButton")%>