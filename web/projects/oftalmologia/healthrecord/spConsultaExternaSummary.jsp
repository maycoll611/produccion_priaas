<%@page import="be.mxs.common.model.vo.healthrecord.TransactionVO,
                be.mxs.common.model.vo.healthrecord.ItemVO,
                be.openclinic.pharmacy.Product,
                java.text.DecimalFormat,
                be.openclinic.medical.Problem,
                be.openclinic.medical.Diagnosis,
                be.openclinic.system.Transaction,
                be.openclinic.system.Item,
                be.openclinic.medical.Prescription,
                java.util.*" %>
<%@ page import="be.openclinic.medical.PaperPrescription" %>
<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>
<%!
    //--- GET KEYWORDS HTML -----------------------------------------------------------------------
    private String getKeywordsHTML(TransactionVO transaction, String itemId, String textField,
                                   String idsField, String language){
        StringBuffer sHTML = new StringBuffer();
        ItemVO item = transaction.getItem(itemId);
        if(item!=null && item.getValue()!=null && item.getValue().length()>0){
            String[] ids = item.getValue().split(";");
            String keyword = "";

            for(int n=0; n<ids.length; n++){
                if(ids[n].split("\\$").length==2){
                    keyword = getTran(null,ids[n].split("\\$")[0],ids[n].split("\\$")[1] , language);

                    sHTML.append("<a href='javascript:deleteKeyword(\"").append(idsField).append("\",\"").append(textField).append("\",\"").append(ids[n]).append("\");'>")
                            .append("<img width='8' src='"+sCONTEXTPATH+"/_img/themes/default/erase.png' class='link' style='vertical-align:-1px'/>")
                            .append("</a>")
                            .append("&nbsp;<b>").append(keyword.startsWith("/")?keyword.substring(1):keyword).append("</b> | ");
                }
            }
        }

        String sHTMLValue = sHTML.toString();
        if(sHTMLValue.endsWith("| ")){
            sHTMLValue = sHTMLValue.substring(0,sHTMLValue.lastIndexOf("| "));
        }

        return sHTMLValue;
    }
%>
<%!
    //--- GET PRODUCT -----------------------------------------------------------------------------
    private Product getProduct(String sProductUid) {
        // search for product in products-table
        Product product = new Product();
        product = product.get(sProductUid);

        if (product != null && product.getName() == null) {
            // search for product in product-history-table
            product = product.getProductFromHistory(sProductUid);
        }

        return product;
    }

    //--- GET ACTIVE PRESCRIPTIONS FROM RS --------------------------------------------------------
    private Vector getActivePrescriptionsFromRs(StringBuffer prescriptions, Vector vActivePrescriptions, String sWebLanguage) throws SQLException {
        Vector idsVector = new Vector();
        java.util.Date tmpDate;
        Product product = null;
        String sClass = "1", sPrescriptionUid = "", sDateBeginFormatted = "", sDateEndFormatted = "",
                sProductName = "", sProductUid = "", sPreviousProductUid = "", sTimeUnit = "", sTimeUnitCount = "",
                sUnitsPerTimeUnit = "", sPrescrRule = "", sProductUnit = "", timeUnitTran = "";
        DecimalFormat unitCountDeci = new DecimalFormat("#.#");
        SimpleDateFormat stdDateFormat = ScreenHelper.stdDateFormat;

        // frequently used translations
        String detailsTran = getTranNoLink("web", "showdetails", sWebLanguage),
                deleteTran = getTranNoLink("Web", "delete", sWebLanguage);
        Iterator iter = vActivePrescriptions.iterator();

        // run thru found prescriptions
        Prescription prescription;

        while (iter.hasNext()) {
            prescription = (Prescription)iter.next();
            sPrescriptionUid = prescription.getUid();
            // alternate row-style
            if (sClass.equals("")) sClass = "1";
            else sClass = "";

            idsVector.add(sPrescriptionUid);

            // format begin date
            tmpDate = prescription.getBegin();
            if (tmpDate != null) sDateBeginFormatted = stdDateFormat.format(tmpDate);
            else sDateBeginFormatted = "";

            // format end date
            tmpDate = prescription.getEnd();
            if (tmpDate != null) sDateEndFormatted = stdDateFormat.format(tmpDate);
            else sDateEndFormatted = "";

            // only search product-name when different product-UID
            sProductUid = prescription.getProductUid();
            if (!sProductUid.equals(sPreviousProductUid)) {
                sPreviousProductUid = sProductUid;
                product = getProduct(sProductUid);
                if (product != null) {
                    sProductName = product.getName();
                } else {
                    sProductName = "";
                }
                if (sProductName.length() == 0) {
                    sProductName = "<font color='red'>"+getTran(null,"web", "nonexistingproduct", sWebLanguage)+"</font>";
                }
            }

            //*** compose prescriptionrule (gebruiksaanwijzing) ***
            // unit-stuff
            sTimeUnit = prescription.getTimeUnit();
            sTimeUnitCount = Integer.toString(prescription.getTimeUnitCount());
            sUnitsPerTimeUnit = Double.toString(prescription.getUnitsPerTimeUnit());

            // only compose prescriptio-rule if all data is available
            if (!sTimeUnit.equals("0") && !sTimeUnitCount.equals("0") && !sUnitsPerTimeUnit.equals("0")) {
                sPrescrRule = getTran(null,"web.prescriptions", "prescriptionrule", sWebLanguage);
                sPrescrRule = sPrescrRule.replaceAll("#unitspertimeunit#", unitCountDeci.format(Double.parseDouble(sUnitsPerTimeUnit)));
                if (product != null) {
                    sProductUnit = product.getUnit();
                } else {
                    sProductUnit = "";
                }
                // productunits
                if (Double.parseDouble(sUnitsPerTimeUnit) == 1) {
                    sProductUnit = getTran(null,"product.unit", sProductUnit, sWebLanguage);
                } else {
                    sProductUnit = getTran(null,"product.unit", sProductUnit, sWebLanguage);
                }
                sPrescrRule = sPrescrRule.replaceAll("#productunit#", sProductUnit.toLowerCase());

                // timeunits
                if (Integer.parseInt(sTimeUnitCount) == 1) {
                    sPrescrRule = sPrescrRule.replaceAll("#timeunitcount#", "");
                    timeUnitTran = getTran(null,"prescription.timeunit", sTimeUnit, sWebLanguage);
                } else {
                    sPrescrRule = sPrescrRule.replaceAll("#timeunitcount#", sTimeUnitCount);
                    timeUnitTran = getTran(null,"prescription.timeunits", sTimeUnit, sWebLanguage);
                }
                sPrescrRule = sPrescrRule.replaceAll("#timeunit#", timeUnitTran.toLowerCase());
            }

            //*** display prescription in one row ***
            prescriptions.append("<tr class='list"+sClass+"' onmouseover=\"this.style.cursor='pointer';\" onmouseout=\"this.style.cursor='default';\" title='"+detailsTran+"'>")
                    .append("<td align='center'><img src='"+sCONTEXTPATH+"/_img/icons/icon_delete.png' border='0' title='"+deleteTran+"' onclick=\"doDelete('"+sPrescriptionUid+"');\">")
                    .append("<td onclick=\"doShowDetails('"+sPrescriptionUid+"');\" >"+sProductName+"</td>")
                    .append("<td onclick=\"doShowDetails('"+sPrescriptionUid+"');\" >"+sDateBeginFormatted+"</td>")
                    .append("<td onclick=\"doShowDetails('"+sPrescriptionUid+"');\" >"+sDateEndFormatted+"</td>")
                    .append("<td onclick=\"doShowDetails('"+sPrescriptionUid+"');\" >"+sPrescrRule.toLowerCase()+"</td>")
                    .append("</tr>");
        }
        return idsVector;
    }

    private class TransactionID {
        public int transactionid = 0;
        public int serverid = 0;
    }

    //--- GET MY TRANSACTION ID -------------------------------------------------------------------
    private TransactionID getMyTransactionID(String sPersonId, String sItemTypes, JspWriter out) {
        TransactionID transactionID = new TransactionID();
        Transaction transaction = Transaction.getSummaryTransaction(sItemTypes, sPersonId);
        try {
            if (transaction != null) {
                String sUpdateTime = ScreenHelper.getSQLDate(transaction.getUpdatetime());
                transactionID.transactionid = transaction.getTransactionId();
                transactionID.serverid = transaction.getServerid();
                out.print(sUpdateTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (Debug.enabled) Debug.println(e.getMessage());
        }
        return transactionID;
    }

    //--- GET MY ITEM VALUE -----------------------------------------------------------------------
    private String getMyItemValue(TransactionID transactionID, String sItemType, String sWebLanguage) {
        String sItemValue = "";
        Vector vItems = Item.getItems(Integer.toString(transactionID.transactionid), Integer.toString(transactionID.serverid), sItemType);
        Iterator iter = vItems.iterator();

        Item item;

        while (iter.hasNext()) {
            item = (Item) iter.next();
            sItemValue = item.getValue();//checkString(rs.getString(1));
            sItemValue = getTranNoLink("Web.Occup", sItemValue, sWebLanguage);
        }
        return sItemValue;
    }
%>
<logic:present name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="healthRecordVO">
    <bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
    <bean:define id="lastTransaction_biometry" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="lastTransactionTypeBiometry"/>

<%
    if (session.getAttribute("sessionCounter")==null){
        session.setAttribute("sessionCounter",new Integer(0));
    }
    else {
        session.setAttribute("sessionCounter",new Integer(((Integer)session.getAttribute("sessionCounter")).intValue()+1));
    }
%>
<input type="hidden" name="sessionCounter" value="<%=session.getAttribute("sessionCounter")%>"/>
<input type='hidden' name='activeDestinationIdField' id='activeDestinationIdField'/>
<input type='hidden' name='activeDestinationTextField' id='activeDestinationTextField'/>
<input type='hidden' name='activeLabeltype' id='activeLabeltype'/>
<input type='hidden' name='activeDivld' id='activeDivld'/>
<table class="list" width="100%" border="0" cellspacing="1" cellpadding="0">
    <tr>
        <%-- LAST GENERAL CLINICAL EXAMINATION --%>
        <td style="vertical-align:top;" height="100%">
            <table class="list" width="100%" border="0" cellspacing="1" cellpadding="1" height="100%">
                <logic:present name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="lastTransactionTypeGeneralClinicalExamination">
                    <bean:define id="lastTransaction_generalClinicalExamination" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="lastTransactionTypeGeneralClinicalExamination"/>
                </logic:present>
                <tr class="gray">
                    <td width="33%"><%=getTran(request,"Web.Occup","medwan.healthrecord.clinical-examination.systeme-cardiovasculaire.TA",sWebLanguage)%>
                            (<%
                            TransactionID transactionID = getMyTransactionID(activePatient.personid,"'be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT','be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT','be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_LEFT','be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_LEFT'", out);
                            String sSBPR = "", sDBPR = "", sSBPL = "", sDBPL = "";
                            if (transactionID.transactionid>0){
                                sSBPR = getMyItemValue(transactionID,sPREFIX+"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT",sWebLanguage);
                                sDBPR = getMyItemValue(transactionID,sPREFIX+"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT",sWebLanguage);
                                sSBPL = getMyItemValue(transactionID,sPREFIX+"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_LEFT",sWebLanguage);
                                sDBPL = getMyItemValue(transactionID,sPREFIX+"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_LEFT",sWebLanguage);
                            }
                        %>)
                    </td>
                    <td width="33%">
                        <b>
                            <span <%=setRightClickMini("ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT")%>>
                                <%=sSBPR%>
                            </span>
                            /
                            <span <%=setRightClickMini("ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT")%>>
                                <%=sDBPR%>
                            </span>
                        </b>
                    </td>
                    <td width="33%">
                        <b>
                            <span <%=setRightClickMini("ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_LEFT")%>>
                                <%=sSBPL%>
                            </span>
                            /
                            <span <%=setRightClickMini("ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_LEFT")%>>
                                <%=sDBPL%>
                            </span>
                        </b>
                    </td>
                </tr>
            </table>
        </td>

        <%-- LAST BIOMETRY EXAMINATION --%>
        <td style="vertical-align:top;" colspan="2" height="100%">
            <table class="list" width="100%" border="0" cellspacing="1" cellpadding="0" height="100%">
                <tr class="gray">
                    <td width="25%">
                        <%=getTran(request,"openclinic.chuk","temperature",sWebLanguage)%> (&deg;C)
                        <b>
                            <%
                                String temperature=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid),"be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE");
                                if(temperature.length()>0){
                            %>
                            <div <%=setRightClickMini("[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE")%>><%=temperature%></div>
                            <input id="lastTemperature" type="hidden" name="lastTemperature" value="<%=temperature%>"/>
                            <%
                                }
                                else {
                                	out.print("<div>&nbsp;</div>");
                                }
                            %>
                        </b>
                    </td>
                    <td width="25%">
                        <%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.weight",sWebLanguage)%>
                        <b>
                            <%
                                String weight=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid),"be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_WEIGHT");
                                if(weight.length()>0){
                            %>
                            <div <%=setRightClickMini("ITEM_TYPE_BIOMETRY_WEIGHT")%>><%=weight%></div>
                            <input id="lastWeight" type="hidden" name="lastWeight" value="<%=weight%>"/>
                            <%
                                }
                                else {
                                	out.print("<div>&nbsp;</div>");
                                }
                            %>
                        </b>
                    </td>
                    <td width="25%">
                        <%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.length",sWebLanguage)%>
                        <b>
                            <%
                                 String height=MedwanQuery.getInstance().getLastItemValue(Integer.parseInt(activePatient.personid),"be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_HEIGHT");
                                if(height.length()>0){
                            %>
                                <div <%=setRightClickMini("ITEM_TYPE_BIOMETRY_HEIGHT")%>><%=height%></div>
                                <input id="lastHeight" type="hidden" name="lastHeight" value="<%=height%>"/>
                            <%
                                }
                                else {
                                	out.print("<div>&nbsp;</div>");
                                }
                            %>
                        </b>
                    </td>
                    <td width="25%">
                        <%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.bmi",sWebLanguage)%>
                        <b>
                            <%
                                if(height.length()*weight.length()>0){
                            %>
                                <div id="BMI"></div>
                                <script>
                                  if (document.getElementsByName('lastHeight')[0].value.length > 0 && document.getElementsByName('lastWeight')[0].value.length>0){
                                    var _BMI = (document.getElementsByName('lastWeight')[0].value * 10000) / (document.getElementsByName('lastHeight')[0].value * document.getElementsByName('lastHeight')[0].value);
                                    document.getElementsByName('BMI')[0].innerHTML = "<b>"+Math.round(_BMI*10)/10+"</b>";
                                  }
                                </script>
                            <%
                                }
                                else {
                                	out.print("<div>&nbsp;</div>");
                                }
                            %>
                        </b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>

    <%-- ANAMNESE ---------------------------------------------------------------------------------------------------%>
    <tr>
        <td width="70%" style="vertical-align:top;">
            <table class="list" width="100%" height="100%" cellspacing="1">
                <tr class="admin">
                    <td colspan="4"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_DIV_SINTOMAS", sWebLanguage, "onchange='showtests();'")%><%=getTran(request,"Web.Occup","vitalsigns",sWebLanguage)%> &nbsp;</td>
                </tr>
                <tr id="div_sintomas">
                    <td class="admin2111" colspan="4">
                        <table width="100%">
                            <tr>
                                <td nowrap><b><%=getTran(request,"openclinic.chuk","temperature",sWebLanguage)%>:</b>  </td><td nowrap><input id='temperature' type="text" class="text" <%=setRightClick(session,"[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE")%> name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE" property="value"/>" onBlur="if(isNumber(this)){if(!checkMinMaxOpen(25,45,this)){alertDialog('Web.Occup','medwan.common.unrealistic-value');}}" size="5"/> &deg; C</td>
                                <td nowrap><b><%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.length",sWebLanguage)%>:</b></td><td nowrap><input <%=setRightClickMini("ITEM_TYPE_BIOMETRY_HEIGHT")%> id="height" class="text" type="text" size="5" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_HEIGHT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_HEIGHT" property="value"/>" onBlur="calculateBMI();"/> cm</td>
                                <td nowrap><b><%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.weight",sWebLanguage)%>:</b></td><td nowrap><input <%=setRightClickMini("ITEM_TYPE_BIOMETRY_WEIGHT")%> id="weight" class="text" type="text" size="5" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_WEIGHT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_WEIGHT" property="value"/>" onBlur="calculateBMI();"/> kg</td>
                                <td nowrap><b><%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.bmi",sWebLanguage)%>:</b></td><td nowrap><input id="BMI" class="text" type="text" size="5" name="BMI" readonly /></td>
                            </tr>
                            <tr>
                                <td nowrap><b><%=getTran(request,"openclinic.chuk","sao2",sWebLanguage)%>:</b></td><td nowrap><input <%=setRightClick(session,"[GENERAL.ANAMNESE]ITEM_TYPE_SATURATION")%> type="text" class="text" size="5" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_SATURATION" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_SATURATION" property="value"/>"/> %</td>
                                <td nowrap><b><%=getTran(request,"web","abdomencircumference",sWebLanguage)%>:</b></td><td nowrap><input <%=setRightClick(session,"ITEM_TYPE_ABDOMENCIRCUMFERENCE")%> type="text" class="text" size="5" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_ABDOMENCIRCUMFERENCE" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_ABDOMENCIRCUMFERENCE" property="value"/>"/> cm</td>
                                <td nowrap><b><%=getTran(request,"web","fhr",sWebLanguage)%>:</b></td><td nowrap><input <%=setRightClick(session,"ITEM_TYPE_FOETAL_HEARTRATE")%> type="text" class="text" size="5" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_FOETAL_HEARTRATE" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_FOETAL_HEARTRATE" property="value"/>"/></td>
                                <td colspan='2'/>
                            </tr>
                            <tr>
                                <td nowrap colspan='2'><b><%=getTran(request,"Web.Occup","medwan.healthrecord.cardial.pression-arterielle",sWebLanguage)%>:</b> </td>
                                <td nowrap colspan='2'><b><%=getTran(request,"openclinic.chuk","respiratory.frequency",sWebLanguage)%>:</b></td>
                                <td nowrap colspan='2'><b><%=getTran(request,"Web.Occup","medwan.healthrecord.cardial.frequence-cardiaque",sWebLanguage)%>:</b></td>
                                <td nowrap colspan='2'><b><%=getTran(request,"Web.Occup","medwan.healthrecord.weightforlength",sWebLanguage)%></b></td>
                            </tr>
                            <tr>
                                <td nowrap colspan='2'><input id="sbpr" <%=setRightClick(session,"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT")%> type="text" class="text" size="3" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT" property="value"/>" onblur="setBP(this,'sbpr','dbpr');"> / <input id="dbpr" <%=setRightClick(session,"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT")%> type="text" class="text" size="3" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT" property="value"/>" onblur="setBP(this,'sbpr','dbpr');"> mmHg</td>
                                <td nowrap colspan='2'><input type="text" class="text" <%=setRightClick(session,"[GENERAL.ANAMNESE]ITEM_TYPE_RESPIRATORY_FRENQUENCY")%> name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_RESPIRATORY_FRENQUENCY" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_RESPIRATORY_FRENQUENCY" property="value"/>" onBlur="isNumber(this)" size="5"/> /min</td>
                                <td nowrap colspan='2'><input <%=setRightClick(session,"ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_HEARTH_FREQUENCY")%> type="text" class="text" size="3" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_HEARTH_FREQUENCY" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_HEARTH_FREQUENCY" property="value"/>" onblur="setHF(this);"> /min</td>
                                <td nowrap colspan='2'><input tabindex="-1" class="text" type="text" size="4" readonly name="WFL" id="WFL"><img id="wflinfo" style='display: none' src="<c:url value='/_img/icons/icon_info.gif'/>"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="admin">
                    <td align="left" colspan="4"><%=getTran(request,"Web.Occup","sp.ananmnesis",sWebLanguage)%></td>
                </tr>
                <tr>
                    <td class="admin" width="50px"><%=getTran(request,"Web.Occup","sp.ananmnesis.t.e",sWebLanguage)%></td>
                    <td colspan="2" class="admin2" width='15%'>
                        <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_TIME_DISASE", 30, 2 , sCONTEXTPATH) %>
                    </td>
                    <%-- KEYWORDS --%>
                    <td id='keywordstd' class="admin2" width="250px" rowspan="7" style="vertical-align:top;padding:0px;">
                        <div id='test'></div>
                        <div style="height:200px;overflow:auto;position: sticky;top: 0" id="keywords"></div>
                    </td>
                </tr>
                <tr height="40">
                    <td class="admin" width="50px"><%=getTran(request,"Web.Occup","sp.ananmnesis.m.c",sWebLanguage)%></td>
                    <td colspan="2" class="admin2">
                        <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_REASON_CONSULT", 30, 2, sCONTEXTPATH) %>
                    </td>
                </tr>
                <tr height="35">
                    <td class='admin' width='50px'><div id="title1"><%=getTran(request,"Web.Occup","sp.ananmnesis.s.s.p",sWebLanguage)%></div></td>
                    <td colspan="2">
                        <table width='100%'>
                            <tr onclick='selectKeywords("functional.signs.ids","functional.signs.text","ikirezi2.functional.signs","keywords",this)'>
                                <td class='admin2'>
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_SYMPTOMS_SIGNS", 30, 2, sCONTEXTPATH) %>
                                </td>
                                <td class='admin2' width='1%' nowrap style="text-align:center">
                                    <img width='16' id='key1' class="link" src='<c:url value="/_img/themes/default/keywords.jpg"/>'/>
                                </td>
                                <td class='admin2' width='50%' style="vertical-align:top;">
                                    <div id='functional.signs.text'><%=getKeywordsHTML((TransactionVO)transaction,ScreenHelper.ITEM_PREFIX+"[GENERAL.ANAMNESE]ITEM_TYPE_SYMPTOMS_SIGNS_IDS","functional.signs.text","functional.signs.ids",sWebLanguage)%></div>
                                    <input type='hidden' id='functional.signs.ids' name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_SYMPTOMS_SIGNS_IDS" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_SYMPTOMS_SIGNS_IDS" property="value"/>"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr height="35">
                    <td class="admin" width="50px"><%=getTran(request,"Web.Occup","sp.ananmnesis.f.f",sWebLanguage)%></td>
                    <td colspan="2" class="admin2">
                        <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_PHYSIO_FUNCTIONS", 30, 2, sCONTEXTPATH) %>
                    </td>
                </tr>
                <tr class="admin">
                    <td align="left" colspan="3"><%=getTran(request,"Web.Occup","sp.physio.examen",sWebLanguage)%></td>
                </tr>
                <tr height="35">
                    <td class='admin' width='20%'><div id="title2"><%=getTran(request,"Web.Occup","sp.ananmnesis.e.f.g",sWebLanguage)%></div></td>
                    <td colspan="2">
                        <table width='100%'>
                            <tr onclick='selectKeywords("inspection.ids","inspection.text","ikirezi2.inspection","keywords",this)'>
                                <td class='admin2'>
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_EXAM_PHYSIO_GENERAL", 30, 2, sCONTEXTPATH) %>
                                </td>
                                <td class='admin2' width='1%' style="text-align:center">
                                    <img width='16' id='key2' class="link" src='<c:url value="/_img/themes/default/keywords.jpg"/>'/>
                                </td>
                                <td class='admin2' width='50%' style="vertical-align:top;">
                                    <div id='inspection.text'><%=getKeywordsHTML((TransactionVO)transaction,ScreenHelper.ITEM_PREFIX+"[GENERAL.ANAMNESE]ITEM_TYPE_EXAM_PHYSIO_GENERAL_IDS","inspection.text","inspection.ids",sWebLanguage)%></div>
                                    <input type='hidden' id='inspection.ids' name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_EXAM_PHYSIO_GENERAL_IDS" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.[GENERAL.ANAMNESE]ITEM_TYPE_EXAM_PHYSIO_GENERAL_IDS" property="value"/>"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr height="35">
                    <td class="admin" width="50px"><%=getTran(request,"Web.Occup","sp.ananmnesis.e.f.r",sWebLanguage)%></td>
                    <td colspan="2" class="admin2">
                        <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_EXAM_PHYSIO_REGIONAL", 30, 2, sCONTEXTPATH) %>
                    </td>
                </tr>
                <tr class="admin">
                    <td colspan="4"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_DIV_AYUDA_AL_DX", sWebLanguage, "onchange='showtests();'")%><%=getTran(request,"web.occup","sp.apoyodx",sWebLanguage)%> &nbsp;</td>
                </tr>
                <tr id="div_apoyo">
                    <td colspan="4">
                        <table width="100%">
                            <tr height="40">
                                <td class="admin" width="20%"><%=getTran(request,"web.occup","sp.apoyolab",sWebLanguage)%></td>
                                <td colspan="3" class="admin2">
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.APOYODX]ITEM_TYPE_LABORATORIO", 60, 2, sCONTEXTPATH) %>
                                </td>
                            </tr>
                            <tr height="40">
                                <td class="admin" width="20%"><%=getTran(request,"web.occup","sp.apoyoima",sWebLanguage)%></td>
                                <td colspan="3" class="admin2">
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.APOYODX]ITEM_TYPE_IMAGENES", 60, 2, sCONTEXTPATH) %>
                                </td>
                            </tr>
                            <tr height="40">
                                <td class="admin" width="20%"><%=getTran(request,"web.occup","sp.apoyorx",sWebLanguage)%></td>
                                <td colspan="3" class="admin2">
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL.APOYODX]ITEM_TYPE_RESULTADOS_RX", 60, 2, sCONTEXTPATH) %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="admin">
                    <td colspan="4"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_DIV_PROCEDIMIENTOS", sWebLanguage, "onchange='showtests();'")%><%=getTran(request,"Web.Occup","sp.procedimientos",sWebLanguage)%> &nbsp;</td>
                </tr>
                <tr id="div_proc">
                    <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.cu",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_CURACION", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.r.p",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_RETIRO_PUNTOS", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.n",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_NEBULIZACION", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.c.s.f",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_SONDA_FOLEY", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.d.a",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_DEBRIDACION_ABSESO", sWebLanguage, "") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.c.y",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_COLOCA_YESO", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.r.y",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_RETIRO_YESO", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.e.u",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_EXTRAXION_UNHA", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.e",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_ESCISION", sWebLanguage, "") %>
                                </td>
                                <td class="admin"><%=getTran(request,"Web.Occup","sp.procedimientos.o",sWebLanguage)%></td>
                                <td class="admin2">
                                    <%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "[GENERAL.PROCEDURE]ITEM_TYPE_OTROS", sWebLanguage, "") %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="admin">
                    <td colspan="4"><%=SH.writeDefaultCheckBox((TransactionVO)transaction, request, "yesno", "ITEM_TYPE_DIV_INTER_REFE", sWebLanguage, "onchange='showtests();'")%><%=getTran(request,"Web.Occup","sp.interconsultas",sWebLanguage)%> &nbsp;</td>
                </tr>
                <tr id="div_interconsulta">
                    <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td class="admin" width="20%"><%=getTran(request,"Web.Occup","sp.generalidades.i",sWebLanguage)%></td>
                                <td colspan="3" class="admin2">
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL]ITEM_TYPE_INTERCONSULTA", 60, 2, sCONTEXTPATH)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="admin" width="20%"><%=getTran(request,"Web.Occup","sp.generalidades.r.o.e",sWebLanguage)%></td>
                                <td colspan="3" class="admin2">
                                    <%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "[GENERAL]ITEM_TYPE_REFERENCIA", 60, 2, sCONTEXTPATH)%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="admin" width="20%"><%=getTran(request,"Web.Occup","sp.proxima.cita",sWebLanguage)%>&nbsp;</td>
                    <td class="admin2" colspan="2">
                        <select class='text' id='frequency' onchange='calculateNextDate()'>
                            <option/>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                            <option value='3'>3</option>
                            <option value='4'>4</option>
                            <option value='5'>5</option>
                            <option value='6'>6</option>
                            <option value='7'>7</option>
                            <option value='8'>8</option>
                            <option value='9'>9</option>
                            <option value='10'>10</option>
                            <option value='11'>11</option>
                            <option value='12'>12</option>
                        </select>
                        <select class='text' id='frequencytype' onchange='calculateNextDate()'>
                            <option/>
                            <option value='week'><%=getTran(request,"web","weeks",sWebLanguage) %></option>
                            <option value='month'><%=getTran(request,"web","months",sWebLanguage) %></option>
                            <option value='year'><%=getTran(request,"web","year",sWebLanguage) %></option>
                        </select>
                        <input type="hidden" id="ccbrtdate" name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_PROXIMA_CITA" property="itemId"/>]>.value"/>
                        <%=(ScreenHelper.writeDateField("vafollowupdate", "transactionForm", ((TransactionVO)transaction).getItemValue("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_PROXIMA_CITA"), false, true, sWebLanguage, sCONTEXTPATH))%>
                    </td>
                    <td class="admin2">
                        <a href="javascript:openPopup('planning/findPlanning.jsp&isPopup=1&FindDate='+document.getElementById('vafollowupdate').value+'&FindUserUID=',1024,600,'Agenda','toolbar=no,status=yes,scrollbars=no,resizable=yes,width=1024,height=600,menubar=no');void(0);"><%=getTran(request,"web","findappointment",sWebLanguage) %></a>
                    </td>
                </tr>
				<tr>
					<td class="admin"><%=getTran(request,"web","evolution",sWebLanguage)%>&nbsp;</td>
					<td class="admin2" colspan="3">
						<%
							Encounter activeEncounter = Encounter.getActiveEncounter(activePatient.personid);
							if(activeEncounter!=null && checkString(activeEncounter.getOutcome()).length()>0){
								out.println(getTran(request,MedwanQuery.getInstance().getConfigString("encounterOutcomeType","encounter.outcome"),activeEncounter.getOutcome(),sWebLanguage)+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
							}
						%>
						<a href='javascript:openEncounter()'><%=getTran(request,"web","editencounter",sWebLanguage) %></a>
					</td>
				</tr>
            </table>
        </td>

        <%-- KLINISCH ONDERZOEK -------------------------------------------------------------------------------------%>
        <td style="vertical-align:top;" colspan="2">
        	<%ScreenHelper.setIncludePage(customerInclude("healthrecord/diagnosesEncoding.jsp"),pageContext);%>
            <br/>
            <table width="100%" class="list" cellspacing="1">
                <tr class="admin">
                    <td align="center"><a href="javascript:showProblemlist();"><%=getTran(request,"web.occup","medwan.common.problemlist",sWebLanguage)%></a></td>
                </tr>
                <tr>
                    <td id="problemList">
                        <%
                            Vector activeProblems = Problem.getActiveProblems(activePatient.personid);
                            if(activeProblems.size()>0){
                                out.print("<table width='100%' cellspacing='0' class='list'><tr class='admin'><td>"+getTran(request,"web.occup","medwan.common.description",sWebLanguage)+"</td><td nowrap>"+getTran(request,"web.occup","medwan.common.datebegin",sWebLanguage)+"</td></tr>");

                                String sClass = "1";

                                for(int n=0;n<activeProblems.size();n++){
                                	// alternate row-style
                                    if(sClass.equals("")) sClass = "1";
                                    else                  sClass = "";
                                	
                                    Problem activeProblem = (Problem)activeProblems.elementAt(n);
                                    String comment="";
                                    if(activeProblem.getComment().trim().length()>0){
                                        comment=":&nbsp;<i>"+activeProblem.getComment().trim()+"</i>";
                                    }
                                    out.print("<tr class='list"+sClass+"'><td><b>"+(activeProblem.getCode()+" "+MedwanQuery.getInstance().getCodeTran(activeProblem.getCodeType()+"code"+activeProblem.getCode(),sWebLanguage)+"</b>"+comment)+"</td><td>"+ScreenHelper.stdDateFormat.format(activeProblem.getBegin())+"</td></tr>");
                                }
                                out.print("</table>");
                            }
                        %>
                    </td>
                </tr>
            </table>
            <br/>
            
            <table width="100%" class="list" cellspacing="1">
                <tr class="admin">
                    <td align="center"><%=getTran(request,"Web.Occup","medwan.healthrecord.medication",sWebLanguage)%> (<a href="javascript:openPopup('medical/managePrescriptionsPopup.jsp&amp;Action=showDetailsNew&amp;Close=true&amp;findProduct=true',650,430,'medication');void(0);"><%=getTran(request,"web","medications",sWebLanguage)%></a>)</td>
                </tr>
                <tr>
                    <td>
                    <%
                    //--- DISPLAY ACTIVE PRESCRIPTIONS (of activePatient) ---------------------------------
                    // compose query
                    Vector vActivePrescriptions = Prescription.findActive(activePatient.personid,activeUser.userid,"","","","","","");

                    StringBuffer prescriptions = new StringBuffer();
                    Vector idsVector = getActivePrescriptionsFromRs(prescriptions, vActivePrescriptions , sWebLanguage);
                    int foundPrescrCount = idsVector.size();

                    if(foundPrescrCount > 0){
                        %>
                            <table width="100%" cellspacing="0" cellpadding="0" class="list">
                                <%-- header --%>
                                <tr class="admin">
                                    <td width="22" nowrap>&nbsp;</td>
                                    <td width="30%"><%=getTran(request,"Web","product",sWebLanguage)%></td>
                                    <td width="15%"><%=getTran(request,"Web","begindate",sWebLanguage)%></td>
                                    <td width="15%"><%=getTran(request,"Web","enddate",sWebLanguage)%></td>
                                    <td width="40%"><%=getTran(request,"Web","prescriptionrule",sWebLanguage)%></td>
                                </tr>

                                <tbody class="hand"><%=prescriptions%></tbody>
                            </table>
                        <%
                    }
                    else{
                        // no records found
                        %><%=getTran(request,"web","noactiveprescriptionsfound",sWebLanguage)%><br><%
                    }
                    %>
                    </td>
                </tr>
                <tr class="admin">
                    <td align="center"><%=getTran(request,"curative","medication.paperprescriptions",sWebLanguage)%> (<%=ScreenHelper.stdDateFormat.format(((TransactionVO)transaction).getUpdateTime())%>) (<a href="javascript:openPopup('medical/managePrescriptionForm.jsp&amp;skipEmpty=1',650,430,'medication');void(0);"><%=getTran(request,"web","medicationpaperprescription",sWebLanguage)%></a>)</td>
                </tr>
                <%
                    Vector paperprescriptions = PaperPrescription.find(activePatient.personid,"",ScreenHelper.stdDateFormat.format(((TransactionVO)transaction).getUpdateTime()),ScreenHelper.stdDateFormat.format(((TransactionVO)transaction).getUpdateTime()),"","DESC");
                    if(paperprescriptions.size()>0){
                        out.print("<tr><td><table width='100%'>");
                        String l="";
                        for(int n=0;n<paperprescriptions.size();n++){
                            if(l.length()==0){
                                l="1";
                            }
                            else{
                                l="";
                            }
                            PaperPrescription paperPrescription = (PaperPrescription)paperprescriptions.elementAt(n);
                            out.println("<tr class='list"+l+"' id='pp"+paperPrescription.getUid()+"'><td valign='top' width='90px'><img src='_img/icons/icon_delete.png' onclick='deletepaperprescription(\""+paperPrescription.getUid()+"\");'/> <b>"+ScreenHelper.stdDateFormat.format(paperPrescription.getBegin())+"</b></td><td><i>");
                            Vector products =paperPrescription.getProducts();
                            for(int i=0;i<products.size();i++){
                                out.print(products.elementAt(i)+"<br/>");
                            }
                            out.println("</i></td></tr>");
                        }
                        out.print("</table></td></tr>");
                    }
                    else{
                        out.print("<tr><td><table width='100%'>");
                        // no records found
                        %><%=getTran(request,"web","noactiveprescriptionsfound",sWebLanguage)%><br><%
                        out.print("</table></td></tr>");
                    }
                %>
            </table>
        </td>
    </tr>
</table>

<script>
	function openEncounter(){
		openPopup("adt/editEncounter.jsp&ReloadParent=no&Popup=yes&EditEncounterUID=" + document.getElementById('encounteruid').value + "&ts=<%=getTs()%>",800);
    }
    function showtests(){
        if(document.getElementById('ITEM_TYPE_DIV_PROCEDIMIENTOS').checked){
            // document.getElementsByClassName('div_proc').item(0).style.display='';
            document.getElementById('div_proc').style.display='';
        }
        else{
            document.getElementById('div_proc').style.display='none';
        }
        if(document.getElementById('ITEM_TYPE_DIV_SINTOMAS').checked){
            document.getElementById('div_sintomas').style.display='';
        }
        else{
            document.getElementById('div_sintomas').style.display='none';
        }
        if(document.getElementById('ITEM_TYPE_DIV_INTER_REFE').checked){
            document.getElementById('div_interconsulta').style.display='';
        }
        else{
            document.getElementById('div_interconsulta').style.display='none';
        }
        if(document.getElementById('ITEM_TYPE_DIV_AYUDA_AL_DX').checked){
            document.getElementById('div_apoyo').style.display='';
        }
        else{
            document.getElementById('div_apoyo').style.display='none';
        }
    }
    showtests();
  function setBP(oObject,sbp,dbp){
    if(oObject.value.length>0){
      if(!isNumberLimited(oObject,40,300)){
        alertDialog("Web.occup","out-of-bounds-value");
      }
      else if((sbp.length>0)&&(dbp.length>0)){
        isbp = document.getElementById(sbp)[0].value*1; // .getElementsByName
        idbp = document.getElementById(dbp)[0].value*1; // .getElementsByName
        if(idbp>isbp){
          alertDialog("Web.occup","error.dbp_greather_than_sbp");
        }
      }
    }
  }

  function setHF(oObject){
    if(oObject.value.length>0){
      if(!isNumberLimited(oObject,30,300)){
        alertDialog("Web.occup","out-of-bounds-value");
      }
    }
  }

  function deleteDiagnose(rowid){
    activeDiagnosis.deleteRow(rowid.rowIndex);
  }

  function showProblemlist(){
    openPopup("medical/manageProblems.jsp&ts=<%=getTs()%>");
  }

  function doShowDetails(uid){
    openPopup("medical/managePrescriptionsPopup.jsp&Action=showDetails&EditPrescrUid="+uid);
  }

  <%-- CALCULATE BMI --%>
  function calculateBMI(){
      var _BMI = 0;
      var heightInput = document.getElementById('height');
      var weightInput = document.getElementById('weight');

      if(heightInput.value > 0){
          _BMI = (weightInput.value * 10000) / (heightInput.value * heightInput.value);
          if (_BMI > 100 || _BMI < 5){
              document.getElementsByName('BMI')[0].value = "";
          }
          else {
              document.getElementsByName('BMI')[0].value = Math.round(_BMI*10)/10;
          }
          var wfl=(weightInput.value*1/heightInput.value*1);
          if(wfl>0){
              document.getElementById('WFL').value = wfl.toFixed(2);
              checkWeightForHeight(heightInput.value,weightInput.value);
          }
      }
  }

  function checkWeightForHeight(height,weight){
      var today = new Date();
      var url= '<c:url value="/ikirezi/getWeightForHeight.jsp"/>?height='+height+'&weight='+weight+'&gender=<%=activePatient.gender%>&ts='+today;
      new Ajax.Request(url,{
              method: "POST",
              postBody: "",
              onSuccess: function(resp){
                  var label = eval('('+resp.responseText+')');
                  if(label.zindex>-999){
                      if(label.zindex<-4){
                          document.getElementById("WFL").className="darkredtext";
                          document.getElementById("wflinfo").title="Z-index < -4: <%=getTranNoLink("web","severe.malnutrition",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else if(label.zindex<-3){
                          document.getElementById("WFL").className="darkredtext";
                          document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","severe.malnutrition",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else if(label.zindex<-2){
                          document.getElementById("WFL").className="orangetext";
                          document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","moderate.malnutrition",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else if(label.zindex<-1){
                          document.getElementById("WFL").className="yellowtext";
                          document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","light.malnutrition",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else if(label.zindex>2){
                          document.getElementById("WFL").className="orangetext";
                          document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","obesity",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else if(label.zindex>1){
                          document.getElementById("WFL").className="yellowtext";
                          document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","light.obesity",sWebLanguage).toUpperCase()%>";
                          document.getElementById("wflinfo").style.display='';
                      }
                      else{
                          document.getElementById("WFL").className="text";
                          document.getElementById("wflinfo").style.display='none';
                      }
                  }
                  else{
                      document.getElementById("WFL").className="text";
                      document.getElementById("wflinfo").style.display='none';
                  }
              },
              onFailure: function(){
              }
          }
      );
  }
  calculateBMI();
</script>
</logic:present>