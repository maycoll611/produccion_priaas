var $j = jQuery.noConflict();

var webcamElement = document.getElementById('webcam');
var canvasElement = document.getElementById('canvasPhoto');
var localstream;

$j("#webcam-switch").change(function () {
    if(this.checked){
        $j('.md-modal').addClass('md-show');
		navigator.mediaDevices.getUserMedia({ video: true })
		.then(
			(stream) => {
				cameraStarted();
				webcamElement.srcObject = stream;
				localstream = stream;
				console.log("webcam started");
			}
		)
		.catch((error) => {
			console.log(error);
			 displayError();
		})
    }
    else {        
        cameraStopped();
		//webcamElement.srcObject = "";
		localstream.getTracks()[0].stop();
        console.log("webcam stopped");
    }        
});

$j('#cameraFlip').click(function() {
    //webcam.flip();
    //webcam.start();  
});

$j('#download-photo').click(function() {
	$j("#submitFormPhoto").click();
});

$j('#closeError').click(function() {
    $j("#webcam-switch").prop('checked', false).change();
});

function displayError(err = ''){
    if(err!=''){
        $j("#errorMsg").html(err);
    }
    $j("#errorMsg").removeClass("d-none");
}

function cameraStarted(){
    $j("#errorMsg").addClass("d-none");
    $j('.flash').hide();
    $j("#webcam-caption").html("on");
    $j("#webcam-control").removeClass("webcam-off");
    $j("#webcam-control").addClass("webcam-on");
    $j(".webcam-container").removeClass("d-none");
	// if( webcam.webcamList.length > 1){
    $j("#cameraFlip").removeClass('d-none');
    // }
    $j("#wpfront-scroll-top-container").addClass("d-none");
    window.scrollTo(0, 0); 
    $j('body').css('overflow-y','hidden');
}

function cameraStopped(){
    $j("#errorMsg").addClass("d-none");
    $j("#wpfront-scroll-top-container").removeClass("d-none");
    $j("#webcam-control").removeClass("webcam-on");
    $j("#webcam-control").addClass("webcam-off");
    $j("#cameraFlip").addClass('d-none');
    $j(".webcam-container").addClass("d-none");
    $j("#webcam-caption").html("Abrir Camara");
    $j('.md-modal').removeClass('md-show');
}

$j("#take-photo").click(function () {
    beforeTakePhoto();
    var canvas = document.getElementById("canvasPhoto");
	var ctx = canvas.getContext("2d");
	webcamElement.width=webcamElement.videoWidth;
	webcamElement.height=webcamElement.videoHeight;
	ctx.drawImage(webcamElement, 0, 0, webcamElement.videoWidth, webcamElement.videoHeight);
	$j("#dataImg").val(canvas.toDataURL('image/jpeg', 1.0));
    afterTakePhoto();
});

function beforeTakePhoto(){
    $j('.flash')
        .show() 
        .animate({opacity: 0.3}, 500) 
        .fadeOut(500)
        .css({'opacity': 0.7});
    window.scrollTo(0, 0); 
    $j('#webcam-control').addClass('d-none');
    $j('#cameraControls').addClass('d-none');
}

function afterTakePhoto(){
	localstream.getTracks()[0].stop();
    $j('#canvasPhoto').removeClass('d-none');
    $j('#take-photo').addClass('d-none');
    $j('#exit-app').removeClass('d-none');
    $j('#download-photo').removeClass('d-none');
    $j('#resume-camera').removeClass('d-none');
    $j('#cameraControls').removeClass('d-none');
}

function removeCapture(){
    $j('#canvasPhoto').addClass('d-none');
    $j('#webcam-control').removeClass('d-none');
    $j('#cameraControls').removeClass('d-none');
    $j('#take-photo').removeClass('d-none');
    $j('#exit-app').addClass('d-none');
    $j('#download-photo').addClass('d-none');
    $j('#resume-camera').addClass('d-none');
}

$j("#resume-camera").click(function () {
	navigator.mediaDevices.getUserMedia({ video: true })
	.then(
		(stream) => {
			removeCapture();
			webcamElement.srcObject = stream;
			localstream = stream;
			console.log("webcam resume");
		}
	)
	.catch((error) => {
		console.log(error);
			displayError();
	})
});

$j("#exit-app").click(function () {
    removeCapture();
    $j("#webcam-switch").prop("checked", false).change();
});

$j("#FormPhoto").submit(function(e){
	// alert("para enviar...");
    e.preventDefault();
    appendFileAndSubmit();
});
