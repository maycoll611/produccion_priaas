<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<%
    String tab = checkString(request.getParameter("Tab"));
    if(tab.equals("")) tab = "Admin";

    boolean activePatientIsUser = false;
    if(activePatient!=null){
        activePatientIsUser = activePatient.isUser();
    }
%>
<%=sJSMASKEDINPUT%>
<form name="PatientEditForm" id="PatientEditForm" method="POST" action='<c:url value='/main.do'/>?Page=_common/patient/patienteditSave.jsp&SavePatientEditForm=ok&Tab=<%=tab%>&ts=<%=getTs()%>'>
	<input type='hidden' name='noimmatcheck' id='noimmatcheck'/>
	<input type='hidden' name='nonatregcheck' id='nonatregcheck'/>
    <%-- TABS -----------------------------------------------------------------------------------%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="tabs">&nbsp;</td>
            <td class="tabunselected" width="1%" onclick="activateTab('Admin')" id="td0" nowrap>&nbsp;<b><%=getTran(request,"Web","actualpersonaldata",sWebLanguage)%></b>&nbsp;</td>
            <td class="tabs">&nbsp;</td>
            <td class="tabunselected" width="1%" onclick="activateTab('AdminPrivate')" id="td1" nowrap>&nbsp;<b><%=getTran(request,"Web","private",sWebLanguage)%></b>&nbsp;</td>
            <td class="tabs">&nbsp;</td>
            <td class="tabunselected" width="1%" onclick="activateTab('AdminFamilyRelation')" id="td3" nowrap>&nbsp;<b><%=getTran(request,"Web","AdminFamilyRelation",sWebLanguage)%></b>&nbsp;</td>
            <td class="tabs">&nbsp;</td>
            <td class="tabunselected" width="1%" onclick="activateTab('AdminResource')" id="td4" nowrap>&nbsp;<b><%=getTran(request,"Web","AdminResource",sWebLanguage)%></b>&nbsp;</td>
            <td class="tabs" width="100%">&nbsp;</td>
        </tr>
    </table>
    
    <%-- ONE TAB --------------------------------------------------------------------------------%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr id="tr0-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("_common/patient/patienteditAdmin.jsp"),pageContext);%></td>
        </tr>
        <tr id="tr1-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("_common/patient/patienteditAdminPrivate.jsp"),pageContext);%></td>
        </tr>
        <tr id="tr3-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("_common/patient/patienteditAdminFamilyRelation.jsp"),pageContext);%></td>
        </tr>
        <tr id="tr4-view" style="display:none">
            <td><%ScreenHelper.setIncludePage(customerInclude("_common/patient/patienteditAdminResource.jsp"),pageContext);%></td>
        </tr>
    </table>
    
    <%-- BUTTONS --%>
    <%
        if (activeUser.getAccessRight("patient.administration.edit")||activeUser.getAccessRight("patient.administration.add")){
            %>
                <div id="saveMsg"><%=getTran(request,"Web","colored_fields_are_obligate",sWebLanguage)%></div>
                <%=ScreenHelper.alignButtonsStart()%>
                    <input class="button" type="button" name="SavePatientEditForm" value="<%=getTranNoLink("Web","Save",sWebLanguage)%>" onclick="checkSubmit();">&nbsp;
                    <input class="button" type="button" name="cancel" value="<%=getTranNoLink("Web","back",sWebLanguage)%>" onClick='window.location.href="<c:url value='/patientdata.do'/>?ts=<%=getTs()%>";'>&nbsp;
                <%=ScreenHelper.alignButtonsStop()%>
            <%
        }
    %>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  var maySubmit = true;
  var displayGenericAlert = true;

  <%-- CHECK SUBMIT --%>
  function checkSubmit(){
    maySubmit = true;
    if(maySubmit){ maySubmit = checkSubmitAdmin(); }
    if(maySubmit){ maySubmit = checkSubmitAdminPrivate(); }
    if(maySubmit){ maySubmit = checkSubmitAdminFamilyRelation(); }

    if(maySubmit){
      openPopup("/_common/patient/patienteditSavePopup.jsp&PersonID=<%=(activePatient!=null?activePatient.personid:"")%>&Lastname="+PatientEditForm.Lastname.value+"&Firstname="+PatientEditForm.Firstname.value+"&DateOfBirth="+PatientEditForm.DateOfBirth.value+"&ImmatNew="+PatientEditForm.ImmatNew.value+"&NatReg="+PatientEditForm.NatReg.value+"&ts=<%=getTs()%>");
    }
    else if(displayGenericAlert){  
      alertDialog("web.manage","somefieldsareempty");
    }
  }

  function SendSyncPatient(){
      let url = "<%=MedwanQuery.getInstance().getConfigString("api.url.clientes")%>"; //'http://159.223.97.149/api/clientes/';
      let fec_nacimiento = document.querySelector('#DateOfBirth').value.substr(6,4)+'-'+document.querySelector('#DateOfBirth').value.substr(3,2)+'-'+document.querySelector('#DateOfBirth').value.substr(0,2)+' 00:00:00';
      let parametros = {
          paterno: document.querySelector('#Lastname').value,
          materno: document.querySelector('#Middlename').value,
          nombres: document.querySelector('#Firstname').value,
          documento_tipo: document.querySelector('select[name="PersonType"]').value,
          documento_numero: document.querySelector('input[name="NatReg"]').value,
          genero: document.querySelector('select[name="Gender"]').value, //'M', asasasasd
          fecha_nac: fec_nacimiento,  // '1944-12-21 00:00:00'
          lugar: document.querySelector('#PCity').value,
          estado_civil: document.querySelector('select[name="CivilStatus"]').value,
          direccion: document.querySelector('#PAddress').value,
          distrito: document.querySelector('select[name="PSector"]').value,
          telefono: document.querySelector('#PTelephone').value,
          correo: document.querySelector('#PEmail').value,
          actividad: document.querySelector('#PFunction').value,
          referido: document.querySelector('#PBusiness').value,
          alergia: document.querySelector('#PComment').value,
          observacion: document.querySelector('#Comment').value,
          personid: document.querySelector('#ImmatNew').value
      };
      axios.post(url, parametros)
          .then(response =>{
              alert('Se inserto en Sistema Externo.');
              PatientEditForm.datacenterpatientexport.value='1';
              PatientEditForm.SavePatientEditForm.disabled = true;
              PatientEditForm.submit();
          });
  }
  var sApiSync = "<%=MedwanQuery.getInstance().getConfigString("api.sync")%>";
  <%-- DO SUBMIT --%>
  function doSubmit(){
    if(sApiSync=="true" && document.querySelector('#isSync').value == 'SI' && document.querySelector('#personid').value != '' && document.querySelector('#personid').value.length > 0) {
        let url = "<%=MedwanQuery.getInstance().getConfigString("api.url.clientes")%>"; //'http://159.223.97.149/api/clientes/';
        let fec_nacimiento = document.querySelector('#DateOfBirth').value.substr(6,4)+'-'+document.querySelector('#DateOfBirth').value.substr(3,2)+'-'+document.querySelector('#DateOfBirth').value.substr(0,2)+' 00:00:00';
        let parametros = {
            paterno: document.querySelector('#Lastname').value,
            materno: document.querySelector('#Middlename').value,
            nombres: document.querySelector('#Firstname').value,
            documento_tipo: document.querySelector('select[name="PersonType"]').value,
            documento_numero: document.querySelector('input[name="NatReg"]').value,
            genero: document.querySelector('select[name="Gender"]').value,
            fecha_nac: fec_nacimiento,
            lugar: document.querySelector('#PCity').value,
            estado_civil: document.querySelector('select[name="CivilStatus"]').value,
            direccion: document.querySelector('#PAddress').value,
            distrito: document.querySelector('select[name="PSector"]').value,
            telefono: document.querySelector('#PTelephone').value,
            correo: document.querySelector('#PEmail').value,
            actividad: document.querySelector('#PFunction').value,
            referido: document.querySelector('#PBusiness').value,
            alergia: document.querySelector('#PComment').value,
            observacion: document.querySelector('#Comment').value,
            personid: document.querySelector('#personid').value
        };
        axios.put(url+document.querySelector('#personid').value, parametros)
            .then(response =>{
                alert('Se actualizo en Sistema Externo.');
                PatientEditForm.datacenterpatientexport.value='1';
                PatientEditForm.SavePatientEditForm.disabled = true;
                PatientEditForm.submit();
            });
    }
    else {
        PatientEditForm.SavePatientEditForm.disabled = true;
        PatientEditForm.submit();
    }
  }

  <%-- ACTIVATE TAB --%>
  function activateTab(sTab){
    document.getElementById("tr0-view").style.display = "none";
    td0.className = "tabunselected";
    if(sTab=="Admin"){
      document.getElementById("tr0-view").style.display = "";
      td0.className = "tabselected";
      PatientEditForm.Lastname.focus();
      document.getElementById("saveMsg").style.display = "block";
    }

    document.getElementById("tr1-view").style.display = "none";
    td1.className = "tabunselected";
    if(sTab=="AdminPrivate"){
      document.getElementById("tr1-view").style.display = "";
      td1.className = "tabselected";
      PatientEditForm.PBegin.focus();
      document.getElementById("saveMsg").style.display = "block";
    }

    document.getElementById("tr3-view").style.display = "none";
    td3.className = "tabunselected";
    if(sTab=="AdminFamilyRelation"){
      document.getElementById("tr3-view").style.display = "";
      td3.className = "tabselected";
      document.getElementById("saveMsg").style.display = "none";
    }

    document.getElementById("tr4-view").style.display = "none";
    td4.className = "tabunselected";
    if(sTab=="AdminResource"){
      document.getElementById("tr4-view").style.display = "";
      td4.className = "tabselected";
      document.getElementById("saveMsg").style.display = "none";
    }
  }
  
  activateTab("<%=tab%>");
</script>

<%=writeJSButtons("PatientEditForm","SavePatientEditForm")%>