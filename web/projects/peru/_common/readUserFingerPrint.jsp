<%@include file="/includes/helper.jsp"%>
<%
	//Clear fingerprint call id
	session.setAttribute("fingerprintid", "");
	session.setAttribute("fingerprintimage", "");
	MedwanQuery.setSession(session,new User());

	//Call local fingerprint reader
	
%>
<head>
    <%=sCSSNORMAL%>
    <%=sJSPROTOTYPE%>
    
    <script>
      window.resizeTo(400,200);
      window.moveTo((self.screen.width-document.body.clientWidth)/2,(self.screen.height-document.body.clientHeight)/2);
    </script>
</head>

<table width='100%'>
	<tr>
		<td>
			<table width='100%'>
				<tr>
					<td>
						<%
							out.print("<img src='"+request.getParameter("referringServer")+"/_img/themes/default/ajax-loader.gif'/>"+
						              "<br><br>"+MedwanQuery.getInstance().getLabel("web","waiting_for_fingerprint","en")+"</br>");
						%>
					</td>
				</tr>
				<tr>
					<td>
						<form name="frmFingerPrint" method="post" action="<c:url value="/_common/readFingerPrint.jsp"/>">
							<input type="hidden" name="language" value="en"/>
							<input type="hidden" name="start" value="<%=ScreenHelper.getTs()%>"/>
							<input type="hidden" name="referringServer" value="<%=request.getParameter("referringServer")%>"/>
							<label name='readerID' id='readerID'></label>
						</form>
					</td>
				</tr>
			</table>
		</td>
		<td>
			<img width='80px' id='fingerprintImage' name='fingerprintImage' src="<c:url value="/_img/fingerprintImageSmallNoPrint.jpg"/>"/>
		</td>
	</tr>
</table>
<%-- <button onclick="doRead()">validar</button>
<button onclick="prueba()">validar1</button> --%>
<IFRAME style="display:none" name="hidden-form"></IFRAME>

<script>

	var reads=0;
	var fingerprintimage = "";
	var fingerprintjpg = "";
	// doRead1();
	function doRead1(idUser){
		var parameters='idUser='+idUser;
		var url = '<c:url value="/projects/peru/_common/dp/getUserSuccess.jsp"/>?ts='+new Date().getTime();
		new Ajax.Request(url,{
		  	method: "POST",
		  	postBody: parameters,
		  	onSuccess: function(resp){
			    var s=eval('('+resp.responseText+')');
				console.log(s);
			    if(s.success==1){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmall.jpg"/>';
					selectUser(s.userid,s.password);
			    }
			    else if(s.success==0){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
			        if(reads<5){
			    		window.open("<%=sCONTEXTPATH%>/util/startFingerPrintReader.jsp","hidden-form");
			    		//window.setTimeout("doRead()",1000);
			    		reads++;
			        }
			        else{
			        	window.close();
			        }
		    	}
			    else if(s.success==-99){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
		    	    window.open("<%=sCONTEXTPATH%>/util/initializeFingerPrintReader.jsp?clear=1","hidden-form");
				    //window.setTimeout("doDetect()",1000);
			    }
			    else{
		    		//window.setTimeout("doRead()",1000);
			    }
		  	},
		  	onFailure: function(){
	    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
	    		//window.setTimeout("doRead()",1000);
		  	}
		});

	}
	
	
	function doRead(){
		var parameters='fingerprintimage='+fingerprintimage+'&fingerprintjpg='+fingerprintjpg;
		var url = '<c:url value="/projects/peru/_common/dp/readUserFingerPrintSecugen.jsp"/>?ts='+new Date().getTime();
		new Ajax.Request(url,{
		  	method: "POST",
		  	postBody: parameters,
		  	onSuccess: function(resp){
			    var s=eval('('+resp.responseText+')');
			    if(s.success==1){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmall.jpg"/>';
					selectUser(s.userid,s.password);
			    }
			    else if(s.success==0){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
			        if(reads<5){
			    		window.open("<%=sCONTEXTPATH%>/util/startFingerPrintReader.jsp","hidden-form");
			    		//window.setTimeout("doRead()",1000);
			    		reads++;
			        }
			        else{
			        	window.close();
			        }
		    	}
			    else if(s.success==-99){
		    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
		    	    window.open("<%=sCONTEXTPATH%>/util/initializeFingerPrintReader.jsp?clear=1","hidden-form");
				    //window.setTimeout("doDetect()",1000);
			    }
			    else{
		    		//window.setTimeout("doRead()",1000);
			    }
		  	},
		  	onFailure: function(){
	    		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
	    		//window.setTimeout("doRead()",1000);
		  	}
		});
	}

	function doDetect(){
		var parameters='';
		document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallNoPrint.jpg"/>';
		var url = '<c:url value="/projects/peru/_common/dp/detectFingerPrintReaderSecugen.jsp"/>?ts='+new Date().getTime();
		new Ajax.Request(url,{
		  	method: "POST",
		  	postBody: parameters,
		  	onSuccess: function(resp){
			    var s=eval('('+resp.responseText+')');
			    if(s.serial!=''){
			    	if(s.serial=='-1'){
			    	    window.open("<%=sCONTEXTPATH%>/util/initializeFingerPrintReader.jsp?clear=1","hidden-form");
					    //window.setTimeout("doDetect()",1000);
			    	}
			    	else{
				    	document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","fingerprint.reader.detected","en")%>:<br/><b>'+s.serial+"</b>";
				    	reads=1;
				    	doRead();
				        window.open("<%=sCONTEXTPATH%>/util/startFingerPrintReader.jsp","hidden-form");
			    	}
			    }
			    else{
				    document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","detecting.reader","en")%>';
				    //window.setTimeout("doDetect()",1000);
			    }
		  	},
		  	onFailure: function(){
			    document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","detecting.reader","en")%>';
			    //window.setTimeout("doDetect()",1000);
		  	}
		});
	}
	
	function selectUser(userid,password){
		window.opener.location.href='<c:url value="/checkLogin.jsp"/>?ts=<%=ScreenHelper.getTs()%>&login='+userid+'&auto=true&password='+password+'&huella=true';
		window.close();
	}

    // window.open("<%=sCONTEXTPATH%>/util/initializeFingerPrintReader.jsp","hidden-form");
	// doDetect();
</script>
<script type="text/javascript">
	var secugen_lic = "";
	var template  = [];
	// var template = "Rk1SACAyMAAAAAFoAAABLAGQAMUAxQEAAABdN0CeAB5nAICDACVrAIBkACZxAEBnADDuAECxADPuAIDEAD91AICEAEFxAIBXAEVxAIAPAExzAECQAE70AEChAFNzAIB7AFZ1AIBXAGr1AIANAGp4AIDBAHV1AICAAHZyAIBCAIV7AECcAIh1AIBuAIx5AIAKAI0BAIBmAJ74AEBhAKN4AIDaAKV4AEAvAK59AEBHALL7AIB4AMVzAICfANdyAIBeAPZ5AEANAQkNAEA3AROKAEDSARnsAIAeAS2YAEBhATCAAICXATLsAICyATNsAEByATd7AIAwAUCeAIBTAUGTAECQAUTbAECEAUbRAECMAUbUAICfAUrhAIA4AU+uAECDAVTDAEC4AVbeAIBMAVa5AECcAVnUAICNAV/GAICDAWavAICbAWnKAEA3AWnAAEBtAWkzAEBiAWtGAECMAXmqAIBpAYKGAAAA";
	var templateLeido = "";
	// ejecutar();
	CallSGIFPGetData(SuccessFunc2, ErrorFunc);
	// selectUser("10","peru");
			    
	// function prueba(){
	// }

	function ejecutar(){
		var url = '<c:url value="/projects/peru/_common/dp/readUser.jsp"/>?ts='+new Date().getTime();
		var huellas = [];
		var match = false;
		new  Ajax.Request(url,{
			method: "POST",
			postBody: "",
			onSuccess: function(resp){
				var s=eval('('+resp.responseText+')');
				template = s.template.split('--|--');
				template.pop();
				var estado = false;
				for (const element of template) {
					if(element!=null){
						var datos = element.split('-|-');
						estado = matchScore(succMatch, failureFunc,datos[1]);
						if(estado){
							doRead1(datos[0]);
						}
					}
				}
				if(!estado){
					document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
					CallSGIFPGetData(SuccessFunc2, ErrorFunc);
				}
				
			},
			onFailure: function(){
				//document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
				//window.setTimeout("doRead()",1000);
			}
		});
		// setTimeout(function() {
		// 	template.pop();
		// 	for (const element of template) {

		// 		var datos = element.split('-|-');
		// 		matchScore(succMatch, failureFunc,datos[1]);
		// 	}
		// },2000);
	}
	// ejecutar();
	// console.log(template);

	function SuccessFunc2(result) {
		if (result.ErrorCode == 0) {
			if (result != null && result.BMPBase64.length > 0) {
				document.getElementById('fingerprintImage').src = "data:image/bmp;base64," + result.BMPBase64;
			}
			templateLeido = result.TemplateBase64;
			return true;
		} else {
			return false;
			// alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
		}
	}

	function ErrorFunc(status) {
		alert("Check if SGIBIOSRV is running; status = " + status + ":");
		return false;
	}

	function CallSGIFPGetData(successCall, failCall) {
		var uri = "https://localhost:8443/SGIFPCapture";
		var xmlhttp = new XMLHttpRequest();
		var statusHuella = false;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				fpobject = JSON.parse(xmlhttp.responseText);
				statusHuella = successCall(fpobject);
				// console.log("bien");
			} else if (xmlhttp.status == 404) {
				statusHuella = failCall(xmlhttp.status)
			}
			// console.log(statusHuella);
			if(statusHuella){
				ejecutar();
			}else{
				console.log("error al iniciar la huella");
			}
		}
		xmlhttp.onerror = function() {
			statusHuella = failCall(xmlhttp.status);
		}
		var params = "Timeout=" + "10000";
		params += "&Quality=" + "50";
		params += "&licstr=" + encodeURIComponent(secugen_lic);
		params += "&templateFormat=" + "ISO";
		xmlhttp.open("POST", uri, true);
		xmlhttp.send(params);
		// console.log(xmlhttp);

		
	}

	function matchScore(succFunction, failFunction,template) {
		var status = false;
		if (template == "" || templateLeido == "") {
			alert("Please scan two fingers to verify!!");
			return;
		}
		var uri = "https://localhost:8443/SGIMatchScore";

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				fpobject = JSON.parse(xmlhttp.responseText);
				status = succFunction(fpobject);
			} else if (xmlhttp.status == 404) {
				status = failFunction(xmlhttp.status)
			}
		}

		xmlhttp.onerror = function() {
			failFunction(xmlhttp.status);
		}
		var params = "template1=" + encodeURIComponent(template);
		params += "&template2=" + encodeURIComponent(templateLeido);
		params += "&licstr=" + encodeURIComponent(secugen_lic);
		params += "&templateFormat=" + "ISO";
		xmlhttp.open("POST", uri, false);
		xmlhttp.send(params);
		return status;
		// console.log(status);
	}

	function succMatch(result) {
		
		var idQuality = 50;
		if (result.ErrorCode == 0) {
			if (result.MatchingScore >= idQuality)
				return true;
			else
				return false;
		} else {
			alert("Error Scanning Fingerprint ErrorCode = " + result.ErrorCode);
		}
	}

	function failureFunc(error) {
		alert("On Match Process, failure has been called");
	}

	function ErrorCodeToString(ErrorCode) {
		var Description;
		switch (ErrorCode) {
			// 0 - 999 - Comes from SgFplib.h
			// 1,000 - 9,999 - SGIBioSrv errors 
			// 10,000 - 99,999 license errors
			case 51:
				Description = "System file load failure";
				break;
			case 52:
				Description = "Sensor chip initialization failed";
				break;
			case 53:
				Description = "Device not found";
				break;
			case 54:
				Description = "Fingerprint image capture timeout";
				break;
			case 55:
				Description = "No device available";
				break;
			case 56:
				Description = "Driver load failed";
				break;
			case 57:
				Description = "Wrong Image";
				break;
			case 58:
				Description = "Lack of bandwidth";
				break;
			case 59:
				Description = "Device Busy";
				break;
			case 60:
				Description = "Cannot get serial number of the device";
				break;
			case 61:
				Description = "Unsupported device";
				break;
			case 63:
				Description = "SgiBioSrv didn't start; Try image capture again";
				break;
			default:
				Description = "Unknown error code or Update code to reflect latest result";
				break;
		}
		return Description;
	}
</script>