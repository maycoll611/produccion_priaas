var final_transcript = '';
var recognizing = false;
var ignore_onend;
var textarea_box;
var micro_img;
if (!('webkitSpeechRecognition' in window)) {
  dictaphoneUpgrade();
}
else {
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.onstart = function() {
    recognizing = true;
    micro_img.src = './_img/mic-animate.gif';
  };
  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      micro_img.src = './_img/mic.gif';
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      micro_img.src = './_img/mic.gif';
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      ignore_onend = true;
    }
  };
  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    micro_img.src = './_img/mic.gif';
    if (!final_transcript) {
      return;
    }
  };
  recognition.onresult = function(event) {
    var interim_transcript = '';
    if (typeof(event.results) == 'undefined') {
      recognition.onend = null;
      recognition.stop();
      dictaphoneUpgrade();
      return;
    }
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    textarea_box.value = final_transcript;
  };
}

function dictaphoneUpgrade() {
  const boxes = document.getElementsByClassName('dictaphone_button');
  for (const box of boxes) {
    box.style.visibility = 'hidden';
  }
}

function dictaphoneButton(event, textarea, img) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  textarea_box=textarea;
  micro_img=img;
  final_transcript = '';
  recognition.lang = dictaphone_lang;
  recognition.start();
  ignore_onend = false;
  micro_img.src = './_img/mic-slash.gif';
}