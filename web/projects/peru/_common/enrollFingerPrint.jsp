<%@include file="/includes/validateUser.jsp"%>
<%
	//Clear fingerprint call id
	session.setAttribute("fingerprintid", "");
	session.setAttribute("fingerprintimage", "");
	session.removeAttribute("fingerprintjpg");
	MedwanQuery.setSession(session,new User());
%>

<form name="frmEnrollFingerPrint" method="post">
    <%=writeTableHeader("web","enrollFingerPrint",sWebLanguage)%>
    <table width="100%" class="list" cellspacing="1" cellpadding="0">
        <tr>
            <td class="admin2" nowrap>
                <input type="radio" id="righthand" name="rightleft" value="R" checked/><%=getLabel(request,"web","right",sWebLanguage,"righthand")%>
                <input type="radio" id="lefthand" name="rightleft" value="L"/><%=getLabel(request,"web","left",sWebLanguage,"lefthand")%>
            </td>
            <td class="admin2">
                <select name="finger" class="text">
                    <option value="0"><%=getTranNoLink("web","thumb",sWebLanguage)%></option>
                    <option selected value="1"><%=getTranNoLink("web","index",sWebLanguage)%></option>
                    <option value="2"><%=getTranNoLink("web","middlefinger",sWebLanguage)%></option>
                    <option value="3"><%=getTranNoLink("web","ringfinger",sWebLanguage)%></option>
                    <option value="4"><%=getTranNoLink("web","littlefinger",sWebLanguage)%></option>
                </select>
            </td>
        </tr>
    </table>
    
    <%=ScreenHelper.alignButtonsStart()%>
        <input type="button" class="button" name="enrollButton" value="Leer" onclick="captureFP()"/>
		<input type="button" class="button" name="enrollButton" value="Guardar" onclick="doRead(true)"/>
        <input type="button" class="button" name="buttonClose" value="<%=getTranNoLink("web","close",sWebLanguage)%>" onclick="window.close()"/>
    <%=ScreenHelper.alignButtonsStop()%>
    
    <table width='100%'>
        <tr>
			<td>
                <%-- <img width='80px' id='fingerprintImage' name='fingerprintImage' src="<c:url value="/_img/fingerprintImageSmallNoPrint.jpg"/>"/> --%>
				<img width='80px' id='fingerprintImage' name='fingerprintImage' src="https://cdn-icons-png.flaticon.com/512/890/890122.png"/>
            </td>
            <td>
                <table>
                    <tr>
                        <td>
                        	<div name='clock' id='clock'></div>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                        	<label name='readerID' id='readerID'></label>
                        </td>
                    </tr>
                </table>
            </td>
            
        </tr>
    </table>
    <br>
</form>

<%-- <img src="" id="FPImage1" alt=""> --%>
<%-- <div id="result"></div> --%>

<IFRAME style="display:none" name="hidden-form"></IFRAME>

<script>
	
	var ncounter=0;
	var fingerprintimage = "";
	var fingerprintjpg = "";
	
	function doRead(bInit){
		if(bInit){
		    // window.open("<%=sCONTEXTPATH%>/util/startFingerPrintReader.jsp","hidden-form");
			ncounter=0;
		}
		ncounter++;
		document.getElementById("clock").innerHTML="<img src='<%=sCONTEXTPATH%>/_img/themes/default/ajax-loader.gif'/>";
		var r = 'L';
	    if(document.getElementById('righthand').checked){
	        r = 'R';
	    }
	    var parameters= 'rightleft='+r+'&finger='+frmEnrollFingerPrint.finger.value+'&fingerprintimage='+fingerprintimage+'&fingerprintjpg='+fingerprintjpg;		
		var url = '<c:url value="/projects/peru/_common/dp/enrollFingerPrintSecugen.jsp"/>?init='+bInit+'&ts='+new Date().getTime();
		new Ajax.Request(url,{
		  	method: "POST",
		  	postBody: parameters,
		  	onSuccess: function(resp){
			    var s=eval('('+resp.responseText+')');
			    if(s.success==1){
				    document.getElementById("clock").innerHTML="";
			    	document.getElementById('readerID').innerHTML = '<h4><%=getTranNoLink("web","enrollment_succeeded",sWebLanguage)%></h4>';
			    	document.getElementById('fingerprintImage').src='https://cdn-icons-png.flaticon.com/512/890/890122.png';
			    	// window.setTimeout("doDetect()",3000);
			    }
			    else if(s.success==0 || ncounter>10){
				    document.getElementById("clock").innerHTML="";
			    	document.getElementById('readerID').innerHTML = '<h4><%=getTranNoLink("web","enrollment_failed",sWebLanguage)%></h4>';
			    }
			    else{
			    	// window.setTimeout("doRead()",1000);
			    }
		  	},
		  	onFailure: function(){
		  		alert("error");
		  	}
		});
	}
	
	function doDetect(){
		document.getElementById('fingerprintImage').src="<c:url value="/_img/fingerprintImageSmallNoPrint.jpg"/>";
		var parameters='';
		var url = '<c:url value="/_common/dp/detectFingerPrintReaderSecugen.jsp"/>?ts='+new Date().getTime();
		new Ajax.Request(url,{
		  	method: "POST",
		  	postBody: parameters,
		  	onSuccess: function(resp){
			    var s=eval('('+resp.responseText+')');
			    if(s.serial!=''){
			    	document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","fingerprint.reader.detected",sWebLanguage)%>:<br/><b>'+s.serial+"</b>";

			    }
			    else{
				    document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","detecting.reader",sWebLanguage)%>';
				    window.setTimeout("doDetect()",1000);
			    }
		  	},
		  	onFailure: function(){
			    document.getElementById('readerID').innerHTML = '<%=getTranNoLink("web","detecting.reader",sWebLanguage)%>';
			    window.setTimeout("doDetect()",1000);
		  	}
		});
	}
	
    // window.open("<%=sCONTEXTPATH%>/util/initializeFingerPrintReader.jsp","hidden-form");
	// doDetect();

</script>


<script>
	var secugen_lic = "";
	function captureFP() {
		CallSGIFPGetData(SuccessFunc, ErrorFunc);
	}

	function CallSGIFPGetData(successCall, failCall) {
		// 8.16.2017 - At this time, only SSL client will be supported.
		var uri = "https://localhost:8443/SGIFPCapture";

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				fpobject = JSON.parse(xmlhttp.responseText);
				successCall(fpobject);    
			}
			else if (xmlhttp.status == 404) {
				failCall(xmlhttp.status)
			}
		}
		var params = "Timeout=" + "10000";
		params += "&Quality=" + "50";
		params += "&licstr=" + encodeURIComponent(secugen_lic);
		params += "&templateFormat=" + "ISO";
		params += "&imageWSQRate=" + "0.75";
		console.log
		xmlhttp.open("POST", uri, true);
		xmlhttp.send(params);

		xmlhttp.onerror = function () {
			failCall(xmlhttp.statusText);
		}
	}


	function SuccessFunc(result) {
		if (result.ErrorCode == 0) {
			
			if (result != null && result.BMPBase64.length > 0) {
				document.getElementById("fingerprintImage").src = "data:image/bmp;base64," + result.BMPBase64;
				fingerprintjpg = "data:image/bmp;base64," + result.BMPBase64;
				fingerprintimage = result.TemplateBase64;
			}
			
		}
		else {
			alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
		}
	}
	function ErrorFunc(status) {

		/* 	
			If you reach here, user is probabaly not running the 
			service. Redirect the user to a page where he can download the
			executable and install it. 
		*/
		alert("Check if SGIBIOSRV is running; Status = " + status + ":");

	}


	function ErrorCodeToString(ErrorCode) {
		var Description;
		switch (ErrorCode) {
			// 0 - 999 - Comes from SgFplib.h
			// 1,000 - 9,999 - SGIBioSrv errors 
			// 10,000 - 99,999 license errors
			case 51:
				Description = "System file load failure";
				break;
			case 52:
				Description = "Sensor chip initialization failed";
				break;
			case 53:
				Description = "Device not found";
				break;
			case 54:
				Description = "Fingerprint image capture timeout";
				break;
			case 55:
				Description = "No device available";
				break;
			case 56:
				Description = "Driver load failed";
				break;
			case 57:
				Description = "Wrong Image";
				break;
			case 58:
				Description = "Lack of bandwidth";
				break;
			case 59:
				Description = "Device Busy";
				break;
			case 60:
				Description = "Cannot get serial number of the device";
				break;
			case 61:
				Description = "Unsupported device";
				break;
			case 63:
				Description = "SgiBioSrv didn't start; Try image capture again";
				break;
			default:
				Description = "Unknown error code or Update code to reflect latest result";
				break;
		}
		return Description;
	}
	// document.addEventListener("DOMContentLoaded", function(event) {
	// 	captureFP();
	// });
</script>