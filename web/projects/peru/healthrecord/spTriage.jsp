<%@page import="be.mxs.common.model.vo.healthrecord.TransactionVO,
                be.mxs.common.model.vo.healthrecord.ItemVO,
                be.openclinic.pharmacy.Product,
                java.text.DecimalFormat,
                be.openclinic.medical.Problem,
                be.openclinic.medical.Diagnosis,
                be.openclinic.system.Transaction,
                be.openclinic.system.Item,
                be.openclinic.medical.Prescription,
                be.openclinic.finance.*,
                java.util.*" %>
<%@include file="/includes/validateUser.jsp"%>
<%@page errorPage="/includes/error.jsp"%>
<%
	String activeEncounterUid = "", sRfe = "";
	SessionContainerWO sessionContainerWO = (SessionContainerWO)SessionContainerFactory.getInstance().getSessionContainerWO(request,SessionContainerWO.class.getName());
	TransactionVO curTran = sessionContainerWO.getCurrentTransactionVO();

	ItemVO oldItemVO = curTran.getItem("be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_ENCOUNTERUID");
	if(oldItemVO!=null && oldItemVO.getValue().length()>0){
		activeEncounterUid = oldItemVO.getValue();
	}
	else{
		Encounter activeEnc = Encounter.getActiveEncounterOnDate(new Timestamp(curTran.getUpdateTime().getTime()),activePatient.personid);
		if(activeEnc!=null){
			activeEncounterUid = activeEnc.getUid();
		}
	}
%>
<%=checkPermission(out,"occup.clinicalexamination","select",activeUser)%>
<%=sJSDIAGRAM%>
<%=sCSS_BOOSTRAP%>
<%=sCSS_CARD_NOTAS%>
<script>
	function AlternarSeNiegaModule(elem) {
		if(document.getElementById(elem.id).checked){
			document.getElementById(elem.id).parentElement.parentElement.parentElement.lastChild.style='display:none;';
		}
		else{
			document.getElementById(elem.id).parentElement.parentElement.parentElement.lastChild.style='display:;';
		}
	}
	function MinMaxDisplay(elem) {
		// console.log(elem.id)
		document.getElementById(elem.id).parentElement.parentElement.parentElement.lastChild.previousElementSibling.classList.toggle("collapse");
		if(document.getElementById(elem.id).firstChild.nextElementSibling.classList.contains('bi-plus-square')) {
			document.getElementById(elem.id).firstChild.nextElementSibling.classList.remove("bi-plus-square");
			document.getElementById(elem.id).firstChild.nextElementSibling.classList.add("bi-dash-square");
		}
		else {
			document.getElementById(elem.id).firstChild.nextElementSibling.classList.add("bi-plus-square");
			document.getElementById(elem.id).firstChild.nextElementSibling.classList.remove("bi-dash-square");
		}
	}
</script>
<form name="transactionForm" id="transactionForm" method="POST" action='<c:url value="/healthrecord/updateTransaction.do"/>?ts=<%=getTs()%>'>
	<bean:define id="transaction" name="be.mxs.webapp.wl.session.SessionContainerFactory.WO_SESSION_CONTAINER" property="currentTransactionVO"/>
	<%=checkPrestationToday(activePatient.personid,false,activeUser,(TransactionVO)transaction)%>
	<input type="hidden" id="transactionId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionId" value="<bean:write name="transaction" scope="page" property="transactionId"/>"/>
	<input type="hidden" id="serverId" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.serverId" value="<bean:write name="transaction" scope="page" property="serverId"/>"/>
	<input type="hidden" id="transactionType" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.transactionType" value="<bean:write name="transaction" scope="page" property="transactionType"/>"/>
	<input type="hidden" readonly name="be.mxs.healthrecord.updateTransaction.actionForwardKey" value="/main.do?Page=curative/index.jsp&ts=<%=getTs()%>"/>
	<input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_RECRUITMENT_CONVOCATION_ID" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_RECRUITMENT_CONVOCATION_ID" property="value"/>"/>
	<input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_DEPARTMENT" translate="false" property="value"/>"/>
	<input type="hidden" readonly name="currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="itemId"/>]>.value" value="<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_CONTEXT_CONTEXT" translate="false" property="value"/>"/>
	
	

<div class="content-wrapper" style="background-color: #e5f1ff5c;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-12">
					<%=contextHeader(request,sWebLanguage)%>
					<%=writeHistoryFunctions(((TransactionVO)transaction).getTransactionType(),sWebLanguage)%>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-left">
						<li class="breadcrumb-item active"><h5><strong><%=getTranNoLink("Web.Occup","History",sWebLanguage)%></strong></h5></li>
						<li class="breadcrumb-item"> <%=getTran(request,"Web.Occup","medwan.common.date",sWebLanguage)%> <input type="text" class="text" size="12" maxLength="10" value="<mxs:propertyAccessorI18N name="transaction" scope="page" property="updateTime" formatType="date"/>" name="currentTransactionVO.<TransactionVO[hashCode=<bean:write name="transaction" scope="page" property="transactionId"/>]>.updateTime" id="trandate" OnBlur='checkDate(this)'> <script>writeTranDate();</script> </li>
						<li class="breadcrumb-item"><button type="button" class="btn btn-primary" onclick="javascript:openHistoryPopup();"><%=getTranNoLink("Web.Occup","History",sWebLanguage)%> Clinico Electronico </button></li>
					</ol>
				</div>
				<div class="col-sm-6">
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary btn-sm" onclick="CallSGIFPGetData(SuccessFunc2, ErrorFunc)"><i class="bi bi-fingerprint mr-0" id="validate_fingerprint"></i></button>
							<span class="alert alert-success pt-4" style="vertical-align:middle;height:30px;">
								<i class="bi bi-person-bounding-box" id="iconFingerprint"></i>
								<span class="text-success" id="fingerprintText" style="display:none">IDENTIFICACION BIOMETRICA</span>
							</span>
						</div>
					</div>	
					<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_FINGERPRINT", 8, "style='display:none'")%>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<!-- Signos Vitales -->
					<div class="card shadow mb-5 bg-white rounded">
						<div class="card-header bg-white">
                                <span class="card-title">
                                    <i class="bi bi-clipboard2-pulse text-info bi_modulo"></i><%=getTran(request,"Web.Occup","vitalsigns",sWebLanguage)%>
                                </span>
							<div class="card-tools">
								<button type="button" class="btn btn-tool" id="MinMax_Vitales" onclick="MinMaxDisplay(this);">
									<i class="bi bi-dash-square" style="font-size: 13px;"></i>
								</button>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<div class="form-group row" style="font-size: 11pt;">
								<div class="col-sm-6">
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-speedometer text-primary"></i>
											<%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.weight",sWebLanguage)%>
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_BIOMETRY_WEIGHT", 8, "onBlur='validateWeight(this);calculateBMI();'")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-rulers text-secondary"></i>
											<%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.length",sWebLanguage)%>
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_BIOMETRY_HEIGHT", 8, "onBlur='validateHeight(this);calculateBMI();'")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-universal-access-circle text-success"></i>
											<%=getTran(request,"web","abdomencircumference",sWebLanguage)%>
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_ABDOMENCIRCUMFERENCE", 8, "")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-universal-access text-success"></i>
											<%=getTran(request,"Web.Occup","medwan.healthrecord.biometry.bmi",sWebLanguage)%>
										</label>
										<div class="col-4 col-sm-4">
											<input tabindex="-1" class="form-control inputForm" type="text" size="10" readonly name="BMI" id="BMI">
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-palette2 text-success"></i>
											<%=getTran(request,"Web.Occup","medwan.healthrecord.weightforlength",sWebLanguage)%> (cm)
										</label>
										<div class="col-4 col-sm-4">
											<input tabindex="-1" class="form-control inputForm" type="text" size="3" readonly name="WFL" id="WFL">
											<img id="wflinfo" style='display: none' src="<c:url value='/_img/icons/icon_info.gif'/>"/>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-thermometer-half text-success"></i>
											<%=getTran(request,"openclinic.chuk","temperature",sWebLanguage)%>&nbsp;(�C)
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_TEMPERATURE", 8, "onBlur='if(isNumber(this)){if(!checkMinMaxOpen(25,45,this)){alertDialog(\"Web.Occup\",\"medwan.common.unrealistic-value\");}}'")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-heart-pulse text-danger"></i>
											<%=getTran(request,"web","pulse",sWebLanguage)%> bmp
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_HEART_FRENQUENCY", 8, "")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-activity text-danger"></i>
											<%=getTran(request,"heart.auscultation","25",sWebLanguage)%> mmHg
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_SYSTOLIC_PRESSURE_RIGHT", 8, "")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-activity text-danger"></i>
											<%=getTran(request,"heart.auscultation","7",sWebLanguage)%> mmHg
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "ITEM_TYPE_CARDIAL_CLINICAL_EXAMINATION_DIASTOLIC_PRESSURE_RIGHT", 8, "")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-lungs text-warning"></i>
											<%=getTran(request,"openclinic.chuk","respiratory.frequency",sWebLanguage)%>&nbsp;r/m
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_RESPIRATORY_FRENQUENCY", 8, "")%>
										</div>
									</div>
									<div class="form-group row">
										<label for="temperature" class="col-6 col-sm-8 col-form-label">
											<i class="bi bi-lungs text-warning"></i>
											<%=getTran(request,"openclinic.chuk","sao2",sWebLanguage)%>&nbsp;r/m
										</label>
										<div class="col-4 col-sm-4">
											<%=SH.writeDefaultTextInput3(session, (TransactionVO)transaction, "[GENERAL.ANAMNESE]ITEM_TYPE_SATURATION", 8, "")%>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<div class="col-md-6 col-lg-4">
					<!-- Antecedentes -->
					<%=SH.writeNotesTextareaEdit(session, (TransactionVO)transaction, "ITEM_TYPE_ALERTS_ANTECEDENTEN", 30, 2, sCONTEXTPATH, getTran(request,"Web.Occup","medwan.healthrecord.anamnese.antecedents",sWebLanguage), (HttpServletRequest)request, sWebLanguage, "bi bi-clock-history text-secondary bi_modulo", "")%>
					<!-- /.card -->
					<!-- Alergias -->
					<%=SH.writeNotesTextareaEdit(session, (TransactionVO)transaction, "ITEM_TYPE_ALERTS_ALLERGY", 30, 2, sCONTEXTPATH, getTran(request,"Web.Occup","medwan.healthrecord.allergy",sWebLanguage), (HttpServletRequest)request, sWebLanguage, "bi bi-person-x text-info bi_modulo", "")%>
					<!-- /.card -->
				</div>
				<div class="col-md-12 col-lg-4">
					<!-- Archivos y Notas (Antecedentes) -->
					<div class="card shadow mb-5 bg-white rounded">
						<div class="card-header bg-white">
                                <span class="card-title">
                                   <i class="bi bi-card-text text-warning bi_modulo"></i> <%=getTran(request,"Web","notes",sWebLanguage)%>
                                </span>
						</div>
						<!-- /.card-header -->
						<div class="card-body">
							<div class="form-group row" style="font-size: 11pt;">
								<div class="col-sm-12">
									<div class="notas-info">
										<label for="ITEM_TYPE_DAILY_NOTE_HISTORY"> <%=getTran(request,"web","additionalnotes",sWebLanguage)%> </label>
										<%=SH.writeDefaultTextArea2(session, (TransactionVO)transaction, "ITEM_TYPE_DAILY_NOTE_HISTORY", 30, 6, sCONTEXTPATH) %>
										<script>
											document.getElementById("ITEM_TYPE_DAILY_NOTE_HISTORY").placeholder='Estas notas s�lo son visibles para el m�dico';
										</script>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer">
						</div>
						<!-- /.card-footer -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
		<br><br><br><br><br><br>
	</section>
	<section class="content-footer fixed-bottom" style="background-color: skyblue;">
		<div class="row justify-content-center alert-secondary">
			<button type="button" class="btn btn-primary btn-lg col-md-11 my-3 terminar" title="" onclick="submitForm();">
				<strong>TERMINAR TRIAJE</strong>
			</button>
		</div>
		<%-- BUTTONS --%>
		<%=ScreenHelper.alignButtonsStart()%>
		<%=getButtonsHtml(request,activeUser,activePatient,"occup.clinicalexamination",sWebLanguage)%>
		<%=ScreenHelper.alignButtonsStop()%>
		<%=ScreenHelper.contextFooter(request)%>
	</section>
</div>

</form>

<script>
  document.getElementById("ITEM_TYPE_BIOMETRY_WEIGHT").focus();
  <%-- SUBMIT FORM --%>
  function submitForm(){
    if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
		alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
		searchEncounter();
	}	
    else {
	    document.getElementById("buttonsDiv").style.visibility = "hidden";
	    var temp = Form.findFirstElement(transactionForm);
	    <%
	        out.print(takeOverTransaction(sessionContainerWO, activeUser,"document.transactionForm.submit();"));
	    %>
    }
  }
  function searchEncounter(){
      openPopup("/_common/search/searchEncounter.jsp&ts=<%=getTs()%>&Varcode=encounteruid&VarText=&FindEncounterPatient=<%=activePatient.personid%>");
  }
  if(<%=((TransactionVO)transaction).getServerId()%>==1 && document.getElementById('encounteruid').value=='' <%=request.getParameter("nobuttons")==null?"":" && 1==0"%>){
	alertDialogDirectText('<%=getTranNoLink("web","no.encounter.linked",sWebLanguage)%>');
	searchEncounter();
  }
</script>

<script>
  <%-- VALIDATE WEIGHT --%>
  <%
      int minWeight = 0;
      int maxWeight = 160;

      String weightMsg = getTran(request,"Web.Occup","medwan.healthrecord.biometry.weight.validationerror",sWebLanguage);
      weightMsg = weightMsg.replaceAll("#min#",minWeight+"");
      weightMsg = weightMsg.replaceAll("#max#",maxWeight+"");
  %>
  function validateWeight(weightInput){
    isNumber(weightInput);
    weightInput.value = weightInput.value.replace(",",".");
    if(weightInput.value.length > 0){
      var min = <%=minWeight%>;
      var max = <%=maxWeight%>;

      if(isNaN(weightInput.value) || weightInput.value < min || weightInput.value > max){
        alertDialogDirectText("<%=weightMsg%>");
        //weightInput.value = "";
        //weightInput.focus();
      }
    }
  }
  <%-- VALIDATE HEIGHT --%>
  <%
      int minHeight = 0;
      int maxHeight = 300;

      String heightMsg = getTran(request,"Web.Occup","medwan.healthrecord.biometry.height.validationerror",sWebLanguage);
      heightMsg = heightMsg.replaceAll("#min#",minHeight+"");
      heightMsg = heightMsg.replaceAll("#max#",maxHeight+"");
  %>
  function validateHeight(heightInput){
    isNumber(heightInput);
    heightInput.value = heightInput.value.replace(",",".");
    if(heightInput.value.length > 0){
      var min = <%=minHeight%>;
      var max = <%=maxHeight%>;

      if(isNaN(heightInput.value) || heightInput.value < min || heightInput.value > max){
    	alertDialogDirectText("<%=heightMsg%>");
        //heightInput.focus();
      }
    }
  }
  <%-- CALCULATE BMI --%>
  function calculateBMI(){
    var _BMI = 0, _WFL=0;
    var heightInput = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_HEIGHT" property="itemId"/>]>.value')[0];
    var weightInput = document.getElementsByName('currentTransactionVO.items.<ItemVO[hashCode=<mxs:propertyAccessorI18N name="transaction.items" scope="page" compare="type=be.mxs.common.model.vo.healthrecord.IConstants.ITEM_TYPE_BIOMETRY_WEIGHT" property="itemId"/>]>.value')[0];

    if(heightInput.value > 0){
        _BMI = (weightInput.value * 10000) / (heightInput.value * heightInput.value);
        _WFL = (weightInput.value) / (heightInput.value);
      if (_BMI > 100 || _BMI < 5){
          document.getElementsByName('BMI')[0].value = "";
      }
      else {
          document.getElementsByName('BMI')[0].value = Math.round(_BMI*10)/10;
      }
      if(_WFL>0){
          document.getElementsByName('WFL')[0].value = _WFL.toFixed(2);
    	  checkWeightForHeight(heightInput.value,weightInput.value);
      }
      else{
          document.getElementsByName('WFL')[0].value = "";
      }
    }
  }
  function checkWeightForHeight(height,weight){
	      var today = new Date();
	      var url= '<c:url value="/ikirezi/getWeightForHeight.jsp"/>?height='+height+'&weight='+weight+'&gender=<%=activePatient.gender%>&ts='+today;
	      new Ajax.Request(url,{
	          method: "POST",
	          postBody: "",
	          onSuccess: function(resp){
	              var label = eval('('+resp.responseText+')');
	    		  if(label.zindex>-999){
	    			  if(label.zindex<-4){
	    				  document.getElementById("WFL").className="darkredtext";
	    				  document.getElementById("wflinfo").title="Z-index < -4: <%=getTranNoLink("web","severe.malnutrition",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else if(label.zindex<-3){
	    				  document.getElementById("WFL").className="darkredtext";
	    				  document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","severe.malnutrition",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else if(label.zindex<-2){
	    				  document.getElementById("WFL").className="orangetext";
	    				  document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","moderate.malnutrition",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else if(label.zindex<-1){
	    				  document.getElementById("WFL").className="yellowtext";
	    				  document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","light.malnutrition",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else if(label.zindex>2){
	    				  document.getElementById("WFL").className="orangetext";
	    				  document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","obesity",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else if(label.zindex>1){
	    				  document.getElementById("WFL").className="yellowtext";
	    				  document.getElementById("wflinfo").title="Z-index = "+(label.zindex*1).toFixed(2)+": <%=getTranNoLink("web","light.obesity",sWebLanguage).toUpperCase()%>";
	    				  document.getElementById("wflinfo").style.display='';
	    			  }
	    			  else{
	    				  document.getElementById("WFL").className="text";
	    				  document.getElementById("wflinfo").style.display='none';
	    			  }
	    		  }
  			  else{
  				  document.getElementById("WFL").className="text";
  				  document.getElementById("wflinfo").style.display='none';
  			  }
	          },
	          onFailure: function(){
	          }
	      }
		  );
	  	}
  calculateBMI();

</script>
<script type="text/javascript">
	let icon = document.getElementById("iconFingerprint");
	let fingerprintText = document.getElementById("fingerprintText");
	let fingerStatus = document.getElementById("ITEM_TYPE_FINGERPRINT");
	
	if(parseInt(fingerStatus.value)>0){
		iconFingerprint(1);
	}else{
		iconFingerprint(5);
	}
	var secugen_lic = "";
	var template  = [];
	var templateLeido = "";

	function ejecutar(){
		var url = '<c:url value="/_common/dp/readFingerPrintSecugen.jsp"/>?ts='+new Date().getTime();
		new  Ajax.Request(url,{
			method: "POST",
			postBody: "",
			onSuccess: function(resp){
				var s=eval('('+resp.responseText+')');
				template = s.template.split('--|--');
				template.pop();
				var estado = false;
				for (const element of template) {
					if(element!=null){
						// var datos = element.split('-|-');
						estado = matchScore(succMatch, failureFunc,element);
						if(estado){
							iconFingerprint(1);
							console.log("coincidencia en huella");
							//document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmall.jpg"/>';
		    				document.getElementById("loader").style.display='none';
							break;
						}
					}
				}
				if(!estado){
					iconFingerprint(2);
					//document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
					//CallSGIFPGetData(SuccessFunc2, ErrorFunc);
				}
				
			},
			onFailure: function(){
				//document.getElementById("fingerprintImage").src = '<c:url value="/_img/fingerprintImageSmallWrong.jpg"/>';
				//window.setTimeout("doRead()",1000);
			}
		});
	}

	function SuccessFunc2(result) {
		if (result.ErrorCode == 0) {
			if (result != null && result.BMPBase64.length > 0) {
				//document.getElementById('fingerprintImage').src = "data:image/bmp;base64," + result.BMPBase64;
			}
			templateLeido = result.TemplateBase64;
			return true;
		} else {
			return false;
			// alert("Fingerprint Capture Error Code:  " + result.ErrorCode + ".\nDescription:  " + ErrorCodeToString(result.ErrorCode) + ".");
		}
	}

	function ErrorFunc(status) {
		alert("Check if SGIBIOSRV is running; status = " + status + ":");
		return false;
	}

	function CallSGIFPGetData(successCall, failCall) {
		iconFingerprint(3);
		var uri = "https://localhost:8443/SGIFPCapture";
		var xmlhttp = new XMLHttpRequest();
		var statusHuella = false;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				fpobject = JSON.parse(xmlhttp.responseText);
				statusHuella = successCall(fpobject);
				// console.log("bien");
			} else if (xmlhttp.status == 404) {
				statusHuella = failCall(xmlhttp.status)
			}
			// console.log(statusHuella);
			if(statusHuella){
				ejecutar();
			}else{
				console.log("error al iniciar la huella");
			}
		}
		xmlhttp.onerror = function() {
			statusHuella = failCall(xmlhttp.status);
		}
		var params = "Timeout=" + "10000";
		params += "&Quality=" + "50";
		params += "&licstr=" + encodeURIComponent(secugen_lic);
		params += "&templateFormat=" + "ISO";
		xmlhttp.open("POST", uri, true);
		xmlhttp.send(params);
		// console.log(xmlhttp);

		
	}

	function matchScore(succFunction, failFunction,template) {
		var status = false;
		if (template == "" || templateLeido == "") {
			alert("Please scan two fingers to verify!!");
			return;
		}
		var uri = "https://localhost:8443/SGIMatchScore";

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				fpobject = JSON.parse(xmlhttp.responseText);
				status = succFunction(fpobject);
			} else if (xmlhttp.status == 404) {
				status = failFunction(xmlhttp.status)
			}
		}

		xmlhttp.onerror = function() {
			failFunction(xmlhttp.status);
		}
		var params = "template1=" + encodeURIComponent(template);
		params += "&template2=" + encodeURIComponent(templateLeido);
		params += "&licstr=" + encodeURIComponent(secugen_lic);
		params += "&templateFormat=" + "ISO";
		xmlhttp.open("POST", uri, false);
		xmlhttp.send(params);
		return status;
		// console.log(status);
	}

	function succMatch(result) {
		
		var idQuality = 100;
		if (result.ErrorCode == 0) {
			if (result.MatchingScore >= idQuality)
				return true;
			else
				return false;
		} else {
			alert("Error Scanning Fingerprint ErrorCode = " + result.ErrorCode);
		}
	}

	function failureFunc(error) {
		alert("On Match Process, failure has been called");
	}

	function ErrorCodeToString(ErrorCode) {
		var Description;
		switch (ErrorCode) {
			// 0 - 999 - Comes from SgFplib.h
			// 1,000 - 9,999 - SGIBioSrv errors 
			// 10,000 - 99,999 license errors
			case 51:
				Description = "System file load failure";
				break;
			case 52:
				Description = "Sensor chip initialization failed";
				break;
			case 53:
				Description = "Device not found";
				break;
			case 54:
				Description = "Fingerprint image capture timeout";
				break;
			case 55:
				Description = "No device available";
				break;
			case 56:
				Description = "Driver load failed";
				break;
			case 57:
				Description = "Wrong Image";
				break;
			case 58:
				Description = "Lack of bandwidth";
				break;
			case 59:
				Description = "Device Busy";
				break;
			case 60:
				Description = "Cannot get serial number of the device";
				break;
			case 61:
				Description = "Unsupported device";
				break;
			case 63:
				Description = "SgiBioSrv didn't start; Try image capture again";
				break;
			default:
				Description = "Unknown error code or Update code to reflect latest result";
				break;
		}
		return Description;
	}
	function iconFingerprint(estado){
		let content = icon.parentElement;
		switch (estado) {
			case 1:
				icon.classList.add("bi");
				icon.classList.remove("bi-person-bounding-box");
				icon.classList.remove("spinner-border");
				icon.classList.remove("text-danger");

				icon.classList.add("bi");
				icon.classList.add("bi-person-check-fill");
				icon.classList.add("text-success");
				content.classList.add("alert-success");
				fingerprintText.style.display = "inline-block";
				fingerStatus.value = "1";
				break;
			case 2:
				icon.classList.remove("bi");
				icon.classList.remove("bi-person-bounding-box");
				icon.classList.remove("spinner-border");
				icon.classList.remove("text-success");
				content.classList.remove("alert-success");
				fingerprintText.style.display = "none";

				icon.classList.add("bi");
				icon.classList.add("bi-person-x-fill");
				icon.classList.add("text-danger");
				fingerStatus.value = "0";
				break;
			case 3:
				icon.classList.remove("text-danger");
				icon.classList.remove("text-success");
				icon.classList.remove("bi");
				icon.classList.remove("bi-person-bounding-box");
				icon.classList.remove("bi-person-x-fill");
				icon.classList.remove("bi-person-check-fill");
				content.classList.remove("alert-success");
				fingerprintText.style.display = "none";

				icon.classList.add("spinner-border");
				break;
		
			default:
				icon.classList.remove("text-danger");
				icon.classList.remove("text-success");
				icon.classList.remove("bi");
				icon.classList.remove("bi-person-check-fill");
				icon.classList.remove("bi-person-x-fill");
				icon.classList.remove("spinner-border");
				content.classList.remove("alert-success");
				fingerprintText.style.display = "none";

				icon.classList.add("bi");
				icon.classList.add("bi-person-bounding-box");
				// fingerStatus.value = "0";
				break;
		}
	}
</script>
<%=writeJSButtons("transactionForm","saveButton")%>