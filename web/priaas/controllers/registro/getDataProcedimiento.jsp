<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
	
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");

	DataTable datatable = new DataTable();
	datatable.setModel("m_apodiag");
	datatable.setColumn(columns);
	
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
	Map<String,String> relation3 = new HashMap<String, String>();
	Map<String,String> relation4 = new HashMap<String, String>();
	Map<String,String> relation5 = new HashMap<String, String>();
	Map<String,String> relation6 = new HashMap<String, String>();
	Map<String,String> relation7 = new HashMap<String, String>();
	List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();
    
    relation1.put("relation", "bdsis_maestros.m_procedimientos");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "m_procedimientos.PRCD_V_CODPROCEDIMIENTO");
    relation1.put("foreign_key", "m_apodiag.apo_CodProcedimiento");
    
    relation2.put("relation", "bdsis_maestros.m_cpms_eqv");
    relation2.put("type_relation", "JOIN");
    relation2.put("key", " PRCD_V_CODPROCEDIMIENTO");
    relation2.put("foreign_key", "CPMS_V_CODSIS");
    
    relation3.put("relation", "bdsis_maestros.m_cpms");
    relation3.put("type_relation", "JOIN");
    relation3.put("key", "CPMS_V_CODIGO");
    relation3.put("foreign_key", "CPMS_V_CODCPMS");
    
    relation4.put("relation", "bdsis_maestros.m_subseccioncpms");
    relation4.put("type_relation", "JOIN");
    relation4.put("key", "SSCP_N_IDSUBSECCIONCPMS");
    relation4.put("foreign_key", "CPMS_N_IDSUBSECCIONCPMS");
    
    relation5.put("relation", "bdsis_maestros.m_seccioncpms");
    relation5.put("type_relation", "JOIN");
    relation5.put("key", "SCPM_N_IDSECCIONCPMS");
    relation5.put("foreign_key", "SSCP_N_IDSECCIONCPMS");
    
    relation6.put("relation", "bdsis_maestros.i_tarifariocpms");
    relation6.put("type_relation", "JOIN");
    relation6.put("key", "TCPM_N_IDCPMS");
    relation6.put("foreign_key", "CPMS_N_IDCPMS AND TCPM_N_CODAPO= CPMS_V_CODSIS");
    
    relation7.put("relation", "bdsis_maestros.m_grupocpms");
    relation7.put("type_relation", "JOIN");
    relation7.put("key", "GCPM_N_IDGRUPOCPMS");
    relation7.put("foreign_key", "SCPM_N_IDGRUPOCPMS");
    
    relations.add(0,relation1);
    relations.add(1,relation2);
    relations.add(2,relation3);
    relations.add(3,relation4);
    relations.add(4,relation5);
    relations.add(5,relation6);
    relations.add(6,relation7);
    
  	datatable.setRelations(relations);

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("(PRCD_V_CODPROCEDIMEQUIVALEN IS NULL OR PRCD_V_CODPROCEDIMEQUIVALEN = '')");

%>
<%=datatable.getDataJson() %>
