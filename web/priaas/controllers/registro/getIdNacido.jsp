<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
	int tipo = Integer.parseInt(request.getParameter("tipo"));
	String documento = request.getParameter("documento");
	String disa = request.getParameter("disa");
	String formato = request.getParameter("formato");
	String correlativo = request.getParameter("correlativo");
	String secuencia = request.getParameter("secuencia");
	
	String columns = request.getParameter("columns");
	
	
	GetDataBD data = new GetDataBD();
	data.setModel("bdsis_asegurados.m_afiliados");
	
	JSONArray whereD = new JSONArray();
	if(tipo==1||tipo==7){
		whereD.put("afi_Dni = "+documento);
		whereD.put("afi_IdTipoDocumento = '"+tipo+"'");
	}
	if(tipo==0){
		if(disa!=""){
			whereD.put("afi_IdDisa = '"+disa+"'");
		}
		whereD.put("afi_TipoFormato = '"+formato+"'");
		whereD.put("afi_NroFormato = '"+correlativo+"'");
		if(secuencia!=""){
			whereD.put("afi_CorrelativoIns = '"+secuencia+"'");
		}
	}
	
	data.setColumn(columns);
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	JSONObject resultado = new JSONObject();
	
	resultado.put("data",getData);
	if(getData.length()==0){
		resultado.put("status",false);
	}else{
		resultado.put("status",true);
	}
	
	

%>
<%=  resultado.toString()  %>