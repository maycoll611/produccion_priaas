<%@page import="org.json.*,pe.gob.sis.arfsisweb.*,pe.gob.sis.arfsisweb.model.Periodo,pe.gob.sis.arfsisweb.model.Atencion"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	
	
	//select
	GetDataBD datos = new GetDataBD();
	datos.setModel("openclinic_dbo.sis_periodo");
	datos.setColumn("SIS_PER_ID,SIS_PER_PERIODO,SIS_PER_MES");
	JSONArray where = new JSONArray();
	where.put("SIS_PER_ESTADO = '0'");
	datos.setWhere(where);
	
	JSONObject getPeriodo = datos.getJsonData();
	
	Periodo periodo = new Periodo();
	periodo.setSis_per_id(getPeriodo.getInt("SIS_PER_ID"));
	periodo.setSis_per_periodo(getPeriodo.getString("SIS_PER_PERIODO"));
	periodo.setSis_per_mes(getPeriodo.getString("SIS_PER_MES"));
	periodo.setSis_per_estado("1");
	
	
	JSONObject result = new JSONObject();
			
	if(periodo.store()){
		
		JSONObject getResult = periodo.closed();
		if(getResult.getBoolean("status")){
			Atencion atencion = new Atencion();
			//atencion.moveNewPeriodo(getResult.getBoolean("status"),getResult.getBoolean("status"));
			atencion.moveNewPeriodo(getResult.getString("periodo"), getResult.getString("mes"));
			result = getResult;
			
		}
		else{
			result.put("status", false);
		}
		
	}else{
		result.put("status", false);
	}
	
	

%>
<%=result.toString() %>



