<%@page import="org.json.*,pe.gob.sis.arfsisweb.model.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	String serverId = request.getParameter("serverId");
	String objectId = request.getParameter("objectId");
	String uid = request.getParameter("uid");
	//Integer.parseInt(objectId);

	FUA fua = new FUA();
	fua.setSis_fua_serverId(Integer.parseInt(serverId));
	fua.setSis_fua_objectId(Integer.parseInt(objectId));
	
	JSONObject resultado = fua.changeStatus("delete");
	fua.moveToHistory();
	
	Atencion atencion = new Atencion();
	atencion.moveToHistory(uid);
	
	Diagnostico diagnostico = new Diagnostico();
	diagnostico.moveToHistory(uid);
	
	Medicamento medicamento = new Medicamento();
	medicamento.moveToHistory(uid);
	
	Insumo insumo = new Insumo();
	insumo.moveToHistory(uid);
	
	Prestacion prestacion = new Prestacion();
	prestacion.moveToHistory(uid);
	
	Preventivo preventivo = new Preventivo();
	preventivo.moveToHistory(uid);
	
	Procedimiento procedimiento = new Procedimiento();
	procedimiento.moveToHistory(uid);
	
	RecienNacido recienNacido = new RecienNacido();
	recienNacido.moveToHistory(uid);

%>
<%=resultado.toString() %>