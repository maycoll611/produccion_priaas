<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
	String idnumreg = request.getParameter("idnumreg");

	GetDataBD data = new GetDataBD();
	data.setModel("openclinic_dbo.sis_atencion");
	
	JSONArray whereD = new JSONArray();
	whereD.put("SIS_ATENCION_1 = "+idnumreg);
	whereD.put("SIS_FUA_STATUS in ('open','observed')");
	//whereD.put("met_idestado = '0'");
	String columns = "";
	for(int i=1;i<=86;i++){
		columns+="SIS_ATENCION_"+i;
		if(i!=86){
			columns+=",";
		}
	}
	columns+=",ipress.pre_Nombre,ipress.pre_CodEjeAdm,cat_Nivel";
	columns+=",offlex.pre_Nombre";
	columns+=",referido.pre_Nombre";
	columns+=",mup_descripcion";
	columns+=",afitipodoc.ide_Descripcion";
	columns+=",sex_Descripcion";
	columns+=",SIS_FUA_PERSONID,SIS_FUA_ENCOUNTERUID,SIS_FUA_SERVERID,SIS_FUA_OBJECTID,SIS_FUA_STATUS";
	columns+=",SIS_FUA_UPDATEUSER";
	columns+=",met_descripcion";
	columns+=",ser_Descripcion,ser_Hosp";
	columns+=",contraref.pre_Nombre";
	columns+=",restipodoc.ide_Descripcion";
	columns+=",responsable.pers_ApePaterno,responsable.pers_ApeMaterno,responsable.pers_PriNombre,responsable.pers_OtrNombre";
	columns+=",tiporesponsable.tps_Descripcion";
	columns+=",esp.esp_Descripcion";
	
	data.setColumn(columns);
		
	JSONArray joinD = new JSONArray();
	joinD.put("JOIN openclinic_dbo.sis_fua ON SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3) = SIS_ATENCION_1");
	joinD.put("JOIN m_eess as ipress ON SUBSTRING(ipress.pre_CodigoRENAES,3,8) = SIS_ATENCION_6");
	joinD.put("JOIN a_categoriaeess on cat_IdCategoriaEESS = ipress.pre_IdCategoriaEESS");
	joinD.put("LEFT JOIN m_eess as offlex ON offlex.pre_CodigoRENAES = SIS_ATENCION_60");
	joinD.put("LEFT JOIN m_eess as referido ON referido.pre_CodigoRENAES = SIS_ATENCION_40");
	joinD.put("LEFT JOIN m_ups ON mup_idups = SIS_ATENCION_64");
	joinD.put("LEFT JOIN a_tipodocumento as afitipodoc ON afitipodoc.ide_IdTipoDocumento = SIS_ATENCION_24");
	joinD.put("LEFT JOIN a_sexo ON sex_IdSexo = SIS_ATENCION_31");
	joinD.put("LEFT JOIN m_etnia ON met_idetnia = SIS_ATENCION_61");
	joinD.put("LEFT JOIN m_servicios ON ser_IdServicio = SIS_ATENCION_42");
	joinD.put("LEFT JOIN m_eess as contraref ON contraref.pre_CodigoRENAES = SIS_ATENCION_48");
	joinD.put("LEFT JOIN a_tipodocumento as restipodoc ON restipodoc.ide_IdTipoDocumento = SIS_ATENCION_72");
	joinD.put("LEFT JOIN a_resatencion as responsable ON responsable.pers_IdResAtencion = SIS_ATENCION_73");
	joinD.put("LEFT JOIN a_tipopersonalsalud as tiporesponsable ON tiporesponsable.tps_IdTipoPersonalSalud = SIS_ATENCION_74");
	joinD.put("LEFT JOIN a_especialidad as esp ON esp.esp_IdEspecialidad = SIS_ATENCION_75");
	data.setJoin(joinD);
	
	data.setWhere(whereD);
	JSONObject getData = data.getJsonData();
	
	//diagnosticos
	GetDataBD diagnostico = new GetDataBD();
	diagnostico.setModel("openclinic_dbo.sis_diagnosticos");
	diagnostico.setColumn("SIS_DIAGNOSTICOS_1,SIS_DIAGNOSTICOS_2,SIS_DIAGNOSTICOS_3,SIS_DIAGNOSTICOS_4,SIS_DIAGNOSTICOS_5,C10_Descripcion");
	
	JSONArray joinDia = new JSONArray();
	joinDia.put("JOIN m_cie10 ON C10_CodDia = SIS_DIAGNOSTICOS_2");
	diagnostico.setJoin(joinDia);
		
	JSONArray whereDia = new JSONArray();
	whereDia.put("SIS_DIAGNOSTICOS_1 = "+idnumreg);
	diagnostico.setWhere(whereDia);
	
	//medicamentos
	GetDataBD medicamento = new GetDataBD();
	medicamento.setModel("openclinic_dbo.sis_medicamentos");
	medicamento.setColumn("SIS_MEDICAMENTOS_1,SIS_MEDICAMENTOS_2,SIS_MEDICAMENTOS_3,SIS_MEDICAMENTOS_4,SIS_MEDICAMENTOS_5,SIS_MEDICAMENTOS_9,med_Nombre");
	
	JSONArray joinMed = new JSONArray();
	joinMed.put("JOIN m_medicamentos ON med_CodMed = SIS_MEDICAMENTOS_2");
	medicamento.setJoin(joinMed);
		
	JSONArray whereMed = new JSONArray();
	whereMed.put("SIS_MEDICAMENTOS_1 = "+idnumreg);
	medicamento.setWhere(whereMed);
	
	//Insumos
	GetDataBD insumo = new GetDataBD();
	insumo.setModel("openclinic_dbo.sis_insumos");
	insumo.setColumn("SIS_INSUMOS_1,SIS_INSUMOS_2,SIS_INSUMOS_3,SIS_INSUMOS_4,SIS_INSUMOS_5,SIS_INSUMOS_9,ins_Nombre");
	
	JSONArray joinIns = new JSONArray();
	joinIns.put("JOIN m_insumos ON ins_CodIns = SIS_INSUMOS_2");
	insumo.setJoin(joinIns);
		
	JSONArray whereIns = new JSONArray();
	whereIns.put("SIS_INSUMOS_1 = "+idnumreg);
	insumo.setWhere(whereIns);
	
	//Procedimiento
	GetDataBD procedimiento = new GetDataBD();
	procedimiento.setModel("openclinic_dbo.sis_procedimientos");
	String columnPro = "SIS_PROCEDIMIENTOS_1,SIS_PROCEDIMIENTOS_2,SIS_PROCEDIMIENTOS_3,SIS_PROCEDIMIENTOS_4,SIS_PROCEDIMIENTOS_5,SIS_PROCEDIMIENTOS_6,SIS_PROCEDIMIENTOS_7,SIS_PROCEDIMIENTOS_8,SIS_PROCEDIMIENTOS_9,SIS_PROCEDIMIENTOS_10,SIS_PROCEDIMIENTOS_11,SIS_PROCEDIMIENTOS_13";
	columnPro+=",CPMS_V_DESCPROCSIS,CPMS_V_DESC_PROCCPMS";
	columnPro+=",ide_Descripcion";
	columnPro+=",pers_ApePaterno,pers_ApeMaterno,pers_PriNombre,pers_OtrNombre,pers_Colegiatura,pers_NroEspecialidad";
	columnPro+=",tps_Descripcion";
	columnPro+=",esp_Descripcion";
	procedimiento.setColumn(columnPro);
	
	JSONArray joinPro = new JSONArray();
	joinPro.put("JOIN m_cpms_eqv ON CPMS_V_CODSIS = SIS_PROCEDIMIENTOS_2 AND CPMS_V_CODCPMS = SIS_PROCEDIMIENTOS_3");
	joinPro.put("LEFT JOIN a_resatencion ON pers_IdResAtencion = SIS_PROCEDIMIENTOS_9");
	joinPro.put("LEFT JOIN a_tipodocumento ON ide_IdTipoDocumento = SIS_PROCEDIMIENTOS_8");
	joinPro.put("LEFT JOIN a_tipopersonalsalud ON tps_IdTipoPersonalSalud = SIS_PROCEDIMIENTOS_10");
	joinPro.put("LEFT JOIN a_especialidad ON esp_IdEspecialidad = SIS_PROCEDIMIENTOS_11");
	procedimiento.setJoin(joinPro);
		
	JSONArray wherePro = new JSONArray();
	wherePro.put("SIS_PROCEDIMIENTOS_1 = "+idnumreg);
	procedimiento.setWhere(wherePro);
	
	//Servicios Adicionales
	GetDataBD prestaciones = new GetDataBD();
	prestaciones.setModel("openclinic_dbo.sis_serviciosadicionales");
	prestaciones.setColumn("SIS_SERVICIOSADICIONALES_1,SIS_SERVICIOSADICIONALES_2,ser_Descripcion");
	
	JSONArray joinAdi = new JSONArray();
	joinAdi.put("JOIN m_servicios ON ser_IdServicio = SIS_SERVICIOSADICIONALES_2");
	prestaciones.setJoin(joinAdi);
		
	JSONArray whereAdi = new JSONArray();
	whereAdi.put("SIS_SERVICIOSADICIONALES_1 = "+idnumreg);
	prestaciones.setWhere(whereAdi);
	
	//Servicios Preventivos
	GetDataBD preventivos = new GetDataBD();
	preventivos.setModel("openclinic_dbo.sis_smi");
	preventivos.setColumn("SIS_SMI_1,SIS_SMI_2,SIS_SMI_3,SIS_SMI_4,smi_Descripcion");
	
	JSONArray joinPre = new JSONArray();
	joinPre.put("JOIN m_smi ON smi_IdSmi = SIS_SMI_2");
	preventivos.setJoin(joinPre);
		
	JSONArray wherePre = new JSONArray();
	wherePre.put("SIS_SMI_1 = "+idnumreg);
	preventivos.setWhere(wherePre);
	
	//Recien Nacidos
	GetDataBD nacidos = new GetDataBD();
	nacidos.setModel("openclinic_dbo.sis_reciennacido");
	String columnNac = "SIS_RECIENNACIDO_1,SIS_RECIENNACIDO_2,SIS_RECIENNACIDO_3,SIS_RECIENNACIDO_4,SIS_RECIENNACIDO_5,SIS_RECIENNACIDO_6";
	columnNac+=",SIS_RECIENNACIDO_7,SIS_RECIENNACIDO_8,SIS_RECIENNACIDO_9,SIS_RECIENNACIDO_10,SIS_RECIENNACIDO_11";
	columnNac+=",SIS_RECIENNACIDO_12,SIS_RECIENNACIDO_13,SIS_RECIENNACIDO_14";
	nacidos.setColumn(columnNac);
	
	/*
	JSONArray joinNac = new JSONArray();
	joinNac.put("JOIN m_smi ON smi_IdSmi = SIS_SMI_2");
	nacidos.setJoin(joinNac);
	*/
		
	JSONArray whereNac = new JSONArray();
	whereNac.put("SIS_RECIENNACIDO_1 = "+idnumreg);
	nacidos.setWhere(whereNac);
	
	//JSONArray getDataDia = diagnostico.getArrayJsonData();
	//resultados
	
	JSONObject resultado = new JSONObject();
	
	resultado.put("data",getData);
	resultado.put("diagnosticos",diagnostico.getArrayJsonData());
	resultado.put("medicamentos",medicamento.getArrayJsonData());
	resultado.put("insumos",insumo.getArrayJsonData());
	resultado.put("procedimientos",procedimiento.getArrayJsonData());
	resultado.put("prestaciones",prestaciones.getArrayJsonData());
	resultado.put("preventivos",preventivos.getArrayJsonData());
	resultado.put("nacidos",nacidos.getArrayJsonData());
	if(getData.length()==0){
		
		resultado.put("status",false);
	}else{
		resultado.put("status",true);
	}
	//System.out.println(getData.length());
	

%>
<%=resultado.toString() %>
