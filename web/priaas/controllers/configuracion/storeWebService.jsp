<%@page import="pe.gob.sis.arfsisweb.model.*,org.json.*,pe.gob.sis.arfsisweb.*,java.io.BufferedReader"%>
<%@ page import="java.sql.*" %>
<%@page import="pe.gob.sis.*,org.json.*, java.io.*"%>
<%
	BufferedReader br = request.getReader();
	String params = br.readLine();
	
	JSONObject json = new JSONObject(params);
		
	String centroDigitacionId = Integer.toString(json.getInt("centro_digitacion_id"));
	int checkDigitacionId = json.getInt("centro_digitacion_id");
	String parametro = json.getString("parametro");
	String valor = json.getString("valor");
	String denominacion = json.getString("denominacion");
	int sisWebServiceId = json.getInt("sis_webservice_id");
	
	Boolean resultado ;
	
	if(checkDigitacionId > 0 && !parametro.equals("") && !valor.equals("") && !denominacion.equals(""))
	{	try
		{
			MysqlConnect conn = new MysqlConnect("bdsis_maestros");
			PreparedStatement updateRow = null;
			
		
			updateRow = conn.connect().prepareStatement("UPDATE openclinic_dbo.sis_webservices SET sis_centro_digitacion = '"+centroDigitacionId+"'");
			updateRow.execute();
			updateRow.close();
			
			WebService webservice = new WebService();
			if(sisWebServiceId > 0)
			{
				webservice.setSis_webservice_id(Integer.toString(sisWebServiceId));
			}
			webservice.setSis_centro_digitacion(centroDigitacionId);
			webservice.setSis_denominacion(denominacion);
			webservice.setSis_parametro(parametro);
			webservice.setSis_valor(valor);
			
			resultado = webservice.store();
		
		} catch (Exception e) {
		    e.printStackTrace();
		    System.out.println(e);
		    resultado = null;
		}
				
	}else{
		resultado = null;
	}
	
%>
<%=resultado %>
