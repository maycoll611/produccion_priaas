<%@page import="pe.gob.sis.arfsisweb.model.*,org.json.*"%>
<%
/*************************************
Nombre         : getListRoles
Proposito      : Listado de Roles
Creado por     : FLIZARRAGA
Fec Creacion   : 20230303
Observaciones  : Ninguna 
*************************************/

	int pagina = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));

	String resultado ;
	Roles datos = new Roles();
	datos.limit(rows);
	datos.page(pagina);	

	resultado = datos.tabla();

%>
<%=resultado %>