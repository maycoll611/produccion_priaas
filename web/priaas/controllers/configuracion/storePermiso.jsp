<%@page import="pe.gob.sis.arfsisweb.model.*,org.json.*,pe.gob.sis.arfsisweb.*,pe.gob.sis.MysqlConnect,java.sql.*,net.admin.AdminPerson,java.util.Date,java.io.BufferedReader"%>
<%@page errorPage="/includes/error.jsp"%>
<%
	BufferedReader br = request.getReader();
	String params = br.readLine();
	
	JSONObject json = new JSONObject(params);
	
    String sis_identificador = json.getString("sis_identificador");
    String sis_descripcion = json.getString("sis_descripcion");
    int sis_permiso_id = json.getInt("sis_permiso_id");
    
	Boolean resultado;
	
	Permisos permiso = new Permisos();
	if(sis_permiso_id > 0)
	{
		permiso.setSis_permiso_id(sis_permiso_id);
		permiso.setSis_identificador(sis_identificador);
		permiso.setSis_descripcion(sis_descripcion);
	}else{
		permiso.setSis_identificador(sis_identificador);
		permiso.setSis_descripcion(sis_descripcion);
	}
	
	resultado = permiso.store();
	
%>
<%=resultado %>