<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<%@page import="java.time.LocalDateTime,java.time.format.DateTimeFormatter,org.json.*,java.io.*" %>
<%
	String db = request.getParameter("db");
	String cpu = request.getParameter("cpu");

    Thread.sleep(1000);
	String executeCmd = "";

	LocalDateTime now = LocalDateTime.now();
	DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");
	String datetime = now.format(format); 
	
	String tmpDirName = application.getRealPath("/")+"/"+MedwanQuery.getInstance().getConfigString("tempdir","/tmp/");
	System.out.println(tmpDirName);

	String rutaIni = tmpDirName;
	String rutaAdic = "";

	if(cpu.equals("V64")){
		if(db.equals("admin")){
			rutaAdic += "bkp_admin_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mariadb\\bin && mariadb-dump -u openclinic -p0pen --port=13306 --databases ocadmin_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		} else if (db.equals("data")){
			rutaAdic += "bkp_data_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mariadb\\bin && mariadb-dump -u openclinic -p0pen --port=13306 --databases openclinic_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		} else if (db.equals("both")){
			rutaAdic += "bkp_both_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mariadb\\bin && mariadb-dump -u openclinic -p0pen --port=13306 --databases ocadmin_dbo openclinic_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		}
	} else if(cpu.equals("V32")){
		if(db.equals("admin")){
			rutaAdic += "bkp_admin_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mysql\\bin && mysqldump -u openclinic -p0pen --port=13306 --databases ocadmin_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		} else if (db.equals("data")){
			rutaAdic += "bkp_data_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mysql\\bin && mysqldump -u openclinic -p0pen --port=13306 --databases openclinic_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		} else if (db.equals("both")){
			rutaAdic += "bkp_both_"+datetime+".sql.7z";
			executeCmd = "cmd /c \" cd C:\\projects\\openclinic\\mysql\\bin && mysqldump -u openclinic -p0pen --port=13306 --databases ocadmin_dbo openclinic_dbo | 7z a -si "+rutaIni + rutaAdic +" -pARFSISsecretWEB";
		}
	}

    Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
    
    String resultado = "";
    
    int processComplete = runtimeProcess.waitFor();
    if(processComplete == 0){
		File zip = new File(rutaIni+rutaAdic);
		
		FileInputStream archivo = new FileInputStream(zip);
		int longitud = archivo.available();
		
		byte[] datos = new byte[longitud];
		archivo.read(datos);
		archivo.close();
		
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment;filename="+rutaAdic);    
		    
		    ServletOutputStream ouputStream = response.getOutputStream();
		    ouputStream.write(datos);
		    ouputStream.flush();
		    ouputStream.close();
		    
		zip.delete();
		
		resultado = "Backup guardado";
		
	} 
	else {
		resultado = "No se pudo generar el backup";
	}

%>