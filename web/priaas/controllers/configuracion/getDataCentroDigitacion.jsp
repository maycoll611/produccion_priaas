<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	String columns = request.getParameter("columns");
	
	DataTable datatable = new DataTable();
	datatable.setModel("m_pinstalacion");
	datatable.setColumn(columns);
	
	Map<String,String> relation1 = new HashMap<String, String>();
 
    List<Map<String , String>> relations  = new ArrayList<Map<String,String>>();

    relation1.put("relation", "bdsis_maestros.m_odsis");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "m_odsis.ods_IdOdsis");
    relation1.put("foreign_key", "m_pinstalacion.pin_IdOdsis");
    
    relations.add(0,relation1);
  
	datatable.setRelations(relations);
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
%>
<%=datatable.getDataJson() %>