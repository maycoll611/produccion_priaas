<%@page import="pe.gob.sis.arfsisweb.model.*,org.json.*,pe.gob.sis.arfsisweb.*,pe.gob.sis.MysqlConnect,java.sql.*,net.admin.AdminPerson,java.util.Date,java.io.BufferedReader"%>
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<%
	BufferedReader br = request.getReader();
	String params = br.readLine();
	
	JSONObject json = new JSONObject(params);
	JSONObject result = new JSONObject();
	
    String centroDigitacion = json.getString("centro_digitacion");
    int centroDigitacionId = json.getInt("centro_digitacion_id");
    String documento = json.getString("documento");
    String emailInstitucional = json.getString("email_institucion");
    String emailParticular = json.getString("email_particular");
    String establecimientoSalud = json.getString("establecimiento_salud");
    String establecimientoSaludId = json.getString("establecimiento_salud_id");
    /* String gmr = json.getString("gmr");
    int gmr_id = json.getInt("gmr_id"); */
    String paterno = json.getString("paterno");
    String materno = json.getString("materno");
    String nombres = json.getString("nombres");
    String genero = json.getString("genero");
    String tipoDocumento = json.getString("tipo_documento");
    
    
    int rol = json.getInt("rol");
    int sisUsuarioId = json.getInt("sis_usuario_id");
    
    
    String userid = json.getString("userid");
    String personid = json.getString("personid");
	Boolean resultado;
	
	/* verificar si existe usuario con mismo dni */
	
	boolean existUser = false;
	MysqlConnect conn = new MysqlConnect("ocadmin_dbo");

	/* AFILIADOS */
	PreparedStatement sqlc = conn.connect().prepareStatement("SELECT * FROM ocadmin_dbo.users AS u JOIN admin AS a ON a.personid = u.personid WHERE a.natreg = '"+documento+"'");
	ResultSet rsc = sqlc.executeQuery();	
	
	if(rsc.next() && personid.equals(""))
	{
		resultado = false;
		result.put("message","El usario con dni: " +documento +" ya fue registrado anteriormente.");
	}else{
	
		/* Guardar persona */
		AdminPerson person = new AdminPerson();
		if(!personid.equals(""))
		{
			/* System.out.println(personid);
			person.personid = personid;
			System.out.println(person.personid); */
			person.initialize(personid);
		}
		person.firstname = nombres;
		person.lastname = paterno;
		person.middlename = materno;
		person.setID("natreg",documento);
		person.gender = genero;
		person.sourceid = "1";
		/* person.personType = "1"; */
		person.personType = tipoDocumento;
		person.updateuserid = activeUser.userid;
		person.store();
		
		Usuario usuario = new Usuario();
		if(userid.equals(""))
		{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy-MM-dd HH:mm:ss");
			String date = df.format(new java.util.Date());
			
			User u = new User();
		    u.personid = person.personid;
		    /* u.password = activeUser.encrypt(documento); */   
		    u.start = date;
			u.project = "peru";
		    u.saveToDB();
		    
		    usuario.setSis_oc_userid(Integer.parseInt(u.userid));
		    
		    byte[] password = u.encryptOld(documento);
	        
	        u.password = password;
	        u.savePasswordToDB();
	       /*  String sSql = "UPDATE UsedPasswords SET updatetime = ?"+
	       	              " WHERE userId = ?"+
	                      "  AND CAST(encryptedPassword AS BINARY) = ?";
	        Connection conn = MedwanQuery.getInstance().getAdminConnection();
	        PreparedStatement ps = conn.prepareStatement(sSql);
	        ps.setTimestamp(1,getSQLTime()); // now
	        ps.setInt(2,Integer.parseInt(u.userid));
	        ps.setBytes(3,password);
	        int updatedRecords = ps.executeUpdate();
	        ps.close();
	        conn.close();
	        
	        if(updatedRecords==0){                
	        	sSql = "INSERT INTO UsedPasswords(usedPasswordId,encryptedPassword,userId,updatetime,serverid)"+
	                   " VALUES (?,?,?,?,?)";
	            conn = MedwanQuery.getInstance().getAdminConnection();
	            ps = conn.prepareStatement(sSql);
	            ps.setInt(1,MedwanQuery.getInstance().getOpenclinicCounter("UsedPasswords"));
	            ps.setBytes(2,password);
	            ps.setInt(3,Integer.parseInt(u.userid));
	            ps.setTimestamp(4,getSQLTime()); // now
	            ps.setInt(5,MedwanQuery.getInstance().getConfigInt("serverId"));
	            ps.executeUpdate();
	            ps.close();
	            conn.close();
	        } */
	        
	        Parameter userproject = new Parameter("userproject","peru");
	        u.updateParameter(userproject);
	        
	        Parameter userprofileid = new Parameter("userprofileid","13");
	        u.updateParameter(userprofileid);
	        
	        Parameter defaultpage = new Parameter("defaultpage","medicalrecord");
	        u.updateParameter(defaultpage);
	        
	        Parameter sa = new Parameter("sa","on");
	        u.updateParameter(sa);
	        
	        Parameter alias = new Parameter("alias",documento);
	        u.updateParameter(alias);
	        
	        Parameter defaultserviceid = new Parameter("defaultserviceid","CLI.TOP");
	        u.updateParameter(defaultserviceid);
	        
	        Parameter invoicingcareprovider = new Parameter("invoicingcareprovider","on");
	        u.updateParameter(invoicingcareprovider);
	        
	        Parameter userlanguage = new Parameter("userlanguage","es");
	        u.updateParameter(userlanguage);
	        
	        Parameter pwdChangeParam = new Parameter("pwdChangeDate",System.currentTimeMillis()+"");
	        u.updateParameter(pwdChangeParam);
	        
	        Parameter UserTheme = new Parameter("UserTheme","default");
	        u.updateParameter(UserTheme);
		    /* UserParameter userParameter = new UserParameter();
			userParameter.saveUserParameter("userproject", "peru",Integer.parseInt(u.userid));
		    
		    userParameter.saveUserParameter("defaultpage", "medicalrecord", Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("sa", "on", Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("alias", documento, Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("defaultserviceid", "CLI.TOP", Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("invoicingcareprovider", "on", Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("userlanguage", "es", Integer.parseInt(u.userid));
		    userParameter.saveUserParameter("UserTheme", "default", Integer.parseInt(u.userid));
		    
		    String sSql = "INSERT INTO UsedPasswords(usedPasswordId,encryptedPassword,userId,updatetime,serverid)"+
	                " VALUES (?,?,?,?,?)";
		    Connection conn = MedwanQuery.getInstance().getAdminConnection();
		    PreparedStatement ps = conn.prepareStatement(sSql);
	        ps.setInt(1,MedwanQuery.getInstance().getOpenclinicCounter("UsedPasswords"));
	        ps.setBytes(2,activeUser.encrypt(documento));
	        ps.setInt(3,Integer.parseInt(u.userid));
	        ps.setTimestamp(4,getSQLTime()); // now
	        ps.setInt(5,MedwanQuery.getInstance().getConfigInt("serverId"));
	        ps.executeUpdate();
	        ps.close();
	        conn.close(); */
		}else{
			usuario.setSis_oc_userid(Integer.parseInt(userid));
		}
		
		if(sisUsuarioId > 0)
		{
			usuario.setSis_usuario_id(sisUsuarioId);
		}
		usuario.setSis_centro_digitacion(Integer.toString(centroDigitacionId));
		MysqlConnect con = new MysqlConnect("bdsis_maestros");
		PreparedStatement sql= null;
		ResultSet rs= null;
		try {
		sql = con.connect().prepareStatement("SELECT * FROM bdsis_maestros.m_eess JOIN bdsis_maestros.m_odsis on m_odsis.ods_IdOdsis = m_eess.pre_IdOdsis WHERE pre_CodigoRENAES="+establecimientoSaludId+" LIMIT 1;");
		rs = sql.executeQuery();
		
			if(rs.next())
			{
				usuario.setSis_disa(rs.getString("pre_IdDisa"));
				usuario.setSis_udr(rs.getString("pre_IdOdsis"));
				usuario.setSis_ue(rs.getString("pre_CodEjeAdm"));
				usuario.setSis_gmr(rs.getInt("ods_IdMacroRegion"));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}finally{
			rs.close();
			sql.close();
		}
		usuario.setSis_eess(establecimientoSaludId);
		usuario.setSis_email_institucion(emailInstitucional);
		usuario.setSis_email_particular(emailParticular);
		/* usuario.setSis_gmr(gmr_id); */
		usuario.setSis_rol(rol);
		resultado = usuario.store();
		
		if(resultado)
		{
			result.put("message","¡Usuario registrado correctamente!");
		}else{
			result.put("message","Hubo un error al momento de registrar el usuario, verifique los datos ingresados");
		}
	}
	
	result.put("status",resultado);
		
	rsc.close();
	sqlc.close();
%>
<%=result %>