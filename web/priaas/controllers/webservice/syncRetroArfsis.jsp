<%@page import="org.apache.commons.io.FileUtils"%>
<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@page import="pe.gob.sis.*,org.json.*, java.io.*"%>
<%@ page import="java.sql.*" %>

<%@page import="pe.gob.sis.UnzipUtility" %>
<%@page import="pe.gob.sis.DownloadUtility" %>                
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>
<%
	String fileName = request.getParameter("fileName");
	
	String message="";
	JSONObject result = new JSONObject();
	JSONArray array = new JSONArray();

	String link = "http://sigeps.sis.gob.pe/sisERP/retroarfsisSigeps/archivos/"+fileName+".zip";
	String tmpDirName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp");
	
	File outFile = new File(tmpDirName+"/"+fileName+".zip");
	DownloadUtility du = new DownloadUtility(link,outFile);
	
	Thread thRead = new Thread(du);
	
	thRead.start();
	try 
	{   
		System.out.println("join");
		thRead.join(); 
    } catch (InterruptedException e) {
       e.printStackTrace();
    }
	
	if(!thRead.isAlive())
	{
		UnzipUtility unzipper = new UnzipUtility();
		String fullFileName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")+"/"+fileName;
		new File(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")).mkdirs();
		try {
		    unzipper.unpack(fullFileName, tmpDirName);
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
		
		File tmpDir = new File(tmpDirName);
       
        PreparedStatement ps = null;
    	ResultSet rs = null;
    	
        File[] tmpFiles = tmpDir.listFiles();
        if(tmpFiles == null || tmpFiles.length == 0)
        {
        	System.out.println("No hay elementos dentro de la carpeta de actualizacion");
        }
        else{
        	String log ="";
        	MysqlConnect con = new MysqlConnect("bdsis_maestros");
        	
        	PreparedStatement fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=0");
        	fkChecks.executeQuery();
        	
        	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_sincronizacion WHERE tipo='2' AND nombre = '"+fileName+".zip"+"' LIMIT 1");
        	ResultSet rsi = sql.executeQuery();
        	
        	int size = 0;
        	String fecha = "";
            while (rsi.next()) {
                size++;
                fecha += rsi.getString("fecha");
            }
            if(size == 0)
            {
            	String s = new String();
           	    StringBuffer sb = new StringBuffer();
           	    if(new File(tmpDirName+"/A_QUERY_ARFSIS.sql").exists())
           	    {
           	    	FileReader fr = new FileReader(new File(tmpDirName+"/A_QUERY_ARFSIS.sql"));
               	 	BufferedReader br = new BufferedReader(fr);
               	 	while((s = br.readLine()) != null){
               	     sb.append(s);
               	   	}
               	   	br.close();
               	 	
               	    String[] inst = sb.toString().split(";");
               	 
                    for(int i = 0; i<inst.length; i++) {
                    
	                    if(!inst[i].trim().equals("")){
	                    	try{
	                    		PreparedStatement sqlTxt = con.connect().prepareStatement(inst[i]+";");
		                    	int resultTxtQuery = sqlTxt.executeUpdate();
		                    	log+=inst[i]+": result("+resultTxtQuery+")\n";
	                    	}
	                    	catch(Exception e)
	                    	{
	                    		String messageTxt = e.getMessage().toString();
	                    	
	                    		log+=inst[i]+": "+messageTxt+"\n";
	                    	}
	                    }
                    }
           	    }
           	 	
            	PreparedStatement updateRow = con.connect().prepareStatement("UPDATE openclinic_dbo.sis_sincronizacion SET estado='1'");
        		updateRow.execute();
        		updateRow.close();
        		
                PreparedStatement insertSync = con.connect().prepareStatement("INSERT INTO openclinic_dbo.sis_sincronizacion "
        				+ "(nombre,tipo,resultado,log,estado) "
        				+ "VALUES (?,?,?,?,?)");
                insertSync.setString(1, fileName+".zip");
                insertSync.setString(2, "2");
                
                int totalArchivos = tmpFiles.length, totalExito=0, totalError=0;
                
            		PreparedStatement sqlc = con.connect().prepareStatement("SELECT tb_Descripcion,tb_archiRetro FROM openclinic_dbo.sis_tablas");
            		
                	ResultSet rsc = sqlc.executeQuery();
                	while(rsc.next())
                	{
                		String tmpTableName= rsc.getString("tb_Descripcion");
                		String tmpTableFile= rsc.getString("tb_archiRetro");
                		
                		File txtFile = new File(tmpDirName+"/"+tmpTableFile);
                		
                		if (txtFile.exists()) {
                			try
                            {
                                
                                String query = "LOAD DATA INFILE '"+tmpDirName+"/"+tmpTableFile+"' REPLACE INTO TABLE `bdsis_maestros`.`"+tmpTableName+"` CHARACTER SET latin1 FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\r\\n';";
                                ps = con.connect().prepareStatement(query);
                    	    	rs = ps.executeQuery();
                            	message = rs.toString();
                            	totalExito++;
                            	result.put("status",true);
                            }
                            catch(Exception e)
                            {
                                message = e.getMessage().toString();
                            	totalError++;   
                            	result.put("status",true);
                            } 
                			result.put("message","total archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
                			log+=tmpTableFile+": "+message+"\n";
                			result.put("log",log);
                		}
            			txtFile.delete();
            		}
                	
            	insertSync.setString(3, "Actualizaci�n realizada con �xito, total de archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
            	insertSync.setString(4, log);
            	insertSync.setString(5, "0");
            	insertSync.execute();
            	insertSync.close();
            	sql.close();
            }
            else{
            	result.put("status",false);
            	result.put("message","La actualizacion ya fue realizada con anterioridad: "+fecha);
            }
            
            fkChecks.close();
            fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=1");
        	fkChecks.executeQuery();
        }
        	
        if(rs!=null)rs.close();
        if(ps!=null)ps.close();             
    	FileUtils.cleanDirectory(tmpDir);
	}
	
%>
<%=result.toString()%>