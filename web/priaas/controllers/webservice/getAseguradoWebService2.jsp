<%@page import="pe.gob.sis.*,org.json.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Date"%>

<%@page import="java.nio.charset.StandardCharsets,be.openclinic.mobilemoney.*"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.entity.*,org.apache.commons.codec.binary.*,javax.crypto.*,javax.crypto.spec.*,org.apache.http.client.*,org.apache.http.client.methods.*,
				org.apache.http.client.utils.*,java.net.*,org.apache.http.impl.client.*,org.apache.http.*"%>

<%@ page import="net.admin.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@page import="pe.gob.sis.arfsisweb.*"%>
<%

	String disa = request.getParameter("disa");
	String lote = request.getParameter("lote");
	String numero = request.getParameter("numero");
	String correlativo = request.getParameter("correlativo");
	String columns = request.getParameter("columns");
	String webservice = request.getParameter("webservice");
	String centroDigitacionId = request.getParameter("centro_digitacion_id");

	MysqlConnect con = new MysqlConnect("openclinic_dbo");
	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_webservices WHERE sis_centro_digitacion = '"+centroDigitacionId+"' AND sis_denominacion = '"+webservice+"'");
	ResultSet rs = sql.executeQuery();
	
	String url = "";
	String urlLogin = "";
	String user = "";
	String password = "";
	String consultor= "";
	while(rs.next())
	{System.out.println(rs.getString("sis_parametro"));
		switch (rs.getString("sis_parametro")){
	        case "url":{
	            url = rs.getString("sis_valor");
	            break;
	        }
	        case "user":{
	        	user = rs.getString("sis_valor");
	            break;
	        }
	        case "password":{
	        	password = new WebService().decrypt(rs.getString("sis_valor"));
	            break;
	        }
	        case "consultor":{
	        	consultor = rs.getString("sis_valor");
	            break;
	        }
	        case "urlLogin":{
	        	urlLogin = rs.getString("sis_valor");
	            break;
	        }
	        default: {
	            System.out.println("Opcion incorrecta");
	        }
	  }
	}
	
	HttpClient httpclientLogin = HttpClients.createDefault();
	String baseurlLogin = urlLogin;
	URIBuilder builderLogin = new URIBuilder(baseurlLogin);
	URI uriLogin = builderLogin.build();
	HttpPost reqLogin = new HttpPost(uriLogin);
	String bodyLogin = "{"+
			"\"userName\" : \""+user+"\","+
		    "\"password\" : \""+password+"\","+
		    "\"dni\" : \""+consultor+"\""+
			"}";
	reqLogin.setHeader("Content-Type", "application/json");
	StringEntity reqEntityLogin = new StringEntity(bodyLogin);
	reqLogin.setEntity(reqEntityLogin);
	HttpResponse respLogin = httpclientLogin.execute(reqLogin);
	HttpEntity entityLogin = respLogin.getEntity();
	
	String responseBodyLogin = EntityUtils.toString(respLogin.getEntity(), StandardCharsets.UTF_8);	
	String token = responseBodyLogin;
	
	String responseBody = "";
	try {
		
		if(lote.equals("2") || lote.equals("E"))
		{
			HttpClient httpclient = HttpClients.createDefault();
			String baseurl=url;
			URIBuilder builder = new URIBuilder(baseurl);
			URI uri = builder.build();
			HttpPost req = new HttpPost(uri);
			
			String auth = "Bearer " + token;	
			req.setHeader("Authorization",auth);
			req.setHeader("Content-Type", "application/json");
			
			String body = "{"+
					"\"afiliacion\" : \""+lote+"-"+numero+"\""+
					"}";
			req.setHeader("Content-Type", "application/json");
			StringEntity reqEntity = new StringEntity(body);
			req.setEntity(reqEntity);
			HttpResponse resp = httpclient.execute(req);
			HttpEntity entity = resp.getEntity();
			
			responseBody = EntityUtils.toString(resp.getEntity(), StandardCharsets.UTF_8);
		}
		else{
			HttpClient httpclient = HttpClients.createDefault();
			String baseurl=url;
			URIBuilder builder = new URIBuilder(baseurl);
			URI uri = builder.build();
			HttpPost req = new HttpPost(uri);
			
			String valueToEncode = user+":"+password;
			String auth = "Bearer " + token;	
			req.setHeader("Authorization",auth);
			req.setHeader("Content-Type", "application/json");
			
			String body = "{"+
				    "\"afiliacion\" : \""+disa+"-"+lote+"-"+numero+"-"+correlativo+"\""+
					"}";
			req.setHeader("Content-Type", "application/json");
			StringEntity reqEntity = new StringEntity(body);
			req.setEntity(reqEntity);
			HttpResponse resp = httpclient.execute(req);
			HttpEntity entity = resp.getEntity();
			
			responseBody = EntityUtils.toString(resp.getEntity(), StandardCharsets.UTF_8);
		}
		
		JSONArray arrayResponse = new JSONArray(responseBody);
		
		JSONObject objResponse = arrayResponse.getJSONObject(0);
		
		
		Afiliado afiliado = new Afiliado();
		afiliado.setAfi_IdSiasis((String) objResponse.get("tabla"));
		afiliado.setAfi_Dni((String) objResponse.get("nroDocumento"));
		afiliado.setAfi_IdTipoDocumento((String) objResponse.get("tipoDocumento"));
		afiliado.setAfi_ApePaterno((String) objResponse.get("apePaterno"));
		afiliado.setAfi_ApeMaterno((String) objResponse.get("apeMaterno"));
		String nombres = (String) objResponse.get("nombres");
		if(nombres.split(" ").length > 1)
		{
			afiliado.setAfi_Nombres(nombres.split(" ")[0]);
			afiliado.setAfi_SegNombre(nombres.split(" ")[1]);	
		}
		else
		{
			afiliado.setAfi_Nombres(nombres);
		}
		
		SimpleDateFormat sdFecFormato = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecFormato = sdFecFormato.parse((String) objResponse.get("fecAfiliacion"));
		sdFecFormato.applyPattern("yyyy-MM-dd");
		
		afiliado.setAfi_FecFormato(sdFecFormato.format(dFecFormato).toString());
		
		afiliado.setAfi_IdEESSAte((String) objResponse.get("eess"));
		afiliado.setAfi_TipoTabla((String) objResponse.get("tablaID"));
		afiliado.setAfi_IdSexo((String) objResponse.get("genero"));
		
		SimpleDateFormat sdFecNac = new SimpleDateFormat("yyyyMMdd");
	  	Date dFecNac = sdFecNac.parse((String) objResponse.get("fecNacimiento"));
	  	
	  	sdFecNac.applyPattern("yyyy-MM-dd");
		afiliado.setAfi_FecNac(sdFecNac.format(dFecNac).toString());
		
		String contrato = (String) objResponse.get("contrato");
		String[] contratoSplit = contrato.split("-");
		
		con = new MysqlConnect("bdsis_maestros");
		sql = con.connect().prepareStatement("SELECT * FROM bdsis_maestros.m_eess WHERE pre_CodigoRENAES = '"+((String) objResponse.get("eess"))+"'");
		rs = sql.executeQuery();
		
		if(rs.next())
		{
			afiliado.setAfi_IdDistrito(rs.getString("pre_IdUbigeo"));
			afiliado.setAfi_IdDisa(rs.getString("pre_IdDisa"));
		}
		if(contratoSplit.length == 2)
		{
			afiliado.setAfi_TipoFormato(contratoSplit[0]);
			afiliado.setAfi_NroFormato(contratoSplit[1]);
			
		}else
		{
			/* afiliado.setAfi_IdDisa(contratoSplit[0]); */
			afiliado.setAfi_TipoFormato(contratoSplit[1]);
			afiliado.setAfi_NroFormato(contratoSplit[2]);
			
			if(contratoSplit.length == 4)
			{
				afiliado.setAfi_CorrelativoIns(contratoSplit[3]);	
			}
		}
		
		/* afiliado.setAfi_IdDistrito((String) objResponse.get("ubigeo")); */
		
		/* AFILIADOS */
		
		afiliado.setAfi_Plan((String) objResponse.get("idPlan"));
		afiliado.setAfi_idGrupoPoblacional((String) objResponse.get("idGrupoPoblacional"));
		
		
		afiliado.store();
		
		sql.close();
		rs.close();
		
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println (e);
	}
	
	String resultado ;
	GetDataAsegurado datos = new GetDataAsegurado("m_afiliados");
	datos.column(columns);
	
	int serverId = MedwanQuery.getInstance().getServerId();
	String userId = activeUser.userid;
	resultado = datos.getDatosJSONU(disa, lote, numero, correlativo,userId);
	out.println(resultado);

%>