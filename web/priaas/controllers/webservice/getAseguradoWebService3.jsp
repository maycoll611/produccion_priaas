<%@page import="pe.gob.sis.*,org.json.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Date"%>

<%@page import="java.nio.charset.StandardCharsets,be.openclinic.mobilemoney.*"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.entity.*,org.apache.commons.codec.binary.*,javax.crypto.*,javax.crypto.spec.*,org.apache.http.client.*,org.apache.http.client.methods.*,
				org.apache.http.client.utils.*,java.net.*,org.apache.http.impl.client.*,org.apache.http.*"%>

<%@ page import="net.admin.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@page import="pe.gob.sis.arfsisweb.*"%>
<%

	String disa = request.getParameter("disa");
	String lote = request.getParameter("lote");
	String numero = request.getParameter("numero");
	String correlativo = request.getParameter("correlativo");
	String columns = request.getParameter("columns");
	String webservice = request.getParameter("webservice");
	String centroDigitacionId = request.getParameter("centro_digitacion_id");

	MysqlConnect con = new MysqlConnect("bdsis_asegurados");
	PreparedStatement sql = null;
	ResultSet rs = null;
	
	SoapWebService sws = new SoapWebService();
	sws.setsDisa(disa);
	sws.setsTipoFormato(lote);
	sws.setsContrato(numero);
	sws.setsCorrelativo(correlativo);
	
	JSONObject objResponse = sws.asegurados();
	String result = objResponse.get("message").toString();
	String status = objResponse.get("status").toString();
	/* try { */
		if(status.equals("true")){
			
			System.out.println(result);
			System.out.println(status);	
			String[] resultSplit = result.split("\\|");
			
			Afiliado afiliado = new Afiliado();
			String idSiasis = resultSplit[0];
			
			afiliado.setAfi_IdSiasis(resultSplit[0]);
			afiliado.setAfi_TipoTabla(resultSplit[1]);
			afiliado.setAfi_IdDisa(resultSplit[2]);
			afiliado.setAfi_TipoFormato(resultSplit[3]);
			afiliado.setAfi_NroFormato(resultSplit[4]);
			
			if(!resultSplit[5].equals(""))
			{
				int correlativoIns = Integer.parseInt(resultSplit[5]);
				afiliado.setAfi_CorrelativoIns(Integer.toString(correlativoIns));
			}
			afiliado.setAfi_IdTipoDocumento(resultSplit[6]);
			afiliado.setAfi_IdEESSAte(resultSplit[7]);
			
			String[] fecFor = resultSplit[8].split("/");
			afiliado.setAfi_FecFormato(fecFor[2]+"-"+fecFor[1]+"-"+fecFor[0]);
			
			afiliado.setAfi_ApePaterno(resultSplit[9]);
			afiliado.setAfi_ApeMaterno(resultSplit[10]);
			afiliado.setAfi_Nombres(resultSplit[11]);
			if(!resultSplit[12].equals(""))
			{
				afiliado.setAfi_SegNombre(resultSplit[12]);	
			}
			afiliado.setAfi_IdSexo(resultSplit[13]);
			
			String[] fecNac = resultSplit[14].split("/");
			afiliado.setAfi_FecNac(fecNac[2]+"-"+fecNac[1]+"-"+fecNac[0]);
			
			afiliado.setAfi_IdDistrito(resultSplit[15]);
			afiliado.setAfi_IdEstado(resultSplit[16]);
			
			
			if(!resultSplit[17].equals(""))
			{
				String[] fecBaja = resultSplit[17].split("/");
				afiliado.setAfi_FecBaja(fecBaja[2]+"-"+fecBaja[1]+"-"+fecBaja[0]);
			}
			
			/* afiliado.setAfi_FecBaja(resultSplit[17]); */
			
			afiliado.setAfi_Dni(resultSplit[18]);
			afiliado.setAfi_Plan(resultSplit[19]);
			afiliado.setAfi_idGrupoPoblacional(resultSplit[23]);
			
			
			afiliado.store();
		}
	/* String result =  objResponseR.toString(); */
	/* } catch (Exception e) {
	    e.printStackTrace();
	    System.out.println (e);
	}
	 */
	String resultado ;
	GetDataAsegurado datos = new GetDataAsegurado("m_afiliados");
	datos.column(columns);
	
	int serverId = MedwanQuery.getInstance().getServerId();
	String userId = activeUser.userid;
	resultado = datos.getDatosJSONU(disa, lote, numero, correlativo,userId);
	out.println(resultado);

%>