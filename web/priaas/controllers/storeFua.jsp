<%@page import="pe.gob.sis.arfsisweb.model.*,pe.gob.sis.arfsisweb.*,org.json.*,java.io.BufferedReader,java.util.Date,java.text.SimpleDateFormat,java.io.PrintWriter,java.io.StringWriter"%>
<%@include file="/includes/validateUser.jsp" %>
<%

	BufferedReader br = request.getReader();
	String params = br.readLine();
	
	JSONObject json = new JSONObject(params);  
		
	Boolean editar = json.getBoolean("editar");
	String encounterID = json.getString("encounterID");
	String Uid = json.getString("Uid");
	String estado = json.getString("estado");
	JSONObject atencion = json.getJSONObject("atencion");
	JSONArray diagnosticos = json.getJSONArray("diagnosticos");
	JSONArray medicamentos = json.getJSONArray("medicamentos");
	JSONArray insumos = json.getJSONArray("insumos");
	JSONArray procedimientos = json.getJSONArray("procedimientos");
	JSONArray prestaciones = json.getJSONArray("prestaciones");
	JSONArray preventivos = json.getJSONArray("preventivos");
	JSONArray nacidos = json.getJSONArray("nacidos");
	
	JSONObject result = new JSONObject();
	int idUser = Integer.parseInt(activeUser.userid);
	try{
		String ate_fecatencion = atencion.optString("ate_fecatencion")+" "+atencion.optString("ate_horaatencion");
		//Fua
		int serverId = MedwanQuery.getInstance().getServerId();
		
		int personId = atencion.getInt("personid");
		//System.out.println("OPENCLINIC:"+activeUser.person.firstname);
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		Date dateFecAtencion = formatter.parse(ate_fecatencion+":00"); 
		int userId = atencion.getInt("userid");
		
		//transaccion
		String sInit= "visit;1;CLI.TOP";
		//TransactionFUA transaction = new TransactionFUA(personId,activeUser.userid,sInit,dateFecAtencion);
		//String encounterId = transaction.getUid();
		
			
		//String[] encounterArray = encounterId.split("\\.");
		//-------periodo----------
		GetDataBD datos = new GetDataBD();
		datos.setModel("openclinic_dbo.sis_periodo");
		datos.setColumn("SIS_PER_PERIODO,SIS_PER_MES");
		JSONArray where = new JSONArray();
		where.put("SIS_PER_ESTADO = '0'");
		datos.setWhere(where);
		JSONObject periodo = datos.getJsonData();
			
		
		JSONObject armAtencion = new JSONObject();
		//armAtencion.put("ate_idnumreg",encounterArray[1].toString());//1
		System.out.println(atencion.getString("ate_idnumreg"));
		armAtencion.put("ate_idnumreg",atencion.getString("ate_idnumreg"));//1
		armAtencion.put("ate_disa",atencion.getString("ate_disa"));//2
		armAtencion.put("ate_lote",atencion.getString("ate_lote"));//3
		armAtencion.put("ate_numregate",atencion.getString("ate_numregate"));//4
		armAtencion.put("ate_idodsis", atencion.getString("ate_idodsis"));//5
		armAtencion.put("ate_ideess",atencion.getString("ate_ideess"));//6
		armAtencion.put("ate_idcategoriaeess",atencion.getString("ate_idcategoriaeess"));//7
		armAtencion.put("ate_nivel",atencion.getString("ate_nivel"));//8
		armAtencion.put("ate_idppdd",atencion.getString("ate_idppdd"));//9
		armAtencion.put("ate_esrec",atencion.getString("ate_esrec"));//10
		armAtencion.put("ate_disarec",atencion.getString("ate_disarec"));//11
		armAtencion.put("ate_loterec",atencion.getString("ate_loterec"));//12
		armAtencion.put("ate_numregaterec",atencion.getString("ate_numregaterec"));//13
		armAtencion.put("ate_codinstitucion","0");//14
		armAtencion.put("ate_idcomponente",atencion.getString("ate_idcomponente"));//15
		armAtencion.put("ate_disaafiins",atencion.getString("ate_disaafiins"));//16
		armAtencion.put("ate_loteafiins",atencion.getString("ate_loteafiins"));//17
		armAtencion.put("ate_numregafiins",atencion.getString("ate_numregafiins"));//18
		armAtencion.put("ate_correlativoaus",atencion.getString("ate_correlativoaus"));//19
		armAtencion.put("ate_tipotablasiasis",atencion.getString("ate_tipotablasiasis"));//20 del formulario
		armAtencion.put("ate_idformatosiasis",atencion.getString("ate_idformatosiasis"));//21 del formulario
		armAtencion.put("ate_plan",atencion.getString("ate_plan"));//22
		armAtencion.put("ate_grupopoblacional",atencion.getString("ate_grupopoblacional"));//23
		int tipodocumento = atencion.getInt("ate_tipodocumento");
		armAtencion.put("ate_tipodocumento",String.valueOf(tipodocumento));//24
		armAtencion.put("ate_dni",atencion.getString("ate_dni"));//25
		armAtencion.put("ate_appat",atencion.getString("ate_appat"));//26
		System.out.println(atencion);
		System.out.println(atencion.getString("ate_apmat"));
		armAtencion.put("ate_apmat",atencion.getString("ate_apmat"));//27
		armAtencion.put("ate_pnom",atencion.getString("ate_pnom"));//28
		armAtencion.put("ate_snom",atencion.getString("ate_snom"));//29
		armAtencion.put("ate_fecnac",atencion.getString("ate_fecnac"));//30
		armAtencion.put("ate_idsexo",atencion.getString("ate_idsexo"));//31
		armAtencion.put("ate_idubigeo",atencion.getString("ate_idubigeo"));//32
		armAtencion.put("ate_historiaclinica",atencion.getString("ate_historiaclinica"));//33
		armAtencion.put("ate_codate",atencion.getString("ate_codate"));//34
		armAtencion.put("ate_esgestante",atencion.getString("ate_esgestante"));//35
		armAtencion.put("ate_idmodalidad",atencion.getString("ate_idmodalidad"));//36
		armAtencion.put("ate_numautorizacion",atencion.getString("ate_numautorizacion"));//37
		//System.out.println(atencion.getString("ate_numautorizacion"));
		armAtencion.put("ate_monto",atencion.getString("ate_monto"));//38 
		//System.out.println(atencion.getString("ate_monto"));
		armAtencion.put("ate_fecatencion",ate_fecatencion);//39

		armAtencion.put("ate_ideessrefirio",atencion.getString("ate_ideessrefirio"));//40
		armAtencion.put("ate_nrohojareferencia",atencion.getString("ate_nrohojareferencia"));//41 del form

		armAtencion.put("ate_idservicio",atencion.getString("ate_idservicio"));//42
		armAtencion.put("ate_idorigenpersonal",atencion.getString("ate_idorigenpersonal"));//43
		armAtencion.put("ate_idlugar",atencion.getString("ate_idlugar"));//44
		armAtencion.put("ate_coddestino",atencion.getString("ate_coddestino"));//45 del formulario
		armAtencion.put("ate_fecinghosp",atencion.getString("ate_fecinghosp"));//46
		armAtencion.put("ate_fecaltahosp",atencion.getString("ate_fecaltahosp"));//47
		armAtencion.put("ate_ideesscontrarefiere",atencion.getString("ate_ideesscontrarefiere"));//48
		armAtencion.put("ate_numhojacontrarefiere",atencion.getString("ate_numhojacontrarefiere"));//49
		armAtencion.put("ate_fecparto",atencion.getString("ate_fecparto"));//50 del formulario
		armAtencion.put("ate_gruporiesgo",atencion.getString("ate_gruporiesgo"));//51
		armAtencion.put("ate_idiiee","");//52
		armAtencion.put("ate_idniveliiee","");//53
		armAtencion.put("ate_idgradoiiee","");//54
		armAtencion.put("ate_seccioniiee","");//55
		armAtencion.put("ate_idturnoiiee","");//56	
		armAtencion.put("ate_iieeeduespecial","");//57
		armAtencion.put("ate_iieeanexo","");//58
		armAtencion.put("ate_fecfallecimiento",atencion.getString("ate_fecfallecimiento"));//59
		armAtencion.put("ate_ideessflexible",atencion.getString("ate_ideessflexible"));//60
		armAtencion.put("ate_idetnia",atencion.getString("ate_idetnia"));//61
		armAtencion.put("ate_idiafas","");//62 
		armAtencion.put("ate_codsegiafas","");//63 
		armAtencion.put("ate_idups",atencion.getString("ate_idups"));//64
		armAtencion.put("ate_feccorteadm",atencion.getString("ate_feccorteadm"));//65
		armAtencion.put("ate_udrautorizafuavinculado",atencion.getString("ate_udrautorizafuavinculado"));//66
		armAtencion.put("ate_loteautorizafuavinculado",atencion.getString("ate_loteautorizafuavinculado"));//67
		armAtencion.put("ate_secautorizafuavinculado",atencion.getString("ate_secautorizafuavinculado"));//68
		armAtencion.put("ate_disafuavinculado",atencion.getString("ate_disafuavinculado"));//69
		armAtencion.put("ate_lotefuavinculado",atencion.getString("ate_lotefuavinculado"));//70
		armAtencion.put("ate_numfuavinculado",atencion.getString("ate_numfuavinculado"));//71
		armAtencion.put("ate_tipodocumentopersonalsalud",atencion.getString("ate_tipodocumentopersonalsalud"));//72
		armAtencion.put("ate_dnipersonalsalud",atencion.getString("ate_dnipersonalsalud"));//73
		armAtencion.put("ate_idtipopersonalsalud",atencion.getString("ate_idtipopersonalsalud"));//74 del formulario
		armAtencion.put("ate_especialidad",atencion.getString("ate_especialidad"));//75
		armAtencion.put("ate_esesgresadopersonalsalud",atencion.getString("ate_esesgresadopersonalsalud"));//76
		armAtencion.put("ate_colegiaturapersonalsalud",atencion.getString("ate_colegiaturapersonalsalud"));//77
		armAtencion.put("ate_rnepersonalsalud",atencion.getString("ate_rnepersonalsalud"));//78
		armAtencion.put("ate_periodo",periodo.get("SIS_PER_PERIODO"));//79
		armAtencion.put("ate_mes",periodo.get("SIS_PER_MES"));//80
		armAtencion.put("ate_tipodocumentodigitador",activeUser.person.personType);//81
		//System.out.println("DNI digitador: "+activeUser.person.dni);
		armAtencion.put("ate_dnidigitador",activeUser.person.dni);//82
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String date = df.format(new java.util.Date());
		armAtencion.put("ate_feccrea",date);//83
		armAtencion.put("ate_observacion",atencion.getString("ate_observacion"));//84
		armAtencion.put("ate_verreg",MedwanQuery.getInstance().getConfigString("fua.version", "asw01"));//85
		armAtencion.put("ate_idpaquete","");//86
		
		//System.out.println("tipo personbal "+atencion.getString("ate_idtipopersonalsalud"));
		//System.out.println(armAtencion);
		//System.out.println(dateFecAtencion.toString());
		
		FUA fua = new FUA(serverId,personId,dateFecAtencion,userId,armAtencion,diagnosticos,medicamentos,insumos,procedimientos,prestaciones,preventivos,nacidos);
		fua.setEdit(editar);
		if(editar){
			fua.setUid(Uid);
			fua.setEncounterId(encounterID);
			String[] encounterArray = encounterID.split("\\.");
			fua.setIdNumReg(encounterArray[1]);
		}
		fua.setActiveUser(activeUser.person.firstname+" "+activeUser.person.lastname);
		fua.setIdUser(idUser);
		result  = fua.store(estado);
		
	}
	catch(Exception e){
		e.printStackTrace();
		
		result.put("status", false);
		result.put("numreg", "");
		result.put("message", "Hubo un error al registrar"+e.toString());
		
		StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	e.printStackTrace(pw);
    	String Json = "";
    	Log log = new Log();
		
    	if(editar){
    		Json +="|encounterID:"+encounterID;
    		log.setAction("Store Fua Editar");
    	}else{
    		log.setAction("Store Fua Registrar");
    	}
    	Json +="|atencion:"+atencion.toString();
    	Json +="|arr_diagnosticos:"+diagnosticos;
    	Json +="|arr_medicamentos:"+medicamentos;
    	Json +="|arr_insumos:"+insumos;
    	Json +="|arr_procedimientos:"+procedimientos;
    	Json +="|arr_prestaciones:"+prestaciones;
    	Json +="|arr_preventivos:"+preventivos;
    	Json +="|arr_recien_nacidos:"+nacidos;
		
		log.setException(sw.toString());
		log.setMessage(e.getMessage());
		log.setJson(Json);
		log.setUserId(idUser);
		log.store();
	}
	
	

%>

<%=result.toString() %>

