<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="javax.json.Json"%>
<%@page import="pe.gob.sis.arfsisweb.model.*,pe.gob.sis.arfsisweb.*,org.json.*,java.io.BufferedReader,java.util.Date,java.text.SimpleDateFormat"%>
<%@page contentType="application/json; charset=UTF-8"  pageEncoding="UTF-8"%>

<%		
	JSONObject resp_general = new JSONObject();
	JSONArray resp_periodo = new JSONArray();
	JSONArray resp_mes = new JSONArray();
	JSONArray resp_reglas = new JSONArray();
	
   	ReglasConsistencia rc = new ReglasConsistencia();
	resp_periodo = rc.get_periodos();
	resp_mes = rc.get_mes();
	resp_reglas = rc.get_reglas();
	
	resp_general.put("periodos" , resp_periodo);
	resp_general.put("mes" , resp_mes);
	resp_general.put("reglas" , resp_reglas);
%>
<%=resp_general.toString() %>

