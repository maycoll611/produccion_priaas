<%@page contentType="application/json; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%@page import="javax.json.Json"%>
<%@page import="pe.gob.sis.arfsisweb.model.*,pe.gob.sis.arfsisweb.*,org.json.*,java.io.BufferedReader,java.util.Date,java.text.SimpleDateFormat"%>


<%		
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));
	

	DataTable datatable = new DataTable();
	datatable.setModel("bdsis_tmp.i_observacionrc_principal");
	datatable.setColumn("id,obs_nro_ejecucion,obs_regla,obs_usuario,obs_periodo,obs_mes,obs_fechaejecucion,obs_total,obs_correctos,obs_observados,obs_estado");
	
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");
	/* datatable.getDataJson(); */
	
%>
<%=datatable.getDataJson() %>

