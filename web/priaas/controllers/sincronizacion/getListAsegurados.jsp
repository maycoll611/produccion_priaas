<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@include file="/includes/validateUser.jsp" %>
<%
/*************************************
Nombre         : ListAsegurados
Proposito      : Listado de Asegurados
Creado por     : ETIPULA
Fec Creacion   : 20230120
Observaciones  : Ninguna 
*************************************/


	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable();
	datatable.setModel("bdsis_asegurados.m_afiliados");
	datatable.setColumn("afi_IdSiasis,afi_FecNac,afi_TipoTabla,afi_TipoFormato,afi_NroFormato,afi_ApePaterno,afi_ApeMaterno,afi_Nombres,afi_Dni");
	/* 
	Map<String,String> relation1 = new HashMap<String, String>();
	Map<String,String> relation2 = new HashMap<String, String>();
 */
    /* List<Map<String , String>> relations  = new ArrayList<Map<String,String>>(); */

    /* relation1.put("relation", "bdsis_maestros.m_eess");
    relation1.put("type_relation", "JOIN");
    relation1.put("key", "m_eess.pre_CodigoRENAES");
    relation1.put("foreign_key", "m_afiliados.afi_IdEESSAte"); */

/*     relation2.put("relation", "r22");
    relation2.put("type_relation", "r23");
    relation2.put("key", "r24");
    relation2.put("foreign_key", "r25"); */
    
  /*   relations.add(0,relation1);
    
	datatable.setRelations(relations); */
	
	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	/* datatable.setWhere("afi_NroFormato like '%41%'"); */
	/* datatable.getDataJson(); */
%>
<%=datatable.getDataJson() %>

