<%@page import="be.mxs.common.util.io.MessageReader,
                be.mxs.common.util.io.MessageReaderMedidoc,
                java.util.*,be.openclinic.finance.*,be.openclinic.pharmacy.*,
                javazoom.upload.MultipartFormDataRequest,
                javazoom.upload.UploadFile,org.dom4j.*,org.dom4j.io.*,be.openclinic.medical.*,
                java.io.*,be.mxs.common.util.system.*,be.mxs.common.util.db.*,
                pe.gob.sis.MysqlConnect,
                org.json.*"%>
<%@page import="pe.gob.sis.UnzipUtility" %>                
<%@page errorPage="/includes/error.jsp"%>
<%@include file="/includes/validateUser.jsp"%>

<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >
    <jsp:setProperty name="upBean" property="folderstore" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>' />
    <jsp:setProperty name="upBean" property="parser" value="<%= MultipartFormDataRequest.DEFAULTPARSER   %>"/>
    <jsp:setProperty name="upBean" property="filesizelimit" value="8589934592"/>
    <jsp:setProperty name="upBean" property="overwrite" value="true"/>
    <jsp:setProperty name="upBean" property="dump" value="true"/>
    <jsp:setProperty name="upBean" property="parsertmpdir" value='<%=MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp") %>'/>
</jsp:useBean>
<%
/*************************************
Nombre         : syncMaestros
Proposito      : Sincronizacion de maestros
Creado por     : ETIPULA
Fec Creacion   : 20230127
Observaciones  : Ninguna 
*************************************/

String message="";
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();

if (MultipartFormDataRequest.isMultipartFormData(request)) {
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    if (mrequest !=null) {
        Hashtable files = mrequest.getFiles();
        if ( (files != null) && (!files.isEmpty()) ) {
            UploadFile file = (UploadFile)files.get("filename");

            if(file!=null && file.getFileName()!=null){
                String fullFileName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")+"/"+file.getFileName();
                String tmpDirName = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp");
                new File(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp")).mkdirs();
                upBean.setFolderstore(MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"));
                upBean.setParsertmpdir(application.getRealPath("/")+"/"+MedwanQuery.getInstance().getConfigString("tempdir","/tmp/"));
                upBean.store(mrequest, "filename");
                
                String zipFilePath = fullFileName.substring(0, fullFileName.length()-4);
				String destDirectory = MedwanQuery.getInstance().getConfigString("tempDirectory","/tmp"); 
				UnzipUtility unzipper = new UnzipUtility();
				try {
				    unzipper.unpack(zipFilePath, destDirectory);
				} catch (Exception ex) {
				    ex.printStackTrace();
				}
                
                /* File txtFile = new File(zipFilePath); */
                /* System.out.println(tmpDirName); */
                File tmpDir = new File(tmpDirName);
                /* System.out.println(tmpDir.isFile()); */
                
                /* String[] fileList = tmpDir.list();
                
                if(fileList == null || fileList.length == 0)
                {
                	System.out.println("No hay elementos dentro de la carpeta actual");
                }
                else{
                	for(int i=0; i < fileList.length; i++)
                	{
                		System.out.println(fileList[i]);
                	}
                } */
                PreparedStatement ps = null;
            	ResultSet rs = null;
            	
                File[] tmpFiles = tmpDir.listFiles();
                if(tmpFiles == null || tmpFiles.length == 0)
                {
                	System.out.println("No hay elementos dentro de la carpeta actual");
                }
                else{
                	
                	/* File fileQuery = new File(tmpDirName+"\\A_QUERY_ARFSIS.sql");
                	InputStream inputStream = null;
                	inputStream = new FileInputStream(fileQuery); */
                	
                	
                	/* File fileQuerySql = new File(tmpDirName+"\\fileName.sql"); // destination dir of your file
                	boolean success = fileQuery.renameTo(fileQuerySql);
                	if (success) {
                	    // File has been renamed
                	} */
                	
                	/* System.out.println(fileQuery); */
                	String log ="";
                	MysqlConnect con = new MysqlConnect("bdsis_maestros");
                	
                	
                    
                	/* PreparedStatement sqlTxt = con.connect().prepareStatement("source "+tmpDirName+"/A_QUERY_ARFSIS.txt");
                	sqlTxt.executeQuery(); */
                	
                	PreparedStatement fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=0");
                	fkChecks.executeQuery();
                	
                	PreparedStatement sql = con.connect().prepareStatement("SELECT * FROM openclinic_dbo.sis_sincronizacion WHERE tipo='2' AND nombre = '"+file.getFileName()+"' LIMIT 1");
                	ResultSet rsi = sql.executeQuery();
                	
                	int size = 0;
                	String fecha = "";
                    while (rsi.next()) {
                        size++;
                        fecha += rsi.getString("fecha");
                    }
                    if(size == 0)
                    {
                    	//actualizacion de base de datos mediante queries
                    	String s = new String();
                   	    StringBuffer sb = new StringBuffer();
                   	    if(new File(tmpDirName+"/A_QUERY_ARFSIS.sql").exists())
                   	    {
                   	    	FileReader fr = new FileReader(new File(tmpDirName+"/A_QUERY_ARFSIS.sql"));
                       	 	BufferedReader br = new BufferedReader(fr);
                       	 	while((s = br.readLine()) != null){
                       	     sb.append(s);
                       	   	}
                       	   	br.close();
                       	 	
                       	    String[] inst = sb.toString().split(";");
                       	 
                            for(int i = 0; i<inst.length; i++) {
                            
        	                    if(!inst[i].trim().equals("")){
        	                    	try{
        	                    		PreparedStatement sqlTxt = con.connect().prepareStatement(inst[i]+";");
        		                    	int resultTxtQuery = sqlTxt.executeUpdate();
        		                    	log+=inst[i]+": result("+resultTxtQuery+")\n";
        	                    	}
        	                    	catch(Exception e)
        	                    	{
        	                    		String messageTxt = e.getMessage().toString();
        	                    	
        	                    		log+=inst[i]+": "+messageTxt+"\n";
        	                    	}
        	                    }
                            }
                   	    }
                   	 	
                        // fin actualizacion de base de datos mediante queries
                    
	                	PreparedStatement updateRow = con.connect().prepareStatement("UPDATE openclinic_dbo.sis_sincronizacion SET estado='1'");
	            		updateRow.execute();
	            		updateRow.close();
	            		
	                    PreparedStatement insertSync = con.connect().prepareStatement("INSERT INTO openclinic_dbo.sis_sincronizacion "
	            				+ "(nombre,tipo,resultado,log,estado) "
	            				+ "VALUES (?,?,?,?,?)");
	                    insertSync.setString(1, file.getFileName());
	                    insertSync.setString(2, "2");
	                    
	                    int totalArchivos = tmpFiles.length, totalExito=0, totalError=0;
	                    /* String log =""; */
	                    
	                	/* for(int i=0; i < tmpFiles.length; i++)
	                	{ */
	                		/* File tmpFile = tmpFiles[i]; */
	                		
	                		/* PreparedStatement sqlc = con.connect().prepareStatement("SELECT tb_Descripcion FROM bdsis_asegurados.m_tablas WHERE tb_archiRetro='"+tmpFile.getName()+"' LIMIT 1"); */
	                		PreparedStatement sqlc = con.connect().prepareStatement("SELECT tb_Descripcion,tb_archiRetro FROM openclinic_dbo.sis_tablas");
	                		
	                    	ResultSet rsc = sqlc.executeQuery();
	                    	while(rsc.next())
	                    	{
	                    		String tmpTableName= rsc.getString("tb_Descripcion");
	                    		String tmpTableFile= rsc.getString("tb_archiRetro");
	                    		
	                    		File txtFile = new File(tmpDirName+"/"+tmpTableFile);
	                    		
	                    		if (txtFile.exists()) {
		                    		/* System.out.println(tmpTableName); */
		                		/* if(tmpFile.getName().contains("_m.txt"))
		                		{ 
		                			String[] name = tmpFile.getName().split("_m.txt");
		                			String nameTable = "m_"+name[0];*/
		                			try
		                            {
		                                /* Class.forName("com.mysql.jdbc.Driver");
		                                Connection con=DriverManager.getConnection(
		                                        "jdbc:mysql://localhost:13306/bdsis_maestros","openclinic","0pen");
		                                Statement con_bdsis=con.createStatement(); */
		                                
		                                
		                                String query = "LOAD DATA INFILE '"+tmpDirName+"/"+tmpTableFile+"' REPLACE INTO TABLE `bdsis_maestros`.`"+tmpTableName+"` CHARACTER SET latin1 FIELDS TERMINATED BY '|' LINES TERMINATED BY '\\r\\n';";
		                                ps = con.connect().prepareStatement(query);
		                    	    	rs = ps.executeQuery();
		                                /* ResultSet rs = con_bdsis.executeQuery(query); */
		                                //esultSet rs = con_bdsis.executeQuery()
		                            	message = rs.toString();
		                            	/* System.out.println(nameTable+":"+message); */
		                            	totalExito++;
		                            	result.put("status",true);
		                            }
		                            catch(Exception e)
		                            {
		                                message = e.getMessage().toString();
		                                /* System.out.println(nameTable+":"+message); */
		                            	totalError++;   
		                            	result.put("status",true);
		                            } 
		                			result.put("message","total archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
		                			log+=tmpTableFile+": "+message+"\n";
		                			result.put("log",log);
		                		}
	                			txtFile.delete();
	                		}
	                	/* } */
	                    	
	                	insertSync.setString(3, "total archivos: "+totalArchivos+" total exito: "+totalExito+" total error: "+totalError);
	                	insertSync.setString(4, log);
	                	insertSync.setString(5, "0");
	                	insertSync.execute();
	                	insertSync.close();
	                	sql.close();
                    }
                    else{
                    	/* System.out.println("este archivo ya se ejecuto el: "+fecha); */
                    	result.put("status",false);
                    	result.put("message","este archivo ya se ejecuto el: "+fecha);
                    }
                    
                    fkChecks.close();
                    fkChecks = con.connect().prepareStatement("SET FOREIGN_KEY_CHECKS=1");
                	fkChecks.executeQuery();
                }
                	
                File zipFile = new File(fullFileName);
                if(rs!=null)rs.close();
	            if(ps!=null)ps.close();             
                /* FileInputStream fis = new FileInputStream(txtFile); */
               /*  try
                {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection con=DriverManager.getConnection(
                            "jdbc:mysql://localhost:13306/bdsis_asegurados","openclinic","0pen");
                    Statement con_bdsis=con.createStatement();  
                    //ResultSet rs=con_bdsis.executeQuery("show databases;");
                    System.out.println("conectado");
                    System.out.println(txtFile.getName());  
                    //String query = "load data infile '\\\\temp\\\\"+txtFile.getName()+"' into table m_afiliados CHARACTER SET latin1 fields terminated by ',' (afi_IdSiasis,afi_TipoTabla) lines terminated by '\\r\\n'";
                    //String query = "LOAD DATA INFILE '\\\\temp\\\\"+txtFile.getName()+"' REPLACE INTO TABLE `bdsis_asegurados`.`m_afiliados` CHARACTER SET utf8 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\r\\n' (`afi_IdSiasis`,`afi_TipoTabla`,`afi_IdDisa`,`afi_TipoFormato`,`afi_NroFormato`,`afi_CorrelativoIns`,`afi_IdTipoDocumento`,`afi_IdEESSAte`,`afi_FecFormato`,`afi_ApePaterno`,`afi_ApeMaterno`,`afi_Nombres`,`afi_SegNombre`,`afi_IdSexo`,`afi_FecNac`,`afi_IdDistrito`,`afi_IdEstado`,`afi_FecBaja`,`afi_Dni`,`afi_Plan`,`afi_idGrupoPoblacional`,`AFI_DIRECCION`,`afi_FecFinCobertura`,`afi_FecFallecimiento`)";
                    String query = "LOAD DATA INFILE '\\\\temp\\\\"+txtFile.getName()+"' REPLACE INTO TABLE `bdsis_asegurados`.`m_afiliados` CHARACTER SET latin1 FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\r\\n' (`afi_IdSiasis`,`afi_TipoTabla`,`afi_IdDisa`,`afi_TipoFormato`,@afi_NroFormato,@afi_CorrelativoIns,@afi_IdTipoDocumento,@afi_IdEESSAte,@afi_FecFormato,@afi_ApePaterno,@afi_ApeMaterno,@afi_Nombres,@afi_SegNombre,@afi_IdSexo,@afi_FecNac,@afi_IdDistrito,@afi_IdEstado,@afi_FecBaja,@afi_Dni,@afi_Plan,@afi_idGrupoPoblacional,@AFI_DIRECCION,@afi_FecFinCobertura,@afi_FecFallecimiento) SET afi_NroFormato=NULLIF(@afi_NroFormato,''),afi_CorrelativoIns=NULLIF(@afi_CorrelativoIns,''),afi_IdTipoDocumento=NULLIF(@afi_IdTipoDocumento,''),afi_IdEESSAte=NULLIF(@afi_IdEESSAte,''),afi_FecFormato=NULLIF(@afi_FecFormato,''),afi_ApePaterno=NULLIF(@afi_ApePaterno,''),afi_ApeMaterno=NULLIF(@afi_ApeMaterno,''),afi_Nombres=NULLIF(@afi_Nombres,''),afi_SegNombre=NULLIF(@afi_SegNombre,''),afi_IdSexo=NULLIF(@afi_IdSexo,''),afi_FecNac=NULLIF(@afi_FecNac,''),afi_IdDistrito=NULLIF(@afi_IdDistrito,''),afi_IdEstado=NULLIF(@afi_IdEstado,''),afi_FecBaja=NULLIF(@afi_FecBaja,''),afi_Dni=NULLIF(@afi_Dni,''),afi_Plan=NULLIF(@afi_Plan,''),afi_idGrupoPoblacional=NULLIF(@afi_idGrupoPoblacional,''),AFI_DIRECCION=NULLIF(@AFI_DIRECCION,''),afi_FecFinCobertura=NULLIF(@afi_FecFinCobertura,''),afi_FecFallecimiento=NULLIF(@afi_FecFallecimiento,'')";                        
                    ResultSet rs = con_bdsis.executeQuery(query);
                    
                    System.out.println(rs);
                    //esultSet rs = con_bdsis.executeQuery()
                	message = rs.toString();
                }
                catch(Exception e)
                {
                    System.out.println(e);
                    message = e.getMessage();
                } */
            /* fis.close(); */
            /* txtFile.delete(); */
            zipFile.delete();
            }
           
        }
       
    }
}
%>
<%=result.toString()%>