<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,be.openclinic.pharmacy.Product,pe.gob.sis.arfsisweb.TransactionFUA"%>
<%@include file="/includes/validateUser.jsp" %>

<%		
/*************************************
Nombre         : getListMaestros
Proposito      : Listado de Maestros
Creado por     : ETIPULA
Fec Creacion   : 20230122
Observaciones  : Ninguna 
*************************************/
	int pag = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));
	String sortField = request.getParameter("sortField");
	String filters = request.getParameter("filters");
	int sortOrder = Integer.parseInt(request.getParameter("sortOrder"));

	DataTable datatable = new DataTable();
	datatable.setModel("bdsis_maestros");
	datatable.setColumn("table_name,table_type,table_rows,create_time,update_time");

	datatable.setPage(pag);
	datatable.setSize(rows);
	datatable.setSortField(sortField);
	datatable.setSortOrder(sortOrder);
	datatable.setFilters(filters);
	datatable.setWhere("");
	/* datatable.getDataBdTableJson(); */
	
	
%>
<%=datatable.getDataBdTableJson() %>

