<%@page import="pe.gob.sis.arfsisweb.reports.*,
                pe.gob.sis.MysqlConnect,
                pe.gob.sis.arfsisweb.reports.jasperreports.*,
                org.json.*,
                net.sf.jasperreports.engine.*,
                net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,
                net.sf.jasperreports.engine.export.JRXlsExporter,
                be.mxs.common.util.db.MedwanQuery,
                java.io.*,
                java.time.*,
                java.time.format.DateTimeFormatter,
                java.util.*,
                java.sql.*"%>

<%	
    String procesoOFormato = request.getParameter("procesoOFormato");
    String opcion = request.getParameter("opcion");
    String componente = request.getParameter("componente");
    String periodo = request.getParameter("periodo");
    String year = request.getParameter("year");
    String desde = request.getParameter("desde");
    String hasta = request.getParameter("hasta");
    String ppdd = request.getParameter("ppdd");
    String eess = request.getParameter("eess");
    String usuario = request.getParameter("usuario");
    String respAten = request.getParameter("respAten");
    String disa = request.getParameter("disa");
    String odsis = request.getParameter("odsis");
            
    String tipoArchivo = request.getParameter("tipoArchivo");
    
    MysqlConnect con = new MysqlConnect("openclinic_dbo");
            
    String sql = "SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS fua,"
                        + " SIS_ATENCION_5 AS udr,"
                        + " SIS_ATENCION_6 AS codRenaes,"
                        + " pre_Nombre AS eess,"
                        + " SIS_ATENCION_7 AS catIpress,"
                        + " SIS_ATENCION_8 AS nivelIpress,"
                        + " pin_Nombre AS puntoDigit,"
                        + " SIS_ATENCION_10 AS reconcideracion,"
                        + " CONCAT(SIS_ATENCION_11,'-',SIS_ATENCION_12,'-',SIS_ATENCION_13) AS fuaReconcideracion,"
                        + " SIS_ATENCION_14 AS idConvenio,"
                        + " com_Descripcion AS componente,"
                        + " CONCAT(SIS_ATENCION_16,'-',SIS_ATENCION_17,'-',SIS_ATENCION_18) AS formAsegurado,"
                        + " SIS_ATENCION_25 AS numDNI,"
                        + " CONCAT(SIS_ATENCION_26,' ',SIS_ATENCION_27,' ',SIS_ATENCION_28,' ',SIS_ATENCION_29) AS beneficiario,"
                        + " SIS_ATENCION_30 AS fecNac,"
                        + " IF(SIS_ATENCION_31 = '0', 'F','M') AS sexoBenef,"
                        + " SIS_ATENCION_32 AS codDistrito,"
                        + " SIS_ATENCION_33 AS histClinica,"
                        + " tat_Descripcion AS tipoAtencion,"
                        + " SIS_ATENCION_35 AS condMaterna,"
                        + " SIS_ATENCION_36 AS modAtencion,"
                        + " SIS_ATENCION_39 AS fecha,"
                        + " CONCAT(pers_PriNombre,' ',pers_OtrNombre,' ',pers_ApePaterno,' ',pers_ApeMaterno) AS personalSalud,"+
                        " SIS_ATENCION_73 AS respDni,"+
                        " ser_Descripcion AS servicio,"
                        + " tps_Descripcion AS tipoProf,"+
                        " SIS_ATENCION_83 AS fecRegistro,"+
                        " iseg_Tarifario AS tarifa,"+
                        " SIS_SMI_3 AS nroCred,"+
                        " SIS_ATENCION_87 AS valorizacion"+
                        " FROM sis_atencion"+
                        " LEFT JOIN bdsis_maestros.a_componentes ON openclinic_dbo.sis_atencion.SIS_ATENCION_15 = bdsis_maestros.a_componentes.com_IdComponente"+
                        " LEFT JOIN bdsis_maestros.a_tipoatencion ON openclinic_dbo.sis_atencion.SIS_ATENCION_34 = bdsis_maestros.a_tipoatencion.tat_IdTipoAtencion"+
                        " LEFT JOIN bdsis_maestros.m_pinstalacion ON openclinic_dbo.sis_atencion.SIS_ATENCION_9 = bdsis_maestros.m_pinstalacion.pin_IdPPDD"+
                        " LEFT JOIN bdsis_maestros.m_servicios ON openclinic_dbo.sis_atencion.SIS_ATENCION_42 = bdsis_maestros.m_servicios.ser_IdServicio"+
                        " LEFT JOIN bdsis_maestros.m_eess ON openclinic_dbo.sis_atencion.SIS_ATENCION_6 = SUBSTRING(bdsis_maestros.m_eess.pre_CodigoRENAES,3,8)"+
                        " LEFT JOIN bdsis_maestros.a_tipopersonalsalud ON openclinic_dbo.sis_atencion.SIS_ATENCION_74 = bdsis_maestros.a_tipopersonalsalud.tps_IdTipoPersonalSalud"+
                        " LEFT JOIN bdsis_maestros.a_resatencion ON openclinic_dbo.sis_atencion.SIS_ATENCION_73 = bdsis_maestros.a_resatencion.pers_IdResAtencion"+
                        " LEFT JOIN bdsis_maestros.a_institucion ON openclinic_dbo.sis_atencion.SIS_ATENCION_5 = bdsis_maestros.a_institucion.iseg_idOdsis"+
                        " LEFT JOIN openclinic_dbo.sis_smi ON (openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_smi.SIS_SMI_1) AND SIS_SMI_2 = '120'";
        
    if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
        if(procesoOFormato.equals("1")) {
            if(opcion.equals("1")){
                sql = sql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";  
            } else if(opcion.equals("2")){
                sql = sql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
            } else if(opcion.equals("3")){
                sql = sql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
            }
        } else if(procesoOFormato.equals("2")) {
            if(opcion.equals("1")){
                sql = sql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  
            } else if(opcion.equals("2")){
                sql = sql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
            } else if(opcion.equals("3")){
                sql = sql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
            }
        }
        if(!componente.equals("3") && !componente.equals("")) {
            sql = sql +" AND SIS_ATENCION_15 = '"+componente+"'";
        }
        if(!ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("")) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("")) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }
    if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
        sql = sql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
        if(!ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("")) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("")) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }
    if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
        if(!ppdd.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("") && ppdd.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
        if(!eess.equals("") && !ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }
            
    PreparedStatement ps = con.connect().prepareStatement(sql);
    ResultSet rs = ps.executeQuery();
            
    List<RegistroAtencion> listItems = new ArrayList<RegistroAtencion>();
    
    Integer totalReg = 0;
            
    while(rs.next()){
        
        RegistroAtencion ra = new RegistroAtencion();
        ra.setFua(rs.getString("fua"));
        ra.setFecha(rs.getString("fecha"));
        ra.setEess(rs.getString("eess"));
        ra.setPuntoDigit(rs.getString("puntoDigit"));
        ra.setComponente(rs.getString("componente"));
        ra.setTipoAtencion(rs.getString("tipoAtencion"));
        ra.setFormAsegurado(rs.getString("formAsegurado"));
        ra.setNumDNI(rs.getString("numDNI"));
        ra.setBeneficiario(rs.getString("beneficiario"));
        ra.setFecNac(rs.getString("fecNac"));
        ra.setSexoBenef(rs.getString("sexoBenef"));
        ra.setHistClinica(rs.getString("histClinica"));
        ra.setServicio(rs.getString("servicio"));
        ra.setRespDni(rs.getString("respDni"));
        ra.setPersonalSalud(rs.getString("personalSalud"));
        ra.setTipoProf(rs.getString("tipoProf"));
        ra.setFecRegistro(rs.getString("fecRegistro"));
        ra.setTarifa(rs.getString("tarifa"));
        if(rs.getString("nroCred") == null){
            ra.setNroCred(" ");
        } else {
            ra.setNroCred(rs.getString("nroCred"));
        }
        ra.setValorizacion(rs.getString("valorizacion"));
        
        totalReg +=1;
            
        listItems.add(ra);
    }
    
    rs.close();
    ps.close();
            
    String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
        " FROM ocadmin_dbo.admin"+
        " WHERE natreg ="+usuario;
    JSONArray arrayUser = new JSONArray();        
    if(!usuario.equals("")) {
        PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
        ResultSet rsUser = psUser.executeQuery();
        while(rsUser.next()){
            JSONObject result = new JSONObject();
            result.put("usuario",rsUser.getString(1));
            arrayUser.put(result);
        }
        rsUser.close();
        psUser.close();
    }
    con.disconnect();
            
    JRBeanCollectionDataSource dsrda = new JRBeanCollectionDataSource(listItems);
    
    String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");
            
    InputStream logoReporte = new FileInputStream(rutaproy + "_img/arfsis/logoReporte.jpg");
            
    JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/DetalleAtencion.jrxml");
    
    Map<String, Object> parameters = new HashMap<String, Object>();
    
    if(!disa.equals("")) {
        parameters.put("disa", disa);
    } else {
        parameters.put("disa", " ");
    }
    if(!odsis.equals("")) {
        parameters.put("odsis", odsis);
    } else {
        parameters.put("odsis", " ");
    }
    if(!usuario.equals("")) {
        parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
    } else {
        parameters.put("usuario", " ");
    }
    parameters.put("subtitle", " ");
    parameters.put("totalReg", totalReg.toString());
    parameters.put("dsrda", dsrda);
    
    if(tipoArchivo.equals("pdf")){
        parameters.put("logoReporte", logoReporte);        
                
        
        byte[] bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource());
        
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition",  "inline; filename='JRDetalleAtencion.pdf'");
        ServletOutputStream output = response .getOutputStream();
        response.getOutputStream();
        output.write(bytes,0,bytes.length);
        output.flush();
        output.close();
    }
    else if(tipoArchivo.equals("excel")){
        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );
                
        byte[] bytes = new byte[10000];
        
        JRXlsExporter exporter = new JRXlsExporter();
        
        ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
        
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
        exporter.exportReport();
        
        bytes = xlsReport.toByteArray();
        
        response.setContentType("application/vnd.ms-excel");
        response.setContentLength(bytes.length);
        xlsReport.close();
        
        OutputStream output = response.getOutputStream();
        output.write(bytes,0,bytes.length);
        output.flush();
        output.close();
    }
%>            