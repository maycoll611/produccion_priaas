<%
/*************************************
Nombre        : jrRepConsumoExcel
Proposito     : Generaci�n de reporte de Regitro Consumo en Excel
Creado por    : FLIZARRAGA
Fec Creacion  : 20230317
Observaciones : Ninguna 
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*,net.sf.jasperreports.engine.JRExporterParameter,java.io.ByteArrayOutputStream,net.sf.jasperreports.engine.export.JRXlsExporter,net.sf.jasperreports.engine.JasperReport,net.sf.jasperreports.engine.JasperRunManager,be.mxs.common.util.db.MedwanQuery,java.io.OutputStream,net.sf.jasperreports.engine.JasperExportManager,net.sf.jasperreports.engine.JREmptyDataSource,net.sf.jasperreports.engine.JasperPrint,net.sf.jasperreports.engine.JasperFillManager,net.sf.jasperreports.engine.JasperCompileManager,java.time.Period,java.time.format.DateTimeFormatter,java.time.LocalDate,pe.gob.sis.MysqlConnect,net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,java.io.InputStream,java.io.FileInputStream,java.util.HashMap,java.util.List,java.util.Map,java.util.ArrayList,java.sql.ResultSet,java.sql.PreparedStatement,pe.gob.sis.arfsisweb.reports.jasperreports.RepConsolidado"%>

<%	
String procesoOFormato = request.getParameter("procesoOFormato");
String opcion = request.getParameter("opcion");
String componente = request.getParameter("componente");
String periodo = request.getParameter("periodo");
String year = request.getParameter("year");
String desde = request.getParameter("desde");
String hasta = request.getParameter("hasta");
String ppdd = request.getParameter("ppdd");
String eess = request.getParameter("eess");
String usuario = request.getParameter("usuario");
String respAten = request.getParameter("respAten");
String disa = request.getParameter("disa");
String odsis = request.getParameter("odsis");
 
MysqlConnect con = new MysqlConnect("openclinic_dbo");

String sql = "SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS fua,"
                + " CONCAT(SIS_ATENCION_26,' ',SIS_ATENCION_27,' ',SIS_ATENCION_28,' ',SIS_ATENCION_29) AS beneficiario,"
                + " CONCAT(SIS_ATENCION_16,'-',SIS_ATENCION_17,'-',SIS_ATENCION_18) AS contrato,"
                + " SIS_DIAGNOSTICOS_3 AS nroDx,"
                + " C10_CodDia AS cie10,"
                + " C10_descripcion AS diagnostico,"
                + " PRCD_V_CODPROCEDIMIENTO AS cpt,"
                + " PRCD_V_DESPROCEDIMIENTO AS descProcedimiento,"
                + " SIS_PROCEDIMIENTOS_5 AS procIndicada,"
                + " SIS_PROCEDIMIENTOS_6 AS procEntregada,"
                + " med_CodMed AS codMed,"
                + " med_Nombre AS descMedicamento,"
                + " SIS_MEDICAMENTOS_4 AS medPrescrita,"
                + " SIS_MEDICAMENTOS_5 AS medEntregada,"
                + " ins_CodIns AS codInsumo,"
                + " ins_Nombre AS descInsumo,"
                + " SIS_INSUMOS_4 AS insPrescrito,"
                + " SIS_INSUMOS_5 AS insEntregado"
                + " FROM sis_atencion"
                + " LEFT JOIN openclinic_dbo.sis_diagnosticos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_diagnosticos.SIS_DIAGNOSTICOS_1"
                + " LEFT JOIN openclinic_dbo.sis_procedimientos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_procedimientos.SIS_PROCEDIMIENTOS_1"
                + " LEFT JOIN openclinic_dbo.sis_medicamentos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_medicamentos.SIS_MEDICAMENTOS_1"
                + " LEFT JOIN openclinic_dbo.sis_insumos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_insumos.SIS_INSUMOS_1"
                + " LEFT JOIN bdsis_maestros.m_cie10 ON openclinic_dbo.sis_diagnosticos.SIS_DIAGNOSTICOS_2 = bdsis_maestros.m_cie10.C10_CodDia"
                + " LEFT JOIN bdsis_maestros.m_procedimientos ON openclinic_dbo.sis_procedimientos.SIS_PROCEDIMIENTOS_2 = bdsis_maestros.m_procedimientos.PRCD_V_CODPROCEDIMIENTO"
                + " LEFT JOIN bdsis_maestros.m_medicamentos ON openclinic_dbo.sis_medicamentos.SIS_MEDICAMENTOS_2 = bdsis_maestros.m_medicamentos.med_CodMed"
                + " LEFT JOIN bdsis_maestros.m_insumos ON openclinic_dbo.sis_insumos.SIS_INSUMOS_2 = bdsis_maestros.m_insumos.ins_CodIns";

    if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
        if(procesoOFormato.equals("1")) {
            if(opcion.equals("1")){
                sql = sql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";  
            } else if(opcion.equals("2")){
                sql = sql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
            } else if(opcion.equals("3")){
                sql = sql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
            }
        } else if(procesoOFormato.equals("2")) {
            if(opcion.equals("1")){
                sql = sql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  
            } else if(opcion.equals("2")){
                sql = sql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
            } else if(opcion.equals("3")){
                sql = sql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
            }
        }
        if(!componente.equals("3") && !componente.equals("")) {
            sql = sql +" AND SIS_ATENCION_15 = '"+componente+"'";
        }
        if(!ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("")) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("")) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }
    if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
        sql = sql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
        if(!ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("")) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("")) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }
    if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
        if(!ppdd.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        }
        if(!eess.equals("") && ppdd.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
            sql = sql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
        if(!eess.equals("") && !ppdd.equals("")) {
            sql = sql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        }
        if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
            sql = sql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        }
        if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
            sql = sql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        }
    }

    PreparedStatement ps = con.connect().prepareStatement(sql);
    ResultSet rs = ps.executeQuery();

    List<RepConsolidado> listItems = new ArrayList<RepConsolidado>();
    
    Integer totalReg = 0;

    while(rs.next()){
                        
        RepConsolidado ra = new RepConsolidado();
        ra.setFua(rs.getString("fua"));
        ra.setBeneficiario(rs.getString("beneficiario"));
        ra.setContrato(rs.getString("contrato"));
        ra.setNroDx(rs.getString("nroDx"));
        ra.setCie10(rs.getString("cie10"));
        ra.setDiagnostico(rs.getString("diagnostico"));
        if(rs.getString("cpt") == null){
            ra.setCpt(" ");
        } else {
            ra.setCpt(rs.getString("cpt"));
        }
        if(rs.getString("descProcedimiento") == null){
            ra.setDescProcedimiento(" ");
        } else {
            ra.setDescProcedimiento(rs.getString("descProcedimiento"));
        }
        if(rs.getString("procIndicada") == null){
            ra.setProcIndicada(" ");
        } else {
            ra.setProcIndicada(rs.getString("procIndicada"));
        }
        if(rs.getString("procEntregada") == null){
            ra.setProcEntregada(" ");
        } else {
            ra.setProcEntregada(rs.getString("procEntregada"));
        }
        if(rs.getString("codMed") == null){
            ra.setCodMed(" ");
        } else {
            ra.setCodMed(rs.getString("codMed"));
        }
        if(rs.getString("descMedicamento") == null){
            ra.setDescMedicamento(" ");
        } else {
            ra.setDescMedicamento(rs.getString("descMedicamento"));
        }
        if(rs.getString("medPrescrita") == null){
            ra.setMedPrescrita(" ");
        } else {
            ra.setMedPrescrita(rs.getString("medPrescrita"));
        }
        if(rs.getString("medEntregada") == null){
            ra.setMedEntregada(" ");
        } else {
            ra.setMedEntregada(rs.getString("medEntregada"));
        }
        if(rs.getString("codInsumo") == null){
            ra.setCodInsumo(" ");
        } else {
            ra.setCodInsumo(rs.getString("codInsumo"));
        }
        if(rs.getString("descInsumo") == null){
            ra.setDescInsumo(" ");
        } else {
            ra.setDescInsumo(rs.getString("descInsumo"));
        }
        if(rs.getString("insPrescrito") == null){
            ra.setInsPrescrito(" ");
        } else {
            ra.setInsPrescrito(rs.getString("insPrescrito"));
        }
        if(rs.getString("insEntregado") == null){
            ra.setInsEntregado(" ");
        } else {
            ra.setInsEntregado(rs.getString("insEntregado"));
        }
        
        totalReg +=1;

        listItems.add(ra);
    }
    
    rs.close();
    ps.close();

    String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
        " FROM ocadmin_dbo.admin"+
        " WHERE natreg ="+usuario;
    JSONArray arrayUser = new JSONArray();        
    if(!usuario.equals("")) {
        PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
        ResultSet rsUser = psUser.executeQuery();
        while(rsUser.next()){
            JSONObject result = new JSONObject();
            result.put("usuario",rsUser.getString(1));
            arrayUser.put(result);
        }
        rsUser.close();
        psUser.close();
    }
    con.disconnect();

    JRBeanCollectionDataSource dsrc = new JRBeanCollectionDataSource(listItems);
    
    String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");
            
    Map<String, Object> parameters = new HashMap<String, Object>();
    
    if(!disa.equals("")) {
        parameters.put("disa", disa);
    } else {
        parameters.put("disa", " ");
    }
    if(!odsis.equals("")) {
        parameters.put("odsis", odsis);
    } else {
        parameters.put("odsis", " ");
    }
    if(!usuario.equals("")) {
        parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
    } else {
        parameters.put("usuario", " ");
    }
    parameters.put("subtitle", " ");
    parameters.put("totalReg", totalReg.toString());
    parameters.put("dsrc", dsrc);   

    JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/RepConsumo.jrxml");
		
        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

        byte[] bytes = new byte[10000];
		
		JRXlsExporter exporter = new JRXlsExporter();
		
		ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
		exporter.exportReport();
		
		bytes = xlsReport.toByteArray();
		
		response.setContentType("application/vnd.ms-excel");
		response.setContentLength(bytes.length);
		xlsReport.close();
		
		OutputStream output = response.getOutputStream();
		output.write(bytes,0,bytes.length);
		output.flush();
		output.close();
%>
