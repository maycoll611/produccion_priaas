<%
/*************************************
Nombre        : jrServAdicionales
Proposito     : Generación de reporte de Servicios Adicionales en PDF
Creado por    : FLIZARRAGA
Fec Creacion  : 20230207
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230207  FLIZARRAGA     Se creo la función para generar el reporte de Servicios Adicionales en PDF
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,
                pe.gob.sis.MysqlConnect,
                pe.gob.sis.arfsisweb.reports.jasperreports.*,
                org.json.*,
                net.sf.jasperreports.engine.*,
                net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,
                net.sf.jasperreports.engine.export.JRXlsExporter,
                be.mxs.common.util.db.MedwanQuery,
                java.io.*,
                java.time.*,
                java.time.format.DateTimeFormatter,
                java.util.*,
                java.sql.*"%>

<%	
	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");
	String disa = request.getParameter("disa");
	String odsis = request.getParameter("odsis");

    String tipoArchivo = request.getParameter("tipoArchivo");

    MysqlConnect con = new MysqlConnect("openclinic_dbo");
	String columns = "fua,fecha,servPrincipal,servAdicional,gestante,edad";
	String[] split = columns.split(",");

	JSONArray array = new JSONArray();
	JSONArray arrayServAdicional = new JSONArray();

	String sql = "SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS fua,"+
				 " SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,10) AS fecha,"+
				 " SIS_ATENCION_42 AS servPrincipal,"+
				 " SIS_SERVICIOSADICIONALES_2 AS servAdicional,"+
				 " SIS_ATENCION_35 AS gestante,"+
				 " SIS_ATENCION_30 AS fec_nac"+
				 " FROM sis_serviciosadicionales"+
				 " JOIN openclinic_dbo.sis_atencion ON openclinic_dbo.sis_serviciosadicionales.SIS_SERVICIOSADICIONALES_1 = openclinic_dbo.sis_atencion.SIS_ATENCION_1";
	String sql2 = "";
	String filtrosSql = "";

	if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
		if(procesoOFormato.equals("1")) {
			if(opcion.equals("1")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";                     
			} else if(opcion.equals("2")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
			} else if(opcion.equals("3")){
				filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
			}
		} else if(procesoOFormato.equals("2")) {
			if(opcion.equals("1")){
				filtrosSql = filtrosSql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  

			} else if(opcion.equals("2")){
				filtrosSql = filtrosSql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
			} else if(opcion.equals("3")){
				filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
			}
		}
		if(!componente.equals("3") && !componente.equals("")) {
			filtrosSql = filtrosSql +" AND SIS_ATENCION_15 = '"+componente+"'";
		}
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}
	if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
		filtrosSql = filtrosSql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}
	if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
		if(!ppdd.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
		}
		if(!eess.equals("") && ppdd.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
			filtrosSql = filtrosSql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
		if(!eess.equals("") && !ppdd.equals("")) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
		}
		if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
		}
		if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
			filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
		}
	}

	sql = sql + filtrosSql;

	sql2 = sql + " GROUP BY fua, fecha, servPrincipal ORDER BY fua, fecha, servPrincipal";
	sql = sql + " ORDER BY fua, fecha, servPrincipal, servAdicional";	

	PreparedStatement ps = con.connect().prepareStatement(sql);
	ResultSet rs = ps.executeQuery();

	PreparedStatement ps2 = con.connect().prepareStatement(sql2);
	ResultSet rs2 = ps2.executeQuery();

	while(rs.next()){        	
		JSONObject result = new JSONObject();
		for(int i=1;i<=6;i+=1)
		{
			result.put(split[i-1], rs.getString(i));
		} 
		array.put(result);
	}

	while(rs2.next()){
		JSONObject result = new JSONObject();
		for(int i=1;i<=6;i+=1)
		{
			if(i==6) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				LocalDate fecha_nacimiento = LocalDate.parse(rs2.getString(i),formatter);
				Period edad = Period.between(fecha_nacimiento,LocalDate.now());
				result.put(split[i-1],edad.getYears());
			} else {
				result.put(split[i-1], rs2.getString(i));
			}
		}  
		arrayServAdicional.put(result);
	}

	rs2.close();
	ps2.close();

	rs.close();
	ps.close();

	String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
		" FROM ocadmin_dbo.admin"+
		" WHERE natreg ="+usuario;
	JSONArray arrayUser = new JSONArray();        
	if(!usuario.equals("")) {
		PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
		ResultSet rsUser = psUser.executeQuery();
		while(rsUser.next()){
			JSONObject result = new JSONObject();
			result.put("usuario",rsUser.getString(1));
			arrayUser.put(result);
		}
		rsUser.close();
		psUser.close();
	}
	con.disconnect();
	
	List<ServAdicionalModel> listItems = new ArrayList<ServAdicionalModel>();

	Integer totalReg = 0;

	if(array.length() > 0) {
		for(int j=0;j<arrayServAdicional.length();j++){

			ServAdicionalModel servAdic = new ServAdicionalModel();

			servAdic.setFua(arrayServAdicional.getJSONObject(j).getString("fua"));
			servAdic.setFecAten(arrayServAdicional.getJSONObject(j).getString("fecha"));
			servAdic.setSerPrinc(arrayServAdicional.getJSONObject(j).getString("servPrincipal"));
			
			String servsAdic = "";
			int total = 0;
			
			for(int s=0;s<array.length();s++) {
				if(arrayServAdicional.getJSONObject(j).getString("fua").equals(array.getJSONObject(s).getString("fua")) && arrayServAdicional.getJSONObject(j).getString("fecha").equals(array.getJSONObject(s).getString("fecha")) && arrayServAdicional.getJSONObject(j).getString("servPrincipal").equals(array.getJSONObject(s).getString("servPrincipal"))) {
					if(servsAdic.equals("")) {
						servsAdic = servsAdic + array.getJSONObject(s).getString("servAdicional");
					} else {
						servsAdic = servsAdic + ","+ array.getJSONObject(s).getString("servAdicional");
					}
					total+=1;
				}	            	
			}	            
			servAdic.setServAdicional(servsAdic);
			String ge1 = "";
			String ge2 = "";
			String ge3 = "";
			String ge4 = "";
			String ge5 = "";
			String gestante = "";
			if(arrayServAdicional.getJSONObject(j).getString("gestante").equals("1")){
				gestante += "S";
			} else {
				gestante += "N";
			}
			if(arrayServAdicional.getJSONObject(j).getInt("edad")>=0 && arrayServAdicional.getJSONObject(j).getInt("edad")<=11){                        
				ge1+="X";
			} else if(arrayServAdicional.getJSONObject(j).getInt("edad")>=12 && arrayServAdicional.getJSONObject(j).getInt("edad")<=17){
				ge2 += "X";
			} else if(arrayServAdicional.getJSONObject(j).getInt("edad")>=18 && arrayServAdicional.getJSONObject(j).getInt("edad")<=29){
				ge3 += "X";
			} else if(arrayServAdicional.getJSONObject(j).getInt("edad")>=30 && arrayServAdicional.getJSONObject(j).getInt("edad")<=59){
				ge4 += "X";
			} else if(arrayServAdicional.getJSONObject(j).getInt("edad")>=60 && arrayServAdicional.getJSONObject(j).getInt("edad")<=120){
				ge5 += "X";
			}
			servAdic.setGe1(ge1);
			servAdic.setGe2(ge2);
			servAdic.setGe3(ge3);
			servAdic.setGe4(ge4);
			servAdic.setGe5(ge5);
			servAdic.setGestante(gestante);
			servAdic.setTotal(total);
			
			totalReg += 1;
			
			listItems.add(servAdic);
		}
	}

	JRBeanCollectionDataSource dssa = new JRBeanCollectionDataSource(listItems);
	
	String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");

	InputStream logoReporte = new FileInputStream(rutaproy + "_img/arfsis/logoReporte.jpg");
	
	JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/ServAdicionales.jrxml");

	Map<String, Object> parameters = new HashMap<String, Object>();

	if(!disa.equals("")) {
		parameters.put("disa", disa);
	} else {
		parameters.put("disa", " ");
	}
	if(!odsis.equals("")) {
		parameters.put("odsis", odsis);
	} else {
		parameters.put("odsis", " ");
	}
	if(!usuario.equals("")) {
		parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
	} else {
		parameters.put("usuario", " ");
	}
	parameters.put("subtitle", " ");
	parameters.put("totalReg", totalReg.toString());
	parameters.put("dssa", dssa);
	
if(tipoArchivo.equals("pdf")){
	parameters.put("logoReporte", logoReporte);
	
	byte[] bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource());
		
	response.setContentType("application/pdf");
	response.setHeader("Content-Disposition",  "inline; filename='JRServAdicionales.pdf'");
	ServletOutputStream output = response .getOutputStream();
	response.getOutputStream();
	output.write(bytes,0,bytes.length);
	output.flush();
	output.close();
}
else if(tipoArchivo.equals("excel")){
	JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

	byte[] bytes = new byte[10000];
	
	JRXlsExporter exporter = new JRXlsExporter();
	
	ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
	
	exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
	exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
	exporter.exportReport();
	
	bytes = xlsReport.toByteArray();
	
	response.setContentType("application/vnd.ms-excel");
	response.setContentLength(bytes.length);
	xlsReport.close();
	
	OutputStream output = response.getOutputStream();
	output.write(bytes,0,bytes.length);
	output.flush();
	output.close();
}
%>
