<%@page import="pe.gob.sis.arfsisweb.reports.*,
                pe.gob.sis.MysqlConnect,
                pe.gob.sis.arfsisweb.reports.jasperreports.*,
                org.json.*,
                net.sf.jasperreports.engine.*,
                net.sf.jasperreports.engine.data.JRBeanCollectionDataSource,
                net.sf.jasperreports.engine.export.JRXlsExporter,
                be.mxs.common.util.db.MedwanQuery,
                java.io.*,
                java.time.*,
                java.time.format.DateTimeFormatter,
                java.util.*,
                java.sql.*"%>

<%	
	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");
	String disa = request.getParameter("disa");
	String odsis = request.getParameter("odsis");

    String tipoArchivo = request.getParameter("tipoArchivo");

    MysqlConnect con = new MysqlConnect("openclinic_dbo");

	JSONArray array = new JSONArray();
	JSONArray arrayMedicamento = new JSONArray();

	String sql = "SELECT CONCAT(SIS_ATENCION_2,'-',SIS_ATENCION_3,'-',SIS_ATENCION_4) AS fua,"
                    + " SIS_DIAGNOSTICOS_3 AS nroDx,"
                    + " C10_CodDia AS cie10,"
                    + " C10_descripcion AS diagnostico,"
                    + " ins_CodIns AS codInsumo,"
                    + " ins_Nombre AS descInsumo,"
                    + " SIS_INSUMOS_4 AS insPrescrito,"
                    + " SIS_INSUMOS_5 AS insEntregado,"
                    + " SIS_INSUMOS_10 AS precio"
                    + " FROM sis_atencion"
                    + " JOIN openclinic_dbo.sis_diagnosticos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_diagnosticos.SIS_DIAGNOSTICOS_1"
                    + " JOIN openclinic_dbo.sis_insumos ON openclinic_dbo.sis_atencion.SIS_ATENCION_1 = openclinic_dbo.sis_insumos.SIS_INSUMOS_1"
                    + " JOIN bdsis_maestros.m_cie10 ON openclinic_dbo.sis_diagnosticos.SIS_DIAGNOSTICOS_2 = bdsis_maestros.m_cie10.C10_CodDia"
                    + " JOIN bdsis_maestros.m_insumos ON openclinic_dbo.sis_insumos.SIS_INSUMOS_2 = bdsis_maestros.m_insumos.ins_CodIns";
                
    String filtrosSql = "";

        if(!procesoOFormato.equals("") && !opcion.equals("") && (!periodo.equals("") || !year.equals("") || !desde.equals(""))){
            if(procesoOFormato.equals("1")) {
                if(opcion.equals("1")){
                    filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+periodo+"%'";                     
                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_83 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_83,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            } else if(procesoOFormato.equals("2")) {
                if(opcion.equals("1")){
                	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_39 LIKE '%"+periodo+"%'";  

                } else if(opcion.equals("2")){
                	filtrosSql = filtrosSql + " WHERE SIS_ATENCION_39 LIKE '%"+year+"%'";
                } else if(opcion.equals("3")){
                	filtrosSql = filtrosSql +" WHERE CAST(CONCAT(SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,7,4), '/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,4,2),'/',SUBSTRING(openclinic_dbo.sis_atencion.SIS_ATENCION_39,1,2)) AS DATE) BETWEEN CAST('"+desde.substring(0,10)+"' AS DATE) AND CAST('"+hasta.substring(0,10)+"' AS DATE)";
                }
            }
            if(!componente.equals("3") && !componente.equals("")) {
            	filtrosSql = filtrosSql +" AND SIS_ATENCION_15 = '"+componente+"'";
            }
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(!componente.equals("3") && !componente.equals("") && procesoOFormato.equals("") && opcion.equals("")) {
        	filtrosSql = filtrosSql +" WHERE SIS_ATENCION_15 = '"+componente+"'";
            if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }
        if(procesoOFormato.equals("") && opcion.equals("") && componente.equals("")) {
        	if(!ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_9 LIKE '%"+ppdd+"'";
        	}
        	if(!eess.equals("") && ppdd.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && ppdd.equals("") && eess.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && ppdd.equals("") && eess.equals("") && usuario.equals("")) {
        		filtrosSql = filtrosSql + " WHERE SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        	if(!eess.equals("") && !ppdd.equals("")) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_6 LIKE '%"+eess+"'";
        	}
        	if(!usuario.equals("") && (!ppdd.equals("") || !eess.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_82 LIKE '%"+usuario+"'";
        	}
        	if(!respAten.equals("") && (!ppdd.equals("") || !eess.equals("") || !usuario.equals(""))) {
        		filtrosSql = filtrosSql + " AND SIS_ATENCION_73 LIKE '%"+respAten+"'";
        	}
        }

        sql = sql + filtrosSql;

		PreparedStatement ps = con.connect().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        List<InsumoModel> listItems = new ArrayList<InsumoModel>();

        Integer totalReg = 0;

        while(rs.next()){
            InsumoModel insumo = new InsumoModel();
            insumo.setFua(rs.getString("fua"));
            insumo.setDiagnostico(rs.getString("diagnostico"));
            insumo.setNroDx(rs.getString("nroDx"));
            insumo.setCie10(rs.getString("cie10"));
            insumo.setCodInsumo(rs.getString("codInsumo"));
            insumo.setDescInsumo(rs.getString("descInsumo"));
            insumo.setInsPrescrito(rs.getInt("insPrescrito"));
            insumo.setInsEntregado(rs.getInt("insEntregado"));
            insumo.setPrecio(rs.getInt("precio"));
            
            totalReg +=1;

            listItems.add(insumo);
        }

		rs.close();
	    ps.close();

		String sqlUser = "SELECT CONCAT(firstname, ' ',lastname) AS usuario"+
            " FROM ocadmin_dbo.admin"+
            " WHERE natreg ="+usuario;
		JSONArray arrayUser = new JSONArray();        
        if(!usuario.equals("")) {
        	PreparedStatement psUser = con.connect().prepareStatement(sqlUser);
            ResultSet rsUser = psUser.executeQuery();
            while(rsUser.next()){
                JSONObject result = new JSONObject();
                result.put("usuario",rsUser.getString(1));
                arrayUser.put(result);
            }
            rsUser.close();
            psUser.close();
        }
	    con.disconnect();

        JRBeanCollectionDataSource dsrdi = new JRBeanCollectionDataSource(listItems);
        
        String rutaproy = MedwanQuery.getInstance().getConfigString("localProjectPath");

        InputStream logoReporte = new FileInputStream(rutaproy + "_img/arfsis/logoReporte.jpg");
        
        JasperReport report = JasperCompileManager.compileReport(rutaproy + "arfsisweb/reportesJasper/DetalleInsumos.jrxml");

        Map<String, Object> parameters = new HashMap<String, Object>();

        if(!disa.equals("")) {
        	parameters.put("disa", disa);
        } else {
        	parameters.put("disa", " ");
        }
        if(!odsis.equals("")) {
        	parameters.put("odsis", odsis);
        } else {
        	parameters.put("odsis", " ");
        }
        if(!usuario.equals("")) {
        	parameters.put("usuario", arrayUser.getJSONObject(0).getString("usuario"));
        } else {
        	parameters.put("usuario", " ");
        }
        parameters.put("subtitle", " ");
        parameters.put("totalReg", totalReg.toString());
        parameters.put("dsrdi", dsrdi);
        
    if(tipoArchivo.equals("pdf")){
        parameters.put("logoReporte", logoReporte);
    	        
		byte[] bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource());
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",  "inline; filename='JRDetalleInsumos.pdf'");
		ServletOutputStream output = response .getOutputStream();
		response.getOutputStream();
		output.write(bytes,0,bytes.length);
		output.flush();
		output.close();
    }
    else if(tipoArchivo.equals("excel")){
        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource() );

        byte[] bytes = new byte[10000];
		
		JRXlsExporter exporter = new JRXlsExporter();
		
		ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, xlsReport);
		exporter.exportReport();
		
		bytes = xlsReport.toByteArray();
		
		response.setContentType("application/vnd.ms-excel");
		response.setContentLength(bytes.length);
		xlsReport.close();
		
		OutputStream output = response.getOutputStream();
		output.write(bytes,0,bytes.length);
		output.flush();
		output.close();
    }
%>