<%
/*************************************
Nombre        : repResRegistro
Proposito     : Generación de reporte de Resumen de Registro
Creado por    : FLIZARRAGA
Fec Creacion  : 20230228
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230228  FLIZARRAGA     Se creo la función para generar el reporte de Resumen de Registro
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	int pagina = Integer.parseInt(request.getParameter("page"));
	int rows = Integer.parseInt(request.getParameter("rows"));

	String procesoOFormato = request.getParameter("procesoOFormato");
	String opcion = request.getParameter("opcion");
	String componente = request.getParameter("componente");
	String periodo = request.getParameter("periodo");
	String year = request.getParameter("year");
	String desde = request.getParameter("desde");
	String hasta = request.getParameter("hasta");
	String ppdd = request.getParameter("ppdd");
	String eess = request.getParameter("eess");
	String usuario = request.getParameter("usuario");
	String respAten = request.getParameter("respAten");

	String resultado ;
	RepResRegistro datos = new RepResRegistro();
	datos.limit(rows);
	datos.page(pagina);
	
	resultado = datos.datosJSON(procesoOFormato,opcion,componente,periodo,year,desde,hasta,ppdd,eess,usuario,respAten);
 
%>
<%=resultado %>
