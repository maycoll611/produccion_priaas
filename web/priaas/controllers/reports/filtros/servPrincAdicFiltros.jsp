<%
/*************************************
Nombre        : servPrincAdicFiltros
Proposito     : Filtros para generación de reporte de Registro de Atencion - Servicio Principal y Adicional
Creado por    : FLIZARRAGA
Fec Creacion  : 20230302
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230302  FLIZARRAGA     Se creo la función de filtros para mejorar filtros de reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepRAServPrincAdic datos = new RepRAServPrincAdic();	

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>