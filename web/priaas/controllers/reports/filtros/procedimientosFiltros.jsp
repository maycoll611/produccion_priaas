<%
/*************************************
Nombre        : procedimientosFiltros
Proposito     : Filtros para generación de reporte de procedimientos
Creado por    : FLIZARRAGA
Fec Creacion  : 20230204
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230204  FLIZARRAGA     Se creo la función de filtros para mejorar filtros de reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepProcedimientos datos = new RepProcedimientos("sis_procedimientos");	

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>