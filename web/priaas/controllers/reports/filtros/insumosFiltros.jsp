<%
/*************************************
Nombre        : insumosFiltros
Proposito     : Filtros para generación de reporte de Insumos
Creado por    : FLIZARRAGA
Fec Creacion  : 20230203
Observaciones : Ninguna 
--------------------------------------------------------------------------------------------------------------
MODIFICACIONES:

FECHA     USUARIO     OBSERVACIONES
--------------------------------------------------------------------------------------------------------------
20230203  FLIZARRAGA     Se creo la función de filtros para mejorar filtros de reporte
*************************************/
%>

<%@page import="pe.gob.sis.arfsisweb.reports.*,org.json.*"%>

<%	
	String filtro = request.getParameter("filtro");
	String permiso = request.getParameter("permiso");
	String identificador = request.getParameter("identificador");

	String resultado;
	RepInsumos datos = new RepInsumos("sis_insumos");	

	resultado = datos.filtros(filtro,permiso,identificador);
	 

%>
<%=resultado %>