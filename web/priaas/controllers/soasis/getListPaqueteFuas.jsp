<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*"%>
<%@page import="org.jnp.interfaces.java.javaURLContextFactory"%>
<%@include file="/includes/validateUser.jsp" %>
<%
    Paquete paquete = new Paquete();

    String data = "";
    int cols =  Integer.parseInt(request.getParameter("rows"));
    int pagina = Integer.parseInt(request.getParameter("page"));
    int inicia = 0;
    int limite = 0;
    int total = 0;
    
    paquete.loadPaquete(request.getParameter("periodo"), request.getParameter("mes"), request.getParameter("ppdd"));

    total = generator.getFuas().size();
	if(total>0){
        inicia = cols * pagina;
        limite = (pagina + 1) * cols;
        if(limite > total){
            limite=total;
        }
	}
	for(int n=inicia;n<limite;n++){
        FUA fua = (FUA)generator.getFuas().elementAt(n);
        Atencion ate = fua.getAtencion();        
        data = data + "{";
        data = data + "\"FUA\": \""+ate.getValueString(6)+"-"+ate.getValueString(3)+"-"+ate.getValueString(4)+"\",";
        data = data + "\"FECHA\": \""+ate.getValueString(39)+"\",";
        data = data + "\"PACIENTE\": \""+ate.getValueString(26)+" "+ate.getValueString(27)+","+ate.getValueString(28)+"\",";
        data = data + "\"SERVICIO\": \""+ate.getValueString(42)+"\",";
        data = data + "\"PERIODO\": \""+ate.getValueString(79)+"\",";
        data = data + "\"MES\": \""+ate.getValueString(80)+"\",";
        data = data + "\"SIS_FUA_STATUS\": \""+fua.getStatus()+"\",";
        data = data + "\"SIS_FUA_ID\": \""+fua.getUid()+"\"";
        data = data + "}";
        if(n<limite-1) {
            data = data + ",";
        }
	}
    
    response.setContentType("application/json;charset=utf8"); 
%>
<%@page contentType="application/json; charset=UTF-8"%>
{
    "resultado":[
        <%=data%>
        ],
    "totalRecords":<%=generator.getFuas().size() %>,
    "page":<%=request.getParameter("page") %>,
    "rows":<%=request.getParameter("rows") %>
}


