<%@page import="org.json.*,pe.gob.sis.arfsisweb.*"%>

<%
	String numreg = request.getParameter("numreg");
	//medicamentos		
	GetDataBD medicamentos = new GetDataBD();
	medicamentos.setModel("openclinic_dbo.sis_medicamentos");
	medicamentos.setColumn("med_Nombre,SIS_MEDICAMENTOS_5,SIS_MEDICAMENTOS_9,SIS_MEDICAMENTOS_10");
	
	JSONArray joinMe = new JSONArray();
	joinMe.put("join m_medicamentos on med_CodMed=SIS_MEDICAMENTOS_2 ");
	medicamentos.setJoin(joinMe);
	
	JSONArray whereMe = new JSONArray();
	whereMe.put("SIS_MEDICAMENTOS_1 = "+numreg);
	
	medicamentos.setWhere(whereMe);
	JSONArray resMedicamento = medicamentos.getArrayJsonData();
	//insumos
	GetDataBD insumos = new GetDataBD();
	insumos.setModel("openclinic_dbo.sis_insumos");
	insumos.setColumn("ins_Nombre,SIS_INSUMOS_5,SIS_INSUMOS_9,SIS_INSUMOS_10");
	JSONArray joinIn = new JSONArray();
	joinIn.put("join m_insumos on ins_CodIns=SIS_INSUMOS_2 ");
	insumos.setJoin(joinIn);
	
	JSONArray whereIn = new JSONArray();
	whereIn.put("SIS_INSUMOS_1 = "+numreg);
	insumos.setWhere(whereIn);
	//procedimientos	
	GetDataBD procedimientos = new GetDataBD();
	procedimientos.setModel("openclinic_dbo.sis_procedimientos");
	procedimientos.setColumn("apo_Nombre,SIS_PROCEDIMIENTOS_6,SIS_PROCEDIMIENTOS_13,SIS_PROCEDIMIENTOS_14");
	JSONArray joinPr = new JSONArray();
	joinPr.put("join m_apodiag on apo_CodProcedimiento=SIS_PROCEDIMIENTOS_2 ");
	procedimientos.setJoin(joinPr);
	
	JSONArray wherePro = new JSONArray();
	wherePro.put("SIS_PROCEDIMIENTOS_1 = "+numreg);
	procedimientos.setWhere(wherePro);
	
	//datos
	GetDataBD datos = new GetDataBD();
	datos.setModel("openclinic_dbo.sis_atencion");
	datos.setColumn("SIS_ATENCION_2,SIS_ATENCION_3,SIS_ATENCION_4,SIS_ATENCION_6,SIS_ATENCION_25,SIS_ATENCION_26,SIS_ATENCION_27,SIS_ATENCION_28,SIS_ATENCION_29,SIS_ATENCION_39,SIS_ATENCION_88,pre_Nombre,pers_IdResAtencion,pers_ApePaterno,pers_ApeMaterno,pers_PriNombre,cod_ud_sis,nombreUd");
	
	JSONArray joinDa = new JSONArray();
	//SUBSTRING(bdsis_maestros.m_eess.pre_CodigoRENAES,3,8)
	joinDa.put("left join m_eess on SUBSTRING(pre_CodigoRENAES,3,8)=SIS_ATENCION_6 ");
	joinDa.put("left join bdsis_tarifas.sis_unidad_ejecutora on pre_CodEjeAdm=cod_ud_sis ");
	joinDa.put("left join a_resatencion on pers_IdResAtencion=SIS_ATENCION_73 ");
	datos.setJoin(joinDa);
	
	JSONArray where = new JSONArray();
	where.put("SIS_ATENCION_1 = "+numreg);
	datos.setWhere(where);
	//JSONObject datoscabecera = datos.getJsonData();
	
	
	
	JSONObject resultado = new JSONObject();
	resultado.put("medicamentos",medicamentos.getArrayJsonData());
	resultado.put("insumos",insumos.getArrayJsonData());
	resultado.put("procedimientos",procedimientos.getArrayJsonData());
	resultado.put("datos",datos.getJsonData());
	

%>
<%=resultado.toString() %>
