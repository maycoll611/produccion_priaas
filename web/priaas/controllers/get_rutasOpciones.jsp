<%@page import="pe.gob.sis.priaas.model.rutas"%>
<%@page import="pe.gob.sis.*,org.json.*,pe.gob.sis.arfsisweb.*,pe.gob.sis.priaas.*"%>
<%@include file="/includes/validateUser.jsp" %>

<%
	JSONObject data = new JSONObject();
	JSONArray resultado1 = new JSONArray();
	
	rutas ru = new rutas();
	resultado1 = rutas.get_rutasOpciones();
	data.put("rutasOpciones", resultado1);
	
	
	// datatable
	GetDataBD datatable = new GetDataBD();
	datatable.setModel("openclinic_dbo.sis_atencion");
	datatable.setColumn("SIS_ATENCION_1,SIS_ATENCION_6,SIS_ATENCION_3,SIS_ATENCION_4,SIS_ATENCION_26,SIS_ATENCION_27,SIS_ATENCION_28,SIS_ATENCION_29,SIS_FUA_STATUS");
	
	JSONArray joinMe = new JSONArray();
	joinMe.put("join openclinic_dbo.sis_fua on SUBSTRING(sis_fua.SIS_FUA_ENCOUNTERUID,3)=SIS_ATENCION_1 ");

	datatable.setJoin(joinMe);
	datatable.setOrder("order by SIS_ATENCION_1 desc");
	datatable.setLimit(10);
	JSONArray resData = datatable.getArrayJsonData();
	
	data.put("datatable", resData);
	
	out.print(data);
%>


