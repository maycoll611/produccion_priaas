import { createRouter, createWebHashHistory } from 'vue-router';
import AppLayout from '@/layout/AppLayout.vue';

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: AppLayout,
            children: [
                {
                    path: '/',
                    name: 'dashboard',
                    component: () => import('@/views/Dashboard.vue')
                },
                //
                {
                    path: '/uikit/formlayout',
                    name: 'formlayout1',
                    component: () => import('@/views/uikit/FormLayout.vue')
                },
                
                {
                    path: '/registrar',
                    name: 'registrar',
                    props: true,
                    component: () => import('@/views/pages/registro/Registrar.vue')
                },
                {
                    path: '/actualizar',
                    name: 'actualizar',
                    component: () => import('@/views/pages/registro/Actualizar.vue')
                },
                {
                    path: '/observados',
                    name: 'observados',
                    component: () => import('@/views/pages/registro/observados.vue')
                },
                {
                    path: '/editar/:idatencion',
                    name: 'editar',
                    component: () => import('@/views/pages/registro/Registrar.vue')
                },
                {
                    path: '/cierre',
                    name: 'cierre',
                    component: () => import('@/views/pages/registro/Cierre.vue')
                },
                {
                    path: '/reportes/atenciones',
                    name: 'atenciones',
                    component: () => import('@/views/pages/reports/RepRegistroAtencion.vue')
                },
                {
                    path: '/reportes/diagnosticos',
                    name: 'diagnosticos',
                    component: () => import('@/views/pages/reports/RepDiagnosticos.vue')
                },
                {
                    path: '/reportes/medicamentos',
                    name: 'medicamentos',
                    component: () => import('@/views/pages/reports/RepMedicamentos.vue')
                },
                {
                    path: '/reportes/insumos',
                    name: 'insumos',
                    component: () => import('@/views/pages/reports/RepInsumos.vue')
                },
                {
                    path: '/reportes/procedimientos',
                    name: 'procedimientos',
                    component: () => import('@/views/pages/reports/RepProcedimientos.vue')
                },
                {
                    path: '/reportes/smi',
                    name: 'smi',
                    component: () => import('@/views/pages/reports/RepSMI.vue')
                },
                {
                    path: '/reportes/serviciosAdicionales',
                    name: 'serviciosAdicionales',
                    component: () => import('@/views/pages/reports/RepServAdicionales.vue')
                },
                {
                    path: '/reportes/serviciosPrestados',
                    name: 'serviciosPrestados',
                    component: () => import('@/views/pages/reports/RepServPrestados.vue')
                },
                {
                    path: '/reportes/serviciosPrincAdic',
                    name: 'serviciosPrincAdic',
                    component: () => import('@/views/pages/reports/RepServPrincAdic.vue')
                },
                {
                    path: '/reportes/resumenRegistro',
                    name: 'resumenRegistro',
                    component: () => import('@/views/pages/reports/RepResRegistro.vue')
                },
                {
                    path: '/reportes/atencionesDetalle',
                    name: 'atencionesDetalle',
                    component: () => import('@/views/pages/reports/RepAtencionesDetalle.vue')
                },
                {
                    path: '/reportes/diagnosticosDetalle',
                    name: 'diagnosticosDetalle',
                    component: () => import('@/views/pages/reports/RepDiagnosticosDetalle.vue')
                },
                {
                    path: '/reportes/medicamentosDetalle',
                    name: 'medicamentosDetalle',
                    component: () => import('@/views/pages/reports/RepMedicamentosDetalle.vue')
                },
                {
                    path: '/reportes/insumosDetalle',
                    name: 'insumosDetalle',
                    component: () => import('@/views/pages/reports/RepInsumosDetalle.vue')
                },
                {
                    path: '/reportes/procedimientosDetalle',
                    name: 'procedimientosDetalle',
                    component: () => import('@/views/pages/reports/RepProcedimientosDetalle.vue')
                },
                {
                    path: '/reportes/smiDetalle',
                    name: 'smiDetalle',
                    component: () => import('@/views/pages/reports/RepSMIDetalle.vue')
                },
                {
                    path: '/reportes/serviciosPrestadosDetalle',
                    name: 'serviciosPrestadosDetalle',
                    component: () => import('@/views/pages/reports/RepServPrestadosDetalle.vue')
                },
                {
                    path: '/reportes/serviciosAdicionalesDetalle',
                    name: 'serviciosAdicionalesDetalle',
                    component: () => import('@/views/pages/reports/RepServAdicionalesDetalle.vue')
                },
                {
                    path: '/reportes/digitacion',
                    name: 'digitacion',
                    component: () => import('@/views/pages/reports/RepDigitacion.vue')
                },
                {
                    path: '/reportes/consumo',
                    name: 'consumo',
                    component: () => import('@/views/pages/reports/RepConsumo.vue')
                },
                {
                    path: '/reglas',
                    name: 'reglas',
                    component: () => import('@/views/pages/reglas/reglas.vue')
                },
                {
                    path: '/reglasmasivo',
                    name: 'reglasmasivo',
                    component: () => import('@/views/pages/reglas/reglasmasivo.vue')
                },
                {
                    path: '/uikit/input',
                    name: 'input',
                    component: () => import('@/views/uikit/Input.vue')
                },
                {
                    path: '/uikit/floatlabel',
                    name: 'floatlabel',
                    component: () => import('@/views/uikit/FloatLabel.vue')
                },
                {
                    path: '/uikit/invalidstate',
                    name: 'invalidstate',
                    component: () => import('@/views/uikit/InvalidState.vue')
                },
                {
                    path: '/uikit/button',
                    name: 'button',
                    component: () => import('@/views/uikit/Button.vue')
                },
                {
                    path: '/uikit/table',
                    name: 'table',
                    component: () => import('@/views/uikit/Table.vue')
                },
                {
                    path: '/uikit/list',
                    name: 'list',
                    component: () => import('@/views/uikit/List.vue')
                },
                {
                    path: '/uikit/tree',
                    name: 'tree',
                    component: () => import('@/views/uikit/Tree.vue')
                },
                {
                    path: '/uikit/panel',
                    name: 'panel',
                    component: () => import('@/views/uikit/Panels.vue')
                },
                // sincronizacion
                {
                    path: '/sincronizacion/asegurados',
                    name: 'asegurados',
                    component: () => import('@/views/pages/sincronizacion/Asegurados.vue')
                },
                {
                    path: '/sincronizacion/maestros',
                    name: 'maestros',
                    component: () => import('@/views/pages/sincronizacion/Maestros.vue')
                },
                {
                    path: '/sincronizacion/precios',
                    name: 'precios',
                    component: () => import('@/views/pages/sincronizacion/Valorizaciones.vue')
                },
                // configuracion
                {
                    path: '/configuracion/usuarios',
                    name: 'usuarios',
                    component: () => import('@/views/pages/configuracion/Usuarios.vue')
                },
                {
                    path: '/configuracion/webservices',
                    name: 'webservices',
                    component: () => import('@/views/pages/configuracion/WebServices.vue')
                },
                // soasis
                {
                    path: '/soasis/enviaFuas',
                    name: 'enviarfuas',
                    component: () => import('@/views/pages/soasis/EnviaFuas.vue')
                },
                {
                    path: '/soasis/paqueteFuas',
                    name: 'paqueteFuas',
                    component: () => import('@/views/pages/soasis/PaqueteFuas.vue')
                },
                //Configuracion
                {
                    path: '/roles',
                    name: 'roles',
                    component: () => import('@/views/pages/configuracion/Roles.vue')
                },
                {
                    path: '/permisos',
                    name: 'permisos',
                    component: () => import('@/views/pages/configuracion/Permisos.vue')
                },
                {
                    path: '/configuracion/backup',
                    name: 'backup',
                    component: () => import('@/views/pages/configuracion/Backup.vue')
                },
                {
                    path: '/usuario/arfsisweb',
                    name: 'arfsisweb',
                    component: () => import('@/views/pages/usuario/arfsisweb.vue')
                }
            ]
        },
        {
            path: '/landing',
            name: 'landing',
            component: () => import('@/views/pages/Landing.vue')
        },
        {
            path: '/pages/notfound',
            name: 'notfound',
            component: () => import('@/views/pages/NotFound.vue')
        },

        {
            path: '/auth/login',
            name: 'login',
            component: () => import('@/views/pages/auth/Login.vue')
        },
        {
            path: '/auth/access',
            name: 'accessDenied',
            component: () => import('@/views/pages/auth/Access.vue')
        },
        {
            path: '/auth/error',
            name: 'error',
            component: () => import('@/views/pages/auth/Error.vue')
        }
    ]
});

export default router;
